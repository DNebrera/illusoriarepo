﻿using UnityEngine;
using System.Collections;

public class Estalactita : MonoBehaviour {
	public GameObject Punta;
	public bool bBreakable = false;
	public bool bRespawn = false;
	public Vector3 StartPos;
	public bool detected = false;
	public float Falltime;
	public float FallMax;
	// Use this for initialization
	void Start () {
		StartPos = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (detected) {
			Falltime += Time.deltaTime;
		}
		if (Falltime >= FallMax) {
			Fall ();
			Falltime = 0;
			detected = false;
		}
	}
	public void OnTriggerEnter(Collider Col){
		if (Col.tag == "Randy") {
			detected = true;
		}
	}
	public void Fall(){
		Debug.Log("Die!");
		this.GetComponent<Rigidbody>().useGravity = true;
	}
	public void Crash(){
		if (!bBreakable) {
			Punta.GetComponent<Collider>().enabled = false;
		}
		if (bBreakable && !bRespawn) {
			Destroy (gameObject);
		}
		if (bBreakable && bRespawn) {
			gameObject.transform.position = StartPos;
			this.GetComponent<Rigidbody>().useGravity = false;
			this.GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
		}
	}
}
