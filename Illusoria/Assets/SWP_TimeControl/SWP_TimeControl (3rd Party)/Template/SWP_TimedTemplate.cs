// --------------ABOUT AND COPYRIGHT----------------------
//  Copyright © 2013 SketchWork Productions Limited
//        support@sketchworkproductions.com
// -------------------------------------------------------

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TrailRenderer))]
public class SWP_TimedTemplate: SWP_InternalTimedGameObject
{
	TrailRenderer trTrailRenderer;
	
	float fOriginalSpeed;
	
	void Awake()
	{
		if (SearchObjects)
		{
			trTrailRenderer = GetComponent<TrailRenderer>();
			fOriginalSpeed = trTrailRenderer.time;
		}
	}

	/// <summary>
	///When not using SeachObjects the caches need to be cleaned.
	/// </summary>
	protected override void ClearAssignedObjects()
	{
		trTrailRenderer = null;
	}

	/// <summary>
	///When not using SeachObject this function loops through the object array and sets the speed for the ohjects.
	/// </summary>
	protected override void SetSpeedLooping(float _fNewSpeed, float _fCurrentSpeedPercent, float _fCurrentSpeedZeroToOne)
	{
		for (int thisCounter = 0; thisCounter < AssignedObjects.Length; thisCounter++)
		{
			if (AssignedObjects[thisCounter].GetType() == typeof(TrailRenderer))
				trTrailRenderer = (TrailRenderer)AssignedObjects[thisCounter];

			SetSpeedAssigned(_fNewSpeed, _fCurrentSpeedPercent, _fCurrentSpeedZeroToOne);
		}
		
		ClearAssignedObjects();
	}

	/// <summary>
	///This function actually sets the new speed of the objects.
	/// </summary>
	protected override void SetSpeedAssigned(float _fNewSpeed, float _fCurrentSpeedPercent, float _fCurrentSpeedZeroToOne)
	{
		if (trTrailRenderer != null)
		{
			trTrailRenderer.time = GetNewSpeedFromPercentage(fOriginalSpeed, trTrailRenderer.time, true);
		}
	}
}