//--------------ABOUT AND COPYRIGHT----------------------
// Copyright © 2013 SketchWork Productions Limited
//       support@sketchworkproductions.com
//-------------------------------------------------------
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(tk2dSpriteAnimator))]
public class SWP_Timed2DTK: SWP_InternalTimedGameObject
{			
	tk2dSpriteAnimator saSpriteAnimator;

	bool bOriginalSpeedSet = false;
	float fOriginalSpeed;

	void Awake()
	{
		if (SearchObjects)
			saSpriteAnimator = GetComponent<tk2dSpriteAnimator>();
	}
	
	/// <summary>
	///When not using SeachObjects the caches need to be cleaned.
	/// </summary>
	protected override void ClearAssignedObjects()
	{
		saSpriteAnimator = null;
	}

	/// <summary>
	///When not using SeachObject this function loops through the object array and sets the speed for the ohjects.
	/// </summary>
	protected override void SetSpeedLooping(float _fNewSpeed, float _fCurrentSpeedPercent, float _fCurrentSpeedZeroToOne)
	{
		for (int thisCounter = 0; thisCounter < AssignedObjects.Length; thisCounter++)
		{
			if (AssignedObjects[thisCounter].GetType() == typeof(tk2dSpriteAnimator))
				saSpriteAnimator = (tk2dSpriteAnimator)AssignedObjects[thisCounter];
			
			SetSpeedAssigned(_fNewSpeed, _fCurrentSpeedPercent, _fCurrentSpeedZeroToOne);
		}
		
		ClearAssignedObjects();
	}

	/// <summary>
	///This function actually sets the new speed of the objects.
	/// </summary>
	protected override void SetSpeedAssigned(float _fNewSpeed, float _fCurrentSpeedPercent, float _fCurrentSpeedZeroToOne)
	{
		if (!bOriginalSpeedSet)
		{
			fOriginalSpeed = saSpriteAnimator.ClipFps;
			bOriginalSpeedSet = true;
		}

		if (saSpriteAnimator != null)
		{
			if (saSpriteAnimator.Playing)
			{
				if (_fCurrentSpeedPercent == 0f)
					saSpriteAnimator.Play(saSpriteAnimator.CurrentClip, 0f, 0.00001f);  //Don't set this to zero
				else
					saSpriteAnimator.Play(saSpriteAnimator.CurrentClip, 0f, GetNewSpeedFromPercentage(fOriginalSpeed, saSpriteAnimator.ClipFps, false));
			}
		}
	}
} 