// --------------ABOUT AND COPYRIGHT----------------------
//  Copyright © 2013 SketchWork Productions Limited
//        support@sketchworkproductions.com
// -------------------------------------------------------

using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

[ActionCategory("SWP Time Controller")]
[HutongGames.PlayMaker.Tooltip("Globally pauses your game.")]
public class SWP_TimedGlobalPausePM: FsmStateAction
{
	[RequiredField]
	[CheckForComponent(typeof(SWP_TimeGlobalController))]
	[HutongGames.PlayMaker.Tooltip("Select your global time controller (SWP_TimeGlobalController).")]
	public FsmGameObject TimeGlobalController;

	[HutongGames.PlayMaker.Tooltip("Event to send when we have actually globally paused.")]
	public FsmEvent OnGlobalPause;
	
	public override void Reset()
	{
		TimeGlobalController = null;
		OnGlobalPause = null;
	}

	public override void OnEnter()
	{
		GameObject _GameObject;
		SWP_TimeGlobalController _TimeGlobalController;

		_GameObject = TimeGlobalController.Value;
		_TimeGlobalController = _GameObject.GetComponent<SWP_TimeGlobalController>();

		if (_TimeGlobalController != null)
		{
			if (!SWP_TimeGlobalController.IsPaused)
			{
				_TimeGlobalController.PauseGlobalTime();
				Fsm.Event(OnGlobalPause);
			}
		}

		Finish();
	}
}