// --------------ABOUT AND COPYRIGHT----------------------
//  Copyright © 2013 SketchWork Productions Limited
//        support@sketchworkproductions.com
// -------------------------------------------------------

using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

[ActionCategory("SWP Time Controller")]
[HutongGames.PlayMaker.Tooltip("Pause time for the attached time control group.")]
public class SWP_TimedGroupPausePM: FsmStateAction
{
	[RequiredField]
	[CheckForComponent(typeof(SWP_TimeGroupController))]
	[HutongGames.PlayMaker.Tooltip("Select your group time controller (SWP_TimeGroupController).")]
	public FsmGameObject TimeGroupController;
	
	[HutongGames.PlayMaker.Tooltip("Event to send when we have actually paused the group.")]
	public FsmEvent OnGroupPause;
	
	public override void Reset()
	{
		TimeGroupController = null;
		OnGroupPause = null;
	}
	
	public override void OnEnter()
	{
		GameObject _GameObject;
		SWP_TimeGroupController _TimeGroupController;
		
		_GameObject = TimeGroupController.Value;
		_TimeGroupController = _GameObject.GetComponent<SWP_TimeGroupController>();
		
		if (_TimeGroupController != null)
		{
			if (_TimeGroupController.ControllerSpeedPercent != 0f)
			{
				_TimeGroupController.PauseGroupTime();
				Fsm.Event(OnGroupPause);
			}
		}

		Finish();
	}
}