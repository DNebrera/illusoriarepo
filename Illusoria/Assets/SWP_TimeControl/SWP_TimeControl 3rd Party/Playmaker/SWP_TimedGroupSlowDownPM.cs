// --------------ABOUT AND COPYRIGHT----------------------
//  Copyright © 2013 SketchWork Productions Limited
//        support@sketchworkproductions.com
// -------------------------------------------------------

using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

[ActionCategory("SWP Time Controller")]
[HutongGames.PlayMaker.Tooltip("Slows down time for the attached time control group by the given percentage.")]
public class SWP_TimedSlowDownGroupPM: FsmStateAction
{
	[RequiredField]
	[CheckForComponent(typeof(SWP_TimeGroupController))]
	[HutongGames.PlayMaker.Tooltip("Select your group time controller (SWP_TimeGroupController).")]
	public FsmGameObject TimeGroupController;

	[HasFloatSlider(1, 99)]
	[HutongGames.PlayMaker.Tooltip("Select your group slow down percentage (between 1 and 99).")]
	public FsmFloat SlowDownGroupPercentage = 100f;
	
	[HutongGames.PlayMaker.Tooltip("Event to send when we have actually slowed down time for the group.")]
	public FsmEvent OnGroupSlowDown;
	
	public override void Reset()
	{
		TimeGroupController = null;
		OnGroupSlowDown = null;
	}
	
	public override void OnEnter()
	{
		GameObject _GameObject;
		SWP_TimeGroupController _TimeGroupController;
		
		_GameObject = TimeGroupController.Value;
		_TimeGroupController = _GameObject.GetComponent<SWP_TimeGroupController>();
		
		if (_TimeGroupController != null)
		{
			_TimeGroupController.SlowDownGroupTime(SlowDownGroupPercentage.Value);
			Fsm.Event(OnGroupSlowDown);
		}
		
		Finish();
	}
}