// --------------ABOUT AND COPYRIGHT----------------------
//  Copyright © 2013 SketchWork Productions Limited
//        support@sketchworkproductions.com
// -------------------------------------------------------

using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

[ActionCategory("SWP Time Controller")]
[HutongGames.PlayMaker.Tooltip("Speeds up time for the attached time control group by the given percentage.")]
public class SWP_TimedSpeedUpGroupPM: FsmStateAction
{
	[RequiredField]
	[CheckForComponent(typeof(SWP_TimeGroupController))]
	[HutongGames.PlayMaker.Tooltip("Select your group time controller (SWP_TimeGroupController).")]
	public FsmGameObject TimeGroupController;

	[HasFloatSlider(101, 500)]
	[HutongGames.PlayMaker.Tooltip("Select your group speed up percentage (must be greater than 100).")]
	public FsmFloat SpeedUpGroupPercentage = 100f;
	
	[HutongGames.PlayMaker.Tooltip("Event to send when we have actually slowed down time for the group.")]
	public FsmEvent OnGroupSpeedUp;
	
	public override void Reset()
	{
		TimeGroupController = null;
		OnGroupSpeedUp = null;
	}
	
	public override void OnEnter()
	{
		GameObject _GameObject;
		SWP_TimeGroupController _TimeGroupController;
		
		_GameObject = TimeGroupController.Value;
		_TimeGroupController = _GameObject.GetComponent<SWP_TimeGroupController>();
		
		if (_TimeGroupController != null)
		{
			_TimeGroupController.SpeedUpGroupTime(SpeedUpGroupPercentage.Value);
			Fsm.Event(OnGroupSpeedUp);
		}
		
		Finish();
	}
}