﻿using UnityEngine;
using System.Collections;

public class SierpeParte : MonoBehaviour {
	public GameObject Controller;
	public GameObject Body;
	public GameObject ColaObject;
	public GameObject Randy;
	public GameObject[] Colaider;
	public bool bController;
	public bool ColaiderBomb;
	public bool Cola;
	public bool ColaDetector;
	public bool Cabeza;
	public bool ZonaDebil;
	public bool ZonaResistente;
	public bool Invulnerable;
	public bool SecondaryAt;
	public Vector2 ColaPosition;
	public AudioClip[] Sounds;
	public int Dif;
	//Mob
	public int MobHealth;
	public int Shields;
	public float ResetTime;
	public float ShieldTime;
	public bool bMob;
	public bool bMobSwim;
	public bool bLeft;
	public bool bEnded;
	public float MobSpeed;
	public float MinX;
	public float MaxX;
	public float RandyMinX;
	public float RandyMaxX;
	//
	private float Invultiempo;
	public int NukeDaño;
	//Cabeza
	public int[] Attacks;
	public float Timetoattack;
	private float AttackTime;
	public bool Activo;
	//Cola
	public float TiempoGiro;
	// Use this for initialization
	void Start () {
		ColaiderBomb = false;
		Randy = GameObject.Find ("Randy");
		Dif = PlayerPrefs.GetInt("Dificultad");
	}
	
	// Update is called once per frame
	void Update () {
		if (Invulnerable) {
			Invultiempo += Time.deltaTime * Time.timeScale;
			if (Invultiempo >= 0.2f) {
				Invulnerable = false;
				Invultiempo = 0;
			}
		}
		if (bMob) {
			if (bEnded) {
				ResetTime += Time.deltaTime * Time.timeScale;
				if (ResetTime >= 4) {
					bEnded = false;
				}
			}
			if (bMobSwim) {
				if (bLeft) {
					gameObject.transform.localScale = new Vector3 (1.75f, 1.75f, 0);
					gameObject.transform.Translate ((-MobSpeed * Time.deltaTime * Time.timeScale) * 100, 0, 0);
				}
				if (!bLeft) {
					gameObject.transform.localScale = new Vector3 (-1.75f, 1.75f, 0);
					gameObject.transform.Translate ((MobSpeed * Time.deltaTime * Time.timeScale) * 100, 0, 0);
				}
				if (gameObject.transform.position.x <= Randy.transform.position.x - 15 && bLeft) {
					bLeft = false;
				}
				if (gameObject.transform.position.x >= Randy.transform.position.x + 15 && !bLeft) {
					bLeft = true;
				}
			}
		}
		if (!bMob) {
			if (!bController) {
				SecondaryAt = Controller.GetComponent<SierpedeLava> ().SecondaryAttack;
				if (Activo) {
					AttackTime += Time.deltaTime * Time.timeScale;
					if (AttackTime >= 3) {
						Attackhead ();
						AttackTime = 0;
					}
					ShieldTime  += Time.deltaTime * Time.timeScale;
					if (ShieldTime >= 10 && Dif == 2) {
						Shields++;
						ShieldTime = 0;
						if (Shields > 5) {
							Shields = 5;
						}
					}
					if (ShieldTime >= 5 && Dif == 3) {
						Shields++;
						ShieldTime = 0;
						if (Shields > 5) {
							Shields = 5;
						}
					}
				}
				if (ColaDetector && Activo) {
					TiempoGiro += Time.deltaTime * Time.timeScale;
					if (TiempoGiro >= 3) {
						//Disable ();
						//Invoke ("ColaGiro", 3);
						TiempoGiro = 0;
					}
				}
			}
		}

	}
	void OnTriggerEnter(Collider other) {
		if (ColaDetector && other.tag == "Randy") {
			gameObject.GetComponent<Animator> ().SetTrigger ("Attack");
		}
		if (bMob && !bController) {
			if (other.tag == "Fireball" || other.tag == "Shield" && Invulnerable == false) {
				Controller.GetComponent<SierpeParte> ().GetHit (1);
			}
			if ((other.tag == "Big_Fireball" || other.tag == "Psychocrusher") && Invulnerable == false) {
				Controller.GetComponent<SierpeParte> ().GetHit (3);
			}
			Invulnerable = true;
		}
		if (!bMob) {
			if (other.tag == "Fireball" || other.tag == "Shield" && Invulnerable == false) {
				Parpadeo ();
				if (ZonaDebil) {
					NukeDaño += 1;
					if(Cola){
						Controller.GetComponent<SierpedeLava> ().GetHit (1,2);
					}
					if(Cabeza && Dif >= 2){
						if (Shields <= 0) {
							Controller.GetComponent<SierpedeLava> ().GetHit (1,1);
						}
						if (Shields > 0) {
							Shields--;
						}
					}
					if(Cabeza && Dif == 1){
						Controller.GetComponent<SierpedeLava> ().GetHit (2,1);
					}
				}
				if (ZonaResistente) {
					NukeDaño += 1;
					if(Cola){
						Controller.GetComponent<SierpedeLava> ().GetHit (1,2);
					}
					if(Cabeza && Dif >= 2){
						if (Shields <= 0) {
							Controller.GetComponent<SierpedeLava> ().GetHit (1,1);
						}
						if (Shields > 0) {
							Shields--;
						}
					}
					if(Cabeza && Dif == 1){
						Controller.GetComponent<SierpedeLava> ().GetHit (1,1);
					}
				}
				Invulnerable = true;
			}
			if ((other.tag == "Big_Fireball" || other.tag == "Psychocrusher") && Invulnerable == false) {
				Parpadeo ();
				if (ZonaDebil) {
					NukeDaño += 3;
					if(Cola){
						Controller.GetComponent<SierpedeLava> ().GetHit (1,2);
					}
					if(Cabeza && Dif == 3){
						if (Shields <= 0) {
							Controller.GetComponent<SierpedeLava> ().GetHit (1,1);
						}
						if (Shields > 0) {
							Shields--;
						}
					}
					if(Cabeza && Dif == 2){
						if (Shields <= 0) {
							Controller.GetComponent<SierpedeLava> ().GetHit (3,1);
						}
						if (Shields > 0) {
							Shields--;
						}
					}
					if(Cabeza && Dif == 1){
						Controller.GetComponent<SierpedeLava> ().GetHit (4,1);
					}
				}
				if (ZonaResistente) {
					NukeDaño += 3;
					if(Cola){
						Controller.GetComponent<SierpedeLava> ().GetHit (1,2);
					}
					if(Cabeza && Dif == 3){
						if (Shields <= 0) {
							Controller.GetComponent<SierpedeLava> ().GetHit (1,1);
						}
						if (Shields > 0) {
							Shields--;
						}
					}
					if(Cabeza && Dif == 2){
						if (Shields <= 0) {
							Controller.GetComponent<SierpedeLava> ().GetHit (3,1);
						}
						if (Shields > 0) {
							Shields--;
						}
					}
					if(Cabeza && Dif == 1){
						Controller.GetComponent<SierpedeLava> ().GetHit (4,1);
					}
				}
				Invulnerable = true;
			}
		}	
	}
	public void ResetPy(){
		Controller.GetComponent<SierpedeLava> ().resetPyro ();
	}
	public void Disable(){
		gameObject.GetComponent<Animator> ().SetBool ("Active", false);
		AttackTime = 0;
	}
	public void Disabled(){
		if (Cabeza && !(ZonaDebil || ZonaResistente)) {
			Activo = false;
		}
		if (ColaDetector) {
			Activo = false;
		}
	}
	public void HealUp(){
		if (Cabeza && Dif == 3) {
			//Controller.GetComponent<SierpedeLava> ().GetHit (-5,1);
			Debug.Log("Heal Up sierpe");
		}
	}
	public void Enable(){
		if (!SecondaryAt) {			
			gameObject.GetComponent<Animator> ().SetBool ("Active", true);
			if (Cabeza && !(ZonaDebil || ZonaResistente)) {
				Activo = true;
				if (gameObject.transform.position.x > Randy.transform.position.x) {
					gameObject.transform.localScale = new Vector3(1.75f,gameObject.transform.localScale.y,gameObject.transform.localScale.z);
				}
				if (gameObject.transform.position.x <= Randy.transform.position.x) {
					gameObject.transform.localScale = new Vector3(-1.75f,gameObject.transform.localScale.y,gameObject.transform.localScale.z);
				}
			}
			if (ColaDetector) {
				Activo = true;
			}
			Debug.Log ("This enabled: " + gameObject.name);
		}	
	}
	public void TurnBomb(){
		bool col = ColaiderBomb;
		if (!col) {
			for (int c = 0; c < Colaider.Length; c++) {
				Colaider [c].tag = "Bomb";
			}
			ColaiderBomb = true;
		}
		if (col) {
			for (int c = 0; c < Colaider.Length; c++) {
				Colaider [c].tag = "Enemy";
			}
			ColaiderBomb = false;
		}
	}
	public void ColaGiro(){
		Debug.Log ("GirandoCola!");
		float x = gameObject.transform.localScale.x;
		if (x > 0) {			
			gameObject.transform.localScale = new Vector3(-1.75f,gameObject.transform.localScale.y,gameObject.transform.localScale.z);
		}
		if (x < 0) {
			gameObject.transform.localScale = new Vector3(1.75f,gameObject.transform.localScale.y,gameObject.transform.localScale.z);
		}
		//Invoke ("Enable", 1);
	}
	public void Spit(){
		StartSound (3);
		Controller.GetComponent<SierpedeLava> ().Spit ();
	}
	public void HardSpit(){
		StartSound (3);
		Controller.GetComponent<SierpedeLava> ().Spit ();
	}
	public void ChargedSpit(){
		Controller.GetComponent<SierpedeLava> ().ChargedSpit ();
	}
	public void PowerUp(){
		Controller.GetComponent<SierpedeLava> ().PowerUP ();
	}
	public void ShootPyro(){
		Controller.GetComponent<SierpedeLava> ().ShootPyro ();
	}
	public void DestroyPyro(){
		Controller.GetComponent<SierpedeLava> ().DestroyPyro ();
	}
	public void Attackhead(){
		int r = Random.Range (1, 11);
		if (Cabeza) {
			if (r <= 4) {
				gameObject.GetComponent<Animator> ().SetTrigger ("ShootA");
			}
			if (r >= 5 && r <= 8) {
				gameObject.GetComponent<Animator> ().SetTrigger ("ShootB");
			}
			if (r >= 9) {
				Disable ();
			}
		}
	}
	public void StopGiro(){
		if (bController) {
			Controller.GetComponent<SierpedeLava> ().StopGiro ();
		}
	}
	public void Parpadeo(){
		Body.GetComponent<SpriteRenderer> ().color = Color.red;
		Invoke ("StopParpadeo", 0.1f);
	}
	public void StopParpadeo(){
		Body.GetComponent<SpriteRenderer> ().color = Color.white;
	}
	public void GoForPrey(){
		//gameObject.GetComponent<SpriteRenderer> ().sortingOrder = 39;
		gameObject.transform.position = new Vector2 (Randy.transform.position.x, gameObject.transform.position.y);
		Debug.Log ("Attacking " + Randy.transform.position.x); 
	}
	public void WimpOut(){
		//gameObject.GetComponent<SpriteRenderer> ().sortingOrder = 1;
		Debug.Log ("PredatorOn!");
		if (!bEnded) {
			if (Randy.transform.position.x >= MinX + 10) {
				gameObject.transform.position = new Vector2 ((Randy.transform.position.x-15), gameObject.transform.position.y);
				bLeft = false;
			}
			if (Randy.transform.position.x <= MaxX - 10) {
				gameObject.transform.position = new Vector2 ((Randy.transform.position.x+15), gameObject.transform.position.y);
				bLeft = true;
			}
			bMobSwim = true;
			gameObject.GetComponent<Animator> ().SetBool ("bMob", true);
			gameObject.GetComponent<Animator> ().SetBool ("Active", true);
		}
	}
	public void ActivatePredator(float Min, float Max){
		MinX = Min;
		MaxX = Max;
		WimpOut ();
	}
	public void EmergeCola(float x){
		//Debug.Log ("Emerge" + x);
		if(Dif >= 2){
			ColaPosition = new Vector2 (x, gameObject.transform.position.y);
			if (gameObject.transform.position.x > Randy.transform.position.x) {
				//Debug.Log ("Look Left");
				gameObject.transform.localScale = new Vector3 (1.75f, 1.75f, 1);
			}
			if (gameObject.transform.position.x <= Randy.transform.position.x) {
				//Debug.Log ("Look Right");
				gameObject.transform.localScale = new Vector3 (-1.75f, 1.75f, 1);
			}
			gameObject.GetComponent<Animator> ().SetTrigger ("Emerge");
		}
	}
	public void MoveCola(){
		gameObject.transform.position = ColaPosition;
	}
	public void StopSwim(){
		bMobSwim = false;
	}
	public void GetHit(int d){
		if (bController && bMob) {
			MobHealth -= d;
			if (MobHealth <= 0) {
				gameObject.GetComponent<Animator> ().SetTrigger ("Hit");
				//Debug.Log ("GetHit");
			}
		}
	}
	public void StartSound(int sound){
		gameObject.GetComponent<AudioSource>().PlayOneShot(Sounds[sound], (gameObject.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
	}
	public void Deactivate(){
		gameObject.GetComponent<Animator> ().SetBool ("Active", false);
		gameObject.GetComponent<Animator> ().SetTrigger ("Ended");
		bMobSwim = false;
		bEnded = true;
	}
	public void DeactivateSwim(){
		bMobSwim = false;
	}
	public void ActivateSwim(){
		bMobSwim = true;
	}
	public void ChangePhase(){
		if (Cabeza) {
			Activo = false;
			Controller.GetComponent<SierpedeLava> ().ChangePhaseLavaRise ();
			ColaObject.GetComponent<SierpeParte>().Disable ();
		}
	}
}
