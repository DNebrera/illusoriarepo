﻿using UnityEngine;
using System.Collections;

public class SierpeNivel : MonoBehaviour {
	public GameObject Randy;
	public float Left;
	public float Right;
	public float RandyStopped;
	public float SierpeHeat;
	public bool bActive;
	public bool bEnded;
	public bool Cabeza;
	public bool Cola;
	public bool bSwimming;
	public GameObject Part;
	public GameObject Min;
	public GameObject Max;
	public GameObject Height;
	public float RandyLastPos;
	// Use this for initialization
	void Start () {
		Randy = GameObject.Find ("Randy");
		SierpeHeat = 0.1f;
	}
	
	// Update is called once per frame
	void Update () {
		if (bActive) {
			if (Randy.transform.position.x > Max.transform.position.x || Randy.transform.position.x < Min.transform.position.x) {
				Deactivate ();
			}
			if (Cabeza) {
				if ((RandyLastPos - Randy.transform.position.x) <= 3 && (RandyLastPos - Randy.transform.position.x) >= -3) {
					//Debug.Log ("" + (RandyLastPos - Randy.transform.position.x));
					SierpeHeat += 0.01f;
					if (SierpeHeat >= 2) {
						SierpeHeat = 2;
					}
					RandyStopped += Time.deltaTime * Time.timeScale*SierpeHeat;
					if (RandyStopped >= 6f) {
						if (Randy.transform.position.y >= Height.transform.position.y) {
							Part.GetComponent<Animator> ().SetTrigger ("AttackHigh");
						}
						if (Randy.transform.position.y < Height.transform.position.y) {
							Part.GetComponent<Animator> ().SetTrigger ("Attack");
						}
						Part.GetComponent<SierpeParte> ().StopSwim ();
						if (!bEnded) {
							Invoke ("Reactivate", 3);
						}
						bActive = false;
						RandyStopped = 0;
					}
				}
				if ((RandyLastPos - Randy.transform.position.x) > 3 || (RandyLastPos - Randy.transform.position.x) < -3) {
					//Debug.Log ("Change Pos");
					RandyLastPos = Randy.transform.position.x;
				}
				Left = Randy.transform.position.x - 5;
				if (Left < Min.transform.position.x) {
					Left = Min.transform.position.x;
				}
				Right = Randy.transform.position.x + 5;
				if (Right > Max.transform.position.x) {
					Right = Max.transform.position.x;
				}
			}
			if (Cola) {
			
			}
		}

	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Randy") {
			RandyLastPos = Randy.transform.position.x;
			bActive = true;
			bEnded = false;
			gameObject.GetComponent<BoxCollider> ().enabled = false;
			if (Cabeza) {
				Part.GetComponent<SierpeParte> ().ActivatePredator (Min.transform.position.x, Max.transform.position.x);
			}
			if (Cola) {
				Part.GetComponent<SierpeParte> ().ActivatePredator (Min.transform.position.x, Max.transform.position.x);
			}
		}
	}
	public void Reactivate(){
		bActive = true;
	}
	public void Deactivate(){
		//Debug.Log ("Deactivate");
		bActive = false;
		StopCoroutine ("Reactivate");
		bEnded = true;
		Part.GetComponent<SierpeParte> ().Deactivate ();
	}
}
