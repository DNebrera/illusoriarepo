﻿using UnityEngine;
using System.Collections;

public class SierpedeLava : MonoBehaviour {
	public int Vida;
	public int NukeDañoCabeza;
	public int NukeDañoCola;
	public int Phase;
	public int Dif;
	public GameObject Randy;
	public GameObject Cola;
	public GameObject Cabeza;
	public GameObject MainCam;
	public GameObject DynCamA;
	public GameObject DynCamB;
	public GameObject DynCamC;
	public GameObject Muro;
	public bool bCola;
	public bool bCabeza;
	public GameObject GiroCuerpo;
	public bool StartJump;
	public bool ChangingPhase;
	public bool beChangingPhase;
	public float JumpGiro;
	public GameObject Boca;
	public GameObject BocaShoot;
	public Vector3[] PartLocations;
	public Vector3[] BodyLocations;
	public GameObject[] MiniBlasts;
	public GameObject[] PyroBlasts;
	public GameObject Pyro;
	public int MiniB;
	public int PyroB;
	public float LavaHeight;
	public float TemporalHeight;
	public bool TemporalRising;
	public bool Comprobar;
	public bool SecondaryAttack;
	public bool bSecondaryAttack;
	public GameObject Lava;
	public GameObject[] Pilares;
	public float Attacktime;
	public float DisableTimeA;
	public float DisableTimeB;
	public int NextAttack;
	public int[] Phase1;
	public int[] Phase2;
	public int[] Phase3;
	public AudioClip[] Sounds;
	// Use this for initialization
	void Start () {
		LavaHeight = 78.5f;
		NextAttack = 0;
		Phase = 0;
		Lava = GameObject.Find ("LavaJefe");
		Randy = GameObject.Find ("Randy");
		MainCam = GameObject.Find ("Main Camera");
		DynCamA = GameObject.Find ("Dynamic_Cam_TriggerBossA");
		DynCamB = GameObject.Find ("Dynamic_Cam_TriggerBossB");
		DynCamC = GameObject.Find ("Dynamic_Cam_TriggerBossC");
		Dif = PlayerPrefs.GetInt("Dificultad");
		DeactivateDynCams ();
	}
	
	// Update is called once per frame
	void Update () {
		bCabeza = Cabeza.GetComponent<SierpeParte> ().Activo;
		bCola = Cola.GetComponent<SierpeParte> ().Activo;
		if (Input.GetKeyDown (KeyCode.Y)) {
			//getOwned ();
			//Jump();
			//EmergeCola();
			//RiseLava();
		}
		if (SecondaryAttack) {
			Cabeza.GetComponent<SierpeParte> ().Disable ();
			Cola.GetComponent<SierpeParte> ().Disable ();
		}
		if (bSecondaryAttack) {
			if(!bCola && !bCabeza){
				Attack ();
				ShiftParts ();
				bSecondaryAttack = false;
			}
		}
		if (StartJump && !bCola && !bCabeza) {
			if (GiroCuerpo.transform.localScale.x > 0) {
				GiroCuerpo.transform.Rotate (new Vector3 (0, 0, 2*Time.timeScale));
				JumpGiro += 2*Time.timeScale;
			}
			if (GiroCuerpo.transform.localScale.x < 0) {
				GiroCuerpo.transform.Rotate (new Vector3 (0, 0, -2*Time.timeScale));
				JumpGiro -= 2*Time.timeScale;
			}
			if (JumpGiro >= 360 || JumpGiro <= -360) {
				StartJump = false;
				SecondaryAttack = false;
				JumpGiro = 0;
			}
		}
		if (!bCola && !bCabeza) {
			if (Lava.transform.position.y > LavaHeight && !TemporalRising) {
				Lava.transform.Translate (0, -1f*Time.timeScale*Time.deltaTime, 0);
			}
			if (Lava.transform.position.y < LavaHeight && !TemporalRising) {
				Lava.transform.Translate (0, 1f*Time.timeScale*Time.deltaTime, 0);
			}
			if (((Lava.transform.position.y - LavaHeight) >= -0.25f && (Lava.transform.position.y - LavaHeight) <= 0.25f) && !TemporalRising && !beChangingPhase) {
				if (SecondaryAttack == true && Comprobar) {
					SecondaryAttack = false;
					Comprobar = false;
				}
				ChangingPhase = false;
				MainCam.GetComponent<PadDetect> ().DisableCont (false);
			}
		}
		if (TemporalRising) {
			Lava.transform.Translate (0, 1f*Time.timeScale*Time.deltaTime, 0);
			if (Lava.transform.position.y > TemporalHeight) {
				TemporalRising = false;
				DisableTimeA = 3;
				DisableTimeB = 3;
			}
		}
		if (!bCabeza && !SecondaryAttack && Phase >= 1 && !ChangingPhase) {
			DisableTimeA += Time.deltaTime * Time.timeScale;
			if (DisableTimeA >= 6 && Phase < 3) {
				Cabeza.GetComponent<SierpeParte> ().Enable ();
				DisableTimeA = 0;
			}
			if (DisableTimeA >= 4 && Phase == 3) {
				Cabeza.GetComponent<SierpeParte> ().Enable ();
				DisableTimeA = 0;
			}
		}
		if (!bCola && !SecondaryAttack && Phase >= 2 && !ChangingPhase) {
			DisableTimeB += Time.deltaTime * Time.timeScale;
			if (DisableTimeB >= 8 && Phase < 3) {
				Cola.GetComponent<SierpeParte> ().Enable ();
				DisableTimeB = 0;
			}
			if (DisableTimeB >= 6 && Phase == 3) {
				Cola.GetComponent<SierpeParte> ().Enable ();
				DisableTimeB = 0;
			}
		}
		if (Phase >= 1 && !SecondaryAttack && !ChangingPhase && !bSecondaryAttack) {
			Attacktime += Time.deltaTime * Time.timeScale;
			if (Attacktime >= 15) {
				Submerge ();
				bSecondaryAttack = true;
				Attacktime = 0;
			}
		}
		if (Phase == 1 && Vida <= 40 && !StartJump) {
			ChangingPhase = true;
			Cabeza.GetComponent<Animator> ().SetTrigger ("EndPhase");
			GiroCuerpo.GetComponent<Animator> ().SetTrigger ("EndPhase");
		}
		if (Phase == 2 && Vida <= 20 && !StartJump) {
			ChangingPhase = true;
			Cabeza.GetComponent<Animator> ().SetTrigger ("EndPhase");
			GiroCuerpo.GetComponent<Animator> ().SetTrigger ("EndPhase");
			Cola.GetComponent<SierpeParte> ().Disable ();
		}
		if (Phase == 3 && Vida <= 0 && !StartJump) {
			ChangingPhase = true;
			Cabeza.GetComponent<Animator> ().SetTrigger ("Die");
			GiroCuerpo.GetComponent<Animator> ().SetTrigger ("EndPhase");
			Cola.GetComponent<SierpeParte> ().Disable ();
		}
	}
	public void ChangePhaseLavaRise(){
		Invoke ("ChangePhaseLavaRise2", 1.5f);
	}
	public void ChangePhaseLavaRise2(){
		Debug.Log ("ChangePhase");
		if (Phase == 1 && Vida <= 40) {
			ChangePhase (2);
		}
		if (Phase == 2 && Vida <= 20) {
			ChangePhase (3);
		}
		if (Phase == 3 && Vida <= 0) {
			ChangePhase (4);
		}
	}
	public void getOwned(){
		Vida -= 20;
	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Randy" || other.tag == "Psychocrusher") {
			gameObject.GetComponent<BoxCollider> ().enabled = false;
			Muro.SetActive (true);
			//MainCam.GetComponent<PadDetect> ().MoveTo (110, 82);
			MainCam.GetComponent<PadDetect> ().DisableCont (true);
			MainCam.GetComponent<PadDetect> ().MoveCamTo2 (108.5f, 82.5f, 0.05f);
			Invoke ("StartFight", 2);
		}
	}
	public void Attack(){
		SecondaryAttack = true;
		if (!bCola && !bCabeza) {
			int a = 0;
			if (Phase == 1) {
				a = Phase1 [NextAttack];
			}
			if (Phase == 2) {
				a = Phase2 [NextAttack];
			}
			if (Phase == 3) {
				a = Phase3 [NextAttack];
			}
			if (a == 1) {				
				Invoke ("Jump", 2);
			}
			if (a == 2) {
				Debug.Log ("Rise Lava Temp!");
				StartSound (0);
				LavaShake ();
				Invoke ("RiseLava", 4);
			}
			if (a == 3) {
				
			}
			NextAttack++;
			if (NextAttack > Phase1.Length) {
				NextAttack = 0;
			}
		}
	}
	public void StartSound(int sound){
		gameObject.GetComponent<AudioSource>().PlayOneShot(Sounds[sound], (gameObject.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
	}
	public void Jump(){
		Cabeza.GetComponent<Animator> ().SetBool ("Active", false);
		Cola.GetComponent<Animator> ().SetBool ("Active", false);
		int s = Random.Range (1, 3);
		if (Phase == 1) {
			GiroCuerpo.transform.position = BodyLocations [0];
		}
		if (Phase == 2) {
			int r = Random.Range (1, 3);
			if (r == 1) {
				GiroCuerpo.transform.position = BodyLocations [1];
			}
			if (r == 2) {
				GiroCuerpo.transform.position = BodyLocations [2];
			}
		}
		if (Phase == 3) {
			int r = Random.Range (1, 4);
			if (r == 1) {
				GiroCuerpo.transform.position = BodyLocations [3];
			}
			if (r == 2) {
				GiroCuerpo.transform.position = BodyLocations [4];
			}
			if (r == 3) {
				GiroCuerpo.transform.position = BodyLocations [5];
			}
		}
		if (s == 1) {
			GiroCuerpo.GetComponent<Animator> ().SetTrigger ("Jump");
		}
		if (s == 2) {
			GiroCuerpo.GetComponent<Animator> ().SetTrigger ("JumpB");
		}
	}
	public void Spit(){
		MiniBlasts [MiniB].transform.position = Boca.transform.position;
		MiniBlasts [MiniB].GetComponent<Rigidbody> ().AddForce (Vector3.up*350f);
		MiniBlasts [MiniB].GetComponent<Rigidbody> ().useGravity = true;
		MiniB++;
		if (MiniB >= MiniBlasts.Length) {
			MiniB = 0;
		}
	}
	public void ChargedSpit(){
		PyroBlasts [PyroB].transform.position = Boca.transform.position;
		Pyro = PyroBlasts [PyroB];
		PyroB++;
		if (PyroB >= PyroBlasts.Length) {
			PyroB = 0;
		}
	}
	public void PowerUP(){
		Pyro.GetComponent<PyroBlast> ().PowerUp ();
	}
	public void ShootPyro(){
		Pyro.GetComponent<PyroBlast> ().bActive = true;
		Pyro.GetComponent<Rigidbody> ().AddForce (new Vector3((BocaShoot.transform.position.x-Boca.transform.position.x),(BocaShoot.transform.position.y-Boca.transform.position.y),Boca.transform.position.z).normalized *100f);
		Pyro = null;
	}
	public void DestroyPyro(){
		if (Pyro != null) {
			Pyro.GetComponent<PyroBlast> ().Burst ();
		}
	}
	public void StartFight(){
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().ShakeCam (0.5f, 1);
		Invoke ("ActivateDynCamA", 1);
		StartSound (0);
		ChangePhase (1);
	}
	public void ActivateDynCamA(){
		DynCamA.SetActive (true);
		Invoke ("DeactivateDynCams", 0.5f);
		MainCam.GetComponent<PadDetect> ().DisableCont (false);
	}
	public void ActivateDynCamB(){
		DynCamB.SetActive (false);
		Invoke ("DeactivateDynCams", 0.5f);
	}
	public void ActivateDynCamC(){
		DynCamC.SetActive (true);
		Invoke ("DeactivateDynCams", 0.5f);
	}
	public void DeactivateDynCams(){
		DynCamA.SetActive (false);
		DynCamB.SetActive (false);
		DynCamC.SetActive (false);
	}
	public void StartMusic(){
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().PlayMusic (2);
	}
	public void Victory(){
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeOutSong ();
		Invoke ("KillMusic", 2);
	}
	public void KillMusic(){
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().PlayMusic (3);
	}
	public void ChangePhase(int phase){
		Phase = phase;
		//StartJump = false;
		Attacktime = 0;
		NextAttack = 0;
		DisableTimeA = 0;
		DisableTimeB = 0;
		beChangingPhase = true;
		GiroCuerpo.GetComponent<Animator> ().ResetTrigger ("Jump");
		GiroCuerpo.GetComponent<Animator> ().ResetTrigger ("JumpB");
		Invoke ("comprobarriselava", 5);
		if (phase == 1) {
			Invoke ("EmergeCabeza", 2f);
			Invoke ("StartMusic", 4f);
		}
		if (phase == 2) {
			StartSound (0);
			Invoke ("RiseLavaChangePhase", 4);
			LavaShake ();
			Pilares [0].GetComponent<SierpePillar> ().bActive = true;
			Pilares [1].GetComponent<SierpePillar> ().bActive = true;
			Invoke ("EmergeCabeza", 18);
			Invoke ("Reactivar", 17);
			Invoke ("EmergeCola", 20);
		}
		if (phase == 3) {
			StartSound (0);
			Invoke ("RiseLavaChangePhase", 4);
			LavaShake ();
			Pilares [2].GetComponent<SierpePillar> ().bActive = true;
			Pilares [3].GetComponent<SierpePillar> ().bActive = true;
			Invoke ("Emerge", 18);
			Invoke ("Reactivar", 17);
		}
		if (phase == 4) {
			StartSound (0);
			Debug.Log ("You Win");
			Invoke ("RiseLavaChangePhase", 4);
			LavaShake ();
			Pilares [4].GetComponent<SierpePillar> ().bActive = true;
			Pilares [5].GetComponent<SierpePillar> ().bActive = true;
		}
	}
	public void RiseLavaChangePhase(){
		resetPyro ();
		if (Phase == 2) {
			LavaHeight = 85.5f;
		}
		if (Phase == 3) {
			LavaHeight = 93.5f;
		}
		if (Phase == 4) {
			LavaHeight = 104.5f;
		}
	}
	public void comprobarriselava(){
		beChangingPhase = false;
	}
	public void LavaShake(){
		iTween.ShakePosition (Lava, new Vector2 (0.5f,0), 1);
		Lava.GetComponent<LavaSierpe> ().HumaredaGuay ();
	}
	public void RiseLava(){
		
		//Debug.Log ("Rise Lava Temp2!");
		if (Phase == 1) {
			TemporalHeight = 83;
		}
		if (Phase == 2) {
			TemporalHeight = 89;
		}
		if (Phase == 3) {
			TemporalHeight = 98.5f;
		}
		TemporalRising = true;
		Comprobar = true;
	}
	public void GetHit(int dañ, int parte){
		Vida -= dañ;
		if (parte == 1) {
			NukeDañoCabeza += dañ;
		}
		if (parte == 2) {
			NukeDañoCola += dañ;
		}
		if (NukeDañoCola >= 3) {
			Cola.GetComponent<Animator> ().SetBool ("Active", false);
			NukeDañoCola = 0;
		}
		if (NukeDañoCabeza >= 10) {
			Cabeza.GetComponent<Animator> ().SetTrigger ("Hit");
			NukeDañoCabeza = 0;
		}
	}
	public void resetPyro(){
		PyroBlasts [0].GetComponent<PyroBlast> ().Burst ();
		PyroBlasts [1].GetComponent<PyroBlast> ().Burst ();
		PyroBlasts [2].GetComponent<PyroBlast> ().Burst ();
		PyroBlasts [3].GetComponent<PyroBlast> ().Burst ();
	}
	public void ReemergeHead(){
		Cabeza.GetComponent<SierpeParte> ().Disable ();
		Cola.GetComponent<SierpeParte> ().Disable ();
		Invoke ("EmergeCabeza", 2);
	}
	public void Reemerge(){
		Cabeza.GetComponent<SierpeParte> ().Disable ();
		Cola.GetComponent<SierpeParte> ().Disable ();
		Invoke ("Emerge", 2);
	}
	public void Submerge(){
		Cabeza.GetComponent<SierpeParte> ().Disable ();
		Cola.GetComponent<SierpeParte> ().Disable ();
	}
	public void Reactivar(){
		Cabeza.GetComponent<Animator> ().SetTrigger ("StartPhase");
		GiroCuerpo.GetComponent<Animator> ().SetTrigger ("StartPhase");
		Cabeza.GetComponent<Animator> ().ResetTrigger ("EndPhase");
		GiroCuerpo.GetComponent<Animator> ().ResetTrigger ("EndPhase");
	}
	public void Emerge(){
		NukeDañoCola = 0;
		NukeDañoCabeza = 0;
		if (Phase == 1) {
			int c = Random.Range (1, 3);
			if (c == 1) {
				Cabeza.transform.position = PartLocations [0];
				Cola.transform.position = PartLocations [1];
			}
			if (c == 2) {
				Cabeza.transform.position = PartLocations [1];
				Cola.transform.position = PartLocations [0];
			}
		}
		if (Phase == 2) {
			int c = Random.Range (1, 5);
			if (c == 1) {
				Cabeza.transform.position = PartLocations [2];
				Cola.transform.position = PartLocations [4];
			}
			if (c == 2) {
				Cabeza.transform.position = PartLocations [3];
				Cola.transform.position = PartLocations [5];
			}
			if (c == 3) {
				Cabeza.transform.position = PartLocations [4];
				Cola.transform.position = PartLocations [2];
			}
			if (c == 4) {
				Cabeza.transform.position = PartLocations [5];
				Cola.transform.position = PartLocations [3];
			}
		}
		if (Phase == 3) {
			int c = Random.Range (1, 6);
			if (c == 1) {
				Cabeza.transform.position = PartLocations [6];
				Cola.transform.position = PartLocations [8];
			}
			if (c == 2) {
				Cabeza.transform.position = PartLocations [8];
				Cola.transform.position = PartLocations [6];
			}
			if (c == 3) {
				Cabeza.transform.position = PartLocations [7];
				Cola.transform.position = PartLocations [9];
			}
			if (c == 4) {
				Cabeza.transform.position = PartLocations [8];
				Cola.transform.position = PartLocations [10];
			}
			if (c == 5) {
				Cabeza.transform.position = PartLocations [10];
				Cola.transform.position = PartLocations [8];
			}
		}
		Cabeza.GetComponent<SierpeParte> ().Enable ();
		if (Dif >= 2) {
			Cola.GetComponent<SierpeParte> ().Enable ();
		}
	}
	public void ShiftParts(){
		if (Phase == 1) {
			int c = Random.Range (1, 3);
			if (c == 1) {
				Cabeza.transform.position = PartLocations [0];
				Cola.transform.position = PartLocations [1];
			}
			if (c == 2) {
				Cabeza.transform.position = PartLocations [1];
				Cola.transform.position = PartLocations [0];
			}
		}
		if (Phase == 2) {
			int c = Random.Range (1, 5);
			int r = Random.Range (1, 3);
			if (r > 1) {
				Cola.GetComponent<SierpeParte> ().ColaGiro ();
			}
			if (c == 1) {
				Cabeza.transform.position = PartLocations [2];
				Cola.transform.position = PartLocations [4];
			}
			if (c == 2) {
				Cabeza.transform.position = PartLocations [3];
				Cola.transform.position = PartLocations [5];
			}
			if (c == 3) {
				Cabeza.transform.position = PartLocations [4];
				Cola.transform.position = PartLocations [2];
			}
			if (c == 4) {
				Cabeza.transform.position = PartLocations [5];
				Cola.transform.position = PartLocations [3];
			}
		}
		if (Phase == 3) {
			int c = Random.Range (1, 6);
			int r = Random.Range (1, 3);
			if (r > 1) {
				Cola.GetComponent<SierpeParte> ().ColaGiro ();
			}
			if (c == 1) {
				Cabeza.transform.position = PartLocations [6];
				Cola.transform.position = PartLocations [8];
			}
			if (c == 2) {
				Cabeza.transform.position = PartLocations [8];
				Cola.transform.position = PartLocations [6];
			}
			if (c == 3) {
				Cabeza.transform.position = PartLocations [7];
				Cola.transform.position = PartLocations [9];
			}
			if (c == 4) {
				Cabeza.transform.position = PartLocations [8];
				Cola.transform.position = PartLocations [10];
			}
			if (c == 5) {
				Cabeza.transform.position = PartLocations [10];
				Cola.transform.position = PartLocations [8];
			}
		}
	}
	public void EmergeCabeza(){
		NukeDañoCola = 0;
		NukeDañoCabeza = 0;
		if (Phase == 1) {
			int c = Random.Range (1, 3);
			if (c == 1) {
				Cabeza.transform.position = PartLocations [0];
				Cola.transform.position = PartLocations [1];
			}
			if (c == 2) {
				Cabeza.transform.position = PartLocations [1];
				Cola.transform.position = PartLocations [0];
			}
		}
		if (Phase == 2) {
			int c = Random.Range (1, 5);
			if (c == 1) {
				Cabeza.transform.position = PartLocations [2];
				Cola.transform.position = PartLocations [4];
			}
			if (c == 2) {
				Cabeza.transform.position = PartLocations [3];
				Cola.transform.position = PartLocations [5];
			}
			if (c == 3) {
				Cabeza.transform.position = PartLocations [4];
				Cola.transform.position = PartLocations [2];
			}
			if (c == 4) {
				Cabeza.transform.position = PartLocations [5];
				Cola.transform.position = PartLocations [3];
			}
		}
		if (Phase == 3) {
			int c = Random.Range (1, 6);
			if (c == 1) {
				Cabeza.transform.position = PartLocations [6];
				Cola.transform.position = PartLocations [8];
			}
			if (c == 2) {
				Cabeza.transform.position = PartLocations [8];
				Cola.transform.position = PartLocations [6];
			}
			if (c == 3) {
				Cabeza.transform.position = PartLocations [7];
				Cola.transform.position = PartLocations [9];
			}
			if (c == 4) {
				Cabeza.transform.position = PartLocations [8];
				Cola.transform.position = PartLocations [10];
			}
			if (c == 5) {
				Cabeza.transform.position = PartLocations [10];
				Cola.transform.position = PartLocations [8];
			}
		}
		Cabeza.GetComponent<SierpeParte> ().Enable ();
	}
	public void EmergeCola(){
		Cola.GetComponent<SierpeParte> ().Enable ();
	}
	public void StopGiro(){
		StartJump = false;
		SecondaryAttack = false;
	}
}
