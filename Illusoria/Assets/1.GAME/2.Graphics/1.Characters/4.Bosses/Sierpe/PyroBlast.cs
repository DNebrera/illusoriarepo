﻿using UnityEngine;
using System.Collections;

public class PyroBlast : MonoBehaviour {
	public int PV;
	public int Dif;
	public float Speed;
	public float MaxSpeed;
	public float PyroTime;
	public Vector3 StartPos;
	public bool bActive;
	public GameObject Randy;
	public AudioClip BurstSound;
	//Miniblast
	public bool bMiniblast;
	// Use this for initialization
	void Start () {
		StartPos = this.transform.position;
		Randy = GameObject.Find ("Randy");
		Dif = PlayerPrefs.GetInt("Dificultad");
	}
	
	// Update is called once per frame
	void Update () {
		if (PV <= 0) {
			Burst ();
		}
		if (bActive) {
			if (Randy != null) {
				this.transform.Translate (new Vector3 ((Randy.transform.position.x - this.transform.position.x), (Randy.transform.position.y+1 - this.transform.position.y), 0).normalized * Speed);
			}
			if (Speed < MaxSpeed) {
				Speed += 0.00025f;
			}
			PyroTime += Time.deltaTime * Time.timeScale;
			if (PyroTime >= 0.5f && Dif == 1) {
				PV -= 1;
				PyroTime = 0;
			} 
			if (PyroTime >= 1 && Dif == 2) {
					PV -= 1;
					PyroTime = 0;
			}
			if (PyroTime >= 2 && Dif == 3) {
				PV -= 1;
				PyroTime = 0;
			} 
		} 
		if (PV == 5) {
			this.transform.localScale = new Vector3 (2,2,2);
			this.GetComponent<SphereCollider> ().radius = 0.5f;
		}
		if (PV == 4) {
			this.transform.localScale = new Vector3 (1.75f,1.75f,2);
			this.GetComponent<SphereCollider> ().radius = 0.45f;
		}
		if (PV == 3) {
			this.transform.localScale = new Vector3 (1.5f,1.5f,2);
			this.GetComponent<SphereCollider> ().radius = 0.4f;
		}
		if (PV == 2) {
			this.transform.localScale = new Vector3 (1.25f,1.25f,2);
			this.GetComponent<SphereCollider> ().radius = 0.35f;
		}
		if (PV == 1 && !bMiniblast) {
			this.transform.localScale = new Vector3 (1f,1f,2);
			this.GetComponent<SphereCollider> ().radius = 0.3f;
		}
		if (bMiniblast && (gameObject.transform.position.y <= 75 || PV <= 0)) {
			Burst ();
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Randy") {
			Debug.Log ("Chocado con Randy");
			gameObject.GetComponent<Animator> ().SetTrigger ("Collide");
		}
		if (other.tag == "Fireball" || other.tag == "Shield") {
			PV-= 1;
			Debug.Log ("Chocado con Fireball");
		}
		if (other.tag == "Big_Fireball" || other.tag == "Psychocrusher") {
			PV -= 5;
			Debug.Log ("Chocado con Fireball");
		}
		if (bMiniblast && other.tag == "Lava" || bMiniblast && other.tag == "Ground") {
			//gameObject.GetComponent<Animator> ().SetTrigger ("Collide");
			//Debug.Log ("Chocado con Suelo");
		}
		if (!bMiniblast && other.tag == "Lava") {
			gameObject.GetComponent<Animator> ().SetTrigger ("Collide");
		}
	}
	public void Reset(){
		//Debug.Log ("Reset");
		PV = 1;
		bActive = false;
		this.transform.position = StartPos;
		Speed = 0.005f;
		gameObject.GetComponent<Animator> ().ResetTrigger ("Collide");
		gameObject.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
		gameObject.GetComponent<Rigidbody> ().useGravity = false;
	}
	public void Burst(){
		gameObject.GetComponent<Animator> ().SetTrigger ("Collide");
	}
	public void PowerUp(){
		PV++;
		if (PV >= 5) {
			gameObject.GetComponent<SphereCollider> ().enabled = true;
		}
		if (PV < 5) {
			gameObject.GetComponent<SphereCollider> ().enabled = false;
		}
	}
	public void PlayBurstSound(){
		gameObject.GetComponent<AudioSource>().PlayOneShot(BurstSound, (gameObject.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
	}
}
