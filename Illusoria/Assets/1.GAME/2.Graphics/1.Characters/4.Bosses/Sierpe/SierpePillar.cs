﻿using UnityEngine;
using System.Collections;

public class SierpePillar : MonoBehaviour {
	public float height;
	public bool bActive;
	// Use this for initialization
	void Start () {
		height = gameObject.transform.position.y;
		gameObject.transform.position = new Vector3 (gameObject.transform.position.x, 60, gameObject.transform.position.z);
	}
	
	// Update is called once per frame
	void Update () {
		if (gameObject.transform.position.y < height && bActive) {
			gameObject.transform.Translate (0, 100f*Time.timeScale*Time.deltaTime, 0);
		}
		if (height - gameObject.transform.position.y < 1.75f) {
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, height, gameObject.transform.position.z);
			bActive = false;
		}
	}
	public void Activate(){
		bActive = true;
	}
}
