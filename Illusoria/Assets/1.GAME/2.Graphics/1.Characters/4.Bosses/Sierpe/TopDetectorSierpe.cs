﻿using UnityEngine;
using System.Collections;

public class TopDetectorSierpe : MonoBehaviour {
	public GameObject[] Escombros;
	public GameObject[] HardEscombros;
	public GameObject Altura;
	public int Esc;
	public int Esc2;
	public int Dif;
	// Use this for initialization
	void Start () {
		Dif = PlayerPrefs.GetInt("Dificultad");
	}
	
	// Update is called once per frame
	void Update () {
		if (Esc > 7) {
			Esc = 0;
		}
		if (Esc2 > 7) {
			Esc2 = 0;
		}
	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Fire") {
			int r = Random.Range (95, 125);
			int t = Random.Range (95, 125);
			int h = Random.Range (95, 125);
			other.transform.position = new Vector3 (r, Altura.transform.position.y, other.transform.position.z);
			other.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
			Escombros[Esc].transform.position = new Vector3 (t, Altura.transform.position.y, this.transform.position.z);
			Escombros [Esc].GetComponent<Rigidbody> ().useGravity = true;
			Esc++;
			if (Dif == 3) {
				HardEscombros[Esc2].transform.position = new Vector3 (h, Altura.transform.position.y, this.transform.position.z);
				HardEscombros [Esc2].GetComponent<Rigidbody> ().useGravity = true;
				Esc2++;
			}
		}
	}
}
