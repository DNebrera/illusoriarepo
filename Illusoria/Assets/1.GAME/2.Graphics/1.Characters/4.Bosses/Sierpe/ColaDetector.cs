﻿using UnityEngine;
using System.Collections;

public class ColaDetector : MonoBehaviour {
	public GameObject Port;
	public GameObject Cola;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Randy") {
			gameObject.SetActive (false);
			Cola.GetComponent<SierpeParte> ().EmergeCola (Port.transform.position.x);
		}
	}
}
