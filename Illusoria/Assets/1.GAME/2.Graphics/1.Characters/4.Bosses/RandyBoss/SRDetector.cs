﻿using UnityEngine;
using System.Collections;

public class SRDetector : MonoBehaviour {
	public GameObject Controller;
	public bool Detector;
	public bool HurtBox;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other) {
		if (Detector) {
			if (other.tag == "Fireball" || other.tag == "Big_Fireball") {
				Controller.GetComponent<ShadowRandy> ().detectattack ();
			}
		}
		if (HurtBox) {
			if (other.tag == "Fireball" || other.tag == "Shield") {
				Controller.GetComponent<ShadowRandy> ().GetHit (1);
			}
			if (other.tag == "Wall") {
				Controller.GetComponent<ShadowRandy> ().CollidedWall ();
			}
			if (other.tag == "Big_Fireball" || other.tag == "Psychocrusher") {
				Debug.Log ("Get hit PC");
				Controller.GetComponent<ShadowRandy> ().GetHit (3);
			}
		}
	}
}
