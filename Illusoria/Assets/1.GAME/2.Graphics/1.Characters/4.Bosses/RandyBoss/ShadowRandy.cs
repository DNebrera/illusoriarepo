﻿using UnityEngine;
using System.Collections;

public class ShadowRandy : MonoBehaviour {
	public int HP;
	public int Dif;
	public float Vel;
	public float previousX;
	public float MoveSpeed;
	public float InvuTime;
	public float PsychoCrushSpeed;
	public Vector3 StartPos;
	public GameObject Randy;
	public GameObject Heart;
	public GameObject TopeLeft;
	public GameObject TopeRight;
	public float DistToRandy;
	public bool Randyleft;
	public float AttackCooldown;
	public AudioClip[] Sounds;
	//Estados
	public bool bActive;
	public bool bAirborne;
	public bool bIdle;
	public bool bTurning;
	public bool bAttacking;
	public bool bLeft;
	public bool bReadyToAttack;
	public bool bFleeing;
	public bool bTackling;
	public bool bPsychoCrush;
	public bool Invulnerable;
	//Partes
	public Animator anim;
	public GameObject ShootPoint;
	public GameObject[] FireballS;
	public int SFireball;
	public GameObject[] FireballB;
	public int BFireball;
	//Roles
	public bool Sniper;
	public bool Blitzer;
	public bool Bomber;
	// Use this for initialization
	void Start () {
		StartPos = gameObject.transform.position;
		anim = gameObject.GetComponent<Animator> ();
		Randy = GameObject.Find ("Randy");
		Dif = PlayerPrefs.GetInt("Dificultad");
		//Enable ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Randy != null) {
			DistToRandy = (Randy.transform.position - gameObject.transform.position).magnitude;
			if (gameObject.transform.position.x > Randy.transform.position.x) {
				Randyleft = false;
			}
			if (gameObject.transform.position.x <= Randy.transform.position.x) {
				Randyleft = true;
			}
		}
		if (Input.GetKeyDown (KeyCode.Y)) {
			//Attack (4);
			//kill();
		}
		if (Invulnerable) {
			InvuTime += Time.deltaTime * Time.timeScale;
			if (InvuTime >= 1) {
				Invulnerable = false;
				InvuTime = 0;
			}
		}
		if (bActive) {
			Vel = (previousX - gameObject.transform.position.x);
			previousX = gameObject.transform.position.x;
			if (Vel != 0) {
				anim.SetBool ("bMoving", true);
			}if (Vel == 0) {
				anim.SetBool ("bMoving", false);
			}
			if (gameObject.transform.position.y <= StartPos.y+0.5f && bAirborne) {
				//Debug.Log ("Grounded!");
				gameObject.transform.position = new Vector3 (gameObject.transform.position.x, StartPos.y-0.5f, gameObject.transform.position.z);
				anim.SetBool ("bAirborne", false);
				bAirborne = false;
				gameObject.GetComponent<Rigidbody> ().useGravity = false;
			}
			if (gameObject.transform.position.y > StartPos.y + 0.45f) {
				//Debug.Log ("Airborne!");
				gameObject.GetComponent<Rigidbody> ().useGravity = true;
				anim.SetBool ("bAirborne", true);
			}
			if (bPsychoCrush) {
				if (bLeft) {
					Debug.Log ("PSLeft!");
					gameObject.transform.Translate (-10 * Time.timeScale * Time.deltaTime, 0, 0);
				}
				if (!bLeft) {
					Debug.Log ("PSRight!");
					gameObject.transform.Translate (10 * Time.timeScale * Time.deltaTime, 0, 0);
				}
			}
			if (bFleeing && !bTurning && !bAttacking) {
				if (bLeft) {
					gameObject.transform.Translate (-MoveSpeed * Time.timeScale * Time.deltaTime, 0, 0);
					if ((DistToRandy >= 8 || gameObject.transform.position.x <= TopeLeft.transform.position.x)) {
						Debug.Log ("StopFleeing!");
						bFleeing = false;
						anim.SetTrigger ("Turn");
						bTurning = true;
						ResetAttack ();
					}
				}
				if (!bLeft) {
					gameObject.transform.Translate (MoveSpeed * Time.timeScale * Time.deltaTime, 0, 0);
					if ((DistToRandy >= 8 || gameObject.transform.position.x >= TopeRight.transform.position.x)) {
						Debug.Log ("StopFleeing!");
						bFleeing = false;
						anim.SetTrigger ("Turn");
						bTurning = true;
						ResetAttack ();
					}
				}
			}
			if (bTackling && !bTurning && !bAttacking) {
				if (bLeft && !bAttacking && !bTurning && gameObject.transform.position.x >= TopeLeft.transform.position.x) {
					gameObject.transform.Translate (-(MoveSpeed+1) * Time.timeScale * Time.deltaTime, 0, 0);
				}
				if (!bLeft && !bAttacking && !bTurning && gameObject.transform.position.x <= TopeRight.transform.position.x) {
					gameObject.transform.Translate ((MoveSpeed+1) * Time.timeScale * Time.deltaTime, 0, 0);
				}
				if ((DistToRandy <= 5 && DistToRandy > 2 && bReadyToAttack)) {
					Debug.Log ("StopTackling!");
					bTackling = false;
					Attack (4);
				}
				if ((DistToRandy <= 2)) {
					Debug.Log ("StopTackling!");
					bTackling = false;
				}
			}
			if (bIdle) {				
				if (Randy.transform.position.x > gameObject.transform.position.x && bLeft && !bFleeing && !bTurning) {
					anim.SetTrigger ("Turn");
					bTurning = true;
				}
				if (Randy.transform.position.x <= gameObject.transform.position.x && !bLeft && !bFleeing && !bTurning) {
					anim.SetTrigger ("Turn");
					bTurning = true;
				}
				if (Sniper) {
					if (DistToRandy >= 10 && !bFleeing) {
						if (bReadyToAttack && !bFleeing && !bTurning && !bAttacking) {
							Debug.Log ("Attack1");
							Attack (1);
						}
					}
					if (DistToRandy >= 3 && DistToRandy < 10 && !bFleeing) {
						if (bReadyToAttack && !bFleeing && !bTurning && !bAttacking) {
							Debug.Log ("Attack1!");
							Attack (1);
						}
					}
					if (bLeft && DistToRandy < 5 && !bFleeing && !(gameObject.transform.position.x >= (TopeRight.transform.position.x - 2)) && !Randyleft) {
						Debug.Log ("FleeingA!");
						anim.SetTrigger ("Turn");
						bFleeing = true;
						bTurning = true;
					}
					if (!bLeft && DistToRandy < 5 && !bFleeing && !(gameObject.transform.position.x <= (TopeLeft.transform.position.x+2)) && Randyleft) {
						Debug.Log ("FleeingB!");
						anim.SetTrigger ("Turn");
						bFleeing = true;
						bTurning = true;
					}
				}
				if (Bomber) {
					if (DistToRandy >= 4 && !bFleeing && bReadyToAttack) {
						if (!bFleeing && !bTurning && !bAttacking) {
							Attack(2);
						}
					}
					if (DistToRandy <= 2 && !bFleeing && !bTackling && bReadyToAttack) {
						if (!bFleeing && !bTurning && !bAttacking) {
							Attack (3);
						}
					}
				}
				if (Blitzer) {
					if (DistToRandy >= 3 && !bFleeing && bReadyToAttack) {
						if (!bFleeing && !bTurning && !bAttacking) {
							bTackling = true;
						}
					}
					if (DistToRandy <= 3 && !bFleeing && !bTackling && bReadyToAttack) {
						if (!bFleeing && !bTurning && !bAttacking) {
							Attack (3);
						}
					}
				}
			}
			if (bReadyToAttack == false) {
				AttackCooldown += Time.deltaTime * Time.timeScale;
				if (AttackCooldown >= 3) {
					AttackCooldown = 0;
					bReadyToAttack = true;
				}
			}
		}	
	}
	public void GetHit(int dmg){
		if (!Invulnerable) {
			HP -= dmg;
			PlaySound (7);
			gameObject.GetComponent<SpriteRenderer> ().color = Color.red;
			Invoke ("ResetColor", 0.1f);
			if (HP <= 0 && bActive) {
				anim.SetTrigger ("Die");
			}
		}
	}
	public void kill(){
		anim.SetTrigger ("Die");
		Heart.GetComponent<HeartOfDarkness> ().GetHit ();
	}
	public void Enable(){
		if (Dif == 1) {
			HP = 1;
		}
		if (Dif == 2) {
			HP = 2;
		}
		if (Dif == 3) {
			HP = 3;
		}
		anim.SetTrigger ("Activated");
		PlaySound (3);
		//DestroyRandy ();
	}
	public void Activate(){		
		bActive = true;
		bIdle = true;
		gameObject.transform.position = new Vector3 (gameObject.transform.position.x, StartPos.y, gameObject.transform.position.z);
		bReadyToAttack = false;
		int r = Random.Range (1, 4);
		if (r == 1) {
			Blitzer = true;
		}
		if (r == 2) {
			Bomber = true;
		}
		if (r == 3) {
			Sniper = true;
		}
	}
	public void Attack(int num){
		bReadyToAttack = false;
		bTackling = false;
		bFleeing = false;
		if (num == 1) {
			anim.SetInteger ("NextAttack", 1);
			anim.SetTrigger ("Attack");
		}
		if (num == 2) {
			anim.SetInteger ("NextAttack", 2);
			anim.SetTrigger ("Attack");
		}
		if (num == 3) {
			anim.SetInteger ("NextAttack", 3);
			anim.SetTrigger ("Attack");
		}
		if (num == 4) {
			anim.SetInteger ("NextAttack", 4);
			anim.SetTrigger ("Attack");
		}
	}
	public void ShootSmall(){
		FireballS [SFireball].GetComponent<BolaDeFuego> ().ShootC (ShootPoint.transform.position, bLeft);
		SFireball++;
		if (SFireball >= FireballS.Length) {
			SFireball = 0;
		}
	}
	public void ShootBig(){
		FireballB [BFireball].GetComponent<BolaDeFuego> ().ShootC (ShootPoint.transform.position, bLeft);
		BFireball++;
		if (BFireball >= FireballB.Length) {
			BFireball = 0;
		}
	}
	public void detectattack(){
		Attack (3);
	}
	public void Turn(){
		Debug.Log ("Turn");
		bTurning = false;
		Invoke ("Idle", 0.25f);
		gameObject.transform.localScale = new Vector3 (gameObject.transform.localScale.x*-1, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
		bool t = bLeft;
		if (t) {			
			bLeft = false;
		}
		if (!t) {
			bLeft = true;
		}
	}
	public void Disable(){
		bActive = false;
	}
	public void HurtHeart(){
		Heart.GetComponent<HeartOfDarkness> ().DestroyRandy (gameObject.transform.position);
	}
	public void Idle(){
		bIdle = true;
	}
	public void Turning(){
		bIdle = false;
	}
	public void Falling(){
		bIdle = false;
		bAirborne = true;
	}
	public void Attacking(){
		bAttacking = true;
	}
	public void PsychoCrush(){
		bPsychoCrush = true;
	}
	public void StopPsychoCrush(){
		bPsychoCrush = false;
	}
	public void ResetColor(){
		gameObject.GetComponent<SpriteRenderer> ().color = Color.white;
	}
	public void DestroyRandy(){
		anim.SetTrigger ("Die");
	}
	public void HerirCorazon(){
		Heart.GetComponent<HeartOfDarkness> ().GetHit ();
	}
	public void Reset(){
		gameObject.transform.position = StartPos;
		Blitzer = false;
		Sniper = false;
		Bomber = false;
	}
	public void CollidedWall(){
		anim.SetTrigger ("Wall");
	}
	public void ResetAttack(){
		anim.ResetTrigger ("Attack");
	}
	public void PlaySound(int s){
		if (s == 2) {
			int r = Random.Range (1, 6);
			if (r == 3) {
				gameObject.GetComponent<AudioSource>().PlayOneShot(Sounds[s], (gameObject.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
			}
		}
		if (s != 5) {
			gameObject.GetComponent<AudioSource>().PlayOneShot(Sounds[s], (gameObject.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
		}
	}
}
