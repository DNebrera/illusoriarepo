﻿using UnityEngine;
using System.Collections;

public class SRSpore : MonoBehaviour {
	public Vector3 StartPos;
	public GameObject Controller;
	public GameObject Controller2;
	public GameObject Randy;
	public float ShotSpeed;
	public bool WhiteSpore;
	public bool bActive;
	public bool bGotoRandy;
	// Use this for initialization
	void Start () {
		StartPos = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (WhiteSpore && bActive) {
			if (ShotSpeed < 6) {
				ShotSpeed += 0.2f;
			}
			gameObject.transform.Translate ((Controller2.transform.position - gameObject.transform.position).normalized*ShotSpeed*Time.deltaTime*Time.timeScale);
			if ((Controller2.transform.position - gameObject.transform.position).magnitude <= 0.5f) {
				//Debug.Log ("HitHeart");
				Reset ();
				//Controller.GetComponent<HeartOfDarkness> ().GetHit ();
				bActive = false;
			}
		}
		if (WhiteSpore && bGotoRandy) {
			if (ShotSpeed < 6) {
				ShotSpeed += 0.2f;
			}
			gameObject.transform.Translate ((Randy.transform.position - gameObject.transform.position).normalized*ShotSpeed*Time.deltaTime*Time.timeScale);
			if ((Randy.transform.position - gameObject.transform.position).magnitude <= 1) {
				//Debug.Log ("HitHeart");
				Reset ();
				//Controller.GetComponent<HeartOfDarkness> ().GetHit ();
				bGotoRandy = false;
			}
		}
		if (Input.GetKeyDown (KeyCode.Y)) {
			//WhiteShoot ();
		}
	}
	public void Shoot(GameObject Target){
		Vector3 V = new Vector3 ((Target.transform.position.x - Controller.transform.position.x), (Target.transform.position.y - Controller.transform.position.y), 0).normalized;
		gameObject.GetComponent<Rigidbody> ().useGravity = true;
		gameObject.GetComponent<Rigidbody> ().AddForce (V*ShotSpeed);
	}
	public void WhiteShoot(){
		int r = Random.Range (1, 3);
		if (r == 1) {
			gameObject.GetComponent<Rigidbody> ().AddForce (new Vector2(25,12));
		}
		if (r == 2) {
			gameObject.GetComponent<Rigidbody> ().AddForce (new Vector2(-25,-12));
		}
		bActive = true;
		Invoke ("resetforce", 0.75f);
	}
	public void Reset(){
		gameObject.GetComponent<Rigidbody> ().useGravity = false;
		gameObject.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
		gameObject.transform.position = StartPos;
	}
	void OnTriggerEnter(Collider other) {		
		if ((other.tag == "Wall" || other.tag == "Ground") && !WhiteSpore) {
			//Debug.Log ("Tocado!");
			Controller.GetComponent<HeartOfDarkness> ().SproutRandy (gameObject.transform.position);
			Reset ();
		}
 	}
	public void resetforce(){
		gameObject.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
	}
	public void GoToRandy(){
		bGotoRandy = true;
	}
}
