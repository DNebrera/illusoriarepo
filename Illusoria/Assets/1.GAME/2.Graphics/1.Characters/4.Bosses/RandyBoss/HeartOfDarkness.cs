﻿using UnityEngine;
using System.Collections;

public class HeartOfDarkness : MonoBehaviour {
	public float HP;
	public int ActiveRandys;
	public float Beat;
	public GameObject[] Spores;
	public GameObject[] WhiteSpores;
	public GameObject[] Randys;
	public GameObject Randy;
	public int NextSpore;
	public int NextWSpore;
	public int NextRandy;
	public int[] Spawn;
	public int SpawnNext;
	private float SpawnTimer;
	public Animator Anim;
	public bool bActive;
	public bool bAlive;
	public bool bEndFight;
	public GameObject SpawnA;
	public GameObject SpawnB;
	public GameObject SpawnC;
	public GameObject SpawnD;
	public GameObject SpawnE;
	public GameObject SpawnBase;
	public GameObject DynCam;
	public GameObject BossTriggerEnd;
	public GameObject KillCollider;
	public int Deaths;
	// Use this for initialization
	void Start () {
		Anim = gameObject.GetComponent<Animator> ();
		SpawnNext = 0;
		NextSpore = 0;
		NextWSpore = 0;
		ActiveRandys = 0;
		//SpawnRandy ();
		Randy = GameObject.Find ("Randy");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Y)) {
			//SpawnRandy ();
			//WonFight();
			//ShootF();
		}
		if (HP <= Deaths && !bEndFight) {
			//Debug.Log ("You win!");
			EliminateRandys ();
			Invoke ("endfight", 2);
			BossTriggerEnd.SetActive (true);
			bActive = false;
			bEndFight = true;
			if (Deaths == 0) {
				GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (18);
			}
			//Destroy (gameObject);
		}
		if(bActive){
			if (HP <= 50 && HP > 37.5f) {
				Anim.SetFloat ("Beat", 1);
			}
			if (HP <= 37.5 && HP > 25f) {
				Anim.SetFloat ("Beat", 2);
			}
			if (HP <= 25 && HP > 12.5f) {
				Anim.SetFloat ("Beat", 3);
			}
			if (HP <= 12.5f && HP > 0) {
				Anim.SetFloat ("Beat", 4);
			}
			SpawnTimer += Time.deltaTime * Time.timeScale;
			if (SpawnTimer >= 15 && ActiveRandys <= 10) {
				SpawnTimer = 0;
				SpawnRandy ();
			}
			if (SpawnTimer >= 7 && ActiveRandys == 0) {
				SpawnTimer = 0;
				SpawnRandy ();
			}
		} 
		if (bAlive) {
			if (HP <= 50 && HP > 37.5f) {
				Anim.SetFloat ("Beat", 1);
			}
			if (HP <= 37.5 && HP > 25f) {
				Anim.SetFloat ("Beat", 2);
			}
			if (HP <= 25 && HP > 12.5f) {
				Anim.SetFloat ("Beat", 3);
			}
			if (HP <= 12.5f && HP > 0) {
				Anim.SetFloat ("Beat", 4);
			}
		}
	}
	public void endfight(){
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().DisableCont (true);
		Invoke ("DelayedMove", 1);
	}
	public void SpawnRandy(){
		if (SpawnNext <= 15) {
			int t = Spawn [SpawnNext];
			if (t == 1) {
				int s = Random.Range (1, 6);
				if (s == 1) {
					ShootA ();
				}
				if (s == 2) {
					ShootB ();
				}
				if (s == 3) {
					ShootF ();
				}
				if (s == 4) {
					ShootD ();
				}
				if (s == 5) {
					ShootE ();
				}
			}
			if (t == 2) {
				int s = Random.Range (1, 6);
				if (s == 1) {
					ShootA ();
					Invoke ("ShootB", 0.5f);
				}
				if (s == 2) {
					ShootB ();
					Invoke ("ShootC", 0.5f);
				}
				if (s == 3) {
					ShootC ();
					Invoke ("ShootF", 0.5f);
				}
				if (s == 4) {
					ShootD ();
					Invoke ("ShootE", 0.5f);
				}
				if (s == 5) {
					ShootE ();
					Invoke ("ShootA", 0.5f);
				}
			}
			if (t == 3) {
				int s = Random.Range (1, 6);
				if (s == 1) {
					ShootA ();
					Invoke ("ShootE", 0.5f);
					Invoke ("ShootF", 1f);
				}
				if (s == 2) {
					ShootB ();
					Invoke ("ShootA", 0.5f);
					Invoke ("ShootE", 1f);
				}
				if (s == 3) {
					ShootC ();
					Invoke ("ShootF", 0.5f);
					Invoke ("ShootA", 1f);
				}
				if (s == 4) {
					ShootD ();
					Invoke ("ShootC", 0.5f);
					Invoke ("ShootB", 1f);
				}
				if (s == 5) {
					ShootE ();
					Invoke ("ShootF", 0.5f);
					Invoke ("ShootC", 1f);
				}
			}
			if (t == 4) {
				int s = Random.Range (1, 6);
				if (s == 1) {
					ShootA ();
					Invoke ("ShootB", 0.5f);
					Invoke ("ShootF", 1f);
					Invoke ("ShootD", 1.5f);
				}
				if (s == 2) {
					ShootB ();
					Invoke ("ShootC", 0.5f);
					Invoke ("ShootF", 1f);
					Invoke ("ShootE", 1.5f);
				}
				if (s == 3) {
					ShootC ();
					Invoke ("ShootD", 0.5f);
					Invoke ("ShootF", 1f);
					Invoke ("ShootA", 1.5f);
				}
				if (s == 4) {
					ShootD ();
					Invoke ("ShootE", 0.5f);
					Invoke ("ShootF", 1f);
					Invoke ("ShootB", 1.5f);
				}
				if (s == 5) {
					ShootE ();
					Invoke ("ShootA", 0.5f);
					Invoke ("ShootF", 1f);
					Invoke ("ShootC", 1.5f);
				}
			}
			if (t == 5) {
				ShootE ();
				Invoke ("ShootD", 0.5f);
				Invoke ("ShootF", 1f);
				Invoke ("ShootB", 1.5f);
				Invoke ("ShootA", 2f);
			}
			SpawnNext++;
		}
	}
	public void SproutRandy(Vector3 Tgt){
		Randys [NextRandy].transform.position = new Vector3 (Tgt.x,Tgt.y+1f,Tgt.z);
		Randys [NextRandy].GetComponent<ShadowRandy> ().Enable ();
		NextRandy++;
		ActiveRandys++;
		if (NextRandy >= Randys.Length) {
			NextRandy = 0;
		}
	}
	public void DestroyRandy(Vector3 loc){
		WhiteSpores [NextWSpore].transform.position = loc;
		WhiteSpores [NextWSpore].GetComponent<SRSpore> ().WhiteShoot ();
		NextWSpore++;
		if (NextWSpore >= WhiteSpores.Length) {
			NextWSpore = 0;
		}
		ActiveRandys--;
		HP--;
	}
	public void EliminateRandys(){
		for (int r = 0; r < Randys.Length; r++) {
			Randys [r].GetComponent<ShadowRandy> ().DestroyRandy ();
		}
		for (int s = 0; s < Spores.Length; s++) {
			Spores [s].GetComponent<SRSpore> ().Reset ();
		}
	}
	public void GetHit(){
		HP--;
		Debug.Log ("Health remaining: " + HP);
	}
	public void SetAlive(){
		bAlive = true;
	}
	public void StartFight(){
		Invoke ("StartFight2", 2);
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().PlayInverseMusic (4);
	}
	public void StartFight2(){
		bAlive = false;
		bActive = true;
		DynCam.SetActive (false);
	}
	public void ShootA(){
		Spores [NextSpore].transform.localPosition = SpawnBase.transform.localPosition;
		Spores [NextSpore].GetComponent<SRSpore> ().Shoot (SpawnA);
		NextSpore++;
		if (NextSpore >= Spores.Length) {
			NextSpore = 0;
		}
	}
	public void ShootB(){
		Spores [NextSpore].transform.localPosition = SpawnBase.transform.localPosition;
		Spores [NextSpore].GetComponent<SRSpore> ().Shoot (SpawnB);
		NextSpore++;
		if (NextSpore >= Spores.Length) {
			NextSpore = 0;
		}
	}
	public void ShootC(){
		Spores [NextSpore].transform.localPosition = SpawnBase.transform.localPosition;
		Spores [NextSpore].GetComponent<SRSpore> ().Shoot (SpawnC);
		NextSpore++;
		if (NextSpore >= Spores.Length) {
			NextSpore = 0;
		}
	}
	public void ShootD(){
		Spores [NextSpore].transform.localPosition = SpawnBase.transform.localPosition;
		Spores [NextSpore].GetComponent<SRSpore> ().Shoot (SpawnD);
		NextSpore++;
		if (NextSpore >= Spores.Length) {
			NextSpore = 0;
		}
	}
	public void ShootE(){
		Spores [NextSpore].transform.localPosition = SpawnBase.transform.localPosition;
		Spores [NextSpore].GetComponent<SRSpore> ().Shoot (SpawnE);
		NextSpore++;
		if (NextSpore >= Spores.Length) {
			NextSpore = 0;
		}
	}
	public void ShootF(){
		Spores [NextSpore].transform.position = new Vector2 (Randy.transform.position.x, SpawnC.transform.position.y);
		Spores [NextSpore].GetComponent<Rigidbody> ().useGravity = true;
		NextSpore++;
		if (NextSpore >= Spores.Length) {
			NextSpore = 0;
		}
	}
	public void SetDeaths(int d){
		Debug.Log ("SetDeaths " + d);
		Deaths = d;
	}
	public void LightToRandyA(){
		Invoke ("DelayedShake", 1.5f);
		WhiteSpores [0].transform.position = SpawnA.transform.position;
		WhiteSpores [1].transform.position = SpawnB.transform.position;
		WhiteSpores [2].transform.position = SpawnC.transform.position;
		WhiteSpores [3].transform.position = SpawnD.transform.position;
		WhiteSpores [4].transform.position = SpawnE.transform.position;
		WhiteSpores [0].GetComponent<SRSpore> ().GoToRandy ();
		WhiteSpores [1].GetComponent<SRSpore> ().GoToRandy ();
		WhiteSpores [2].GetComponent<SRSpore> ().GoToRandy ();
		WhiteSpores [3].GetComponent<SRSpore> ().GoToRandy ();
		WhiteSpores [4].GetComponent<SRSpore> ().GoToRandy ();
	}
	public void LightToRandyB(){
		Invoke ("DelayedShake", 1.5f);
		WhiteSpores [5].transform.position = SpawnA.transform.position;
		WhiteSpores [6].transform.position = SpawnB.transform.position;
		WhiteSpores [7].transform.position = SpawnC.transform.position;
		WhiteSpores [8].transform.position = SpawnD.transform.position;
		WhiteSpores [9].transform.position = SpawnE.transform.position;
		WhiteSpores [5].GetComponent<SRSpore> ().GoToRandy ();
		WhiteSpores [6].GetComponent<SRSpore> ().GoToRandy ();
		WhiteSpores [7].GetComponent<SRSpore> ().GoToRandy ();
		WhiteSpores [8].GetComponent<SRSpore> ().GoToRandy ();
		WhiteSpores [9].GetComponent<SRSpore> ().GoToRandy ();
	}
	public void LightToRandyC(){
		Invoke ("DelayedShake", 1.5f);
		WhiteSpores [10].transform.position = SpawnA.transform.position;
		WhiteSpores [11].transform.position = SpawnB.transform.position;
		WhiteSpores [12].transform.position = SpawnC.transform.position;
		WhiteSpores [13].transform.position = SpawnD.transform.position;
		WhiteSpores [14].transform.position = SpawnE.transform.position;
		WhiteSpores [10].GetComponent<SRSpore> ().GoToRandy ();
		WhiteSpores [11].GetComponent<SRSpore> ().GoToRandy ();
		WhiteSpores [12].GetComponent<SRSpore> ().GoToRandy ();
		WhiteSpores [13].GetComponent<SRSpore> ().GoToRandy ();
		WhiteSpores [14].GetComponent<SRSpore> ().GoToRandy ();
	}
	public void ActivateKill(){
		KillCollider.SetActive (true);
	}
	public void WonFight(){
		Invoke ("LightToRandyA", 3);
		Invoke ("LightToRandyB", 5);
		Invoke ("LightToRandyC", 7);
		Invoke ("LightToRandyA", 9);
		Invoke ("LightToRandyB", 11);
		Invoke ("LightToRandyC", 13);
		Invoke ("LightToRandyA", 15);
		//Invoke ("ActivateKill", 17);
		Invoke ("FinishGame", 20);
	}
	public void DelayedMove(){
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().MoveTo (9, 97.7f);
	}
	public void DelayedShake(){
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().ShakeCam (0.5f, 0.5f);
	}
	public void FinishGame(){
		GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().GoodGameEnding (15);
	}
}
