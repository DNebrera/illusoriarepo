﻿using UnityEngine;
using System.Collections;

public class AbsorbMicWolf : MonoBehaviour {
	public bool Left;
	public bool Right;
	public float strength;
	public GameObject Boss;
	public float DistanceToBoss;
	public bool bMichaelorWolf;
	// Use this for initialization
	void Start () {
		bMichaelorWolf = Boss.GetComponent<MandWBoss> ().MichElseWolf;
		if (PlayerPrefs.GetInt ("Dificultad") == 3) {
			strength = 0.1f;
		}
	}

	// Update is called once per frame
	void Update () {
		DistanceToBoss = (this.transform.position.x - Boss.transform.position.x);
		if (DistanceToBoss > 0) {
			if (bMichaelorWolf) {
				Right = true;
				Left = false;
			}
			if (!bMichaelorWolf) {
				Right = false;
				Left = true;
			}
		}
		if (DistanceToBoss < 0) {
			if (bMichaelorWolf) {
				Right = false;
				Left = true;
			}
			if (!bMichaelorWolf) {
				Right = true;
				Left = false;
			}
		}
	}
	public void OnTriggerStay(Collider Col){
		if(Col.tag == "Randy"){
			if (Right){
				Col.transform.Translate(strength,0,0);
			}
			if (Left){
				Col.transform.Translate(-strength,0,0);
			}
		}
	}
}
