﻿using UnityEngine;
using System.Collections;

public class Globulo : MonoBehaviour {
	private Vector3 StartPos;
	public int Phase;
	public bool bDisabled;
	public bool LeftelseRight;
	public float speed;
	public Animator Anim;
	public float livingtime;
	// Use this for initialization
	void Start () {
		StartPos = new Vector3 (25, 14, 2.5f);
		Anim = gameObject.GetComponent<Animator> ();
		if (PlayerPrefs.GetInt ("Dificultad") == 3) {
			speed = 0.03f;
		}
		if (PlayerPrefs.GetInt ("Dificultad") == 1) {
			speed = 0.01f;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!bDisabled) {
			livingtime += Time.deltaTime;
			if (LeftelseRight) {
				transform.localScale = new Vector3 (-1f,1f,1);
				gameObject.transform.Translate (new Vector3 ((-speed*Time.deltaTime*Time.timeScale)*100, 0, 0));
			}
			if (!LeftelseRight) {
				transform.localScale = new Vector3 (1f,1f,1);
				gameObject.transform.Translate (new Vector3 ((speed*Time.deltaTime*Time.timeScale)*100, 0, 0));
			}
			if (Phase == 1 && (gameObject.transform.position.x >= 54 || gameObject.transform.position.x <= 31.5f)) {
				Anim.SetTrigger ("Collided");
				//Debug.Log ("Pasado!");
			}
			if (Phase == 2 && (gameObject.transform.position.x >= 172 || gameObject.transform.position.x <= 152)) {
				Anim.SetTrigger ("Collided");
			}
		}
		if (livingtime >= 30) {
			Anim.SetTrigger ("Collided");
			livingtime = 0;
		}
	}
	public void Spawn(bool bleft, int phase){
		Phase = phase;
		LeftelseRight = !bleft;
		Anim.ResetTrigger ("Collided");
		Anim.SetTrigger ("Spawned");
		Debug.Log ("Globulo spawned");
	}
	public void activate(){
		bDisabled = false;
	}
	public void disable(){
		Phase = 0;
		bDisabled = true;
		this.transform.position = StartPos;
	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Randy") {
			Anim.SetTrigger ("Collided");
			//Debug.Log ("Tocado a Randy!");
		}
		if (other.tag == "Laser") {
			//Debug.Log ("Tocado laser!");
			Anim.SetTrigger ("Collided");
		}
		if (other.tag == "Shield" || other.tag == "Fireball" || other.tag == "Big_Fireball" || other.tag == "Psychocrusher") {
			//Debug.Log ("Destruido!");
			Anim.SetTrigger ("Collided");
		}
	}
	public void Collided(){
		Anim = gameObject.GetComponent<Animator> ();
		Anim.SetTrigger ("Collided");
		//Debug.Log ("Colisionado!");
	}
	public void Reset(){
		Anim = gameObject.GetComponent<Animator> ();
		Anim.SetTrigger ("Collided");
		//Debug.Log ("Colisionado!");
	}
	void OnTriggerExit(Collider other) {

	}
	public void OnLevelWasLoaded(int level){
		StartPos = new Vector3 (25, 14, 2.5f);
		Anim = gameObject.GetComponent<Animator> ();
	}
}
