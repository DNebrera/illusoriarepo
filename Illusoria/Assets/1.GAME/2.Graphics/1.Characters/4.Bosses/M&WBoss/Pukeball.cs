﻿using UnityEngine;
using System.Collections;

public class Pukeball : MonoBehaviour {
	public Vector3 StartPos;
	private Animator Anim;

	// Use this for initialization
	void Start () {
		StartPos = this.transform.position;
		Anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Randy" || other.tag == "Wall" || other.tag == "Fireball" || other.tag == "Shield" || other.tag == "PsychoCrusher" || other.tag == "Big_Fireball") {
			this.GetComponent<Rigidbody> ().velocity = new Vector3 (0, this.GetComponent<Rigidbody> ().velocity.y, 0);
			this.GetComponent<Rigidbody> ().angularVelocity = new Vector3 (0, this.GetComponent<Rigidbody> ().velocity.y, 0);
			Anim.SetTrigger ("collided");
		}
		if (other.tag == "Ground") {
			this.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
			this.GetComponent<Rigidbody> ().angularVelocity = new Vector3 (0, 0, 0);
			Anim.SetTrigger ("collided");
		}
	}
	public void Collide(){
		this.transform.position = StartPos;
		this.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
		this.GetComponent<Rigidbody> ().angularVelocity = new Vector3 (0, 0, 0);
		GetComponent<Rigidbody> ().useGravity = false;
	}
}
