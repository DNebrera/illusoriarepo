﻿using UnityEngine;
using System.Collections;

public class MandWTop : MonoBehaviour {
	public int Phase;
	public GameObject Place;
	public GameObject MichaelPH2;
	public GameObject WolfgangPH2;
	public float DisttoPort1;
	public float DisttoPort2;
	public GameObject MichaelPort1Ph2;
	public GameObject MichaelPort2Ph2;
	public GameObject Randy;
	public bool bDisabled;
	public float disabletime;
	public bool bDisabled2;
	public float disabletime2;
	public bool bPhase1end;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (bDisabled) {
			disabletime += Time.deltaTime;
		}
		if (disabletime >= 1.5f) {
			bDisabled = false;
			disabletime = 0;
		}
		if (bDisabled2) {
			disabletime2 += Time.deltaTime;
		}
		if (disabletime2 >= 0.2f) {
			bDisabled2 = false;
			disabletime2 = 0;
		}
		if (Phase == 2) {
			if (MichaelPort1Ph2 != null && MichaelPort2Ph2 != null) {
				DisttoPort1 = new Vector2 ((MichaelPort1Ph2.transform.position.x - MichaelPH2.transform.position.x), (MichaelPort1Ph2.transform.position.y - MichaelPH2.transform.position.y)).magnitude;
				DisttoPort2 = new Vector2 ((MichaelPort2Ph2.transform.position.x - MichaelPH2.transform.position.x), (MichaelPort2Ph2.transform.position.y - MichaelPH2.transform.position.y)).magnitude;
			}
		}
	}
	void OnTriggerEnter(Collider other) {
		if (Phase == 1) {
			if (!bPhase1end) {
				if (!bDisabled) {
					bDisabled = true;
					bDisabled2 = true;
					Debug.Log ("Swap!");
					other.transform.position = Place.transform.position;
				}
			}
			if (other.GetComponent<MandWBoss> ().MichElseWolf == true && bPhase1end) {
				other.GetComponent<MandWBoss> ().SetPosPhase2 ();
				other.GetComponent<MandWBoss> ().GroundLevel = other.transform.position.y;
				other.transform.localScale = new Vector3 (-1,1,1);
			}
			if (other.GetComponent<MandWBoss> ().MichElseWolf == false && bPhase1end) {
				other.GetComponent<MandWBoss> ().SetPosPhase2 ();
				other.GetComponent<MandWBoss> ().GroundLevel = other.transform.position.y;
				other.transform.localScale = new Vector3 (1,1,1);
			}
		}	
		if (Phase == 2) {
			if (other.GetComponentInParent<MandWBoss>().MichElseWolf == true && !bDisabled) {
				bDisabled = true;
				if (DisttoPort1 >= DisttoPort2){
					//Debug.Log ("Swap!PH2-To1");
					other.transform.position = MichaelPort1Ph2.transform.position;
				}
				if (DisttoPort2 > DisttoPort1){
					//Debug.Log ("Swap!PH2-To2");
					other.transform.position = MichaelPort2Ph2.transform.position;
				}
			}
			if (other.GetComponentInParent<MandWBoss>().MichElseWolf == false && !bDisabled) {
				bDisabled = true;
				if (Randy.transform.position.x >= 154 && Randy.transform.position.x <= 171) {
					other.transform.position = new Vector2 (Randy.transform.position.x,other.transform.position.y);
				}
				if (Randy.transform.position.x < 154) {
					other.transform.position = new Vector2 (154,other.transform.position.y);
				}
				if (Randy.transform.position.x > 171) {
					other.transform.position = new Vector2 (171,other.transform.position.y);
				}
			}	
		}
	}
	void OnTriggerExit(Collider other) {
		if (Phase == 1) {
			if (!bPhase1end) {
				if (!bDisabled2) {
					other.GetComponent<MandWBoss> ().TouchedTop ();
				}
			}
		}
		if (Phase == 2) {
		
		}
	}
	public void OnLevelWasLoaded(int level){
		StopAllCoroutines ();
		if (level == 12) {
			Randy = GameObject.Find ("Randy");
		}
	}
}
