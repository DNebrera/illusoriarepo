﻿using UnityEngine;
using System.Collections;

public class RandyDetector : MonoBehaviour {
	public bool MichOrWolf;
	public GameObject Cam;		
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerExit(Collider other) {
		if (other.tag == "Randy") {
			if (MichOrWolf) {
				Cam.GetComponent<PadDetect> ().Dial (43, 48);
				this.GetComponent<BoxCollider> ().enabled = false;
			}
			if (!MichOrWolf) {
				Cam.GetComponent<PadDetect> ().Dial (50, 55);
				this.GetComponent<BoxCollider> ().enabled = false;
			}
		}
	}
	public void OnLevelWasLoaded(int level){
		Cam = GameObject.Find ("Main Camera");
	}
}
