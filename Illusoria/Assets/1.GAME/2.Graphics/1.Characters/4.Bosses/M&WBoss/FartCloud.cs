﻿using UnityEngine;
using System.Collections;

public class FartCloud : MonoBehaviour {
	public float DurTime;
	private float Duration;
	public bool bdisabled;
	public Vector3 StartPos;
	// Use this for initialization
	void Start () {
		StartPos = this.transform.position;
		if (PlayerPrefs.GetInt ("Dificultad") == 3) {
			Duration = 35;
		}
		if (PlayerPrefs.GetInt ("Dificultad") == 1) {
			Duration = 15;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!bdisabled) {
			Duration += Time.deltaTime;
			if (Duration >= DurTime) {
				gameObject.GetComponent<Animator> ().SetBool ("bActive", false);
				bdisabled = true;
				Duration = 0;
			}
		}
	}
	public void Disable(){
		this.transform.position = StartPos;
		bdisabled = true;
		Duration = 0;
	}
	public void Activate(){
		gameObject.GetComponent<Animator> ().SetBool ("bActive", true);
		bdisabled = false;
	}
}
