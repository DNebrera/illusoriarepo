﻿using UnityEngine;
using System.Collections;

public class MandWDevourMouth : MonoBehaviour {
	public GameObject Controller;
	public GameObject Kill;
	public GameObject DynCamStop;
	public GameObject Randy;
	// Use this for initialization
	void Start () {
		//Randy = GameObject.Find ("Randy");
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other){
		if (other.tag == "Randy") {
			DynCamStop.SetActive (true);
			//Invoke ("MoveRandy",0.05f);
			Controller.GetComponent<MandWBoss> ().DevourRandy ();
			Invoke ("resetDynCam", 0.2f);
		}
	}
	public void resetDynCam(){
		DynCamStop.SetActive (false);
	}
	public void MoveRandy(){
		Randy.transform.localPosition = Kill.transform.localPosition;
	}
}
