﻿using UnityEngine;
using System.Collections;

public class MandWBoss : MonoBehaviour {
	//Stats
	public int Health;
	public int DangerHealth;
	private int DañoRecib;
	private int DañoNuke;
	public int Phase2Health;
	public int Phase;
	public int Dif;
	public int[] Phase1;
	public int[] Phase2;
	public int Nextattack;
	public bool EndedFight;
	public float MovSpeed;
	public Color StarCol;
	public Vector3 StartPosPhase1;
	public Vector3 StartPosPhase2;
	public AudioClip[] Sounds;
	//Trozos
	public Animator Anim;
	public GameObject StompBox;
	public GameObject Ass;
	public GameObject Mouth;
	public GameObject Absorb;
	public GameObject Puker;
	public GameObject VientoLeft;
	public GameObject VientoRight;
	public GameObject[] FartClouds;
	public GameObject[] BileGoblets;
	public GameObject Controller;
	public GameObject Randy;
	public GameObject Engranaje;
	//Estados
	public bool bDisabled;
	public bool bAttack;
	public bool bIdle;
	public bool bWalking;
	public bool bTurning;
	public bool bjumping;
	public bool bHurt;
	public bool MichElseWolf;
	public bool bWaitingforDeath;
	public bool bAbsorbing;
	private float idletime;
	public float idlemax;
	private float JumpTime;
	public bool bForcedJump;
	//Comprobaciones
	private float WalkingTime;
	public float WalkingTimeMax;
	public Vector3 LastPos;
	public float GroundLevel;
	public float Speed;
	public float Fallspeed;
	public float UpDrift;
	public AnimationClip[] Anims;
	public int Fartcloud = 0;
	public int Bile = 0;
	public float DistancetoRandy;
	// Use this for initialization
	void Start () {
		Anim = GetComponent<Animator> ();
		LastPos = transform.position;
		GroundLevel = transform.position.y;
		Dif = PlayerPrefs.GetInt("Dificultad");
		if (Dif == 3) {
			MovSpeed = 0.06f;
		}
		if (Dif == 1) {
			MovSpeed = 0.03f;
			idlemax = 4;
		}
		//bIdle = true;
		//StartPosPhase1 = gameObject.transform.position;
		StarCol = gameObject.GetComponent<SpriteRenderer> ().color;
		if (MichElseWolf) {
			Anim.SetBool ("bMichael", true);
		}
		if (!MichElseWolf) {
			Anim.SetBool ("bMichael", false);
		}
	}

	// Update is called once per frame
	void Update () {
		if (DañoNuke >= 5) {
			PlaySound (2);
			DañoNuke = 0;
		}
		if (Fartcloud >= FartClouds.Length) {
			Fartcloud = 0;
		}
		if (Bile >= BileGoblets.Length) {
			Bile = 0;
		}
		if (Input.GetKeyDown (KeyCode.Y)) {
			if (MichElseWolf) {
				Health = DangerHealth;
			}
			//Debug.Log ("Jump!");
			//StartTurn ();
			//Hurt();
			//AttackJump();
			//StartAttack (4);
			//ActivateVientoParticulas();
		}
		if (Input.GetKeyDown (KeyCode.T)) {
			//Debug.Log ("Jump!");
			//StartTurn ();
			//Hurt();
			//AttackJump();
			//StartAttack (2);
		}
		if (bWalking) {
			WalkingTime += Time.deltaTime;
			if (WalkingTime >= WalkingTimeMax) {
				bWalking = false;
				WalkingTime = 0;
			}
		} 
		if (!bForcedJump) {
			JumpTime += Time.deltaTime * Time.timeScale;
			if (JumpTime >= 4) {
				bForcedJump = true;
				JumpTime = 0;
			}
		}
		if (Health <= DangerHealth && Phase == 1 && !EndedFight && !bjumping) {
			if (MichElseWolf) {
				Controller.GetComponent<MandWController> ().EndPhase1 (1);

			}
			if (!MichElseWolf) {
				Controller.GetComponent<MandWController> ().EndPhase1 (2);
			}
		}
		if (Health <= 0 && Phase == 2 && !EndedFight) {
			bIdle = false;
			bAttack = false;
			EndedFight = true;
			idletime = 0;
			Anim.SetBool ("bHurt", true);
			DeactivateVientoParticulas ();
			Absorb.SetActive (false);
			Mouth.SetActive (false);
			ResetAttacks ();
			Debug.Log ("" + gameObject.name + " Killed with remaining hp:  " + Health); 
			if (MichElseWolf) {
				Controller.GetComponent<MandWController> ().Phase2End (1);
			}
			if (!MichElseWolf) {
				Controller.GetComponent<MandWController> ().Phase2End (2);
			}
		}
		if (bIdle) {			
			if (Health > DangerHealth && Phase == 1) {
				idletime += Time.deltaTime;
				if (idletime >= idlemax) {
					bIdle = false;
					idletime = 0;
					StartAttack (Phase1[Nextattack]);
					Nextattack++;
				}
			}
			if (Health > 0 && Phase == 2) {
				if (!bAttack) {
					idletime += Time.deltaTime;
					if (idletime >= idlemax) {
						bIdle = false;
						idletime = 0;
						StartAttack (Phase2[Nextattack]);
						Nextattack++;
					}
				}
				if (DañoRecib >= 10) {
					DañoRecib = 0;
					Anim.SetTrigger ("ForcedJump");
				}
				if (DistancetoRandy > -7 && DistancetoRandy < 7 && MichElseWolf && !bWalking) {
					bWalking = true;
				}
				if ((DistancetoRandy < -6 || DistancetoRandy > 6) && !MichElseWolf && !bWalking) {
					bWalking = true;
				}
				if (!bAttack && !bTurning && !bHurt && bWalking) {
					if ((transform.position.x < -71 || transform.position.x > -52) && MichElseWolf) {
						bIdle = false;
						Anim.SetTrigger ("ForcedJump");
					}
					if ((DistancetoRandy < -9 || DistancetoRandy > 9) && !MichElseWolf) {
						if (bForcedJump) {
							bIdle = false;
							Anim.SetTrigger ("ForcedJump");
						}
					}
					if (transform.position.x >= -71 && transform.position.x <= -52 && MichElseWolf && bIdle) {
						if (DistancetoRandy < 0 && this.transform.localScale.x < 0) {
							StartTurn ();
						}
						if (DistancetoRandy >= 0 && this.transform.localScale.x > 0) {
							StartTurn ();
						}
						if (transform.localScale.x > 0) {	
							gameObject.transform.Translate (new Vector3 (-MovSpeed, 0, 0));
						}
						if (transform.localScale.x < 0) {	
							gameObject.transform.Translate (new Vector3 (MovSpeed, 0, 0));
						}
					}	
					if (transform.position.x >= 152 && transform.position.x <= 172 && !MichElseWolf && bIdle) {
						if (DistancetoRandy >= 0 && this.transform.localScale.x < 0) {
							StartTurn ();
						}
						if (DistancetoRandy < 0 && this.transform.localScale.x > 0) {
							StartTurn ();
						}
						if (transform.localScale.x > 0) {	
							gameObject.transform.Translate (new Vector3 (-MovSpeed, 0, 0));
						}
						if (transform.localScale.x < 0) {	
							gameObject.transform.Translate (new Vector3 (MovSpeed, 0, 0));
						}
					}	
				}
			}
		}	
		if (transform.position.y < GroundLevel) {
			GetComponent<Rigidbody> ().useGravity = false;
			if (StompBox.activeSelf == true) {
				StompBox.SetActive (false);
				bjumping = false;
				bIdle = true;
				bAttack = false;
				Anim.SetBool ("bJumping", false);
				if (Phase == 2 && MichElseWolf) {
					GastroAttack ();
				}
			}
			gameObject.transform.position = new Vector3 (gameObject.transform.position.x, GroundLevel, gameObject.transform.position.z);
			GetComponent<Rigidbody> ().velocity = (new Vector3 (0, 0, 0));
			GetComponent<Rigidbody> ().angularVelocity = (new Vector3 (0, 0, 0));
		}
		if (transform.position.y > GroundLevel) {
			
			GetComponent<Rigidbody> ().useGravity = true;
		}
		if (Phase != 1 && Phase != 2) {
			Anim.SetInteger ("Phase", 0);
		}
		if (bjumping) {
			Anim.SetFloat ("DownSpeed",this.GetComponent<Rigidbody>().velocity.y);
		}
		//Fase 1
		if (Phase == 1) {
			Anim.SetInteger ("Phase", 1);
			if (Nextattack >= Phase1.Length) {
				Nextattack = 0;
			}
		}
		//Fase 2
		if (Phase == 2 && !bWaitingforDeath) {
			Anim.SetInteger ("Phase", 2);
			Speed = new Vector2 ((transform.position.x - LastPos.x),0).magnitude;
			Fallspeed = this.GetComponent<Rigidbody> ().velocity.y;
			if (Fallspeed < 0) {
				StompBox.SetActive (true);
			}
			LastPos = transform.position;
			Anim.SetFloat ("Speed",Speed*100);
			Anim.SetFloat ("DownSpeed",Fallspeed);
			DistancetoRandy = this.transform.position.x - Randy.transform.position.x;
		}
	}
	public void EndPhase1(){
		EndedFight = true;
		Absorb.SetActive (false);
		Mouth.SetActive (false);
		DeactivateVientoParticulas ();
		Anim.SetTrigger ("EndPhase1");
		Disable ();
	}
	public void TriggerConvo(){
		if (MichElseWolf) {
			Controller.GetComponent<MandWController> ().Phase1End ();
		}
	}
	void OnTriggerEnter(Collider other) {
		if (Phase == 1 || Phase == 2) {
			if (other.tag == "Fireball" || other.tag == "Shield") {
				gameObject.GetComponent<SpriteRenderer> ().color = Color.black;
				Invoke ("resetcolor",0.1f);
				Health -= 1;
				DañoNuke += 1;
				if (Phase == 2) {
					DañoRecib += 1;
				}
			}
			if (other.tag == "Big_Fireball" || (other.tag == "Psychocrusher" && !bAbsorbing)) {
				gameObject.GetComponent<SpriteRenderer> ().color = Color.black;
				Invoke ("resetcolor",0.1f);
				Health -= 5;
				DañoNuke += 3;
				if (Phase == 2) {
					DañoRecib += 3;
				}
			}
		}
		if ((other.tag == "Big_Fireball" || other.tag == "Psychocrusher") && bWaitingforDeath) {
			if (MichElseWolf) {
				Anim.SetTrigger ("Die");
			}
			if (!MichElseWolf) {
				Anim.SetTrigger ("Die");
			}
		}
	}
	public void OnLevelWasLoaded(int level){
		if (level == 13) {
			Randy = GameObject.Find ("Randy");
		}
	}
	public void resetcolor(){
		gameObject.GetComponent<SpriteRenderer> ().color = StarCol;
	}
	public void StartAttack(int attack){
		if (Phase == 2 && attack != 2 && attack != 3) {
			if (DistancetoRandy < 0 && this.transform.localScale.x < 0) {
			}
			if (DistancetoRandy < 0 && this.transform.localScale.x > 0) {
				StartTurn ();
			}
			if (DistancetoRandy >= 0 && this.transform.localScale.x > 0) {
			}
			if (DistancetoRandy >= 0 && this.transform.localScale.x < 0) {
				StartTurn ();
			}
		}
		Anim.SetTrigger ("bAttack");
		Anim.SetInteger ("NextAttack", attack);
	}
	public void StartTurn(){
		bTurning = true;
		Anim.SetBool ("bTurn", true);
	}
	public void Turn(){
		float Tx = transform.localScale.x;
		if (Tx > 0) {
			transform.localScale = new Vector3 (-Tx,1,1);
		}
		if (Tx < 0) {
			transform.localScale = new Vector3 (-Tx,1,1);
		}
		Anim.SetBool ("bTurn", false);
		bTurning = false;
	}
	public void Hurt(){
		bHurt = true;
		Anim.SetBool ("bHurt", true);
	}
	public void AttackJump(){
		if (Phase != 0) {
			GetComponent<Rigidbody> ().AddForce (0, UpDrift, 0);
			//bjumping = true;
			Anim.SetBool ("bJumping", true);
		}
	}
	public void AttackJump2(){
		GetComponent<Rigidbody> ().AddForce (0, UpDrift, 0);
		//bjumping = true;
		Anim.SetBool ("bJumping", true);
		EndedFight = false;
	}
	public void EndPhaseJump(){
		Phase2Health = Health;
		Anim.SetTrigger ("ChangePhaseJump");
	}
	public void StartPhase2(){
		Randy = GameObject.Find ("Randy");
		Phase = 2;
		bIdle = true;
	}
	public void SetIdle(){
		//Debug.Log ("SetIdle!");
		bAttack = false;
		bIdle = true;
		if (Phase == 2) {
			if (DistancetoRandy < 0 && this.transform.localScale.x < 0) {	}
			if (DistancetoRandy < 0 && this.transform.localScale.x > 0) {
				StartTurn ();
			}
			if (DistancetoRandy >= 0 && this.transform.localScale.x > 0) {	}
			if (DistancetoRandy >= 0 && this.transform.localScale.x < 0) {
				StartTurn ();
			}
		}
	}
	public void Disable(){
		bIdle = false;
		idletime = 0;
		Phase = 0;
		ResetAttacks ();
	}
	public void Dissapear(){
		this.GetComponent<Animator> ().enabled = false;
	}
	public void GastroAttack(){
		//Debug.Log ("Gastroattack!");
		if(Phase == 1){
			if (MichElseWolf) {
				//Michael
				FartClouds[Fartcloud].transform.position = Ass.transform.position;
				FartClouds [Fartcloud].GetComponent<FartCloud> ().Activate ();
				Fartcloud++;
			}
			if (!MichElseWolf) {
				//Wolfgang
				if(Dif >= 2){
					BileGoblets [Bile].transform.position = Ass.transform.position;
					//BileGoblets [Bile].GetComponent<Rigidbody> ().AddForce (new Vector3(-10,1,0));
					BileGoblets [Bile].GetComponent<Globulo> ().Spawn (true,1);
					Bile++;
				}
				BileGoblets [Bile].transform.position = Ass.transform.position;
				//BileGoblets [Bile].GetComponent<Rigidbody> ().AddForce (new Vector3(-10,1,0));
				BileGoblets [Bile].GetComponent<Globulo> ().Spawn (false,1);
				Bile++;
			}
		}
		if(Phase == 2){
			if (MichElseWolf) {
				//Michael
				FartClouds[Fartcloud].transform.position = Ass.transform.position;
				FartClouds [Fartcloud].GetComponent<FartCloud> ().Activate ();
				Fartcloud++;
			}
			if (!MichElseWolf) {
				//Wolfgang
				if(Dif >= 2){
					BileGoblets [Bile].transform.position = Ass.transform.position;
					BileGoblets [Bile].GetComponent<Globulo> ().Spawn (true,2);
					Bile++;
				}
				BileGoblets [Bile].transform.position = Ass.transform.position;
				BileGoblets [Bile].GetComponent<Globulo> ().Spawn (false,2);
				Bile++;
			}
		}
	}
	public void TouchedTop(){
		if (Phase == 1) {
			StartTurn ();
			StompBox.SetActive (true);
		}
	}
	public void Puke(){
		if (Puker.GetComponent<Puker>().bdisabled == false) {
			Puker.GetComponent<Puker> ().bdisabled = true;
		}
		if (Puker.GetComponent<Puker>().bdisabled == true) {
			Puker.GetComponent<Puker> ().bdisabled = false;
		}
	}
	public void ToggAbsorb(){
		if (Absorb.activeSelf == false) {
			Absorb.SetActive(true);
			Mouth.SetActive (true);
		}
		else if (Absorb.activeSelf == true) {
			Absorb.SetActive(false);
			Mouth.SetActive (false);
		}
	}
	public void StartPhase1(){
		bIdle = true;
		Phase = 1;
	}
	public void ResetPhase1(){
		bIdle = false;
		Phase = 0;
		Health = 85;
		Nextattack = 0;
		bAttack = false;
		bDisabled = false;
		bWalking = false;
		bjumping = false;
		bHurt = false;
		EndedFight = false;
		bTurning = false;
		Puker.GetComponent<Puker> ().bdisabled = true;
		Absorb.SetActive (false);
		StompBox.SetActive (false);
		Mouth.SetActive (false);
		DeactivateVientoParticulas ();
		GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
		if (MichElseWolf) {
			for (int s = 0; s < FartClouds.Length; s++) {
				FartClouds [s].GetComponent<Animator> ().SetBool ("bActive", false);
			}
			gameObject.transform.localScale = new Vector3 (-1, 1, 1);
		}
		if (!MichElseWolf) {
			for (int s = 0; s < BileGoblets.Length; s++) {
				BileGoblets [s].GetComponent<Globulo> ().Collided ();
			}
			gameObject.transform.localScale = new Vector3 (1, 1, 1);
		}
	}
	public void ResetPhase2(){
		EndedFight = false;
		bIdle = false;
		Phase = 0;
		Health = Phase2Health;
		Nextattack = 0;
		bAttack = false;
		bDisabled = false;
		bWalking = false;
		bjumping = false;
		bHurt = false;
		bTurning = false;
		bWaitingforDeath = false;
		Absorb.SetActive (false);
		StompBox.SetActive (false);
		Mouth.SetActive (false);
		Puker.GetComponent<Puker> ().bdisabled = true;
		DeactivateVientoParticulas ();
		Anim.ResetTrigger ("ForcedJump");
		gameObject.transform.localPosition = StartPosPhase2;
		GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
		if (MichElseWolf) {
			for (int s = 0; s < FartClouds.Length; s++) {
				FartClouds [s].GetComponent<FartCloud> ().Disable ();
			}
			gameObject.transform.localScale = new Vector3 (-1, 1, 1);
		}
		if (!MichElseWolf) {
			for (int s = 0; s < BileGoblets.Length; s++) {
				BileGoblets [s].GetComponent<Globulo> ().Collided ();
			}
			gameObject.transform.localScale = new Vector3 (1, 1, 1);
		}
	}
	public void PlaySound(int s){
		gameObject.GetComponent<AudioSource>().PlayOneShot(Sounds[s], (gameObject.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
	}
	public void DevourRandy(){
		DeactivateVientoParticulas ();
		Anim.SetTrigger ("KillRandy");
	}
	public void Regenerate(){
		if (Health < 85 && Dif >= 2) {
			Health += 1;
		}
		if (Health < 100 && Dif == 3) {
			Health += 1;
		}
	}
	public void ActivateEngranaje(){
		Engranaje.SetActive (true);
		EndedFight = false;
	}
	public void ActivateVientoParticulas(){
		bAbsorbing = true;
		if (gameObject.transform.localScale.x > 0) {
			VientoLeft.transform.position = gameObject.transform.position;
			VientoLeft.SetActive (true);
		}
		if (gameObject.transform.localScale.x < 0) {
			VientoRight.transform.position = gameObject.transform.position;
			VientoRight.SetActive (true);
		}
	}
	public void DeactivateVientoParticulas(){
		bAbsorbing = false;
		Absorb.SetActive (false);
		Mouth.SetActive (false);
		VientoLeft.SetActive (false);
		VientoRight.SetActive (false);
	}
	public void SetPosPhase1(){
		gameObject.transform.localPosition = StartPosPhase1;
	}
	public void SetPosPhase2(){
		gameObject.transform.localPosition = StartPosPhase2;
	}
	public void ResetAttacks(){
		if (MichElseWolf) {
			for (int s = 0; s < FartClouds.Length; s++) {
				FartClouds [s].GetComponent<Animator> ().SetBool ("bActive", false);
			}
		}
		if (!MichElseWolf) {
			for (int s = 0; s < BileGoblets.Length; s++) {
				BileGoblets [s].GetComponent<Globulo> ().Collided ();
			}
		}
	}
	public void ActivateAttack(){
		bAttack = true;
	}
	public void EndAttack(){
		bAttack = true;
	}
	public void StartJumping(){
		bjumping = true;
	}
	public void EndJumping(){
		bjumping = false;
	}
	public void ReactivateAnimator(){
		gameObject.GetComponent<Animator> ().enabled = true;
	}
	public void DeactivateAbsorb(){
		DeactivateVientoParticulas ();
		Absorb.SetActive (false);
	}
	public void Defeated(){
		bAbsorbing = false;
	}
	public void WaitingDeath(){
		bWaitingforDeath = true;
		Phase = 0;
		if (MichElseWolf) {
			Controller.GetComponent<MandWController> ().ActivateMichaelDoor (false);
		}
		if (!MichElseWolf) {
			Controller.GetComponent<MandWController> ().ActivateWolfgangDoor (false);
		}
	}
	public void CheckSide(){
		if (DistancetoRandy < 0 && this.transform.localScale.x > 0) {
			Anim.SetTrigger ("Turn");
		}
		if (DistancetoRandy >= 0 && this.transform.localScale.x < 0) {
			Anim.SetTrigger ("Turn");
		}
	}
}
