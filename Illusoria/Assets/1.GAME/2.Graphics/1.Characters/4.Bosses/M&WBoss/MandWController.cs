﻿using UnityEngine;
using System.Collections;

public class MandWController : MonoBehaviour {
	public GameObject Cam;
	public GameObject Michael;
	public GameObject Wolfgang;
	public int End;
	public Vector3 MichaelStartPos;
	public Vector3 WolfgangStartPos;
	public GameObject NightmareMichael;
	public GameObject NightmareWolfgang;
	public GameObject LaserFase1;
	public GameObject MichaelPH2;
	public GameObject WolfgangPH2;
	public GameObject StartTrigger;
	public GameObject[] TopesFase1;
	public GameObject[] EngranajesLeft;
	public GameObject[] EngranajesRight;
	public GameObject PuertaA;
	public GameObject PuertaB;
	public GameObject PuertaMichPH2;
	public GameObject PuertaWolfPH2;
	public float Phasetime;
	public float Phase1Time;
	public int Phase;
	public bool MichaelDead;
	public bool WolfgangDead;
	public bool EngranajeMGot;
	public bool EngranajeWGot;
	public bool EndedPhase1;
	public bool bCounting;
	public bool StartEncounter;
	// Use this for initialization
	void Awake () {
		if (GameObject.Find (gameObject.name) != gameObject) {
			Destroy (gameObject);
		}
	}
	void Start () {
		//SetPhase2 (100,100);
		DontDestroyOnLoad (gameObject);
		Cam = GameObject.Find ("Main Camera");
		Michael = GameObject.Find ("Michael");
		Wolfgang = GameObject.Find ("Wolfgang");
		MichaelStartPos = Michael.transform.position;
		WolfgangStartPos = Wolfgang.transform.position;
	}	
	// Update is called once per frame
	void Update () {
		if (Phase == 1 && !EndedPhase1 && bCounting) {
			Phasetime += Time.deltaTime*Time.timeScale;
			if (Phasetime >= Phase1Time) {
				EndPhase1 (3);
				Phasetime = 0;
			}
		}
	}
	public void TransformBoss(){
		StartCoroutine ("Transform");
	}
	public void Phase1Start(){
		NightmareMichael.GetComponent<MandWBoss> ().StartPhase1 ();
		NightmareWolfgang.GetComponent<MandWBoss> ().StartPhase1 ();
		Phase = 1;
		bCounting = true;
		Phasetime = 0;
		LaserFase1.SetActive (true);
	}
	public void Phase2Start(bool MorW){
		if (MorW) {
			NightmareMichael.GetComponent<MandWBoss> ().StartPhase2 ();
			ActivateMichaelDoor (true);
		}
		if (!MorW) {
			NightmareWolfgang.GetComponent<MandWBoss> ().StartPhase2 ();
			ActivateWolfgangDoor (true);
		}
		Phase = 2;
	}
	public void EndPhase1(int e){
		NightmareMichael.GetComponent<MandWBoss> ().EndPhase1 ();
		NightmareWolfgang.GetComponent<MandWBoss>().EndPhase1();
		End = e;
		bCounting = false;
		Phasetime = 0;
	}
	public void Phase1End(){
		//Debug.Log ("Phase 1 end!");
		Cam.GetComponent<PadDetect>().FadeOutSong();
		//Phase = 0;
		if (End == 1) {
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (16);
			StartCoroutine ("EndPh1One");
		}
		if (End == 2) {
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (16);
			StartCoroutine ("EndPh1Two");
		}
		if (End == 3) {
			StartCoroutine ("EndPh1Three");
		}
		DeactivateAbsorbs ();
		PuertaA.SetActive (false);
		PuertaB.SetActive (false);
		PuertaMichPH2.SetActive (false);
		PuertaWolfPH2.SetActive (false);
	}
	public void Phase2End(int end){
		Cam.GetComponent<PadDetect>().FadeOutSong();
		if (end == 1) {
			StartCoroutine ("EndPh2One");
		}
		if (end == 2) {
			StartCoroutine ("EndPh2Two");
		}
	}
	public void Phase1Trigger(){
		Cam.GetComponent<PadDetect> ().Dial (370,425);
	}
	public void Phase2MichaelTrigger(){
		Cam.GetComponent<PadDetect> ().Dial (455, 458);
	}
	public void Phase2WolfgangTrigger(){
		Cam.GetComponent<PadDetect> ().Dial (460, 463);
	}
	public void SetPhase2(int Michael, int Wolfgang){
		if (Michael > 0) {
			NightmareMichael.SetActive (true);
			NightmareMichael.transform.position = MichaelPH2.transform.position;
			NightmareMichael.GetComponent<MandWBoss> ().GroundLevel = NightmareMichael.transform.position.y;
			NightmareMichael.transform.localScale = new Vector3 (1,1,1);
			NightmareMichael.GetComponent<MandWBoss> ().Health = Michael;
		}
		if (Wolfgang > 0) {
			NightmareWolfgang.SetActive (true);
			NightmareWolfgang.transform.position = WolfgangPH2.transform.position;
			NightmareWolfgang.GetComponent<MandWBoss> ().GroundLevel = NightmareWolfgang.transform.position.y;
			NightmareWolfgang.transform.localScale = new Vector3 (-1,1,1);
			NightmareWolfgang.GetComponent<MandWBoss> ().Health = Wolfgang;
		}
	}
	public void Phase2Jump(){
		Phase = 0;
		EndedPhase1 = true;
		LaserFase1.SetActive (false);
		NightmareMichael.GetComponent<MandWBoss> ().EndPhaseJump ();
		NightmareWolfgang.GetComponent<MandWBoss> ().EndPhaseJump ();
		TopesFase1 [0].SetActive (false);
		TopesFase1 [1].SetActive (false);
		TopesFase1 [2].SetActive (true);
	}
	IEnumerator Transform(){
		//Cam.GetComponent<PadDetect> ().bDisabled = true;
		StartEncounter = false;
		Michael.transform.position = new Vector2(20,90);
		Wolfgang.transform.position = new Vector2(20,90);
		NightmareMichael.GetComponent<MandWBoss> ().SetPosPhase1 ();
		NightmareWolfgang.GetComponent<MandWBoss> ().SetPosPhase1 (); 
		NightmareMichael.SetActive (true);
		NightmareWolfgang.SetActive (true);
		PuertaA.SetActive (true);
		PuertaB.SetActive (true);
		yield return new WaitForSeconds (1.0f);
		//Cam.GetComponent<PadDetect> ().Dial (391,393);
		//Cam.GetComponent<PadDetect> ().bDisabled = false;
		yield return null;
	}
	IEnumerator EndPh1One(){
		yield return new WaitForSeconds (1.0f);
		Cam.GetComponent<PadDetect> ().Dial (427,428);
	}
	IEnumerator EndPh1Two(){
		yield return new WaitForSeconds (1.0f);
		Cam.GetComponent<PadDetect> ().Dial (430,431);
	}
	IEnumerator EndPh1Three(){
		yield return new WaitForSeconds (1.0f);
		Cam.GetComponent<PadDetect> ().Dial (433,435);
	}
	IEnumerator EndPh2One(){
		yield return new WaitForSeconds (1.0f);
		Cam.GetComponent<PadDetect> ().Dial (437,444);
		MichaelDead = true;
	}
	IEnumerator EndPh2Two(){
		yield return new WaitForSeconds (1.0f);
		if (PlayerPrefs.GetInt ("IgnoredWolf") == 1) {
			Cam.GetComponent<PadDetect> ().Dial (446,453);
			WolfgangDead = true;
		}
		if (PlayerPrefs.GetInt ("IgnoredWolf") == 0) {
			Cam.GetComponent<PadDetect> ().Dial (448,453);
			WolfgangDead = true;
		}
	}
	public void OnLevelWasLoaded(int level){
		StopAllCoroutines ();
		if (level == 12) {
			StartTrigger = GameObject.Find ("TriggerBossStartA");
			Cam = GameObject.Find ("Main Camera");
			Michael = GameObject.Find ("Michael");
			Wolfgang = GameObject.Find ("Wolfgang");
			NightmareMichael.GetComponent<MandWBoss> ().ResetAttacks ();
			NightmareWolfgang.GetComponent<MandWBoss> ().ResetAttacks ();
			if (StartEncounter) {
				ResetPhase1 ();
			}
			if (Phase == 1 && !EndedPhase1) {
				ResetPhase1 ();
			}
			if (EndedPhase1) {
				ResetPhase2 ();
			}

			if (MichaelDead) {
			}
			if (WolfgangDead) {
			}
		}
		if (level != 12) {
			Destroy (gameObject);
		}
	}
	public void ResetPhase1(){
		Phase = 0;
		End = 0;
		Phasetime = 0;
		bCounting = false;
		Debug.Log ("ResetPhase1");
		StartEncounter = false;
		Michael.transform.position = MichaelStartPos;
		Wolfgang.transform.position = WolfgangStartPos;
		Michael.transform.localScale = new Vector3 (-0.75f, 0.75f, 1);
		Wolfgang.transform.localScale = new Vector3 (0.75f, 0.75f, 1);
		Michael.GetComponent<Michael_Wolfgang> ().HasConvo = true;
		StartTrigger.GetComponent<Triggers> ().Activar ();
		NightmareMichael.GetComponent<MandWBoss> ().ResetPhase1 ();
		NightmareMichael.GetComponent<MandWBoss> ().DeactivateVientoParticulas ();
		NightmareMichael.GetComponent<Animator> ().ResetTrigger ("bAttack");
		NightmareMichael.GetComponent<Animator> ().SetTrigger ("Reset");
		NightmareMichael.GetComponent<Animator> ().SetBool ("bJumping", false);
		NightmareMichael.SetActive (false);
		NightmareWolfgang.GetComponent<MandWBoss> ().ResetPhase1 ();
		NightmareWolfgang.GetComponent<MandWBoss> ().DeactivateVientoParticulas ();
		NightmareWolfgang.SetActive (false);
		NightmareWolfgang.GetComponent<Animator> ().ResetTrigger ("bAttack");
		NightmareWolfgang.GetComponent<Animator> ().SetTrigger ("Reset");
		NightmareWolfgang.GetComponent<Animator> ().SetBool ("bJumping", false);
		TopesFase1 [0].GetComponent<MandWTop> ().bPhase1end = false;
		TopesFase1 [1].GetComponent<MandWTop> ().bPhase1end = false;
		PuertaA.SetActive (false);
		PuertaB.SetActive (false);
		LaserFase1.SetActive (false);
	}
	public void ResetPhase2(){
		Phase = 0;
		ActivateMichaelDoor (false);
		ActivateWolfgangDoor (false);
		if (!MichaelDead) {
			GameObject.Find ("TriggerBossStartMichael2").GetComponent<Triggers> ().Activar ();
			NightmareMichael.GetComponent<MandWBoss> ().ResetPhase2 ();
			NightmareMichael.GetComponent<Animator> ().ResetTrigger ("bAttack");
			NightmareMichael.GetComponent<Animator> ().SetTrigger ("Reset");
			NightmareMichael.GetComponent<Animator> ().SetBool ("bJumping", false);
		}
		if (!WolfgangDead) {
			GameObject.Find ("TriggerBossStartWolfgang2").GetComponent<Triggers> ().Activar ();
			NightmareWolfgang.GetComponent<MandWBoss> ().ResetPhase2 ();
			NightmareWolfgang.GetComponent<Animator> ().ResetTrigger ("bAttack");
			NightmareWolfgang.GetComponent<Animator> ().SetTrigger ("Reset");
			NightmareWolfgang.GetComponent<Animator> ().SetBool ("bJumping", false);
		}
	}
	public void EngranajeGet(int e){
		if (e == 1) {
			EngranajeMGot = true;
		}
		if (e == 2) {
			EngranajeWGot = true;
		}
	}
	public void StartEnc(){
		StartEncounter = true;
	}
	public void ActivateMichaelDoor(bool d){
		PuertaMichPH2.SetActive (d);
	}
	public void ActivateWolfgangDoor(bool d){
		PuertaWolfPH2.SetActive (d);
	}
	public void DeactivateAbsorbs(){
		if (NightmareMichael != null) {
			NightmareMichael.GetComponent<MandWBoss> ().DeactivateAbsorb ();
		}
		if (NightmareWolfgang != null) {
			NightmareWolfgang.GetComponent<MandWBoss> ().DeactivateAbsorb ();
		}
	}
}
