﻿using UnityEngine;
using System.Collections;

public class Puker : MonoBehaviour {
	private float PukeTimer;
	public float TimetoPuke;
	public float PukeStrength;
	public bool bdisabled;
	public int PukeAngle = 0;
	public int PukeBall = 0;
	public GameObject[] SubPuker;
	public GameObject[] PukeBallBag;
	public Vector2[] PukingForce;
	// Use this for initialization
	void Start () {
		PukingForce = new Vector2[3];
	}
	
	// Update is called once per frame
	void Update () {
		if (PukeAngle > 2) {
			PukeAngle = 0;
		}
		if (PukeBall >= PukeBallBag.Length) {
			bdisabled = true;
			PukeBall = 0;
		}
		if (!bdisabled) {
			PukeTimer += Time.deltaTime;
			if (PukeTimer >= TimetoPuke) {
				ThrowPukeBall();
				PukeTimer = 0;
			}
		}
	}
	public void ThrowPukeBall(){
		PukingForce [0] = new Vector2 ((SubPuker [0].transform.position.x - this.transform.position.x), (SubPuker [0].transform.position.y - this.transform.position.y)).normalized;
		PukingForce [1] = new Vector2 ((SubPuker [1].transform.position.x - this.transform.position.x), (SubPuker [1].transform.position.y - this.transform.position.y)).normalized;
		PukingForce [2] = new Vector2 ((SubPuker [2].transform.position.x - this.transform.position.x), (SubPuker [2].transform.position.y - this.transform.position.y)).normalized;
		PukeBallBag [PukeBall].transform.position = this.transform.position;
		PukeBallBag [PukeBall].GetComponent<Rigidbody> ().useGravity = true;
		PukeBallBag [PukeBall].GetComponent<Rigidbody> ().AddForce ((PukingForce [PukeAngle])*PukeStrength);
		PukeBall++;
		PukeAngle++;
	}
}
