﻿using UnityEngine;
using System.Collections;

public class BolaDeFuego : MonoBehaviour {
	private Animator Anim;
	public float ShotSpeed;
	public Vector3 StartPos;
	public GameObject Randy;
	public GameObject hueco;
	private float Activetime;
	public bool bActive;
	// Use this for initialization
	void Start () {
		Anim = GetComponent<Animator> ();
		StartPos = this.transform.position;
		Randy = GameObject.Find ("Randy");
	}
	
	// Update is called once per frame
	void Update () {
		if (bActive) {
			Activetime += Time.deltaTime * Time.timeScale;
			if (Activetime >= 2) {
				
				Anim.SetTrigger ("collided");
				Activetime = 0;
				bActive = false;
			}
		}
	}
	public void Reset(){
		//Debug.Log ("Fade Bola de fuego");
		this.transform.position = StartPos;
		this.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
		this.GetComponent<Rigidbody> ().angularVelocity = new Vector3 (0, 0, 0);
		gameObject.GetComponent<SphereCollider> ().enabled = true;
		//hueco.SetActive (true);
	}
	public void NullTrig(){
		gameObject.GetComponent<SphereCollider> ().enabled = false;
	}
	public void Shoot(Vector2 V, float Angle, bool bLeft){
		this.transform.position = V;
		if (bLeft) {
			this.transform.eulerAngles = new Vector3 (0, 0, Angle);
		}
		if (!bLeft) {
			this.transform.eulerAngles = new Vector3 (0, 0, -Angle);
		}
		Vector2 VV = new Vector2 ((Randy.transform.position.x - this.transform.position.x), ((Randy.transform.position.y+1f) - this.transform.position.y)).normalized* ShotSpeed;
		gameObject.GetComponent<Rigidbody> ().velocity = VV;
	}
	public void ShootB(Vector2 V, float Angle){
		this.transform.position = V;
		this.transform.eulerAngles = new Vector3 (0, 0, Angle);
		gameObject.GetComponent<Rigidbody> ().velocity = transform.up.normalized*ShotSpeed;
	}
	public void ShootC(Vector2 V, bool bleft){
		Debug.Log ("ShootC");
		bActive = true;
		this.transform.position = V;
		if (bleft) {
			this.transform.eulerAngles = new Vector3 (0, 0, 90);
			gameObject.GetComponent<Rigidbody> ().velocity = new Vector2(-1,0)*ShotSpeed;
		}
		if (!bleft) {
			this.transform.eulerAngles = new Vector3 (0, 0, 270);
			gameObject.GetComponent<Rigidbody> ().velocity = new Vector2(1,0)*ShotSpeed;
		}
	}
	public void ShootE(Vector2 V, float Angle, bool bLeft, Vector2 Objective){
		this.transform.position = V;
		if (bLeft) {
			this.transform.eulerAngles = new Vector3 (0, 0, -Angle);
		}
		if (!bLeft) {
			this.transform.eulerAngles = new Vector3 (0, 0, Angle);
		}
		Vector2 VV = new Vector2 ((Objective.x - this.transform.position.x), ((Objective.y+1f) - this.transform.position.y)).normalized* ShotSpeed;
		gameObject.GetComponent<Rigidbody> ().velocity = VV;
		//Debug.Log ("Shooting to: " + Objective + " At an angle of: " + VV);
	}
	public void ShootD(Vector2 V, Vector2 v, float Angle, bool bLeft){
		this.transform.position = V;
		if (bLeft) {
			this.transform.eulerAngles = new Vector3 (0, 0, Angle);
		}
		if (!bLeft) {
			this.transform.eulerAngles = new Vector3 (0, 0, -Angle);
		}
		Vector2 VV = new Vector2 ((v.x - this.transform.position.x), ((v.y) - this.transform.position.y)).normalized* ShotSpeed;
		gameObject.GetComponent<Rigidbody> ().velocity = VV;
	}
	void OnTriggerEnter(Collider other) {
		//hueco.SetActive (false);
		if (other.tag == "Randy" || other.tag == "Wall") {
			Invoke ("Collided", 0.25f);
		}
		if (other.tag == "Ground") {
			Invoke ("Collided", 0.05f);
		}
		if (other.tag == "Fireball" || other.tag == "Shield" || other.tag == "Big_Fireball" || other.tag == "Psychocrusher") {
			Invoke ("Collided", 0.05f);
		}
	}
	public void Collided(){
		this.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
		this.GetComponent<Rigidbody> ().angularVelocity = new Vector3 (0, this.GetComponent<Rigidbody> ().velocity.y, 0);
		Anim.SetTrigger ("collided");
	}
}
