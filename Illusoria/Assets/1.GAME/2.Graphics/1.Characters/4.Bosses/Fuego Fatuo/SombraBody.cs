﻿using UnityEngine;
using System.Collections;

public class SombraBody : MonoBehaviour {
	public GameObject P;
	public GameObject Randy;
	public GameObject Gestor;
	public GameObject Núcleo;
	public Vector2 StartPos;
	public float invul;
	public bool invulnerable;
	public AudioClip[] Sounds;
	// Use this for initialization
	void Start () {
		Randy = GameObject.Find ("Randy");
		Gestor = GameObject.Find ("GestorFuegoFatuo");
	}
	
	// Update is called once per frame
	void Update () {
		Núcleo.GetComponent<AudioSource> ().volume = PlayerPrefs.GetFloat ("EffectVolume")*0.4f;
		if (invulnerable) {
			invul += Time.deltaTime;
			if (invul >= 0.2f) {
				invulnerable = false;
				invul = 0;
			}
		}
	}
	public void Shoot(){
		P.GetComponent<FuegoFatuo> ().Shoot ();
	}
	public void ShootB(){
		P.GetComponent<FuegoFatuo> ().ShootB ();
	}
	public void SpawnKids(){
		P.GetComponent<FuegoFatuo> ().SpawnKids ();
	}
	public void Turn(){
		/*float Tx = gameObject.transform.localScale.x;
		float Ty = gameObject.transform.localScale.y;
		transform.localScale = new Vector3(-Tx, Ty, 1);*/
	}
	public void ChargingTurn(){
		P.GetComponent<FuegoFatuo> ().ChargingTurn ();
	}
	public void ShadowSneak(){
		this.transform.position = new Vector2 (Randy.transform.position.x, Randy.transform.position.y + 3);
	}
	public void EndAttack(){
		P.GetComponent<FuegoFatuo> ().EndAttack ();
	}
	public void Return(){
		this.transform.position = StartPos;
	}
	public void MedusaActivate(bool l){
		P.GetComponent<FuegoFatuo> ().MedusaMode (l);
	}
	public void StartVomit(){
		P.GetComponent<FuegoFatuo> ().Phase2AttackB ();
	}
	public void StopVomit(){
		P.GetComponent<FuegoFatuo> ().stopvomit ();
	}
	public void Waiting(){
		P.GetComponent<FuegoFatuo> ().wait ();
	}
	public void Spawned(){
		P.GetComponent<FuegoFatuo> ().Spawn ();
	}
	public void Spawned2(){
		P.GetComponent<FuegoFatuo> ().Enable ();
	}
	void OnTriggerEnter(Collider other)	{
		if (!invulnerable) {
			if ((other.tag == "Fireball" || other.tag == "Shield") && gameObject.GetComponent<SphereCollider>().enabled) {
				P.GetComponent<FuegoFatuo> ().GetHit (1);
				invulnerable = true;
			}
			if (other.tag == "Big_Fireball" && gameObject.GetComponent<SphereCollider>().enabled) {
				P.GetComponent<FuegoFatuo> ().GetHit (5);
				invulnerable = true;
			}
		}
	}
	public void Killed(){
		P.GetComponent<FuegoFatuo> ().Disable ();
	}
	public void Split(Vector2 dir){
		Vector2 VV = new Vector2 ((gameObject.transform.position.x - dir.x), (gameObject.transform.position.y - dir.y)).normalized* 10;
		gameObject.GetComponent<Rigidbody> ().velocity = VV;
		gameObject.GetComponent<Rigidbody> ().useGravity = true;
			}
	public void ReadyPH2(int a, bool s){
		P.GetComponent<FuegoFatuo> ().ReadyPH2 (a, s);
	}
	public void SetStartPos(){
		StartPos = gameObject.transform.position;
	}
	public void PlaySound(int s){
		gameObject.GetComponent<AudioSource>().PlayOneShot(Sounds[s], (gameObject.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
	}
	public void StopSound(){
		gameObject.GetComponent<AudioSource> ().Stop ();
	}
	public void IsWaiting(){
		P.GetComponent<FuegoFatuo> ().BeWaiting ();
	}
	public void StopAttack(){
		P.GetComponent<FuegoFatuo> ().bAttacking = false;
	}
	public void StartCharge(){
		P.GetComponent<FuegoFatuo> ().StartCharge ();
	}
	public void ResetTrigger(){
		gameObject.GetComponent<Animator> ().ResetTrigger ("Wait");
	}
}
