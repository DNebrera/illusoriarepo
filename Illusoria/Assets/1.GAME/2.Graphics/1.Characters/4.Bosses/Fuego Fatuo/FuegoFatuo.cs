﻿using UnityEngine;
using System.Collections;

public class FuegoFatuo : MonoBehaviour {
	//Phases
	public int Phase;
	public int Size;
	private Animator Anim;
	public int HealthPoints;
	public int Dif;
	public float movspeed;
	public bool bSpawning;
	public bool bSpawned;
	public bool bIdle;
	public bool bMoving;
	public bool bAttacking;
	public bool bVomit;
	public bool bVomLeft;
	public bool bVomSide;
	public bool bCharge;
	public bool bLeftCharge;
	public bool bReturn;
	public bool bWaiting;
	public bool bdisabled;
	public bool bCinematic;
	public GameObject Gestor;
	public GameObject Top;
	public GameObject Bot;
	public GameObject Left;
	public GameObject Right;
	public GameObject Left2;
	public GameObject Right2;
	public GameObject Body;
	public GameObject HurtPart;
	public Color StartColor;
	public GameObject KillCol;
	public Vector2 Dest;
	public int NextFireball;
	public GameObject Randy;
	public GameObject[] Fireballs;
	public GameObject LeftShoot;
	public GameObject RightShoot;
	public float Angle;
	//IdleStance
	public float IdleTime;
	//Phase 2
	public bool PH2bLeft;
	public float MedusaTop;
	public float MedusaBot;
	public int bMedusaUp;
	public int MedusaTimes;
	//Split
	public GameObject[] Hijos;
	//Cinematic
	public GameObject StartPos;
	// Use this for initialization
	void Start () {
		NextFireball = 0;
		Randy = GameObject.Find ("Randy");
		Anim = Body.GetComponent<Animator> ();
		StartColor = HurtPart.GetComponent<SpriteRenderer> ().color;
		Dif = PlayerPrefs.GetInt("Dificultad");
	}	
	// Update is called once per frame
	void Update () {
		if (bCinematic && bIdle) {
			if (Body.transform.position.x > Dest.x) {
				Body.transform.Translate(new Vector3 (movspeed*Time.timeScale*Time.deltaTime*(Dest.x-Body.transform.position.x),0,0));
			}
			if (Body.transform.position.x < Dest.x) {
				Body.transform.Translate(new Vector3 (movspeed*Time.timeScale*Time.deltaTime*(Dest.x-Body.transform.position.x),0,0));
			}
			if (Body.transform.position.y < Dest.y) {
				Body.transform.Translate(new Vector3 (0,movspeed*Time.timeScale*Time.deltaTime*(Dest.y-Body.transform.position.y),0));
			}
			if (Body.transform.position.y > Dest.y) {
				Body.transform.Translate(new Vector3(0,movspeed*Time.timeScale*Time.deltaTime*(Dest.y-Body.transform.position.y),0));
			}
			if (new Vector2 ((Dest.x - Body.transform.position.x), (Dest.y - Body.transform.position.y)).magnitude <= 2) {
				bMoving = false;
			}
		}
		if (!bCinematic && Body.activeInHierarchy) {
			if (bdisabled) {
				Anim.SetBool ("bReforming", true);
				Phase = 0;
			}
			if (NextFireball >= Fireballs.Length) {
				NextFireball = 0;
			}
			Angle = Randy.transform.position.x - Body.transform.position.x;
			Vector2 Start = LeftShoot.transform.up;
			Anim.SetFloat("Side",(Randy.transform.position.x - Body.transform.position.x));
			//Idle
			if (bIdle && !bAttacking && Phase == 1) {
				if (IdleTime <= 10) {
					IdleTime += Time.deltaTime;
				}
				if (bMoving && !bAttacking){
					if (Body.transform.position.x > Dest.x) {
						Body.transform.Translate(new Vector3 (movspeed*Time.timeScale*Time.deltaTime*(Dest.x-Body.transform.position.x),0,0));
					}
					if (Body.transform.position.x < Dest.x) {
						Body.transform.Translate(new Vector3 (movspeed*Time.timeScale*Time.deltaTime*(Dest.x-Body.transform.position.x),0,0));
					}
					if (Body.transform.position.y < Dest.y) {
						Body.transform.Translate(new Vector3 (0,movspeed*Time.timeScale*Time.deltaTime*(Dest.y-Body.transform.position.y),0));
					}
					if (Body.transform.position.y > Dest.y) {
						Body.transform.Translate(new Vector3(0,movspeed*Time.timeScale*Time.deltaTime*(Dest.y-Body.transform.position.y),0));
					}
					if (new Vector2 ((Dest.x - Body.transform.position.x), (Dest.y - Body.transform.position.y)).magnitude <= 2) {
						bMoving = false;
						Invoke ("SelectDest", 1f);
					}
				}
			}
			if (Phase == 2) {
				/*if (!bAttacking) {
				int R = Random.Range (1, 3);
				if (R == 1) {
					Phase2AttackB ();
				}
				if (R == 2) {
					Phase2AttackB ();
				}
			}*/
				if (bCharge && !bReturn) {
					if (!bLeftCharge) {
						Body.transform.Translate(new Vector3 (movspeed*Time.timeScale*Time.deltaTime*20,0,0));
					}
					if (bLeftCharge) {
						Body.transform.Translate(new Vector3 (-movspeed*Time.timeScale*Time.deltaTime*20,0,0));
					}
					if (bMedusaUp == 1) {
						Body.transform.Translate(new Vector3 (0,movspeed*Time.timeScale*Time.deltaTime*10,0));
						if (Body.transform.position.y > MedusaTop) {
							bMedusaUp = 2;
						}
					}
					if (bMedusaUp == 2) {
						Body.transform.Translate(new Vector3 (0,-movspeed*Time.timeScale*Time.deltaTime*10,0));
						if (Body.transform.position.y < MedusaBot) {
							bMedusaUp = 1;
						}
					}
					if (Body.transform.position.x >= 475 || Body.transform.position.x <= 425) {
						Debug.Log ("went too far!");
						Body.GetComponent<SombraBody> ().Return ();
						bMedusaUp = 0;
						MedusaTimes = 0;
						bCharge = false;
						Anim.SetBool ("bCharging", false);
						Phase = 1;
						Gestor.GetComponent<GestorFFatuo> ().StartPhase1 ();
						Anim.SetInteger ("Phase", 1);
						bAttacking = false;
						ResetScale ();
						Invoke ("SelectDest", 1);
					}
				}
				if (bVomit && !bReturn) {
					if (bVomLeft) {
						Body.transform.Translate(new Vector3 (movspeed*Time.timeScale*Time.deltaTime*20,0,0));
					}
					if (!bVomLeft) {
						Body.transform.Translate(new Vector3 (-movspeed*Time.timeScale*Time.deltaTime*20,0,0));
					}
					if (Body.transform.position.x >= Right2.transform.position.x) {
						//Debug.Log ("Go Right");
						bVomLeft = false;
					}
					if (Body.transform.position.x <= Left2.transform.position.x) {
						//Debug.Log ("Go Left");
						bVomLeft = true;
					}
				}
			}
		}

	}
	public void ChargingTurn(){
		bool b = bLeftCharge;
		if (b) {
			bLeftCharge = false;
		}
		if (!b) {
			bLeftCharge = true;
		}
	}
	public void WantToAct(){
		Invoke ("WantToAct", 0.25f);
		int R = Random.Range (20, 101);
		if (R <= IdleTime * 10) {
			Debug.Log ("" + R);
			if (Dif <= 2) {
				IdleTime = 0;
			}
			if (Dif == 3) {
				IdleTime = 1.5f;
			}
			int R2 = Random.Range (1, 11);
			if (R2 >= 1 && R2 < 5 && !bWaiting) {
				Debug.Log ("Do idle Action 1");
				Anim.SetInteger ("NextAttack", 1);
				Body.GetComponent<SombraBody> ().PlaySound (2);
				Anim.SetTrigger("Attack");
				bAttacking = true;
			}
			if (R2 >= 5 && R2 < 9 && !bWaiting) {
				Debug.Log ("Do idle Action 2");
				Anim.SetInteger ("NextAttack", 2);
				Anim.SetTrigger("Attack");
				bAttacking = true;
			}
			if (R2 >= 9 && R2 <= 10 && !bWaiting) {
				Evade ();
			}
		}
	}
	public void SelectDest(){
		float X = Random.Range (Left.transform.position.x, Right.transform.position.x);
		float Y = Random.Range (Bot.transform.position.y, Top.transform.position.y);
		Dest = new Vector2 (X, Y);
		bMoving = true;
		//Debug.Log ("New destination: " + X + ", " + Y);
	}
	public void Shoot(){
		if ((Randy.transform.position.x - Body.transform.position.x) < 0) {
			float A;
			//Debug.Log ("Shoot A");
			Vector2 Start = LeftShoot.transform.up;
			Vector2 Objective = Randy.transform.position - LeftShoot.transform.position;
			A = Vector2.Angle (Start, Objective);
			Fireballs [NextFireball].GetComponent<BolaDeFuego> ().Shoot (LeftShoot.transform.position,-A,false);
			NextFireball++;
		}
		if ((Randy.transform.position.x - Body.transform.position.x) >= 0) {
			float A;
			//Debug.Log ("Shoot A");
			Vector2 Start = LeftShoot.transform.up;
			Vector2 Objective = Randy.transform.position - LeftShoot.transform.position;
			A = Vector2.Angle (Start, Objective);
			Fireballs [NextFireball].GetComponent<BolaDeFuego> ().Shoot (LeftShoot.transform.position,-A,true);
			NextFireball++;
		}
	}
	public void ShootB(){
		float A = 180;
		//Debug.Log ("Shoot B");
		Fireballs [NextFireball].GetComponent<BolaDeFuego> ().ShootB (RightShoot.transform.position,-A);
		NextFireball++;
	}
	public void EndAttack(){
		bAttacking = false;
		bIdle = true;
		ResetScale ();
	}
	public void Phase2AttackA(){
		//Debug.Log("Phase 2 Attack A");
		bAttacking = true;
		if (Size == 2) {
			int s = Random.Range (1, 3);
			if (s == 1) {
				ChargeAttack (true);
			}
			if (s == 2) {
				ChargeAttack (false);
			}
		}
		if (Size < 2) {
			if (PH2bLeft) {
				ChargeAttack (true);
			}
			if (!PH2bLeft) {
				ChargeAttack (false);
			}
		}
	}
	public void Phase2AttackB(){
		//Debug.Log("Phase 2 Attack B");
		bVomit = true;
		bAttacking = true;
	}
	public void Evade(){
		Anim.SetTrigger ("Evade");
	}
	public void ChargeAttack(bool bLeft){
		Anim.SetBool ("bCharging", true);
		if (bLeft) {
			bLeftCharge = true;
			bCharge = true;
		}
		if (!bLeft) {
			bLeftCharge = false;
			bCharge = true;
		}
	}
	public void UTurn(Vector2 Dest){
		bool t = PH2bLeft;
		if (t) {
			PH2bLeft = false;
			Body.GetComponent<SombraBody> ().Turn ();
			Body.transform.position = Dest;
		}
		if (!t) {
			PH2bLeft = true;
			Body.transform.position = Dest;
		}
	}
	public void MedusaMode(bool bleft){
		MedusaTimes++;
		MedusaTop = Body.transform.position.y + 2;
		MedusaBot = Body.transform.position.y - 2;
		if (bleft) {
			bMedusaUp = 1;
		}
		if (!bleft) {
			bMedusaUp = 2;
		}
		if (MedusaTimes >= 3 && Size == 2) {
			Body.GetComponent<SombraBody> ().Return ();
			bMedusaUp = 0;
			MedusaTimes = 0;
			bCharge = false;
			Anim.SetBool ("bCharging", false);
			Phase = 1;
			Gestor.GetComponent<GestorFFatuo> ().StartPhase1 ();
			Anim.SetInteger ("Phase", 1);
			bAttacking = false;
			ResetScale ();
			Invoke ("SelectDest", 1);
		}
		if (MedusaTimes >= 2 && Size <= 1) {
			Body.GetComponent<SombraBody> ().Return ();
			bMedusaUp = 0;
			MedusaTimes = 0;
			bCharge = false;
			Anim.SetBool ("bCharging", false);
			Phase = 1;
			Gestor.GetComponent<GestorFFatuo> ().StartPhase1 ();
			Anim.SetInteger ("Phase", 1);
			bAttacking = false;
			ResetScale ();
			Invoke ("SelectDest", 1);
		}
	}
	public void ResetScale(){
		if (Size == 2) {
			//Body.transform.localScale = new Vector3(1.5f, 1.5f, 1);
		}
		if (Size == 1) {
			//Body.transform.localScale = new Vector3(1.125f, 1.125f, 1);
		}
		if (Size == 0) {
			//Body.transform.localScale = new Vector3(0.75f, 0.75f, 1);
		}
	}
	public void stopvomit(){
		bVomit = false;
		Phase = 1;
		Gestor.GetComponent<GestorFFatuo> ().StartPhase1 ();
		Anim.SetInteger ("Phase", 1);
		bAttacking = false;
		ResetScale ();
		Invoke ("SelectDest", 1);
	}
	public void wait(){
		Debug.Log ("Waiting");
		bWaiting = true;

		Anim.SetBool ("bReturn",false);
	}
	public void Stopwait(){
		bWaiting = false;
		bReturn = false;
		//Debug.Log ("Stopped Waiting");
		Body.GetComponent<SombraBody>().ResetTrigger();
		Anim.SetTrigger ("AttackB");
	}
	public void Spawn(){
		Phase = 1;
		Anim.SetInteger ("Size", Size + 1);
		Anim.SetInteger ("Phase", 1);
		SetLife ();
		WantToAct ();
		IdleTime = 0;
		Body.GetComponent<Rigidbody>().velocity = new Vector3 (0, 0, 0);
		Body.GetComponent<Rigidbody>().angularVelocity = new Vector3 (0, 0, 0);
		Body.GetComponent<Rigidbody> ().useGravity = false;
		Anim.SetTrigger ("bReturn2");
		Invoke ("SelectDest", 2);
		Gestor.GetComponent<GestorFFatuo> ().ChangeActive (1);
	}
	public void ReadyPH2(int a, bool s){
		Anim.SetTrigger ("Wait");
		Phase = 2;
		bVomit = false;
		bCharge = false;
		if (a == 1) {
			Anim.SetInteger ("NextAttack", 2);
			if (s) {
				bVomLeft = true;
				bVomSide = true;
			}
			if (!s) {
				bVomLeft = false;
				bVomSide = true;
			}
		}
		if (a == 2) {
			Anim.SetInteger ("NextAttack", 1);
		}

	}
	public void Reform(){
		//Debug.Log ("Reforming");
		Anim.SetBool ("bReforming",false);
		bdisabled = false;
		bIdle = true;
		Phase = 1;
	}
	public void StartCharge(){
		if (Size == 2) {
			int g = Random.Range (1, 3);
			if (g == 1) {
				ChargeAttack (true);
			}
			if (g == 2) {
				ChargeAttack (false);
			}
		}
		if (Size < 2) {
			if (PH2bLeft) {
				ChargeAttack (true);
			}
			if (!PH2bLeft) {
				ChargeAttack (false);
			}
		}
	}
	public void SetLife(){
		if (Dif == 2) {
			if (Size == 2) {
				HealthPoints = 4;
			}
			if (Size == 1) {
				HealthPoints = 3;
			}
			if (Size == 0) {
				HealthPoints = 2;
			}
		}
		if (Dif == 1) {
			if (Size == 2) {
				HealthPoints = 3;
			}
			if (Size == 1) {
				HealthPoints = 2;
			}
			if (Size == 0) {
				HealthPoints = 1;
			}
		}
		if (Dif == 3) {
			if (Size == 2) {
				HealthPoints = 8;
			}
			if (Size == 1) {
				HealthPoints = 6;
			}
			if (Size == 0) {
				HealthPoints = 4;
			}
		}
		bSpawned = true;
	}
	public void GetHit(int dmg){
		HealthPoints -= dmg;
		HurtPart.SetActive (true);
		Invoke ("ResetColor", 0.1f);
		if (HealthPoints <= 0 && bSpawned) {
			Anim.SetTrigger ("Die");
			Gestor.GetComponent<GestorFFatuo> ().GotKilled ();
			KillCol.SetActive (false);
			bSpawned = false;
		}
	}
	public void ResetColor(){
		HurtPart.SetActive (false);
	}
	public void SpawnKids(){
		if (Size == 2 || Size == 1) {
			for (int h = 0; h < 2; h++) {
				Gestor.GetComponent<GestorFFatuo> ().StartPhase1 ();
				Hijos [h].transform.position = Body.transform.position;
				Hijos [h].GetComponent<SombraBody> ().Spawned2 ();
				Hijos [0].GetComponent<SombraBody> ().Split (LeftShoot.transform.position);
				Hijos [1].GetComponent<SombraBody> ().Split (RightShoot.transform.position);
			}
		}
	}
	public void CinematicMove(){
		Invoke ("CinematicMove2", 2);
	}
	public void CinematicMove2(){
		if (Size == 0) {
			//Debug.Log ("Start Boss CinematicB");
			bCinematic = true;
			bIdle = false;
			Body.SetActive (true);
			Anim.SetTrigger ("bCinematic");
			Body.transform.position = StartPos.transform.position;
			bAttacking = false;
			Dest = new Vector2 (450, 30);
			Invoke ("setIdle", 2); 
		}
	}
	public void setIdle(){
		bIdle = true;
	}
	public void Disable(){
		Phase = 0;
		bIdle = false;
		Body.GetComponent<SphereCollider> ().enabled = false;
		Body.GetComponent<SombraBody> ().Return ();
		Body.SetActive (false);
		bCinematic = false;

	}
	public void Enable(){
		Body.SetActive (true);
		ActGrav ();
		Invoke ("Reform", 1f);
	}
	public void ActGrav(){
		Body.GetComponent<Rigidbody> ().useGravity = true;
		Invoke ("DesactGrav", 0.5f);
	}
	public void DesactGrav(){
		Body.GetComponent<Rigidbody> ().useGravity = false;
	}
	public void BeWaiting(){
		bWaiting = true;
	}
}
