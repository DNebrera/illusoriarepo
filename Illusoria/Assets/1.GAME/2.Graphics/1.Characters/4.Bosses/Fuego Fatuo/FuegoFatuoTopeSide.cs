﻿using UnityEngine;
using System.Collections;

public class FuegoFatuoTopeSide : MonoBehaviour {
	public bool bSide;
	private float CoolTime;
	public bool bEntered;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!bEntered) {
			CoolTime += Time.deltaTime;
			if (CoolTime >= 0.2f) {
				bEntered = true;
				CoolTime = 0;
			}
		}
	}
	void OnTriggerEnter(Collider other)	{
		if (other.tag == "Enemy" && bEntered) {
			bEntered = false;
			Debug.Log ("Enemy entered");
			other.transform.position = this.transform.position;
			other.GetComponent<SombraBody> ().ChargingTurn ();
			other.GetComponent<SombraBody> ().MedusaActivate (bSide);
		}
	}

}
