﻿using UnityEngine;
using System.Collections;

public class GestorFFatuo : MonoBehaviour {
	public bool bActivated;
	public int Phase;
	public GameObject[] FuegosFatuos;
	public GameObject FuegoFatuo3;
	public GameObject mainCam;
	public GameObject Puerta;
	public GameObject DynCam;
	public float Phase2Time;
	public float Phase2TimeMax;
	public int Phase2NextAttack;
	public int ActiveFFs;
	public bool bLeft;
	public bool bwaiting;
	public int FFReady;
	public int FFWaiting;
	public float mag;
	public int Killed;
	// Use this for initialization
	void Start () {
		Killed = 0;
		mainCam = GameObject.Find ("Main Camera");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.T)) {
			
		}
		mag = new Vector2 ((450 - FuegoFatuo3.transform.position.x), (30 - FuegoFatuo3.transform.position.y)).magnitude;
		//Debug.Log ("" + new Vector2 ((450 - FuegoFatuo3.transform.position.x), (30 - FuegoFatuo3.transform.position.y)).magnitude);
		if (Phase == 0 && new Vector2 ((450 - FuegoFatuo3.transform.position.x), (30 - FuegoFatuo3.transform.position.y)).magnitude <= 1.25f) {
				Phase = 1;
			bActivated = true;
			mainCam.GetComponent<PadDetect> ().Flash ();
				for (int s = 3; s < FuegosFatuos.Length; s++) {
					FuegosFatuos [s].GetComponent<FuegoFatuo> ().Disable ();
				}
				FuegosFatuos [0].GetComponent<FuegoFatuo> ().Enable ();
		}
		if (Phase == 1 && bActivated) {
			Phase2Time += Time.deltaTime;
			if (Phase2Time >= Phase2TimeMax) {
				Phase = 2;
				Phase2Time = 0;
				Debug.Log ("StartPhase2");
				StartPhase2 ();
			}
		}
		if (Phase == 2 && bwaiting) {
			for (int s = 0; s < FuegosFatuos.Length; s++) {
				if (FuegosFatuos [s].GetComponent<FuegoFatuo> ().Body.activeSelf == true) {
					FFReady++;
				}
				if (FuegosFatuos [s].GetComponent<FuegoFatuo> ().bWaiting == true) {
					FFWaiting++;
				}
			}
			if (FFReady != FFWaiting){
				Debug.Log("Not ready: " + FFReady + ", " + FFWaiting);
				InsistPhase2 ();
			}
			if (FFReady == FFWaiting){
				FFReady = 0;
				FFWaiting = 0;
				Invoke ("StopWaiting", 1);
				bwaiting = false;
			}
			FFReady = 0;
			FFWaiting = 0;
		}
		if (Phase == 2) {
			if (ActiveFFs <= 0) {
				Phase = 1;
				bwaiting = false;
				Phase2Time = 0;
			}
		}
	}
	public void StartPhase2(){
		bwaiting = true;
		Phase = 2;
		int r = Random.Range (1, 3);
		int p = Random.Range (1, 3);
		Phase2NextAttack = p;
		if (r == 1) {
			bLeft = true;
		}
		if (r == 2) {
			bLeft = false;
		}
		Debug.Log ("Phase 2 Attack " + r + " " + p);
		for (int s = 0; s < FuegosFatuos.Length; s++) {
			if (FuegosFatuos [s].GetComponent<FuegoFatuo> ().Body.activeSelf == true) {
				
				FuegosFatuos [s].GetComponent<FuegoFatuo> ().ReadyPH2 (p, bLeft);
			}
		}
	}
	public void InsistPhase2(){
		Debug.Log ("Insist Phase 2 Fuego Fatuo: ");
		for (int s = 0; s < FuegosFatuos.Length; s++) {
			if (FuegosFatuos [s].GetComponent<FuegoFatuo> ().Body.activeSelf == true) {
				//Debug.Log ("Phase 2 Fuego Fatuo: " + s);
				FuegosFatuos [s].GetComponent<FuegoFatuo> ().ReadyPH2 (Phase2NextAttack, bLeft);
			}
		}
	}
	public void TestStartPhase2(int r, int p){
		bwaiting = true;
		if (r == 1) {
			bLeft = true;
		}
		if (r == 2) {
			bLeft = false;
		}
		//Debug.Log ("Phase 2 Attack " + r + " " + p);
		for (int s = 0; s < FuegosFatuos.Length; s++) {
			if (FuegosFatuos [s].GetComponent<FuegoFatuo> ().Body.activeSelf == true) {
				//Debug.Log ("Phase 2 Fuego Fatuo: " + s);
				FuegosFatuos [s].GetComponent<FuegoFatuo> ().ReadyPH2 (p, bLeft);
			}
		}
	}
	public void StartPhase1(){
		if (Phase == 2) {
			Phase = 1;
			//Debug.Log ("StartPhase1");
		}
	}
	public void StopWaiting(){
		for (int s = 0; s < FuegosFatuos.Length; s++) {
			if (FuegosFatuos [s].GetComponent<FuegoFatuo> ().Body.activeSelf == true) {
				FuegosFatuos [s].GetComponent<FuegoFatuo> ().Stopwait ();
			}
		}
	}
	void OnTriggerEnter(Collider other)	{
		/*if (other.tag == "Randy" && bActivated) {			
			mainCam.GetComponent<PadDetect>().Dial(243, 249);
			mainCam.GetComponent<PadDetect> ().ToggleCinematic (4);
			//Debug.Log ("Start Boss Cinematic");
			//Cinematic ();

		}*/
	}
	public void Cinematic(){
		Puerta.GetComponent<Puertas> ().Toggle (false);
		mainCam.GetComponent<PadDetect> ().MoveCamToRandy (0.03f);
		Invoke ("ActivateDynCam", 1.5f);
		bActivated = false;
		Phase = 0;
		Invoke ("PlaybossMusic", 9);
		for (int s = 0; s < FuegosFatuos.Length; s++) {
			FuegosFatuos [s].GetComponent<FuegoFatuo> ().Body.GetComponent<SombraBody> ().SetStartPos ();
			if (s >= 3) {
				FuegosFatuos [s].GetComponent<FuegoFatuo> ().CinematicMove ();
			}

		}
	}
	public void PlaybossMusic(){
		mainCam.GetComponent<PadDetect> ().PlayMusic (2);
	}
	public void ActivateDynCam(){
		DynCam.SetActive (true);
		mainCam.GetComponent<PadDetect> ().StopMoveCam ();
	}
	public void PlayVictoryMusic(){
		mainCam.GetComponent<PadDetect> ().DisableCont (true);
		mainCam.GetComponent<PadDetect> ().PlayMusic (3);
	}
	public void NextLevel(){
		GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().NextLevel (10);
	}
	public void GotKilled(){
		Killed++;
		ChangeActive (-1);
		if (Killed == 7) {
			mainCam.GetComponent<PadDetect> ().FadeOutSong ();
			Invoke ("PlayVictoryMusic", 2);
			Invoke ("NextLevel", 8);
		}
	}
	public void ForceWin(){
		mainCam.GetComponent<PadDetect> ().FadeOutSong ();
		Invoke ("PlayVictoryMusic", 2);
		Invoke ("NextLevel", 8);
	}
	public void ChangeActive(int f){
		ActiveFFs += f;
	}
}
