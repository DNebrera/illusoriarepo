﻿using UnityEngine;
using System.Collections;

public class Amelia3Part : MonoBehaviour {
	public int HP;
	public int Dif;
	public bool Ribs;
	public bool Head;
	public bool Heart;
	public bool Eye;
	public bool bActive;
	public bool bDead;
	public bool bCorazonVulnerable;
	private float InvulTime;
	private float HealTime;
	public float DisableTime;
	public bool Inv;
	public GameObject[] Ribz;
	public GameObject Torso;
	public GameObject Corazon;
	public GameObject Cabeza;
	public GameObject Pupila;
	public GameObject Controller;
	public GameObject ChungoRandy;
	public AudioClip[] Audio;
	public Color StartCol;
	// Use this for initialization
	void Start () {
		if (Ribs) {
			Controller.GetComponent<Amelia3> ().SetHP (HP);
			CheckRibs ();
			bDead = false;
		}
		if (Eye) {
			StartCol = gameObject.GetComponent<SpriteRenderer> ().color;
		}
		if (Heart) {
			//CorazonVulnerable ();
		}
		Dif = PlayerPrefs.GetInt("Dificultad");
	}
	
	// Update is called once per frame
	void Update () {
		if (!bActive && Controller.GetComponent<Amelia3> ().bActive && Head) {
			DisableTime += Time.deltaTime * Time.timeScale;
			if (DisableTime >= 30 && Dif == 1) {
				gameObject.GetComponent<Animator> ().SetTrigger ("Awaken");
				gameObject.GetComponent<Animator> ().SetBool ("bActive", true);
				Controller.GetComponent<Amelia3> ().WakeUpHead ();
				bActive = true;
				DisableTime = 0;
				HP = 15;
			}
			if (DisableTime >= 20 && Dif == 2) {
				gameObject.GetComponent<Animator> ().SetTrigger ("Awaken");
				gameObject.GetComponent<Animator> ().SetBool ("bActive", true);
				Controller.GetComponent<Amelia3> ().WakeUpHead ();
				bActive = true;
				DisableTime = 0;
				HP = 15;
			}
			if (DisableTime >= 10 && Dif == 3) {
				gameObject.GetComponent<Animator> ().SetTrigger ("Awaken");
				gameObject.GetComponent<Animator> ().SetBool ("bActive", true);
				Controller.GetComponent<Amelia3> ().WakeUpHead ();
				bActive = true;
				DisableTime = 0;
				HP = 15;
			}
		}
		//bActive = Controller.GetComponent<Amelia3> ().bActive;
		if (bActive && Controller.GetComponent<Amelia3> ().bActive == false) {
			bActive = false;
			if (Head) {
				gameObject.GetComponent<Animator> ().SetBool ("bActive",false);
			}

		}
		if (!bActive && Controller.GetComponent<Amelia3> ().bActive && Ribs) {
			CheckRibs ();
		}
		if (Inv) {
			InvulTime += Time.deltaTime * Time.timeScale;
			if (InvulTime >= 0.2f) {
				Inv = false;
				InvulTime = 0;
			}
		}
		if (Head && HP <= 0 && bActive) {
			gameObject.GetComponent<Animator> ().SetTrigger ("Hit");
			Controller.GetComponent<Amelia3> ().Knockout ();
			HealTime = 0;
			bActive = false;
		}
		if (Heart && HP <= 0) {
			//GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().BadGameEnding (14);
			//Controller.GetComponent<Amelia3> ().CheckAchievement ();
		}
		if (Head && bActive) {
			HealTime += Time.deltaTime * Time.timeScale;
			if (HealTime >= 60 && Dif == 1) {
				Controller.GetComponent<Amelia3> ().playSound (0);
				gameObject.GetComponent<Animator> ().SetTrigger ("Roar");
				HP += 5;
				HealTime = 0;
			}
			if (HealTime >= 45 && Dif == 2) {
				Controller.GetComponent<Amelia3> ().playSound (0);
				gameObject.GetComponent<Animator> ().SetTrigger ("Roar");
				HP += 5;
				HealTime = 0;
			}
			if (HealTime >= 30 && Dif == 3) {
				Controller.GetComponent<Amelia3> ().playSound (0);
				gameObject.GetComponent<Animator> ().SetTrigger ("Roar");
				HP += 5;
				HealTime = 0;
			}
		}
		if (Ribs) {
			CheckRibs ();
		}
	}
	void OnTriggerEnter(Collider other) {
		if (!Inv) {
			
			if (Ribs) {	
				if (other.tag == "Fireball" || other.tag == "Shield") {
					Inv = true;
					HP -= 1;
					Ribz [0].GetComponent<SpriteRenderer> ().color = Color.red;
					Ribz [1].GetComponent<SpriteRenderer> ().color = Color.red;
					Ribz [2].GetComponent<SpriteRenderer> ().color = Color.red;
					Invoke ("ResetColorRibs", 0.1f);
					Controller.GetComponent<Amelia3> ().GetHit (1f);
					CheckRibs ();
				}
				if (other.tag == "Big_Fireball" || other.tag == "Psychocrusher") {
					HP -= 5;
					Ribz [0].GetComponent<SpriteRenderer> ().color = Color.red;
					Ribz [1].GetComponent<SpriteRenderer> ().color = Color.red;
					Ribz [2].GetComponent<SpriteRenderer> ().color = Color.red;
					Invoke ("ResetColorRibs", 0.1f);
					Controller.GetComponent<Amelia3> ().GetHit (5f);
					Inv = true;
					CheckRibs ();
				}
			}
			if (Head) {
				if (other.tag == "Fireball" || other.tag == "Shield") {
					HP--;
					Inv = true;
					gameObject.GetComponent<SpriteRenderer> ().color = Color.red;
					Invoke ("ResetColor", 0.1f);
				}
				if (other.tag == "Big_Fireball" || other.tag == "Psychocrusher") {
					HP -= 5;
					Inv = true;
					gameObject.GetComponent<SpriteRenderer> ().color = Color.red;
					Invoke ("ResetColor", 0.1f);
				}
			}
			if (Heart) {
				if (other.tag == "Fireball" || other.tag == "Shield") {
					/*HP--;
					Inv = true;
					gameObject.GetComponent<SpriteRenderer> ().color = Color.red;
					Invoke ("ResetColor", 0.1f);*/
				}
				if (other.tag == "Psychocrusher") {
					Debug.Log ("EndGame");
					GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().BadGameEnding (14);
					Controller.GetComponent<Amelia3> ().CheckAchievement ();
				}
			}
			if (Eye) {
				if (other.tag == "Fireball" || other.tag == "Psychocrusher") {
					//Debug.Log ("Close Eye");
					gameObject.GetComponent<SpriteRenderer> ().color = Color.red;
					Invoke ("ResetColor2", 0.1f);
					Pupila.GetComponent<Amelia3Eye>().CloseEye ();
				}
			}
		}
	}
	public void CheckRibs(){
		if (Ribs && !bDead) {
			if (HP <= 0) {
				Cabeza.GetComponent<Animator> ().SetTrigger ("Die");
				Ribz [0].GetComponent<Animator> ().SetBool ("Active", false);
				Ribz [1].GetComponent<Animator> ().SetBool ("Active", false);
				Ribz [2].GetComponent<Animator> ().SetBool ("Active", false);
				bDead = true;
			}
			if (HP > 0 && HP <= 20) {
				Ribz [0].GetComponent<Animator> ().SetBool ("Active", false);
				Ribz [1].GetComponent<Animator> ().SetBool ("Active", false);
				Ribz [2].GetComponent<Animator> ().SetBool ("Active", true);
			}
			if (HP > 20 && HP <= 40) {
				Ribz [0].GetComponent<Animator> ().SetBool ("Active", false);
				Ribz [1].GetComponent<Animator> ().SetBool ("Active", true);
				Ribz [2].GetComponent<Animator> ().SetBool ("Active", true);
			}
			if (HP > 40) {
				Ribz [0].GetComponent<Animator> ().SetBool ("Active", true);
				Ribz [1].GetComponent<Animator> ().SetBool ("Active", true);
				Ribz [2].GetComponent<Animator> ().SetBool ("Active", true);
			}
		}	
	}
	public void HealRibs(){
		HP += 15;
		Controller.GetComponent<Amelia3> ().GetHit (-15);
		CheckRibs ();
	}
	public void healUp(){
		//HP += 10;
		Torso.GetComponent<Amelia3Part> ().HealRibs ();
	}
	public void CorazonVulnerable(){
		Invoke ("CorazonVulnerable2", 1);
	}
	public void CorazonVulnerable2(){
		gameObject.GetComponent<SphereCollider> ().enabled = true;
		gameObject.GetComponent<SpriteRenderer> ().color = Color.white;
		ChungoRandy.SetActive (true);
		Corazon.GetComponent<Amelia3Part>().bCorazonVulnerable = true;
		Corazon.GetComponent<Amelia3Part> ().ResetCorazonColor2 ();
	}
	public void ResetCorazonColor2(){
		Invoke ("ChangeCorazonColor", 2f);
	}
	public void ChangeCorazonColor(){
		gameObject.GetComponent<SpriteRenderer> ().color = Color.red;
		Invoke ("ResetCorazonColor", 0.2f);
	}
	public void ResetCorazonColor(){
		gameObject.GetComponent<SpriteRenderer> ().color = Color.white;
		Invoke ("ChangeCorazonColor", 2f);
	}
	public void ActivateHead(){
		gameObject.GetComponent<Animator> ().SetBool ("bActive",true);
	}
	public void KnockedOutHead(){
		
	}
	public void playSound(int sound){
		gameObject.GetComponent<AudioSource> ().PlayOneShot (Audio [sound], (PlayerPrefs.GetFloat ("EffectVolume") * gameObject.GetComponent<AudioSource> ().volume));
	}
	public void ResetColor(){
		gameObject.GetComponent<SpriteRenderer> ().color = Color.white;
	}
	public void ResetColor2(){
		gameObject.GetComponent<SpriteRenderer> ().color = StartCol;
	}
	public void ResetColorRibs(){
		Ribz [0].GetComponent<SpriteRenderer> ().color = Color.white;
		Ribz [1].GetComponent<SpriteRenderer> ().color = Color.white;
		Ribz [2].GetComponent<SpriteRenderer> ().color = Color.white;
	}
	public void EndGame(){
		Corazon.GetComponent<Amelia3Part> ().CorazonVulnerable();
		gameObject.GetComponent<SphereCollider> ().enabled = false;
		Controller.GetComponent<Amelia3> ().EndFight ();
		Ribz [0].GetComponent<Animator> ().SetBool ("Active", false);
		Ribz [1].GetComponent<Animator> ().SetBool ("Active", false);
		Ribz [2].GetComponent<Animator> ().SetBool ("Active", false);
	}
	public void ResetHit(){
		gameObject.GetComponent<Animator> ().ResetTrigger ("Hit");
		gameObject.GetComponent<Animator> ().ResetTrigger ("Awaken");
	}
}
