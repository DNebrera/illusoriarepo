﻿using UnityEngine;
using System.Collections;

public class Amelia3Claw : MonoBehaviour {
	public float MovSpeed;
	public float Top;
	public float Bot;
	public float X;
	public bool bBot;
	public bool bCheck;
	public bool bMove;
	public bool bFake;
	public GameObject AmeliaBody;
	// Use this for initialization
	void Start () {
		X = gameObject.transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
		if (bMove && bBot) {
			gameObject.transform.Translate (0,MovSpeed * Time.deltaTime * Time.timeScale, 0);
		}
		if (bMove && !bBot) {
			gameObject.transform.Translate (0,-MovSpeed * Time.deltaTime * Time.timeScale,0);
		}
		if (bBot) {
			gameObject.transform.localScale = new Vector3 (gameObject.transform.localScale.x, 1.5f, gameObject.transform.localScale.z);
		}
		if (!bBot) {
			gameObject.transform.localScale = new Vector3 (gameObject.transform.localScale.x, -1.5f, gameObject.transform.localScale.z);
		}
	}
	public void TopLaunch(){
		gameObject.transform.localPosition = new Vector2 (X, Top);
		bBot = false;
		bMove = true;
		Invoke ("CheckLimit", 2);
	}
	public void BotLaunch(){
		gameObject.transform.localPosition = new Vector2 (X, Bot);
		bBot = true;
		bMove = true;
		Invoke ("CheckLimit", 2);
	}
	public void CheckLimit(){
		bCheck = true;
	}
	public void ShClaw(int side, float delay){
		if (side == 1) {
			Invoke ("BotLaunch", delay);
		}
		if (side == 2) {
			Invoke ("TopLaunch", delay);
		}
	}
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Luz" && bCheck && bMove && !bFake) {
			//Debug.Log ("Reached top True");
			AmeliaBody.GetComponent<Amelia3>().DropHands();
			bool b = bBot;
			if (b) {
				bBot = false;
			}
			if (!b) {
				bBot = true;
			}
			bMove = false;
			bCheck = false;
		} 
		if (other.tag == "Luz" && bCheck && bMove && bFake) {
			//Debug.Log ("Reached top");
			bool b = bBot;
			if (b) {
				bBot = false;
			}
			if (!b) {
				bBot = true;
			}
			bMove = false;
			bCheck = false;
		} 
	}
}
