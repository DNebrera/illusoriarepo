﻿using UnityEngine;
using System.Collections;

public class Amelia3Eye : MonoBehaviour {
	public GameObject[] positions;
	public GameObject Randy;
	public GameObject Parpado;
	public GameObject Fuego;
	public GameObject FakeFuego;
	public GameObject Controller;
	public float RandyX;
	public float RandyY;
	public Vector2 ShootTo;
	public int Sector;
	public int Dif;
	public bool bActive;
	public bool bEnraged;
	private float ShootTime;
	private float BlinkTime;
	public int HP;
	// Use this for initialization
	void Start () {
		//OpenEye ();
		Dif = PlayerPrefs.GetInt("Dificultad");
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Y)) {
			//Shoot ();
		}
		if (bActive) {
			bEnraged = Controller.GetComponent<Amelia3> ().Enraged;
			RandyX = Randy.transform.position.x;
			RandyY = Randy.transform.position.y;
			ShootTime += Time.deltaTime * Time.timeScale;
			BlinkTime += Time.deltaTime * Time.timeScale;
			if (ShootTime >= 5 && bEnraged && Dif == 1) {
				gameObject.GetComponent<Animator> ().SetTrigger ("Shoot");
				ShootTime = 0;
			}
			if (ShootTime >= 10 && !bEnraged && Dif == 1) {
				gameObject.GetComponent<Animator> ().SetTrigger ("Shoot");
				ShootTime = 0;
			}
			if (ShootTime >= 4 && bEnraged && Dif == 2) {
				gameObject.GetComponent<Animator> ().SetTrigger ("Shoot");
				ShootTime = 0;
			}
			if (ShootTime >= 8 && !bEnraged && Dif == 2) {
				gameObject.GetComponent<Animator> ().SetTrigger ("Shoot");
				ShootTime = 0;
			}
			if (ShootTime >= 3 && bEnraged && Dif == 3) {
				gameObject.GetComponent<Animator> ().SetTrigger ("Shoot");
				ShootTime = 0;
			}
			if (ShootTime >= 6 && !bEnraged && Dif == 3) {
				gameObject.GetComponent<Animator> ().SetTrigger ("Shoot");
				ShootTime = 0;
			}
			if (BlinkTime >= 4) {
				int r = Random.Range (1, 11);
				if (r <= 6) {
					Parpado.GetComponent<Animator> ().SetTrigger ("Blink");
				}
				BlinkTime = 0;
			}
		}
	}
	public void OpenEye(){
		Parpado.GetComponent<Animator> ().SetBool ("Active", true);
		bActive = true;
		LookAt ();
	}
	public void CloseEye(){
		Parpado.GetComponent<Animator> ().SetBool ("Active", false);
		bActive = false;
		ShootTime = 0;
	}
	public void Shoot(){
		if (bActive) {
			if ((Randy.transform.position.x - gameObject.transform.position.x) < 0) {
				float A;
				//Debug.Log ("Shoot A");
				Vector2 Start = gameObject.transform.up;
				Vector2 Objective = Randy.transform.position - gameObject.transform.position;
				ShootTo = Randy.transform.position;
				A = Vector2.Angle (Start, Objective);
				FakeFuego.GetComponent<BolaDeFuego> ().Shoot (gameObject.transform.position,A,false);
				Invoke ("Shoot2", 2);
			}
			if ((Randy.transform.position.x - gameObject.transform.position.x) >= 0) {
				float A;
				//Debug.Log ("Shoot A");
				Vector2 Start = gameObject.transform.up;
				Vector2 Objective = Randy.transform.position - gameObject.transform.position;
				ShootTo = Randy.transform.position;
				A = Vector2.Angle (Start, Objective);
				FakeFuego.GetComponent<BolaDeFuego> ().Shoot (gameObject.transform.position,-A,true);
				Invoke ("Shoot2", 2);
			}
		}
	
	}
	public void Shoot2(){
		if (bActive) {
			Parpado.GetComponent<Amelia3Part> ().playSound (0);
			if ((Randy.transform.position.x - gameObject.transform.position.x) < 0) {
				float A;
				//Debug.Log ("Shoot A");
				Vector2 Start = gameObject.transform.up;
				A = Vector2.Angle (Start, ShootTo);
				Fuego.GetComponent<BolaDeFuego> ().ShootE (gameObject.transform.position,-A,false,ShootTo);
			}
			if ((Randy.transform.position.x - gameObject.transform.position.x) >= 0) {
				float A;
				//Debug.Log ("Shoot A");
				Vector2 Start = gameObject.transform.up;
				A = Vector2.Angle (Start, ShootTo);
				Fuego.GetComponent<BolaDeFuego> ().ShootE (gameObject.transform.position,-A,true, ShootTo);
			}
		}
	}
	public void LookAt(){
		int s = Sector;
		if (bActive) {
			if (RandyX < 208 && RandyY <= 125 && s != 1) {
				Sector = 1;
				gameObject.transform.position = positions [0].transform.position;
			}
			if (RandyX >= 208 && RandyX <= 214 && RandyY <= 125 && s != 2) {
				Sector = 2;
				gameObject.transform.position = positions [1].transform.position;
			}
			if (RandyX > 214 && RandyY <= 125 && s != 3) {
				Sector = 3;
				gameObject.transform.position = positions [2].transform.position;
			}
			if (RandyX < 210 && RandyY > 125 && s != 4) {
				Sector = 4;
				gameObject.transform.position = positions [3].transform.position;
			}
			if (RandyX > 210 && RandyY > 125 && s != 5) {
				Sector = 5;
				gameObject.transform.position = positions [4].transform.position;
			}
			Invoke ("LookAt", 0.25f);
		}	
	}
	void OnTriggerEnter(Collider other) {
		
	}
}
