﻿using UnityEngine;
using System.Collections;

public class Amelia3Hand : MonoBehaviour {
	public Animator anim;
	public bool Raised;
	public AudioClip[] Audio;
	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Attack(){
		if (!Raised) {
			anim.SetTrigger ("Attack");
		}
	}
	public void Raise(bool up){
		Raised = up;
		anim.SetBool ("Up", up);
	}
	public void playSound(int sound){
		gameObject.GetComponent<AudioSource> ().PlayOneShot (Audio [sound], (PlayerPrefs.GetFloat ("EffectVolume") * gameObject.GetComponent<AudioSource> ().volume));
	}
}
