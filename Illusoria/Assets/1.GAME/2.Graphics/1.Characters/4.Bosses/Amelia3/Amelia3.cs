﻿using UnityEngine;
using System.Collections;

public class Amelia3 : MonoBehaviour {
	public float HP;
	public bool bActive;
	public bool bAlive;
	public bool Enraged;
	public bool KnockedOut;
	public bool OutCold;
	public int OpenEyes;
	public int Dif;
	public float ZarpazoAttackTime;
	public float ClawsAttackTime;
	public float EyeOpenTime;
	public GameObject[] Claws;
	public GameObject[] Hands;
	public GameObject[] Eyes;
	public GameObject Heart;
	public Animator HeartAnim;
	public AudioClip[] Audio;
	// Use this for initialization
	void Start () {
		HeartAnim = Heart.GetComponent<Animator> ();
		OpenEyes = 0;
		KnockedOut = false;
		//StartFight ();
		Dif = PlayerPrefs.GetInt("Dificultad");
	}
	
	// Update is called once per frame
	void Update () {
		if (bAlive) {
			if (HP <= 60 && HP > 40f) {
				HeartAnim.SetFloat ("Beat", 0.5f);
			}
			if (HP <= 40 && HP > 20f) {
				HeartAnim.SetFloat ("Beat", 1);
			}
			if (HP <= 20 && HP > 10f) {
				HeartAnim.SetFloat ("Beat", 1.5f);
			}
			if (HP <= 10f && HP > 0) {
				HeartAnim.SetFloat ("Beat", 2);
			}
			if (HP <= 0f) {
				HeartAnim.SetFloat ("Beat", 2.5f);
			}
		}
		/*if (Input.GetKeyDown (KeyCode.Y)) {
			ShootClaws (1);
		}
		if (Input.GetKeyDown (KeyCode.U)) {
			ShootClaws (2);
		}
		if (Input.GetKeyDown (KeyCode.I)) {
			ShootClaws (3);
		}
		if (Input.GetKeyDown (KeyCode.H)) {
			ShootClaws (4);
		}
		if (Input.GetKeyDown (KeyCode.J)) {
			ShootClaws (5);
		}
		if (Input.GetKeyDown (KeyCode.K)) {
			ShootClaws (6);
		}*/
		if (bActive) {
			ClawsAttackTime += Time.deltaTime * Time.timeScale;
			if (ClawsAttackTime >= 18 && Dif == 1) {
				int r = Random.Range (1, 7);
				ClawsAttackTime = 0;
				ShootClaws (r);
			}
			if (ClawsAttackTime >= 15 && Dif == 2) {
				int r = Random.Range (1, 7);
				ClawsAttackTime = 0;
				ShootClaws (r);
			}
			if (ClawsAttackTime >= 12 && Dif == 3) {
				int r = Random.Range (1, 7);
				ClawsAttackTime = 0;
				ShootClaws (r);
			}
			if (!OutCold) {
				ZarpazoAttackTime += Time.deltaTime * Time.timeScale;
				if (ZarpazoAttackTime >= 8 && Dif == 1) {
					ZarpazoAttackTime = 0;
					Zarpazo ();
				}
				if (ZarpazoAttackTime >= 6 && Dif == 2) {
					ZarpazoAttackTime = 0;
					Zarpazo ();
				}
				if (ZarpazoAttackTime >= 5 && Dif == 3) {
					ZarpazoAttackTime = 0;
					Zarpazo ();
				}
				if (OpenEyes <= 4) {
					EyeOpenTime += Time.deltaTime * Time.timeScale;
					if (EyeOpenTime >= 15) {
						AwakenEye ();
						EyeOpenTime = 0;
					}
				}
				if (OpenEyes >= 4) {
					if (Dif >= 2) {
						Enraged = true;
					}
				}
				if (OpenEyes < 4) {
					Enraged = false;
				}
				if (HP <= 50 && HP > 37.5f) {

				}
				if (HP <= 37.5 && HP > 25f) {

				}
				if (HP <= 25 && HP > 12.5f) {

				}
				if (HP <= 12.5f && HP > 0) {

				}
				if (HP <= 0) {

					//GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (17);
					//GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().NextLevel(14);
				}
			}
		
		}
	}
	public void ShootClaws(int mode){
		if(bActive){
			if (mode == 1) {
				Hands [0].GetComponent<Amelia3Hand> ().Raise (true);
				ShootC (1, 0,2.5f);
				ShootC (1, 1,3f);
				ShootC (1, 2,3.5f);
				ShootC (1, 3,4f);
				ShootC (1, 6,0);
				ShootC (1, 7,0.5f);
				ShootC (1, 8,1);
				ShootC (1, 9,1.5f);
			}
			if (mode == 2) {
				Hands [2].GetComponent<Amelia3Hand> ().Raise (true);
				ShootC (1, 5,2.5f);
				ShootC (1, 4,3f);
				ShootC (1, 3,3.5f);
				ShootC (1, 2,4f);
				ShootC (1, 11,0);
				ShootC (1, 10,0.5f);
				ShootC (1, 9,1);
				ShootC (1, 8,1.5f);
			}
			if (mode == 3) {
				Hands [0].GetComponent<Amelia3Hand> ().Raise (true);
				Hands [2].GetComponent<Amelia3Hand> ().Raise (true);
				ShootC (1, 0,3f);
				ShootC (1, 1,3.5f);
				ShootC (1, 4,3.5f);
				ShootC (1, 5,3f);
				ShootC (1, 2,4.5f);
				ShootC (1, 3,4.5f);
				ShootC (1, 6,0);
				ShootC (1, 7,0.5f);
				ShootC (1, 10,0.5f);
				ShootC (1, 11,0);
				ShootC (1, 8,2);
				ShootC (1, 9,2);
			}
			if (mode == 4) {
				Hands [1].GetComponent<Amelia3Hand> ().Raise (true);
				ShootC (2, 0,2.5f);
				ShootC (2, 1,3f);
				ShootC (2, 2,3.5f);
				ShootC (2, 3,4f);
				ShootC (2, 6,0);
				ShootC (2, 7,0.5f);
				ShootC (2, 8,1);
				ShootC (2, 9,1.5f);
			}
			if (mode == 5) {
				Hands [3].GetComponent<Amelia3Hand> ().Raise (true);
				ShootC (2, 5,2.5f);
				ShootC (2, 4,3f);
				ShootC (2, 3,3.5f);
				ShootC (2, 2,4f);
				ShootC (2, 11,0);
				ShootC (2, 10,0.5f);
				ShootC (2, 9,1);
				ShootC (2, 8,1.5f);
			}
			if (mode == 6) {
				Hands [1].GetComponent<Amelia3Hand> ().Raise (true);
				Hands [3].GetComponent<Amelia3Hand> ().Raise (true);
				ShootC (2, 0,3f);
				ShootC (2, 5,3f);
				ShootC (2, 2,5f);
				ShootC (2, 3,5f);
				ShootC (2, 6,0);
				ShootC (2, 11,0);
				ShootC (2, 8,2);
				ShootC (2, 9,2);
			}
		}
	}
	public void ShootC(int side,int claw, float delay){
		Claws [claw].GetComponent<Amelia3Claw> ().ShClaw(side,delay);
	}
	public void AwakenEye(){
		int r = Random.Range (1, 3);

		if (r == 1) {
			Debug.Log ("Awaken Eye :" + r);
			for (int e = 0; e < Eyes.Length; e++) {
				if (!Eyes [e].GetComponent<Amelia3Eye> ().bActive) {
					Debug.Log ("Opened eye " + e);
					Eyes [e].GetComponent<Amelia3Eye> ().OpenEye ();
					WakeUp (true);
					break;
				}
			}
		}
		if (r == 2) {
			Debug.Log ("Awaken Eye :" + r);
			for (int e = 3; e >= 0; e--) {
				if (!Eyes [e].GetComponent<Amelia3Eye> ().bActive) {
					Debug.Log ("Opened eye " + e);
					Eyes [e].GetComponent<Amelia3Eye> ().OpenEye ();
					WakeUp (true);
					break;
				}
			}
		}
	}
	public void DropHands(){
		Hands [0].GetComponent<Amelia3Hand> ().Raise (false);
		Hands [1].GetComponent<Amelia3Hand> ().Raise (false);
		Hands [2].GetComponent<Amelia3Hand> ().Raise (false);
		Hands [3].GetComponent<Amelia3Hand> ().Raise (false);
	}
	public void AttackHand(int hand){
		Hands [hand].GetComponent<Amelia3Hand> ().Attack();
	}
	public void WakeUp (bool open){
		if (open) {
			OpenEyes++;
		}
		if (!open) {
			OpenEyes--;
		}
	}
	public void CloseEyes (){
		for (int e = 0; e < Eyes.Length; e++) {
			Eyes [e].GetComponent<Amelia3Eye> ().CloseEye ();
		}
	}
	public void Zarpazo(){
		int r = Random.Range (1, 5);
		if (r == 1) {
			//Debug.Log ("Zarpazo: " + r);
			AttackHand (0);
			AttackHand (2);
		}
		if (r == 2) {
			//Debug.Log ("Zarpazo: " + r);
			AttackHand (1);
			AttackHand (3);
		}
		if (r == 3) {
			//Debug.Log ("Zarpazo: " + r);
			AttackHand (0);
			AttackHand (2);
		}
		if (r == 4) {
			//Debug.Log ("Zarpazo: " + r);
			AttackHand (1);
			AttackHand (2);
		}
	}
	public void playSound(int sound){
		gameObject.GetComponent<AudioSource> ().PlayOneShot (Audio [sound], (PlayerPrefs.GetFloat ("EffectVolume") * gameObject.GetComponent<AudioSource> ().volume));
	}
	public void StartFight(){
		bActive = true;

	}
	public void Revive(){
		bAlive = true;
	}
	public void Knockout(){
		KnockedOut = true;
		OutCold = true;
		CloseEyes ();
	}
	public void WakeUpHead(){
		OutCold = false;
	}
	public void EndFight(){
		Debug.Log ("You Win, FINISH HER!");
		bActive = false;
		CloseEyes ();
		ClawsAttackTime = 0;
		ZarpazoAttackTime = 0;
	}
	public void GetHit(float Dmg){
		HP -= Dmg;
	}
	public void SetHP(float Dmg){
		HP = Dmg;
	}
	public void CheckAchievement(){
		if (!KnockedOut) {
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (17);
		}
	}
}
