﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using InControl;
using UnityEngine.SceneManagement;

public class Cinematicas : MonoBehaviour {
	//Componentes
	public GameObject Dialog;
	public GameObject Arrow;
	public GameObject Fadein;
	public GameObject Randychungo;
	//Parametros
	public int Cine;
	public int language;
	public int NextLine;
	public int EndLine;
	public AudioClip[] Songs;
	public bool bDuringLine;
	public bool bFinishedLine;
	public bool bChangeOpt;
	public bool bWaitforaction;
	public bool FadingAudioDown;
	public bool FadingAudioUp;
	public AudioClip LetterSound;
	public AudioClip RandyRisa;
	public float EffectVolume;
	public float MusicVolume;
	private float ChangeOptTime;
	//Archivos
	public string[] CinematicasTexts;
	public TextAsset CinTxT;
	public TextAsset CinEnglish;
	public TextAsset CinSpanish;
	public TextAsset CinFrench;
	// Use this for initialization
	void Start () {
		//ActivateCamMov();
		FadeIn();
		Dialog.GetComponentInChildren<Text>().CrossFadeAlpha(0f, 0f, false);
		language = PlayerPrefs.GetInt ("Lang");
		if (language == 1) {
			CinTxT = CinEnglish;
		}
		if (language == 2) {
			CinTxT = CinSpanish;
		}
		if (language == 3) {
			CinTxT = CinFrench;
		}
		if (CinTxT != null)
		{
			CinematicasTexts = (CinTxT.text.Split('\n'));
			//Debug.Log ("" + CurrentLang[1]);
		}
		//SetCinematic(8);
	}
	
	// Update is called once per frame
	void Update () {
		var inputDevice = InputManager.ActiveDevice;
		if (bChangeOpt == false) {
			ChangeOptTime += Time.deltaTime;
			if (ChangeOptTime >= 0.15f) {
				bChangeOpt = true;
				ChangeOptTime = 0;
			}
		}
		if (FadingAudioUp) {
			gameObject.GetComponent<AudioSource> ().volume += 0.02f*PlayerPrefs.GetFloat ("MusicVolume");
			if (gameObject.GetComponent<AudioSource> ().volume == 1*PlayerPrefs.GetFloat ("MusicVolume")) {
				FadingAudioUp = false;
			}
		}
		if (FadingAudioDown) {
			gameObject.GetComponent<AudioSource> ().volume -= 0.02f*PlayerPrefs.GetFloat ("MusicVolume");
			if (gameObject.GetComponent<AudioSource> ().volume == 0) {
				FadingAudioDown = false;
			}
		}
		if (Input.GetKeyDown (KeyCode.Escape) || inputDevice.MenuWasPressed) {
			if (Cine < 7) {
				Invoke ("LoadLevel", 2);
				FadeOut ();
			}
			if (Cine >= 7) {
				if (Cine == 7) {
					DestroyedIllusoria ();
				}
				if (Cine == 8) {
					SavedIllusoria ();
				}
				Invoke ("LoadCredits", 2);
				FadeOut ();
			}
		}
		if (bDuringLine) {
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("return") || inputDevice.Action1.WasPressed) && bChangeOpt){
				bChangeOpt = false;
				StopCoroutine ("TypeText");
				Dialog.GetComponentInChildren<Text>().text = CinematicasTexts[NextLine];
				bDuringLine = false;
				bFinishedLine = true;
			}
		}
		if (bFinishedLine) {
			Arrow.SetActive (true);
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("return") || inputDevice.Action1.WasPressed) && bChangeOpt) {
				SpecialEvent (NextLine);
				bChangeOpt = false;
				bFinishedLine = false;
				if (NextLine == EndLine) {
					Arrow.SetActive (false);
					EndDial ();
				}
				if (NextLine < EndLine) {
					Arrow.SetActive (false);
					NextLine++;
					StartCoroutine ("TypeText");
				}
			}
		}
	}
	public void ActivateCamMov(){
		gameObject.GetComponent<Animator> ().SetFloat ("Speed", 1);
	}
	public void DeactivateCamMov(){
		gameObject.GetComponent<Animator> ().SetFloat ("Speed", 0);
	}
	public void Dial(int StartLine, int lastline){
		NextLine = StartLine;
		EndLine = lastline;
		Dialog.SetActive (true);
		StartCoroutine ("TypeText");
	}
	public void EndDial(){
		Dialog.SetActive (false);
		StopCoroutine ("TypeText");
	}
	public void PauseDial(){
		gameObject.GetComponent<Animator> ().SetFloat ("Speed", 0);
	}
	IEnumerator TypeText () 
	{
		bDuringLine = true;
		Dialog.GetComponentInChildren<Text>().text = "";
		foreach (char letter in CinematicasTexts[NextLine].ToCharArray()) 
		{
			Dialog.GetComponentInChildren<Text>().text += letter;
			GetComponent<AudioSource>().PlayOneShot(LetterSound, (GetComponent<AudioSource>().volume*EffectVolume));
			yield return 0;
			yield return new WaitForSeconds (0.05f);
		}
		bDuringLine = false;
		bFinishedLine = true;
	}
	public void SetCinematic(int cin){
		Debug.Log ("Set Cinematic" + cin);
		Cine = cin;
		GetComponent<Animator> ().SetInteger ("NextCinematic",cin);
		GetComponent<Animator> ().SetTrigger ("Start");
		gameObject.GetComponent<AudioSource> ().volume = PlayerPrefs.GetFloat ("MusicVolume");
		if (cin == 1) {
			gameObject.GetComponent<AudioSource> ().clip = Songs [3];
			gameObject.GetComponent<AudioSource> ().loop = false;
			FadeAudio (true);
			gameObject.GetComponent<AudioSource> ().Play ();
		}
		if (cin >= 4 && cin < 7) {
			gameObject.GetComponent<AudioSource> ().clip = Songs [0];
			FadeAudio (true);
			gameObject.GetComponent<AudioSource> ().Play ();
		}
		if (cin > 1 && cin < 4) {
			gameObject.GetComponent<AudioSource> ().clip = Songs [2];
			FadeAudio (true);
			gameObject.GetComponent<AudioSource> ().Play ();
		}
		if (cin == 7) {
			gameObject.GetComponent<AudioSource> ().clip = Songs [1];
			gameObject.GetComponent<AudioSource> ().pitch = -0.5f;
			FadeAudio (true);
			gameObject.GetComponent<AudioSource> ().Play ();
		}
		if (cin == 8) {
			gameObject.GetComponent<AudioSource> ().clip = Songs [1];
			FadeAudio (true);
			gameObject.GetComponent<AudioSource> ().Play ();
		}
	}
	public void PassText(){
		if (bFinishedLine) {
			SpecialEvent (NextLine);
			bChangeOpt = false;
			bFinishedLine = false;
			if (NextLine == EndLine) {
				Arrow.SetActive (false);
				EndDial ();
			}
			if (NextLine < EndLine) {
				Arrow.SetActive (false);
				NextLine++;
				StartCoroutine ("TypeText");
			}
		}
	}
	public void StartDialog(int cin){
		if (cin == 1) {
			Dial (1, 4);
			GetComponent<Animator> ().SetFloat ("Speed",0);
		}
		if (cin == 2) {
			EndDial ();
			GetComponent<Animator> ().SetFloat ("Speed",0);
			Dial (5, 6);
		}
		if (cin == 3) {
			GetComponent<Animator> ().SetFloat ("Speed",1);
			Dial (8, 10);
		}
		if (cin == 4) {
			GetComponent<Animator> ().SetFloat ("Speed",1);
			Dial (12, 14);
		}
	}
	public void SpecialEvent(int line){
		Debug.Log ("Line: " + line);
		if (line == 2) {
			GetComponent<Animator> ().SetFloat ("Speed",1);
		}
		if (line == 5) {
			GetComponent<Animator> ().SetFloat ("Speed",1);
		}
	}
	public void ShowText(int line){
		Dialog.GetComponentInChildren<Text> ().text = CinematicasTexts [line];
		Dialog.GetComponentInChildren<Text>().CrossFadeAlpha(1f, 0.2f, false);
	}
	public void HideText(){
		Dialog.GetComponentInChildren<Text>().CrossFadeAlpha(0f, 0.2f, false);
	}
	public void FadeIn()
	{
		//Debug.Log("Fading in");
		Fadein.GetComponent<Image>().CrossFadeAlpha(0f, 0, false);
	}
	public void FadeOut()
	{
		//Debug.Log("Fading out");
		Fadein.GetComponent<Image>().CrossFadeAlpha(1f, 2.0f, false);
	}
	public void LoadLevel(){
		FadeAudio (false);
		SceneManager.LoadScene(14);
	}
	public void DelayedLoadCredits(){
		Invoke ("LoadCredits", 5);
	}
	public void LoadCredits(){
		FadeAudio (false);
		SceneManager.LoadScene(17);
	}
	public void FadeAudioOut(){
		FadeAudio (false);
	}
	public void SavedIllusoria(){
		if (PlayerPrefs.GetInt("Ach15") == 0) {
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (15);
		}
	}
	public void DestroyedIllusoria(){
		if (PlayerPrefs.GetInt("Ach15") == 0) {
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (14);
		}
	}
	public void FadeAudio(bool Up){
		if (Up) {
			FadingAudioUp = true;
		}
		if (!Up) {
			FadingAudioDown = true;
		}
	}
	public void PlayRandyLaugh(){
		Randychungo.GetComponent<AudioSource> ().volume = PlayerPrefs.GetFloat("EffectVolume");
		Randychungo.SetActive (true);
	}
}
