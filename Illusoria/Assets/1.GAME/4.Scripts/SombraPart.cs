﻿using UnityEngine;
using System.Collections;

public class SombraPart : MonoBehaviour {
	public Animator Anim;
	public int Size;
	public bool bTentaculo;
	public bool bPustula;
	// Use this for initialization
	void Awake () {
		Anim = gameObject.GetComponent<Animator> ();
		if (bTentaculo) {
			Anim.SetBool ("bTentaculo", true);
		}
		if (bPustula) {
			Anim.SetBool ("bPustulas", true);
		}
		Anim.SetInteger ("Size", Size);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
