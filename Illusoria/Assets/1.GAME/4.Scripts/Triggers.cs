﻿using UnityEngine;
using System.Collections;

public class Triggers : MonoBehaviour {
	public int TriggerN;
	public int Level;
	public GameObject MainCamera;
	public GameObject Randy;
	public bool bActive;
	public bool bRepeat;
	// Use this for initialization
	void Awake () {
	}
	void Start () {
		DontDestroyOnLoad (gameObject);
		MainCamera = GameObject.Find ("Main Camera");
		Randy = GameObject.Find ("Randy");
		if (GameObject.Find (gameObject.name) != gameObject) {
			Destroy (gameObject);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other)
	{
		if (bActive) {
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 1) {
					MainCamera.GetComponent<PadDetect> ().MoveTo (35,0);
					MainCamera.GetComponent<PadDetect>().Dial(141, 150);
					bActive = false;
				}
				if (TriggerN == 2) {
					//Debug.Log("Has ignorado a Wolfgang, te parecerá bonito");
					//MainCamera.GetComponent<PadDetect>().AchievementUnlock(1);
					GameObject.Find ("Supervisor").GetComponent<LoadingGestor>().Achievements(5);
					PlayerPrefs.SetInt ("IgnoredWolf", 1);
					bActive = false;
				}
				if (TriggerN == 3) {
					MainCamera.GetComponent<PadDetect>().Dial(296, 317);
					MainCamera.GetComponent<PadDetect> ().ToggleCinematic (6);
					bActive = false;
				}
				if (TriggerN == 4) {
					GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().GetPower ();
					bActive = false;
				}
				if (TriggerN == 5) {
					MainCamera.GetComponent<PadDetect>().Dial(243, 249);
					MainCamera.GetComponent<PadDetect> ().ToggleCinematic (4);
					GameObject.Find ("GestorFuegoFatuo").GetComponent<GestorFFatuo> ().bActivated = false;
					bActive = false;
				}
				if (TriggerN == 6) {
					Invoke ("DelayedConvoA", 2);
					bActive = false;
				}
				if (TriggerN == 7) {
					Invoke ("Turn", 2);
					Invoke ("Turn", 4);
					Invoke ("DelayedMoveToA", 4.1f);
					Invoke ("DelayedConvoB", 7);
					bActive = false;
				}
				if (TriggerN == 8) {
					GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().CinematicPoof (true);
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
					MainCamera.GetComponent<PadDetect> ().MoveTo (151, 95.9f);
					bActive = false;
				}
				if (TriggerN == 9) {
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
					Invoke ("DelayedCamMoveToA", 1);
					Invoke ("DelayedConvoC", 3);
					bActive = false;
				}
				if (TriggerN == 10) {
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
					Invoke ("DelayedConvoD", 1);
					MainCamera.GetComponent<PadDetect> ().MoveCamTo2 (-255, -34.5f,0.03f);
					bActive = false;
				}
				if (TriggerN == 11) {
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
					Invoke ("DelayedConvoE", 5);
					GameObject.Find ("Lanzador_01").GetComponent<BoxCollider> ().enabled = true;
					bActive = false;
				}
				if (TriggerN == 12) {
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
					Invoke ("DelayedConvoF", 6);
					GameObject.Find ("Lanzador_02").GetComponent<BoxCollider> ().enabled = true;
					bActive = false;
				}
				if (TriggerN == 13) {
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
					MainCamera.GetComponent<PadDetect>().Dial(57, 58);
					MainCamera.GetComponent<PadDetect> ().MoveCamTo2 (280, 64.5f,0.03f);
					bActive = false;
				}
				if (TriggerN == 14) {
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
					MainCamera.GetComponent<PadDetect> ().MoveTo (68, -3.144f);
					MainCamera.GetComponent<PadDetect> ().MoveCamTo (68, 0f);
					GameObject.Find ("MM_Intro").GetComponent<PlayMakerFSM> ().enabled = false;
					bActive = false;
				}
				if (TriggerN == 15) {
					if (PlayerPrefs.GetInt ("Ach06") == 0) {
						GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (6);
					}
					bActive = false;
				}
				if (TriggerN == 16) {
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
					GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().PoofIn ();
					MainCamera.GetComponent<PadDetect> ().Dial (475, 476);
					bActive = false;
				}
				if (TriggerN == 20) {
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
					Debug.Log ("Start encounter");
					GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().StartEnc ();
					MainCamera.GetComponent<PadDetect> ().MoveTo (41, 77.57f);
					//MainCamera.GetComponent<PadDetect> ().MoveCamTo (42, 81);
					bActive = false;
				}
				if (TriggerN == 21) {
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
					MainCamera.GetComponent<PadDetect> ().Dial (455, 458);
					bActive = false;
				}
				if (TriggerN == 22) {
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
					MainCamera.GetComponent<PadDetect> ().Dial (460, 463);
					bActive = false;
				}

			}
		}
		if (!bRepeat) {
			
		}
	}
	public void OnLevelWasLoaded(int level){
		MainCamera = GameObject.Find ("Main Camera");
		Randy = GameObject.Find ("Randy");
		CancelInvoke ();
		if (level != Level) {
			Destroy (gameObject);
		}
	}
	public void Desactivar(){
		bActive = false;
	}
	public void Activar(){
		bActive = true;
		MainCamera = GameObject.Find ("Main Camera");
		Randy = GameObject.Find ("Randy");
	}
	public void Turn(){
		MainCamera.GetComponent<PadDetect> ().Turn ();
	}
	public void DelayedMoveToA(){
		MainCamera.GetComponent<PadDetect>().MoveTo (27f, -4.204f);
	}
	public void DelayedMoveToB(){
		MainCamera.GetComponent<PadDetect>().MoveTo (19f, -4.204f);
	}
	public void DelayedCamMoveToA(){
		MainCamera.GetComponent<PadDetect> ().MoveCamTo2 (68, 3.0f,0.05f);
	}
	public void DelayedConvoA(){
		GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (0.7f, -4.142f);
		MainCamera.GetComponent<PadDetect>().Dial(289, 292);
	}
	public void DelayedConvoB(){		
		MainCamera.GetComponent<PadDetect>().Dial(293, 294);
	}
	public void DelayedConvoC(){		
		MainCamera.GetComponent<PadDetect>().Dial(39, 39);
	}
	public void DelayedConvoD(){
		Debug.Log ("Viene de " + gameObject.name);
		MainCamera.GetComponent<PadDetect>().Dial(43, 44);
	}
	public void DelayedConvoE(){		
		MainCamera.GetComponent<PadDetect>().Dial(49, 50);
	}
	public void DelayedConvoF(){		
		MainCamera.GetComponent<PadDetect>().Dial(52, 55);
	}
	public void DelayedLoadScreen(){		
		GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().CargarLoadScreen ();
	}
}
