﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Pergamino : MonoBehaviour {
	public int Num;
	public bool Obt;
	public Sprite Sello;
	public Sprite SelloReal;
	public GameObject S;
	// Use this for initialization
	void Start () {
	
	}	
	// Update is called once per frame
	void Update () {
		if (Obt) {
			S.SetActive (false);
		}
		if (!Obt) {
			S.SetActive (true);
			if (Num % 5 == 0) {
				S.GetComponent<RectTransform> ().localScale = new Vector3 (3, 3, 1);
				S.GetComponent<Image> ().sprite = SelloReal;
			}
			if (Num % 5 != 0) {
				S.GetComponent<RectTransform> ().localScale = new Vector3 (1.15f, 1.15f, 1);
				S.GetComponent<Image> ().sprite = Sello;
			}
		}
	}
	public void Set(int n, bool b){
		Num = n;
		Obt = b;
	}
}
