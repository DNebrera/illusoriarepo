﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using InControl;

public class CreditosScript : MonoBehaviour
{
    public Vector2 FinalCredits;
    public float MoveSpeed;
    public bool bMove;
    public GameObject Fadein;
    // Use this for initialization
    void Start()
    {
        FadeIn();
		gameObject.GetComponent<AudioSource> ().volume = PlayerPrefs.GetFloat ("MusicVolume") * gameObject.GetComponent<AudioSource> ().volume;
        Invoke("StartMove", 2);
    }

    // Update is called once per frame
    void Update()
    {
		var inputDevice = InputManager.ActiveDevice;
        if (bMove)
        {
            if (gameObject.transform.position.y > FinalCredits.y)
            {
                gameObject.transform.Translate(new Vector3(0, -MoveSpeed / 1.66f, 0));
            }
            if (new Vector2((FinalCredits.x - gameObject.transform.position.x), (FinalCredits.y - gameObject.transform.position.y)).magnitude < 0.25f)
            {
                FadeOut();
                Invoke("Back", 2);
            }
        }
		if ((inputDevice.Action2.WasPressed || Input.GetKeyDown ("escape"))) {
			FadeOut();
			Invoke("Back", 2);
		}
    }
    public void StartMove()
    {
        bMove = true;
    }
    public void Back()
    {
        SceneManager.LoadScene(1);
    }
    public void FadeIn()
    {
        //Debug.Log("Fading in");
        Fadein.GetComponent<Image>().CrossFadeAlpha(0f, 2.0f, false);
    }
    public void FadeOut()
    {
        //Debug.Log("Fading out");
        Fadein.GetComponent<Image>().CrossFadeAlpha(1f, 2.0f, false);
    }
}