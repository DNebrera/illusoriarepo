﻿using UnityEngine;
using System.Collections;

public class Laser : MonoBehaviour {
	public Vector3 XScale;
	public bool bReloading;
	public float ReloadTime;
	public float ReloadMax;
	public bool bActive;
	public float ActiveTime;
	public float ActiveMax;
	// Use this for initialization
	void Start () {
		XScale = gameObject.transform.localScale;
		bReloading = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (bReloading){
			ReloadTime += Time.deltaTime;
		}
		if (ReloadTime >= (ReloadMax - 1)){
			gameObject.transform.localScale = new Vector3(0.5f,XScale.y, XScale.z);
			gameObject.GetComponent<MeshRenderer>().enabled = true;
		}
		if (ReloadTime >= ReloadMax){
			bReloading = false;
			bActive = true;
			ReloadTime = 0;
			gameObject.GetComponent<Collider>().enabled = true;
			gameObject.transform.localScale = XScale;
		}
		if (bActive){
			ActiveTime += Time.deltaTime;
		}
		if (ActiveTime >= ActiveMax){
			bReloading = true;
			bActive = false;
			ActiveTime = 0;
			gameObject.GetComponent<Collider>().enabled = false;
			gameObject.GetComponent<MeshRenderer>().enabled = false;
			gameObject.GetComponent<Collider>().enabled = false;
		}
	}
}
