﻿using UnityEngine;
using System.Collections;

public class AmeliaPowerOrb : MonoBehaviour {
	public GameObject Amelia;
	public GameObject Luz;
	public Color Blue;
	public Color Purple;
	public Color Blue2;
	public Color Purple2;
	public Vector3 StartPos;
	public int TimeToErupt;
	private float EruptTime;
	public bool bActive;
	public bool ImBlue;
	public bool bErupt;
	public float scalex;
	// Use this for initialization
	void Start () {
		StartPos = gameObject.transform.position;
		//Spawn ();
	}
	
	// Update is called once per frame
	void Update () {
		if (ImBlue) {
			gameObject.GetComponent<SpriteRenderer> ().color = Blue;
			Luz.GetComponent<tk2dSprite> ().color = Blue2;
		}
		if (!ImBlue) {
			gameObject.GetComponent<SpriteRenderer> ().color = Purple;
			Luz.GetComponent<tk2dSprite> ().color = Purple2;
		}
		if (bActive) {
			
			gameObject.transform.Translate(new Vector2(0,0.6f*Time.deltaTime*Time.timeScale));
			scalex += 0.007f*Time.timeScale;
			gameObject.transform.localScale = new Vector2 (scalex,scalex);
			EruptTime += Time.deltaTime * Time.timeScale;
			if (EruptTime >= TimeToErupt) {
				EruptTime = 0;
				Invoke ("Erupt", 2);
				bActive = false;
				Invoke ("Force",1);
			}
		}
		if (bErupt) {
			if (new Vector2 ((Amelia.transform.position.x - gameObject.transform.position.x), (Amelia.transform.position.y - gameObject.transform.position.y)).magnitude > 5) {
				gameObject.GetComponent<Rigidbody> ().AddForce(new Vector2((Amelia.transform.position.x-gameObject.transform.position.x),(Amelia.transform.position.y-gameObject.transform.position.y)).normalized*0.5f);	
			}
			if (new Vector2 ((Amelia.transform.position.x - gameObject.transform.position.x), (Amelia.transform.position.y - gameObject.transform.position.y)).magnitude <= 1) {
				if (!ImBlue) {
					Absorb ();
				}
				if (ImBlue) {
					Absorb2 ();
				}
			}
			if (new Vector2 ((Amelia.transform.position.x - gameObject.transform.position.x), (Amelia.transform.position.y - gameObject.transform.position.y)).magnitude <= 5) {
				gameObject.GetComponent<Rigidbody> ().velocity = new Vector2 (0, 0);
				gameObject.transform.Translate(new Vector2((Amelia.transform.position.x-gameObject.transform.position.x),(Amelia.transform.position.y-gameObject.transform.position.y)).normalized*0.3f);
			}
		}
	}
	public void OnTriggerEnter(Collider Col){
		if (Col.tag == "Fireball" || Col.tag == "Shield" || Col.tag == "Big_Fireball" || Col.tag == "Psychocrusher") {
			Debug.Log ("PowerPellet blue!");
			TurnBlue();
		}
	}
	public void Spawn(){
		float x = Random.Range (132, 163);
		ImBlue = false;
		gameObject.transform.position = new Vector2 (x, 94);
		bActive = true;
		gameObject.transform.localScale = new Vector2 (0.05f, 0.05f);
		scalex = gameObject.transform.localScale.x;
	}
	public void Erupt(){
		//gameObject.GetComponent<Rigidbody> ().velocity = new Vector2 (0, 0);
		bErupt = true;
	}
	public void Force(){
		Debug.Log ("AddForce!");
		if (gameObject.transform.position.x >= Amelia.transform.position.x) {
			gameObject.GetComponent<Rigidbody> ().AddForce (new Vector2 (2, 1) * 7);
		}
		if (gameObject.transform.position.x < Amelia.transform.position.x) {
			gameObject.GetComponent<Rigidbody> ().AddForce (new Vector2 (-2, 1) * 7);
		}
	}
	public void Absorb(){
		bErupt = false;
		ImBlue = false;
		gameObject.transform.position = StartPos;
		Amelia.GetComponent<AmeliaBoss> ().AbsorbPower ();
		gameObject.GetComponent<Rigidbody> ().velocity = new Vector2 (0, 0);
		gameObject.transform.localScale = new Vector2 (0.05f, 0.05f);
		EruptTime = 0;
	}
	public void Absorb2(){
		bErupt = false;
		ImBlue = false;
		gameObject.transform.position = StartPos;
		Amelia.GetComponent<AmeliaBoss> ().AbsorbBadPower ();
		gameObject.GetComponent<Rigidbody> ().velocity = new Vector2 (0, 0);
		gameObject.transform.localScale = new Vector2 (0.05f, 0.05f);
		EruptTime = 0;
	}
	public void Destroy(){
		bErupt = false;
		bActive = false;
		gameObject.transform.position = StartPos;
		gameObject.GetComponent<Rigidbody> ().velocity = new Vector2 (0, 0);
		gameObject.transform.localScale = new Vector2 (0.05f, 0.05f);
		EruptTime = 0;
	}
	public void TurnBlue(){
		ImBlue = true;
	}
}
