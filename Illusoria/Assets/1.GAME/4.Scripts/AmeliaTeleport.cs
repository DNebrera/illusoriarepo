﻿using UnityEngine;
using System.Collections;

public class AmeliaTeleport : MonoBehaviour {
	public Animator Anim;
	public AudioClip Llama;
	public AudioClip Humo;
	// Use this for initialization
	void Start () {
		Anim = gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Smoke(){
		Anim.SetTrigger ("Smoke");
	}
	public void DelayedFlame(float t){
		Invoke ("Flame", t);
		//Debug.Log ("" + gameObject.name + " is exploding");
	}
	public void Flame(){
		Anim.SetTrigger ("Flame");
	}
	public void PlaySoundLlama(){
		gameObject.GetComponent<AudioSource>().PlayOneShot(Llama, (gameObject.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
	}
	public void PlaySoundSmoke(){
		gameObject.GetComponent<AudioSource>().PlayOneShot(Humo, (gameObject.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
	}
	public void DisableFlame(){
		gameObject.GetComponent<BoxCollider> ().tag = "Untagged";
	}
}
