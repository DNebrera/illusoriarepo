﻿using UnityEngine;
using System.Collections;

public class Puertas : MonoBehaviour {
	public AudioClip CerrarPuerta;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Toggle(bool o){
		gameObject.GetComponent<Animator> ().SetBool ("bOpen", o);
		gameObject.GetComponent<AudioSource> ().PlayOneShot (CerrarPuerta,(gameObject.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
	}
}
