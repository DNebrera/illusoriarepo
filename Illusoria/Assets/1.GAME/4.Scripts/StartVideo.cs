﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using InControl;
using UnityEngine.SceneManagement;

public class StartVideo : MonoBehaviour {
    public RawImage rim;
    public MovieTexture Mt;
    public GameObject Fadein;
	// Use this for initialization
	void Start () {
		Debug.Log ("" + Screen.currentResolution.height);
        Cursor.visible = false;
		PlayerPrefs.SetInt("MaxRes",3);
		QualitySettings.SetQualityLevel(2, true);
		Screen.SetResolution(1920,1080,Screen.fullScreen);
		/*if (Screen.currentResolution.height > 800) {
			QualitySettings.SetQualityLevel(2, true);
			Screen.SetResolution(1920,1080,true);
			PlayerPrefs.SetInt("MaxRes",3);
		}
		if (Screen.currentResolution.height <= 800 && Screen.currentResolution.height > 600) {
			QualitySettings.SetQualityLevel(1, true);
			Screen.SetResolution(1280,720,true);
			PlayerPrefs.SetInt("MaxRes",2);
		}
		if (Screen.currentResolution.height <= 600) {
			QualitySettings.SetQualityLevel(0, true);
			Screen.SetResolution(960,540,true);
			PlayerPrefs.SetInt("MaxRes",1);
		}*/
#if UNITY_EDITOR
        Debug.Log("Unity Editor");
        //Cursor.visible = true;
#endif
        rim = GetComponent<RawImage>();
        Mt = (MovieTexture)rim.mainTexture;
        Mt.Play();
        Invoke("LoadNext", 10f);
        Invoke("FadeOut", 8f);
    }
	// Update is called once per frame
	void Update () {
		var inputDevice = InputManager.ActiveDevice;
		if (Input.GetKeyDown (KeyCode.Escape) || inputDevice.MenuWasPressed) {
			FadeOut ();
			Invoke ("LoadNext", 2);
		}
	}
    public void LoadNext() {
        SceneManager.LoadScene(15);
    }
    public void FadeOut()
    {
        //Debug.Log("Fading out");
        Fadein.GetComponent<Image>().CrossFadeAlpha(25f, 2.0f, false);
    }
}
