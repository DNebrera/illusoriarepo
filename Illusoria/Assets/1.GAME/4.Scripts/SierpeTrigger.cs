﻿using UnityEngine;
using System.Collections;

public class SierpeTrigger : MonoBehaviour {
	public GameObject Controller;
	public GameObject DynCam;
	public GameObject StopDynCam;
	public GameObject MainCam;
	public GameObject Randy;
	public bool bActive;
	public bool bBoss;
	public int Height;
	// Use this for initialization
	void Start () {
		if (bBoss) {
			MainCam = GameObject.Find ("Main Camera");
			Randy = GameObject.Find ("Randy");
			//StopDynCam = GameObject.Find ("Trigger_SalidaBoss");
		}
	}
	
	// Update is called once per frame
	void Update () {
	}
	public void DeactivateStopDynCam(){
		StopDynCam.SetActive (false);
	}
	public void ActivateDynCam(){
		DynCam.SetActive (true);
		MainCam.GetComponent<PadDetect> ().DisableCont (false);
		Invoke ("DeactivateDynCam",0.2f);
	}
	public void DeactivateDynCam(){
		DynCam.SetActive (false);
	}
	public void MoveCamA(){
		Debug.Log ("Camera is on: " + MainCam.transform.position.x + ", " + MainCam.transform.position.y);
		MainCam.GetComponent<PadDetect> ().MoveCamTo2 (Randy.transform.position.x, 90.5f, 0.05f);
	}
	void OnTriggerEnter(Collider other) {
		if ((other.tag == "Randy" || other.tag == "Psychocrusher") && bActive && !bBoss) {
			Controller.GetComponent<SierpeNivel> ().Deactivate ();
			//Invoke ("DisSierpe", 1);
			bActive = false;
		}
		if ((other.tag == "Randy" || other.tag == "Psychocrusher") && bActive && bBoss) {
			bActive = false;
			if (Height == 1) {
				MainCam.GetComponent<PadDetect> ().DisableCont (true);
				StopDynCam.SetActive (true);
				Invoke ("DeactivateStopDynCam",0.2f);
				MainCam.GetComponent<PadDetect> ().MoveCamTo2 (Randy.transform.position.x, 90.5f, 0.05f);
				Invoke ("ActivateDynCam",1);
			}
			if (Height == 2) {
				MainCam.GetComponent<PadDetect> ().DisableCont (true);
				StopDynCam.SetActive (true);
				Invoke ("DeactivateStopDynCam",0.2f);
				MainCam.GetComponent<PadDetect> ().MoveCamTo2 (Randy.transform.position.x, 98.5f, 0.05f);
				Invoke ("ActivateDynCam",1);
			}
			if (Height == 3) {
				Controller.GetComponent<SierpedeLava> ().Victory ();
				GameObject.Find ("Elevator_Ground2").GetComponent<PlayMakerFSM> ().enabled = true;
			}
		}
	}
	public void DisSierpe(){
		Controller.GetComponent<SierpeNivel> ().Deactivate ();
	}
}
