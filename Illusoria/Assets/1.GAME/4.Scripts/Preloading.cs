﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using InControl;
using UnityEngine.UI;
using HutongGames.PlayMaker;

public class Preloading : MonoBehaviour {
    public float Progress;
	public bool bMoving;
	public bool bChangeOpt;
	public bool bChangeOpt2;
	public bool bSelected;
	public bool bLoading;
	public float ChangeOptTime;
	public float ChangeOptTime2;
	public float Vert;
	public float Hor;
    public int Language;
    public AsyncOperation Async;
    public bool bLoaded;
	public bool LevelSel;
    public GameObject PressStart;
    public GameObject Fadein;
	public GameObject Main;
	public GameObject Titulo;
	public GameObject Texto;
	public GameObject DifMarc;
	public GameObject[] Nubes;
	public GameObject[] Gold;
	public Sprite[] Easy;
	public Sprite[] Normal;
	public Sprite[] Hard;
    public int level;
	public int Checkpoint;
    public Sprite[] Levels;
	public Vector3[] LastPositionCamera;
	public Vector3[] LastPositionCheckpoint;
    public GameObject Libro;
	public Animator Anim;
	public AudioClip Vela;
    //Textos
    public GameObject text;
    public string[] Loadingtext;
    public TextAsset English;
    public TextAsset Castellano;
    public TextAsset Frances;
	public TextAsset Aleman;
    // Use this for initialization
    void Start () {
		Cursor.visible = false;
        Language = PlayerPrefs.GetInt("Lang");
		Anim = GetComponent<Animator> ();
		bChangeOpt = true;
		Titulo.GetComponent<Text>().CrossFadeAlpha(0f, 0f, false);
		text.GetComponent<Text>().CrossFadeAlpha(0, 0f, false);
		gameObject.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("MusicVolume");
		SetActive ();
        if (Language == 1)
        {
            Loadingtext = (English.text.Split('\n'));
			if (PlayerPrefs.GetInt ("Dificultad") == 1) {
				DifMarc.GetComponent<Image> ().sprite = Easy [1];
			}
			if (PlayerPrefs.GetInt ("Dificultad") == 2) {
				DifMarc.GetComponent<Image> ().sprite = Normal [1];
			}
			if (PlayerPrefs.GetInt ("Dificultad") == 3) {
				DifMarc.GetComponent<Image> ().sprite = Hard [1];
			}
        }
        if (Language == 2)
        {
            Loadingtext = (Castellano.text.Split('\n'));
			if (PlayerPrefs.GetInt ("Dificultad") == 1) {
				DifMarc.GetComponent<Image> ().sprite = Easy [2];
			}
			if (PlayerPrefs.GetInt ("Dificultad") == 2) {
				DifMarc.GetComponent<Image> ().sprite = Normal [2];
			}
			if (PlayerPrefs.GetInt ("Dificultad") == 3) {
				DifMarc.GetComponent<Image> ().sprite = Hard [2];
			}
        }
        if (Language == 3)
        {
            Loadingtext = (Frances.text.Split('\n'));
        }
		if (Language == 4)
		{
			Loadingtext = (Aleman.text.Split('\n'));
		}
		if (PlayerPrefs.GetInt ("Gold01") == 1) {
			Gold [0].SetActive (true);
		}
		if (PlayerPrefs.GetInt ("Gold02") == 1) {
			Gold [1].SetActive (true);
		}
		if (PlayerPrefs.GetInt ("Gold03") == 1) {
			Gold [2].SetActive (true);
		}
		if (PlayerPrefs.GetInt ("Gold04") == 1) {
			Gold [3].SetActive (true);
		}
		if (PlayerPrefs.GetInt ("Gold05") == 1) {
			Gold [4].SetActive (true);
		}
		if (PlayerPrefs.GetInt ("Gold06") == 1) {
			Gold [5].SetActive (true);
		}
		if (PlayerPrefs.GetInt ("Gold07") == 1) {
			Gold [6].SetActive (true);
		}
		if (PlayerPrefs.GetInt ("Gold08") == 1) {
			Gold [7].SetActive (true);
		}
		if (PlayerPrefs.GetInt ("Gold09") == 1) {
			Gold [8].SetActive (true);
		}
		if (PlayerPrefs.GetInt ("Gold10") == 1) {
			Gold [9].SetActive (true);
		}
		if (PlayerPrefs.GetInt ("Gold11") == 1) {
			Gold [10].SetActive (true);
		}
		if (PlayerPrefs.GetInt ("Gold12") == 1) {
			Gold [11].SetActive (true);
		}
		if (LevelSel) {
			Nubes[0].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
			Nubes[1].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
			Nubes[2].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
			Nubes[3].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
			Nubes[4].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
			Nubes[5].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
			Invoke("FadeIn", 0.5f);
			int s = Random.Range (1, 13);
			int r = Random.Range (0, 3);
			Debug.Log ("Consejo: " + s + r + " " + (((s * 4) + 11) + r));
			text.GetComponent<Text> ().text = Loadingtext [((s * 4) + 11) + r];
			text.GetComponent<Text>().CrossFadeAlpha(1f, 0.2f, false);
			level = 1;
			Anim.SetInteger ("Level", 1);
		}
		if (!LevelSel) {
			Debug.Log ("Loading level: " + (level-1));
			Anim.SetInteger ("Level", (level-1));
			Anim.SetBool ("bAdelante", true);
			int r = Random.Range (0, 3);
			text.GetComponent<Text> ().text = Loadingtext [(((level - 2) * 4) + 15) + r];
			text.GetComponent<Text>().CrossFadeAlpha(1f, 0.2f, false);
			Invoke("StartLoad", 1f);
			if (Checkpoint >= 4){
				Nubes[0].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
			}
			if (Checkpoint >= 8){
				Nubes[0].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[1].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
			}
			if (Checkpoint >= 16){
				Nubes[0].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[1].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[2].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
			}
			if (Checkpoint >= 24){
				Nubes[0].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[1].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[2].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[3].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
			}
			if (Checkpoint >= 32){
				Nubes[0].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[1].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[2].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[3].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[4].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
			}
			if (Checkpoint >= 40){
				Nubes[0].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[1].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[2].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[3].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[4].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
				Nubes[5].GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
			}
		}
}	
	// Update is called once per frame
	void Update () {
        var inputDevice = InputManager.ActiveDevice;
		Vert = inputDevice.Direction.Y;
		if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.UpArrow)) {
			Vert = -1;
		}
		if (Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.DownArrow)) {
			Vert = 1;
		}
		if ((Input.GetKey (KeyCode.W) && Input.GetKey (KeyCode.S)) || (Input.GetKey (KeyCode.UpArrow) && Input.GetKey (KeyCode.DownArrow))) {
			Vert = 0;
		}
		if(bChangeOpt){
			
		}
		if(!bChangeOpt){

		}
		if (LevelSel) {
			if (bChangeOpt2 == false) {
				ChangeOptTime += Time.deltaTime;
				if (ChangeOptTime >= 0.5f) {
					bChangeOpt2 = true;
					ChangeOptTime = 0;
				}
			}
			if (Vert > 0.7f && bChangeOpt && level > 1 && !bSelected) {
				//Debug.Log ("Previous Level");
				Titulo.GetComponent<Text>().CrossFadeAlpha(0f, 0.5f, false);
				level--;
				bChangeOpt = false;
				bChangeOpt2 = false;
				Invoke ("ForceMoving", 0.2f);
				Anim.SetInteger ("Level", level);
				Anim.SetTrigger ("Active");
				Anim.SetBool ("bAdelante", false);
			}
			if (Vert < -0.7f && bChangeOpt && level < 12 && !bSelected) {
				//Debug.Log ("Next Level");
				Titulo.GetComponent<Text>().CrossFadeAlpha(0f, 0.5f, false);
				level++;
				bChangeOpt = false;
				bChangeOpt2 = false;
				Invoke ("ForceMoving", 0.2f);
				Anim.SetInteger ("Level", level);
				Anim.SetTrigger ("Active");
				Anim.SetBool ("bAdelante", true);
			}
			if ((inputDevice.Action1.WasPressed || (inputDevice.Command.WasPressed && !Input.GetKeyDown("escape")) || Input.GetKeyDown("space") || Input.GetKeyDown("return")) && bChangeOpt && !bSelected)
			{
				//Debug.Log ("Load level: " + level);
				GameObject.Find ("Supervisor").GetComponent<LoadingGestor>().ResetDeathsPerLevel();
				bChangeOpt = false;
				bSelected = true;
				FadeOut();
				LoadNext ();
			}
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("escape")) && bChangeOpt)
			{
				//Debug.Log ("Back to menu");
				bChangeOpt = false;
				FadeOut();
				Invoke ("LoadMainMenu", 2);
			}
			if (bMoving) {
				if (Vert < -0.7f || Vert > 0.7f || inputDevice.Command.WasPressed || Input.GetKeyDown("space") || Input.GetKeyDown("return")) {
					Anim.SetFloat ("Speed",2);
				}
			}
			if (bLoading) {
				if (Async != null) {
					//Debug.Log("" + Async.progress);            
					Progress = Async.progress;
				}
				if (Progress == 0.9f && bLoaded == false)
				{
					//Debug.Log ("Preloaded");
					Invoke ("Load", 3);
				}
			}
		}
		if (!LevelSel) {
			if (Async != null) {
				//Debug.Log("" + Async.progress);            
				Progress = Async.progress;
			}
			if (Progress == 0.9f && bLoaded == false)
			{
				bLoaded = true;
				PressStart.SetActive(true);
				StartFadeIn();

			}
			if ((inputDevice.Action1.WasPressed || inputDevice.Command.WasPressed || Input.GetKeyDown("space") || Input.GetKeyDown("return")) && bLoaded )
			{
				FadeOut();
				Invoke("Load", 2f);
			}
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("escape")) && bChangeOpt)
			{
				//Debug.Log ("Back to menu");
				bChangeOpt = false;
				FadeOut();
				Invoke ("LoadMainMenu", 2);
			}
		}
    }
	public void SetLevel(int lvl, int ch) {
        Debug.Log("Setting level: " + lvl);
        level = lvl;
		Checkpoint = ch;
    }
    public IEnumerator LoadLevel(){
        yield return new WaitForSeconds(10);
        Async.allowSceneActivation = true;
    }
    public IEnumerator StartLoadLevel()
    {
        yield return new WaitForSeconds(0);
        Async = SceneManager.LoadSceneAsync(2);
    }
    void StartLoad() {
        bLoaded = false;
        //Debug.Log("Loading level: " + level);
        Async = SceneManager.LoadSceneAsync(level);
        Async.allowSceneActivation = false;
		Invoke("FadeIn", 0.5f);
    }
	void StartLoad2() {
		bLoading = true;
		//Debug.Log("Loading level: " + level);
		Async = SceneManager.LoadSceneAsync(level+1);
		Async.allowSceneActivation = false;
	}
    void Load()
    {
        Async.allowSceneActivation = true;
    }
	void LoadScene()
	{
		SceneManager.LoadScene(level+1);
	}
    public void FadeIn()
    {
        //Debug.Log("Fading in");
        Fadein.GetComponent<Image>().CrossFadeAlpha(0f, 2.0f, false);

    }
    public void FadeOut()
    {        
        //Debug.Log("Fading out");
        Fadein.GetComponent<Image>().CrossFadeAlpha(1f, 2.0f, false);
		Invoke ("Loading", 0.5f);
    }
    public void StartFadeIn()
    {
        //Debug.Log("Fading in");
        PressStart.GetComponent<Image>().CrossFadeAlpha(0f, 1f, false);
        Invoke("StartFadeOut", 1f);
    }
    public void StartFadeOut()
    {
        //Debug.Log("Fading out");
        PressStart.GetComponent<Image>().CrossFadeAlpha(1f, 1f, false);
        Invoke("StartFadeIn", 1f);
    }
	public void SetChangeOpt(){
		if (bChangeOpt2) {
			bChangeOpt = true;
			bMoving = false;
			Titulo.GetComponent<Text> ().text = Loadingtext [level];
			Titulo.GetComponent<Text>().CrossFadeAlpha(1f, 0.2f, false);
			Anim.SetFloat ("Speed",1);
		}
	}
	public void ForceChangeOpt(){
		bChangeOpt = true;
	}
	public void ForceMoving(){
		bMoving = true;
	}
	public void SetActive(){
		Anim.SetTrigger ("Active");
		if (!LevelSel) {
			Anim.SetFloat ("Speed",0);
			Invoke ("SetLoadingSpeed", 2f);
		}
		if (Checkpoint == 4){
			Nubes[0].GetComponent<Image>().CrossFadeAlpha(0f, 2f, false);
		}
		if (Checkpoint == 8){
			Nubes[1].GetComponent<Image>().CrossFadeAlpha(0f, 3f, false);
		}
		if (Checkpoint == 16){
			Nubes[2].GetComponent<Image>().CrossFadeAlpha(0f, 3f, false);
		}
		if (Checkpoint == 24){
			Nubes[3].GetComponent<Image>().CrossFadeAlpha(0f, 3f, false);
		}
		if (Checkpoint == 32){
			Nubes[4].GetComponent<Image>().CrossFadeAlpha(0f, 3f, false);
		}
		if (Checkpoint == 40){
			Nubes[5].GetComponent<Image>().CrossFadeAlpha(0f, 3f, false);
		}
	}
	public void ResetVariables(){
		FsmVariables.GlobalVariables.FindFsmBool("Item_1").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_2").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_3").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_4").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_5").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_6").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Kinematic_Climbs").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_1").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_2").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_3").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_4").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_5").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_6").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_7").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_8").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_9").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Only_Idle").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("RandyDirection").Value = true;
		FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Cinematic").Value = false;
	}
	public void LoadNext(){
		Debug.Log ("Loading level: " + level);
		ResetVariables();
		FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_Camera").Value = new Vector3(0,0,-10);
		FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_CheckPoint").Value = LastPositionCheckpoint[level];
		GameObject.Find("Supervisor").GetComponent<LoadingGestor>().SetLevel((level+1), ((level-1)*4));
		if (level == 1) {
			FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = true;
			PlayerPrefs.SetInt ("Save1", 1);
		}
		if (level == 2) {
			FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Cinematic").Value = true;
			PlayerPrefs.SetInt ("Save1", 4);
		}
		if (level == 3) {
			PlayerPrefs.SetInt ("Save1", 8);
		}
		if (level == 4) {
			PlayerPrefs.SetInt ("Save1", 12);
		}
		if (level == 5) {
			PlayerPrefs.SetInt ("Save1", 16);
		}
		if (level == 6) {
			PlayerPrefs.SetInt ("Save1", 20);
		}
		if (level == 7) {
			PlayerPrefs.SetInt ("Save1", 24);
		}
		if (level == 8) {
			PlayerPrefs.SetInt ("Save1", 28);
		}
		if (level == 9) {
			PlayerPrefs.SetInt ("Save1", 32);
		}
		if (level == 10) {
			PlayerPrefs.SetInt ("Save1", 36);
		}
		if (level == 11) {
			PlayerPrefs.SetInt ("Save1", 40);
		}
		if (level == 12) {
			PlayerPrefs.SetInt ("Save1", 44);
		}
		Invoke ("StartLoad2", 3f);
	}
	public void LoadMainMenu(){
		SceneManager.LoadScene(1);
	}
	public void Loading(){
		GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Loading (true);
	}
	public void Unloading(){
		GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Loading (false);
	}
	public void SetLoadingSpeed(){
		Anim.SetFloat ("Speed",0.5f);
	}
}
