﻿using UnityEngine;
using System.Collections;

public class RisingLava : MonoBehaviour {
	public GameObject Top;
	public GameObject Bottom;
	public GameObject Lava;
	public bool bRising;
	public bool bWaiting;
	public bool bActive;
	public float TimetoriseMax;
	private float TimetoRise;
	public float RisingSpeed;
	public float DroppingSpeed;
	// Use this for initialization
	void Start () {
		bActive = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (bActive) {
			if (bWaiting) {
				TimetoRise += Time.deltaTime;
				if (TimetoRise >= TimetoriseMax) {
					bWaiting = false;
					bRising = true;
					TimetoRise = 0;
				}
			}
			if (!bWaiting) {
				if (bRising) {
					Lava.transform.Translate (0, RisingSpeed*Time.timeScale, 0);
					if (Lava.transform.position.y >= Top.transform.position.y) {
						bRising = false;
					}
				}
				if (!bRising) {
					Lava.transform.Translate (0, -DroppingSpeed*Time.timeScale, 0);
					if (Lava.transform.position.y <= Bottom.transform.position.y) {
						bRising = true;
						bWaiting = true;
					}
				}
			}
		}
	
	}
	public void DisableLava(){
		Invoke ("DisableLava2", 1f);
	}
	public void DisableLava2(){
		bActive = false;
	}
}
