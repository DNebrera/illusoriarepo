﻿using UnityEngine;
using System.Collections;

public class Hojas : MonoBehaviour {
	public int NumHoja;
	public GameObject Supervisor;
	// Use this for initialization
	void Start () {
		if (GameObject.Find ("Supervisor") != null) {
			Supervisor = GameObject.Find ("Supervisor");
		}
		if (Supervisor.GetComponent<LoadingGestor> ().ComprobarHoja (NumHoja)) {
			//Debug.Log ("No tienes esa hoja");
			gameObject.SetActive(false);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Randy") {
			Supervisor.GetComponent<LoadingGestor> ().SavePapiros (NumHoja);
			gameObject.SetActive(false);
		}
	}
}
