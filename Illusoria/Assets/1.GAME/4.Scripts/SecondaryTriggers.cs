﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class SecondaryTriggers : MonoBehaviour {
	public int TriggerN;
	public GameObject MainCamera;
	public GameObject Randy;
	public bool bActive;
	public string Zonas;
	public int UnloSec;
	public string[] Sec;
	public AudioClip PuertaSonido;
	public AudioClip EngranajeSonido;
	// Use this for initialization
	void Start () {
		MainCamera = GameObject.Find ("Main Camera");
		Randy = GameObject.Find ("Randy");
	}
	
	// Update is called once per frame
	void Update () {
		if (TriggerN == 8) {
			if (Input.GetKeyDown (KeyCode.L)) {
				//SonidoPuerta ();
			}
		}
	}
	public void DisableCont(){
		MainCamera.GetComponent<PadDetect> ().DisableCont (true);
	}
	public void EnableCont(){
		MainCamera.GetComponent<PadDetect> ().DisableCont (false);
	}
	void OnTriggerEnter(Collider other)
	{
		if (bActive) {
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 1) {
					GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().JugarConPesadilla (false);
					bActive = false;
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 2 && PlayerPrefs.GetInt ("Ach6") == 0) {
					GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (6);
					bActive = false;
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 3 && PlayerPrefs.GetInt ("Ach19") == 0) {
					GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (19);
					bActive = false;
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 4 && PlayerPrefs.GetInt ("Ach20") == 0) {
					GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (20);
					bActive = false;
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 5) {
					GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().EngranajeGet (1);
					SonidoEngranaje ();
					bActive = false;
					gameObject.GetComponent<SpriteRenderer> ().enabled = false;
					gameObject.GetComponent<BoxCollider> ().enabled = false;
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 6) {
					GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().EngranajeGet (2);
					SonidoEngranaje ();
					bActive = false;
					gameObject.GetComponent<SpriteRenderer> ().enabled = false;
					gameObject.GetComponent<BoxCollider> ().enabled = false;
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 7) {
					GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().NextLevel (13);
					bActive = false;
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 8) {
					if (GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().EngranajeMGot == true) {
						Debug.Log ("Got Michaels Cog");
						gameObject.GetComponent<Animator> ().SetBool ("MichaelDead", true);
					}
					if (GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().EngranajeWGot == true) {
						Debug.Log ("Got Wolfgangs Cog");
						gameObject.GetComponent<Animator> ().SetBool ("WolfgangDead", true);
					}
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 9) {
					MainCamera.GetComponent<PadDetect> ().DisableCont (true);
					GameObject.Find ("Amelia3").GetComponent<Amelia3> ().Revive ();
					bActive = false;
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 10) {					
					Debug.Log ("StartFight");
					Invoke ("DelayedStartFinalBoss", 1);
					GameObject.Find ("Amelia3").GetComponent<Amelia3> ().playSound (0);
					GameObject.Find ("Amelia3").GetComponent<Amelia3> ().StartFight ();
					bActive = false;
					MainCamera.GetComponent<PadDetect> ().PlayMusic (6);

				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 11) {					
					Debug.Log ("StartFightRandys");
					GameObject.Find ("HeartofDarkness").GetComponent<HeartOfDarkness> ().StartFight ();
					MainCamera.GetComponent<PadDetect> ().DisableCont (false);
					bActive = false;
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 12) {					
					GameObject.Find ("HeartofDarkness").GetComponent<HeartOfDarkness> ().WonFight ();
					bActive = false;
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 13) {					
					ExtractSecrets ();
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 14) {
					MainCamera.GetComponent<PadDetect> ().MuerteFogonazoRandy ();
				}
			}
			if (other.tag == "Randy" || other.tag == "Psychocrusher") {
				if (TriggerN == 15) {
					GameObject.Find ("Primer_Plano").GetComponent<VignetteAndChromaticAberration> ().intensity = 7;
					GameObject.Find ("Primer_Plano").GetComponent<VignetteAndChromaticAberration> ().blur = 1;
				}
			}
		}
	}
	public void DelayedStartFinalBoss(){
		//MainCamera.GetComponent<PadDetect> ().ShakeCam (0.5f, 0.5f);
		MainCamera.GetComponent<PadDetect> ().DisableCont (false);
	}
	public void ExtractSecrets(){
		Zonas = PlayerPrefs.GetString ("Secrets");
		int charnum = 0;
		UnloSec = 0;
		foreach (char letter in Zonas.ToCharArray()) {
			Sec [charnum] = letter.ToString();
			if (Sec [charnum] == "1") {
				UnloSec++;
			}
			charnum++;
		}
		Invoke ("PonerOrbes", 0.5f);
	}
	public void PonerOrbes(){
		Debug.Log ("Trigger Secrets is: " + PlayerPrefs.GetString ("Secrets"));
		if (Sec [0] == "1") {
			GameObject.Find ("Orbe01").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (Sec [1] == "1") {
			GameObject.Find ("Orbe02").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (Sec [2] == "1") {
			GameObject.Find ("Orbe03").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (Sec [3] == "1") {
			GameObject.Find ("Orbe04").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (Sec [4] == "1") {
			GameObject.Find ("Orbe05").GetComponent<MeshRenderer> ().enabled = true;
		}
		if (Sec [5] == "1") {
			GameObject.Find ("Orbe06").GetComponent<MeshRenderer> ().enabled = true;
		}
	}
	public void SonidoPuerta(){
		gameObject.GetComponent<AudioSource> ().PlayOneShot (PuertaSonido,GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume"));
	}
	public void SonidoEngranaje(){
		Debug.Log ("Sonido Engranaje");
		gameObject.GetComponent<AudioSource> ().PlayOneShot (EngranajeSonido,1);
	}
}
