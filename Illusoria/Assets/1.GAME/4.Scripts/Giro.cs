﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Giro : MonoBehaviour {
	public float Speed;
	public bool NoTime;
	public bool Blink;
	public bool bInactive;
	// Use this for initialization
	void Start () {
		if (Blink) {
			StartFadeOut ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!Blink) {
			if (!bInactive) {
				gameObject.transform.Rotate(0,0,(Speed * Time.timeScale * Time.deltaTime)*100);
				if (NoTime) {
					gameObject.transform.Rotate(0,0,(Speed*Time.unscaledDeltaTime)*100);
				}
			}
		}

	}
	public void StartFadeIn()
	{
		//Debug.Log("Fading in");
		gameObject.GetComponent<Image>().CrossFadeAlpha(0f, 0.5f, false);
		Invoke("StartFadeOut", 1f);
	}
	public void StartFadeOut()
	{
		//Debug.Log("Fading out");
		gameObject.GetComponent<Image>().CrossFadeAlpha(1f, 0.5f, false);
		Invoke("StartFadeIn", 1f);
	}
	public void Activate(bool a){
		bInactive = a;
	}
}
