﻿using UnityEngine;
using System.Collections;
using InControl;

public class Michael_Wolfgang : MonoBehaviour {
    public float MovSpeed;
	public GameObject Randy;
    public GameObject MainCam;
    public GameObject TopeLeft;
    public GameObject TopeRight;
	public float DistToRandy;
    public float Left;
    public float Right;
    public bool HasConvo;
    public int Dial;
    public bool Patrol;
    public bool bWalk;
    public bool bTurn;
    public bool bLeft;
    public bool bMichael;
    public bool bMoveto;
	public bool bRandyclose;
    public Vector3 Destination;
    public Animator Anim;
    public int ActualLevel = 3;
	public int Save;
	public bool bAwake;
    private float IdleTime;
    private float IdleMax;
	public Vector2[] StartPos;
	public int Controls;
	void Awake(){
		if (GameObject.Find (gameObject.name) != gameObject) {
			Destroy (gameObject);
		}
	}
	// Use this for initialization
    void Start () {
        IdleMax = 25;
		Controls = PlayerPrefs.GetInt ("Controls");
        Anim = GetComponent<Animator>();
		Anim.SetBool("bMichael", bMichael);
        Left = TopeLeft.transform.position.x;
        Right = TopeRight.transform.position.x;
        DontDestroyOnLoad(gameObject);
		if (ActualLevel == 18) {
			if (!bMichael) {
				DelayMove (0.75f, -4.142f);
			}
		}
        //Moveto(20, 0);
		//ChangeState(1);
    }	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.T)) {
			if (!bMichael) {
				//DelayMove (0.8f, -4.142f);
			}
		}
		if (ActualLevel != 3 && ActualLevel != 12 && ActualLevel != 18) {
			Debug.Log ("Destroy!");
			Destroy(gameObject);
		}
		MainCam = GameObject.Find ("Main Camera");
		Randy = GameObject.Find ("Randy");
		if (Randy != null) {
			DistToRandy = new Vector2((Randy.transform.position.x - gameObject.transform.position.x),(Randy.transform.position.y - gameObject.transform.position.y)).magnitude;
		}
		var inputDevice = InputManager.ActiveDevice;
        Anim.SetBool("bMichael", bMichael);        
		if (!bWalk && !bMoveto) {
            IdleTime += Time.deltaTime;
            if(IdleTime >= IdleMax){
                Anim.SetTrigger("Idle3");
                IdleTime = 0;
            }
        }
        if (gameObject.transform.position.x < Left && bWalk && bLeft)
        {
            bWalk = false;
            Anim.SetBool("bWalk", false);
            bTurn = true;
			Anim.SetTrigger("Idle2");
        }
        if (gameObject.transform.position.x > Right && bWalk && !bLeft)
        {
            bWalk = false;
            Anim.SetBool("bWalk", false);
            bTurn = true;
            Anim.SetTrigger("Idle2");
        }
        if (bWalk) {
            IdleTime = 0;
            Anim.SetBool("bWalk", true);
            if (bLeft){
				gameObject.transform.Translate(new Vector3(-MovSpeed*Time.timeScale*Time.deltaTime, 0, 0));
            }
            if (!bLeft)
            {
				gameObject.transform.Translate(new Vector3(MovSpeed*Time.timeScale*Time.deltaTime, 0, 0));
            }
        }
        if (bMoveto)
        {
			if (gameObject.transform.localPosition.x < Destination.x)
            {
				if (bLeft && bMoveto) {
                    bTurn = true;
                    Turn();
                }
                if (!bLeft) {
                    //Debug.Log("GoingLeft");
                    Anim.SetBool("bWalk", true);
					gameObject.transform.Translate(new Vector3(MovSpeed*Time.timeScale*Time.deltaTime, 0, 0));
                    if (gameObject.transform.position.x >= Destination.x)
                    {
                        bMoveto = false;
                        Anim.SetBool("bWalk", false);
                        Left = TopeLeft.transform.position.x;
                        Right = TopeRight.transform.position.x;
						MainCam.GetComponent<PadDetect> ().DialWaitFor (false);
                    }
                }                
            }
            if (gameObject.transform.localPosition.x > Destination.x)
            {
				if (!bLeft && bMoveto)
                {
                    bTurn = true;
                    Turn();
                }
                if (bLeft)
                {
                    //Debug.Log("GoingRight");
                    Anim.SetBool("bWalk", true);
					gameObject.transform.Translate(new Vector3(-MovSpeed*Time.timeScale*Time.deltaTime, 0, 0));
                    if (gameObject.transform.localPosition.x <= Destination.x)
                    {
                        bMoveto = false;
                        Anim.SetBool("bWalk", false);
                        Left = TopeLeft.transform.position.x;
                        Right = TopeRight.transform.position.x;
						MainCam.GetComponent<PadDetect> ().DialWaitFor (false);
                    }
                }                  
            }
        }
		if (DistToRandy <= 1.5f && DistToRandy >= -1.5f) {
			if (Randy != null) {
				if (Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Use").Value == true) {
					if (bMichael) {
						int r = Random.Range (1, 4);
						if (r == 1) {
							if (Dial == 1) {MainCam.GetComponent<PadDetect>().Dial(15, 15);}
							if (Dial == 2) {MainCam.GetComponent<PadDetect>().Dial(23, 23);					}
							if (Dial == 3) {MainCam.GetComponent<PadDetect> ().Dial (31, 31);					}
							if (Dial == 4) {MainCam.GetComponent<PadDetect> ().Dial (39, 39);}
							if (Dial == 5) {MainCam.GetComponent<PadDetect> ().Dial (47, 47);}
							if (Dial == 6) {MainCam.GetComponent<PadDetect> ().Dial (55, 55);}
						}
						if (r == 2) {
							if (Dial == 1) {MainCam.GetComponent<PadDetect>().Dial(16, 16);}
							if (Dial == 2) {MainCam.GetComponent<PadDetect>().Dial(24, 24);					}
							if (Dial == 3) {MainCam.GetComponent<PadDetect> ().Dial (32, 32);					}
							if (Dial == 4) {MainCam.GetComponent<PadDetect> ().Dial (40, 40);}
							if (Dial == 5) {MainCam.GetComponent<PadDetect> ().Dial (48, 48);}
							if (Dial == 6) {MainCam.GetComponent<PadDetect> ().Dial (56, 56);}
						}
						if (r == 3) {
							if (Dial == 1) {MainCam.GetComponent<PadDetect>().Dial(17, 17);}
							if (Dial == 2) {MainCam.GetComponent<PadDetect>().Dial(25, 25);					}
							if (Dial == 3) {MainCam.GetComponent<PadDetect> ().Dial (33, 33);					}
							if (Dial == 4) {MainCam.GetComponent<PadDetect> ().Dial (41, 41);}
							if (Dial == 5) {MainCam.GetComponent<PadDetect> ().Dial (49, 49);}
							if (Dial == 6) {MainCam.GetComponent<PadDetect> ().Dial (57, 57);}
						}
					}
					if (!bMichael) {
						int r = Random.Range (1, 4);
						if (r == 1) {
							if (Dial == 1) {MainCam.GetComponent<PadDetect>().Dial(19, 19);}
							if (Dial == 2) {MainCam.GetComponent<PadDetect>().Dial(27, 27);					}
							if (Dial == 3) {MainCam.GetComponent<PadDetect> ().Dial (35, 35);					}
							if (Dial == 4) {MainCam.GetComponent<PadDetect> ().Dial (43, 43);}
							if (Dial == 5) {MainCam.GetComponent<PadDetect> ().Dial (51, 51);}
							if (Dial == 6) {MainCam.GetComponent<PadDetect> ().Dial (59, 59);}
						}
						if (r == 2) {
							if (Dial == 1) {MainCam.GetComponent<PadDetect>().Dial(20, 20);}
							if (Dial == 2) {MainCam.GetComponent<PadDetect>().Dial(28, 28);					}
							if (Dial == 3) {MainCam.GetComponent<PadDetect> ().Dial (36, 36);					}
							if (Dial == 4) {MainCam.GetComponent<PadDetect> ().Dial (44, 44);}
							if (Dial == 5) {MainCam.GetComponent<PadDetect> ().Dial (52, 52);}
							if (Dial == 6) {MainCam.GetComponent<PadDetect> ().Dial (60, 60);}
						}
						if (r == 3) {
							if (Dial == 1) {MainCam.GetComponent<PadDetect>().Dial(21, 21);}
							if (Dial == 2) {MainCam.GetComponent<PadDetect>().Dial(29, 29);					}
							if (Dial == 3) {MainCam.GetComponent<PadDetect> ().Dial (37, 37);					}
							if (Dial == 4) {MainCam.GetComponent<PadDetect> ().Dial (45, 45);}
							if (Dial == 5) {MainCam.GetComponent<PadDetect> ().Dial (53, 53);}
							if (Dial == 6) {MainCam.GetComponent<PadDetect> ().Dial (61, 61);}
						}
					}
				}
			}
		}
    }
    void OnTriggerEnter(Collider other)
    {
		if (HasConvo && other.tag == "Randy")
        {
			//Debug.Log ("Convo");
			HasConvo = false;
            if(Dial == 1)
            {
				if (bMichael) {
					MainCam.GetComponent<PadDetect>().Dial(99, 118);
					MainCam.GetComponent<PadDetect> ().ToggleCinematic (1);
				}
				if (!bMichael) {
					MainCam.GetComponent<PadDetect>().Dial(120, 139);
				}
            }
			if(Dial == 2)
			{
				if (bMichael) {
				}
				if (!bMichael) {
					MainCam.GetComponent<PadDetect>().Dial(156, 172);
					MainCam.GetComponent<PadDetect> ().ToggleCinematic (2);
					Moveto(107.9f,-2.6f);
				}
			}
			if(Dial == 3)
			{
				if (!bMichael && PlayerPrefs.GetInt("IgnoredWolf") == 0) {
					MainCam.GetComponent<PadDetect>().Dial(178, 241);
					MainCam.GetComponent<PadDetect> ().ToggleCinematic (3);
					MainCam.GetComponent<PadDetect> ().PlayMusic (1);
				}
				if (!bMichael && PlayerPrefs.GetInt("IgnoredWolf") == 1) {
					MainCam.GetComponent<PadDetect>().Dial(175, 241);
					MainCam.GetComponent<PadDetect> ().ToggleCinematic (3);
					MainCam.GetComponent<PadDetect> ().PlayMusic (1);
				}
			}
			if(Dial == 4)
			{
				if (bMichael) {
					MainCam.GetComponent<PadDetect>().Dial(259, 288);
				}
				if (!bMichael && PlayerPrefs.GetInt("IgnoredWolf") == 0) {
					MainCam.GetComponent<PadDetect> ().PlayMusic (1);
					MainCam.GetComponent<PadDetect>().Dial(254, 258);
					MainCam.GetComponent<PadDetect> ().ToggleCinematic (5);
				}
				if (!bMichael && PlayerPrefs.GetInt("IgnoredWolf") == 1) {
					MainCam.GetComponent<PadDetect> ().PlayMusic (1);
					MainCam.GetComponent<PadDetect>().Dial(251, 258);
					MainCam.GetComponent<PadDetect> ().ToggleCinematic (5);
				}
			}
			if(Dial == 5)
			{
				if (bMichael) {
					MainCam.GetComponent<PadDetect>().Dial(370,425);
					MainCam.GetComponent<PadDetect> ().ToggleCinematic (7);
				}
			}
        }
		if (other.tag == "Randy") {
			bRandyclose = true;
		}
    }
	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Randy") {
			bRandyclose = false;
		}
	}
    public void Turn()
    {
        float Tx = gameObject.transform.localScale.x;
        if (Tx > 0 && bTurn)
        {
            Debug.Log("Turn Left");
            if (!bLeft)
            {
                bLeft = true;
            }
            transform.localScale = new Vector3(-Tx, 0.75f, 1);
            if (Patrol && !bMoveto)
            {
                bWalk = true;
            }
        }
        if (Tx < 0 && bTurn)
        {
            Debug.Log("Turn Right");
            if (bLeft)
            {
                bLeft = false;
            }
            transform.localScale = new Vector3(-Tx, 0.75f, 1);
            if (Patrol && !bMoveto)
            {
                bWalk = true;
            }
        }
        // Anim.SetBool("bTurn", false);
        bTurn = false;      
    }
    public void OnLevelWasLoaded(int level)
    {
        ActualLevel = level;
    }
    public void Moveto(float x, float y) {
        bMoveto = true;
		IdleTime = 0;
        Destination = new Vector2(x, y);
    }
	public void MovetoVector(Vector2 Vec) {
		bMoveto = true;
		IdleTime = 0;
		Destination = Vec;
	}
	public void StartPatrol(){
		bWalk = true;
		Patrol = true;
	}
	public void ChangeState(int state){
		bWalk = false;
		if (state == 0) {
			HasConvo = false;
			Anim.SetBool("bWalk", false);
			bMoveto = false;
		}
		if (state == 1) {
			MainCam = GameObject.Find ("Main Camera");
			Randy = GameObject.Find ("Randy");
			bAwake = true;
			gameObject.transform.position = StartPos [0];
			HasConvo = true;
			Dial = 1;
			if (bMichael) {
				//MainCam.GetComponent<PadDetect> ().DisableCont (true);
				Invoke ("InvokeConvoA", 5);
			}
		}
		if (state == 2) {
			gameObject.transform.position = StartPos [1];
			HasConvo = true;
			Dial = 2;
			Anim.SetBool("bWalk", false);
			MainCam.GetComponent<PadDetect> ().SetCheckpoint (5);
			GameObject.Find ("TriggerA").GetComponent<Triggers> ().Desactivar ();
		}
		if (state == 3) {
			gameObject.transform.position = StartPos [2];
			HasConvo = true;
			Dial = 3;
			GameObject.Find ("TriggerA").GetComponent<Triggers> ().Desactivar ();
			GameObject.Find ("TriggerB").GetComponent<Triggers> ().Activar ();
			if (!bMichael) {
				Moveto(115f,-2.6f);
			}
		}
	}
	public void InvokeConvoA(){
		MainCam.GetComponent<PadDetect> ().Dial (94, 97);
	}
	public void DelayMove(float x, float y){
		StartCoroutine (DelayedMoveTo(x, y));
		//Debug.Log ("Moving in 3");
	}
	public void Mutate(){
		Anim.SetTrigger("Mutate");
	}
	public IEnumerator DelayedMoveTo(float x, float y) {		
		yield return new WaitForSeconds(3);
		bMoveto = true;
		Destination = new Vector2(x, y);
	}
}
