﻿using UnityEngine;
using System.Collections;

public class Lava : MonoBehaviour {
	public GameObject RisingLava;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void OnTriggerEnter(Collider Col){
		if (Col.tag == "Randy"){
			RisingLava.GetComponent<RisingLava> ().DisableLava();
		}
	}
}
