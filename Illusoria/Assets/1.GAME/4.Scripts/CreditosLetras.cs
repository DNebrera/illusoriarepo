﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CreditosLetras : MonoBehaviour {
	public string Inglés;
	public string Castellano;
	public string Aleman;
	public string Frances;
	// Use this for initialization
	void Start () {
		if (PlayerPrefs.GetInt ("Lang") == 1) {
			gameObject.GetComponent<Text> ().text = Inglés;
		}
		if (PlayerPrefs.GetInt ("Lang") == 2) {
			gameObject.GetComponent<Text> ().text = Castellano;
		}
		if (PlayerPrefs.GetInt ("Lang") == 3) {
			gameObject.GetComponent<Text> ().text = Aleman;
		}
		if (PlayerPrefs.GetInt ("Lang") == 4) {
			gameObject.GetComponent<Text> ().text = Frances;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
