﻿using UnityEngine;
using System.Collections;

public class AmeliaDetector : MonoBehaviour {
	public GameObject Amelia;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		gameObject.transform.position = Amelia.transform.position;
	}
	public void OnTriggerEnter(Collider Col){
		if (Col.tag == "Fireball" || Col.tag == "Big_Fireball") {
			Amelia.GetComponent<AmeliaBoss> ().Vanish ();
		}
		if (Col.tag == "Randy") {
			Amelia.GetComponent<AmeliaBoss> ().TooClose ();
		}
	}
}
