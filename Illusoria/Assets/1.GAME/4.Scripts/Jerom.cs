﻿using UnityEngine;
using System.Collections;

public class Jerom : MonoBehaviour {
	public bool bLeft;
	public bool bGoto;
	public float Dir;
	public float Speed;
	// Use this for initialization
	void Start () {
		//MoveTo (175);
	}
	
	// Update is called once per frame
	void Update () {
		if (bLeft) {
			transform.localScale = new Vector3(-1.25f, 1.25f, 2);
		}
		if (!bLeft) {
			transform.localScale = new Vector3(1.25f, 1.25f, 2);
		}
		if (bGoto) {
			if (Dir > gameObject.transform.position.x) {
				bLeft = false;
				gameObject.transform.Translate(new Vector3(Speed*Time.timeScale*Time.deltaTime, 0, 0));
			}
			if (Dir < gameObject.transform.position.x) {
				bLeft = true;
				gameObject.transform.Translate(new Vector3(-Speed*Time.timeScale*Time.deltaTime, 0, 0));
			}
		}
		if (new Vector2 ((Dir - gameObject.transform.position.x), 0).magnitude <= 0.25f) {
			bGoto = false;
		}
	}
	public void MoveTo(float x){
		Dir = x;
		bGoto = true;
	}
}
