﻿using UnityEngine;
using System.Collections;

public class BackRandy : MonoBehaviour {
	public GameObject[] BackRandys;
	public int Deaths;
	public bool Controller;
	public float IdleMax;
	private float IdleTime;
	// Use this for initialization
	void Start () {		
		if (Controller) {
			DontDestroyOnLoad (gameObject);
			if (GameObject.Find("BackRandysBoss")){
				Destroy (gameObject);
			}
			if (!GameObject.Find("BackRandysBoss")){
				gameObject.name = "BackRandysBoss";
			}
			//StartBackRandys ();
		}
		if (!Controller) {
			IdleMax = Random.Range (5f, 15f);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!Controller) {
			IdleTime += Time.deltaTime * Time.timeScale;
			if (IdleTime >= IdleMax) {
				gameObject.GetComponent<Animator> ().SetTrigger ("Idle2");
				IdleTime = 0;
			}
		}
	}
	public void OnLevelWasLoaded(int level){
		if (level != 13 && Controller) {
			Destroy (gameObject);
		}
		if (level == 13 && Controller) {
			Invoke ("StartBackRandys", 0.2f);
		}
	}
	public void StartBackRandys(){
		Debug.Log ("Go Randys " + Deaths);
		GameObject.Find ("HeartofDarkness").GetComponent<HeartOfDarkness> ().SetDeaths (Deaths);
		for (int r = 0; r < BackRandys.Length; r++) {
			if (r < (Deaths)) {
				BackRandys [r].SetActive (true);
				BackRandys [r].GetComponent<Animator> ().SetBool ("bWait", true);
			}
		}
	}
	public void DeathbyRandy(){
		Deaths++;
		if (Deaths >= 49) {
			Deaths = 49;
		}
	}
}
