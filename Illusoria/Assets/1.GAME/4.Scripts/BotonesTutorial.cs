﻿using UnityEngine;
using System.Collections;

public class BotonesTutorial : MonoBehaviour {
	public bool bmando;
	public int MandoConfig;
	public bool Bypass;
	public bool Salto;
	public bool Disparo;
	public bool Interactuar;
	public bool Run;
	public Sprite[] PlaystationSprites;
	public Sprite[] XboxSprites;
	public Sprite[] PcSprites;
	public Sprite[] MacSprites;
	public bool Playstation;
	public bool Xbox;
	public bool PC;
	public bool Mac;
	// Use this for initialization
	void Start () {
		#if UNITY_EDITOR
		Debug.Log("Unity Editor");

		#endif
		#if UNITY_STANDALONE_WIN
		Debug.Log("Windows");
		if(!Bypass){
			PC = true;
		}
		#endif
		#if UNITY_PS4
		Debug.Log("Playstation 4");
		Playstation = true;
		#endif
		#if UNITY_XBOXONE
		Debug.Log("Xbox One");
		Xbox = true;
		#endif
		#if UNITY_STANDALONE_LINUX
		Debug.Log("Linux");
		PC = true;
		#endif
		#if UNITY_STANDALONE_OSX
		Debug.Log("Mac OS");
		Mac = true;
		#endif
		bmando = GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().bmando;
	}
	
	// Update is called once per frame
	void Update () {
		if(MandoConfig != PlayerPrefs.GetInt("Controls")){
			MandoConfig = PlayerPrefs.GetInt("Controls");
		}
		if (Salto){
			if (Playstation) {
				if (MandoConfig == 1) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = PlaystationSprites [0];
				}
				if (MandoConfig == 2) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = PlaystationSprites [0];
				}
				if (MandoConfig == 3) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = PlaystationSprites [2];
				}
			}
			if (Xbox) {
				if (MandoConfig == 1) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [0];
				}
				if (MandoConfig == 2) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [0];
				}
				if (MandoConfig == 3) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [2];
				}
			}
			if (PC && !bmando) {
				gameObject.GetComponent<SpriteRenderer> ().sprite = PcSprites [0];
			}
			if ((PC || Mac) && bmando) {
				if (MandoConfig == 1) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [0];
				}
				if (MandoConfig == 2) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [0];
				}
				if (MandoConfig == 3) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [2];
				}
			}
			if (Mac && !bmando) {
				gameObject.GetComponent<SpriteRenderer> ().sprite = MacSprites [0];
			}
		}
		if (Disparo){
			if (Playstation) {
				if (MandoConfig == 1) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = PlaystationSprites [1];
				}
				if (MandoConfig == 2) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = PlaystationSprites [2];
				}
				if (MandoConfig == 3) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = PlaystationSprites [4];
				}
			}
			if (Xbox) {
				if (MandoConfig == 1) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [1];
				}
				if (MandoConfig == 2) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [2];
				}
				if (MandoConfig == 3) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [4];
				}
			}
			if (PC && !bmando) {
				gameObject.GetComponent<SpriteRenderer> ().sprite = PcSprites [1];
			}
			if ((PC || Mac) && bmando) {
				if (MandoConfig == 1) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [1];
				}
				if (MandoConfig == 2) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [2];
				}
				if (MandoConfig == 3) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [4];
				}
			}
			if (Mac && !bmando) {
				gameObject.GetComponent<SpriteRenderer> ().sprite = MacSprites [1];
			}
		}
		if (Interactuar){
			if (Playstation) {
				if (MandoConfig == 1) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = PlaystationSprites [3];
				}
				if (MandoConfig == 2) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = PlaystationSprites [3];
				}
				if (MandoConfig == 3) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = PlaystationSprites [1];
				}
			}
			if (Xbox) {
				if (MandoConfig == 1) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [3];
				}
				if (MandoConfig == 2) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [3];
				}
				if (MandoConfig == 3) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [1];
				}
			}
			if (PC && !bmando) {
				gameObject.GetComponent<SpriteRenderer> ().sprite = PcSprites [3];
			}
			if ((PC || Mac) && bmando) {
				if (MandoConfig == 1) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [3];
				}
				if (MandoConfig == 2) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [3];
				}
				if (MandoConfig == 3) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [1];
				}
			}
			if (Mac && !bmando) {
				gameObject.GetComponent<SpriteRenderer> ().sprite = MacSprites [3];
			}
		}
		if (Run){
			if (Playstation) {
				if (MandoConfig == 1) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = PlaystationSprites [2];
				}
				if (MandoConfig == 2) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = PlaystationSprites [1];
				}
				if (MandoConfig == 3) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = PlaystationSprites [5];
				}
			}
			if (Xbox) {
				if (MandoConfig == 1) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [2];
				}
				if (MandoConfig == 2) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [1];
				}
				if (MandoConfig == 3) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [5];
				}
			}
			if (PC && !bmando) {
				gameObject.GetComponent<SpriteRenderer> ().sprite = PcSprites [2];
			}
			if ((PC || Mac) && bmando) {
				if (MandoConfig == 1) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [2];
				}
				if (MandoConfig == 2) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [1];
				}
				if (MandoConfig == 3) {
					gameObject.GetComponent<SpriteRenderer> ().sprite = XboxSprites [5];
				}
			}
			if (Mac && !bmando) {
				gameObject.GetComponent<SpriteRenderer> ().sprite = MacSprites [2];
			}
		}
	}
}
