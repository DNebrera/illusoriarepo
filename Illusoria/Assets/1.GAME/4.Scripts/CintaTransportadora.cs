﻿using UnityEngine;
using System.Collections;

public class CintaTransportadora : MonoBehaviour {
	public bool Left;
	public bool Right;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}
	public void OnTriggerStay(Collider Col){
		if(Col.tag == "Randy"){
			if (Right){
				Col.transform.Translate(0.04f,0,0);
			}
			if (Left){
				Col.transform.Translate(-0.04f,0,0);
			}
		}
		if(Col.tag == "Enemy"){
			if (Right){
				Col.transform.Translate(0.01f,0,0);
			}
			if (Left){
				Col.transform.Translate(-0.01f,0,0);
			}
		}
	}
}
