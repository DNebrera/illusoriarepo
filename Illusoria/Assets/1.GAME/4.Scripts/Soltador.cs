﻿using UnityEngine;
using System.Collections;

public class Soltador : MonoBehaviour {
	public GameObject Randy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Check(){
		Debug.Log ("Soltar Randy");
		gameObject.GetComponent<BoxCollider> ().enabled = true;
		Invoke ("Reset", 0.1f);
		Invoke ("SoltarRandy", 0.25f);
	}
	public void Reset(){
		gameObject.GetComponent<BoxCollider> ().enabled = false;
	}
	public void SoltarRandy(){
		GameObject.Find("Randy").GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Jump").Value = true;
		Invoke ("ResetJump", 0.1f);
	}
	public void ResetJump(){
		GameObject.Find("Randy").GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Jump").Value = false;
	}
	void OnTriggerEnter(Collider other)	{
		if (other.tag == "Randy") {
			CancelInvoke ("SoltarRandy");
		}
	}
}
