﻿using UnityEngine;
using System.Collections;
using InControl;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using HutongGames.PlayMaker;
using Steamworks;

public class LoadingGestor : MonoBehaviour {
    public int Deaths;
    public int Deathsthislevel;
    public int PreviousLevel;
    public int lv;
	public int lev;
	public int Check;
	public bool SelLev;
	public bool bmando;
	public bool IgnoredJerom;
	public bool ComoAntaño;
	public bool SteamAch01;
	public bool SteamAch02;
	public bool SteamAch03;
	public bool SteamAch04;
	public bool SteamAch05;
	public bool SteamAch06;
	public bool SteamAch07;
	public bool SteamAch08;
	public bool SteamAch09;
	public bool SteamAch10;
	public bool SteamAch11;
	public bool SteamAch12;
	public bool SteamAch13;
	public bool SteamAch14;
	public bool SteamAch15;
	public bool SteamAch16;
	public bool SteamAch17;
	public bool SteamAch18;
	public bool SteamAch19;
	public bool SteamAch20;
	public int mandos;
	public float JugarPesadilla;
	public bool JPesadilla;
	public string Papiros;
	public string[] PapiHojas;
	public string[] HojasText;
	public GameObject LoadingIcon;
	public GameObject MainCam;
	// Use this for initialization
	void Start () {
        //lv = 10;
		gameObject.name = "Supervisor";
		DontDestroyOnLoad (gameObject);
        Deaths = PlayerPrefs.GetInt("Deaths");
        Deathsthislevel = PlayerPrefs.GetInt("DeathsLevel");
		SteamAPI.Init ();
		if(SteamManager.Initialized) {
			string name = SteamFriends.GetPersonaName();
			Debug.Log(name);
			Debug.Log("Steam Lang: " + SteamApps.GetCurrentGameLanguage());
			CheckSteamAchievements ();
		}
    }
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.T)) {
			//ResetSteamAchievements ();
			//Screen.fullScreen = !Screen.fullScreen;
		}
		if (Input.GetKeyDown (KeyCode.Y)) {
			//ResetSteamAchievements ();
			//Screen.fullScreen = Screen.fullScreen;
		}
		var inputDevice = InputManager.ActiveDevice;
		if (!bmando) {
			if(inputDevice.MenuWasPressed  || inputDevice.Action1.WasPressed || inputDevice.Action2.WasPressed || inputDevice.Action3.WasPressed || inputDevice.Action4.WasPressed || inputDevice.Direction.Y > 0.6f || inputDevice.Direction.X > 0.6f || inputDevice.Direction.Y < -0.6f || inputDevice.Direction.X < -0.6f) {
				//Debug.Log ("Mando detectado");
				bmando = true;
				if (GameObject.Find ("Main Camera 1") != null) {
					GameObject.Find ("Main Camera 1").GetComponentInChildren<MainMenu> ().SetMando (bmando);
				}
			}
		}
		if (bmando) {
			if(Input.GetKeyDown("return") || Input.GetKeyDown("escape") || Input.GetKeyDown("space") || Input.GetKeyDown(KeyCode.LeftControl)|| Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D)) {
				//Debug.Log ("Mando detectado");
				bmando = false;
				if (GameObject.Find ("Main Camera 1") != null) {
					GameObject.Find ("Main Camera 1").GetComponentInChildren<MainMenu> ().SetMando (bmando);
				}
			}
		}
		if (JPesadilla) {
			JugarPesadilla += Time.deltaTime * Time.timeScale;
			if (JugarPesadilla >= 60) {
				Achievements(3);
				JPesadilla = false;
			}
		}
		/*if (!bmando) {
			InputManager.OnDeviceAttached += inputDevice2 => bmando = true;
			if (GameObject.Find ("Main Camera 1") != null) {
				GameObject.Find ("Main Camera 1").GetComponentInChildren<MainMenu> ().SetMando (bmando);
			}
		}
		if (bmando) {
			InputManager.OnDeviceDetached += inputDevice2 => bmando = false;
			if (GameObject.Find ("Main Camera 1") != null) {
				GameObject.Find ("Main Camera 1").GetComponentInChildren<MainMenu> ().SetMando (bmando);
			}
		}*/
		if (lev >= 2 && lev <= 13) {
			if (MainCam != null) {
				MainCam.GetComponent<PadDetect> ().PlugMando (bmando);
			}
		}
	}
	public void OnLevelWasLoaded(int level){
		Loading (false);
		lev = level;
		JugarPesadilla = 0;
		//Debug.Log ("Reiniciar"+ level + Check);
		if (level >= 2 && level <= 13) {
			MainCam = GameObject.Find ("Main Camera");
		}
		if (PreviousLevel == level) {
			MainCam.GetComponent<PadDetect> ().FadeIn ();
			MainCam.GetComponent<PadDetect> ().HideIntroText ();
			MainCam.GetComponent<PadDetect> ().SetCheckpoint (Check);
			if (level == 5) {
				//Debug.Log ("ReiniciarA"+ level + Check);
				if (Check == 12) {
					GameObject.Find ("Trigger_Dani_01").GetComponent<Triggers> ().Activar ();
					GameObject.Find ("Trigger_Dani_02").GetComponent<Triggers> ().Activar ();
				}
			}
			if (level == 7) {
				//Debug.Log ("ReiniciarA"+ level + Check);
				if (Check == 20) {
					GameObject.Find ("TriggerA").GetComponent<Triggers> ().Activar ();
				}
				if (Check == 22) {
					GameObject.Find ("TriggerB").GetComponent<Triggers> ().Activar ();
				}
			}
			if (level == 9) {
				GameObject.Find ("TriggerA").GetComponent<Triggers> ().Activar ();
				GameObject.Find ("TriggerB").GetComponent<Triggers> ().Activar ();
			}
			if (level == 13) {
				if (IgnoredJerom) {
					GameObject.Find ("StartTrigger").GetComponent<Triggers> ().TriggerN = 16;
				}
			}
		}
		if (PreviousLevel != level)
        {
            PreviousLevel = level;
            if ((level >= 2 && level <= 13) && PreviousLevel != 1)
            {
				GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeInIntroText ();
				GameObject.Find ("Main Camera").GetComponent<PadDetect> ().SetCheckpoint (Check);
                PlayerPrefs.SetInt("DeathsLevel", 0);
            }
			if (level == 3) {
				if (Check == 4) {
					GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().ChangeState (1);
					GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().ChangeState (1);
					GameObject.Find ("Main Camera").GetComponent<PadDetect> ().LockRes ();
				}
				if (Check == 5) {
					GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeIn ();
					GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().ChangeState (2);
					GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().ChangeState (2);
				}
				if (Check == 6) {
					GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeIn ();
					GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().ChangeState (3);
					GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().ChangeState (3);
				}
			}
			if (level == 7) {
				GameObject.Find ("TriggerA").SetActive(true);
				GameObject.Find ("TriggerB").SetActive(true);
			}
			if (level == 18) {
				//GameObject.Find("Wolfgang").GetComponent<Michael_Wolfgang>().DelayMove (0.8f, -4.142f);
			}
		}
        if (level == 14)
        {
			if (!SelLev) {
				GameObject.Find("Main").GetComponent<Preloading>().SetLevel(lv,Check);
			}            
			GameObject.Find ("Main").GetComponent<Preloading> ().LevelSel = SelLev;
        }
		if (level == 16) {
			if (lv == 2) {
				GameObject.Find ("Cinematicas").GetComponent<Cinematicas> ().SetCinematic (1);
				Debug.Log ("Cinematica Nivel 1");
			}
			if (lv == 4) {
				GameObject.Find ("Cinematicas").GetComponent<Cinematicas> ().SetCinematic (2);
				Debug.Log ("Cinematica Nivel 2");
			}
			if (lv == 6) {
				GameObject.Find ("Cinematicas").GetComponent<Cinematicas> ().SetCinematic (3);
				Debug.Log ("Cinematica Nivel 3");
			}
			if (lv == 8) {
				GameObject.Find ("Cinematicas").GetComponent<Cinematicas> ().SetCinematic (4);
				Debug.Log ("Cinematica Nivel 4");
			}
			if (lv == 10) {
				GameObject.Find ("Cinematicas").GetComponent<Cinematicas> ().SetCinematic (5);
				Debug.Log ("Cinematica Nivel 5");
			}
			if (lv == 12) {
				GameObject.Find ("Cinematicas").GetComponent<Cinematicas> ().SetCinematic (6);
				Debug.Log ("Cinematica Nivel 6");
			}
			if (lv == 14) {
				GameObject.Find ("Cinematicas").GetComponent<Cinematicas> ().SetCinematic (7);
				Debug.Log ("Cinematica BadEnding");
			}
			if (lv == 15) {
				GameObject.Find ("Cinematicas").GetComponent<Cinematicas> ().SetCinematic (8);
				Debug.Log ("Cinematica GoodEnding");
			}
		}
    }
	public void RandyDeath(int Death) {
		JPesadilla = false;

		if (Death == 0) {
			Debug.Log ("Randy explota");
		}
		if (Death == 1) {
			Debug.Log ("Randy a los pinchos");
		}
		if (Death == 2) {
			Debug.Log ("Randy acido");
		}
		if (Death == 3) {
			Debug.Log ("Randy planta");
		}
		if (Death == 4) {
			Debug.Log ("Randy mosquito");
		}
		if (Death == 5) {
			Debug.Log ("Randy devorador");
		}
		if (Death == 6) {
			Debug.Log ("Randy pesadilla");
		}
		if (Death == 7) {
			Debug.Log ("Randy laser");
		}
		if (Death == 8) {
			Debug.Log ("Randy fuego");
		}
		if (Death == 9) {
			Debug.Log ("Randy corte");
		}
		if (Death == 10) {
			Debug.Log ("Randy caida");
		}
		if (Death == 11) {
			Debug.Log ("Randy RandyChungo");
			GameObject.Find ("BackRandysBoss").GetComponent<BackRandy> ().DeathbyRandy ();
		}
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeOut ();
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().Dead ();
        Deaths++;
		if (Deaths >= 10 && PlayerPrefs.GetInt ("Ach01") != 1) {
			Achievements (1);
		}
		if (Deaths >= 100 && PlayerPrefs.GetInt ("Ach02") != 1) {
			Achievements (2);
		}
        PlayerPrefs.SetInt("Deaths", Deaths);
        Deathsthislevel++;
        PlayerPrefs.SetInt("DeathsLevel", Deathsthislevel);
    }
	public void CheckSteamAchievements(){
		if (SteamManager.Initialized) {
			SteamUserStats.GetAchievement ("ILLUS_DIEDX10", out SteamAch01);
			SteamUserStats.GetAchievement ("ILLUS_DIEDX100", out SteamAch02);
			SteamUserStats.GetAchievement ("ILLUS_NIGHTMARE", out SteamAch03);
			SteamUserStats.GetAchievement ("ILLUS_SCROLLS", out SteamAch04);
			SteamUserStats.GetAchievement ("ILLUS_IGNOREDWOLF", out SteamAch05);
			SteamUserStats.GetAchievement ("ILLUS_FIFTHHEAD", out SteamAch06);
			SteamUserStats.GetAchievement ("ILLUS_FIRSTFLAME", out SteamAch07);
			SteamUserStats.GetAchievement ("ILLUS_SECONDFLAME", out SteamAch08);
			SteamUserStats.GetAchievement ("ILLUS_THIRDFLAME", out SteamAch09);
			SteamUserStats.GetAchievement ("ILLUS_FOURTHFLAME", out SteamAch10);
			SteamUserStats.GetAchievement ("ILLUS_FIFTHFLAME", out SteamAch11);
			SteamUserStats.GetAchievement ("ILLUS_SIXTHFLAME", out SteamAch12);
			SteamUserStats.GetAchievement ("ILLUS_UNDYINGLEVELS", out SteamAch13);
			SteamUserStats.GetAchievement ("ILLUS_SAVED", out SteamAch15);
			SteamUserStats.GetAchievement ("ILLUS_DESTROYED", out SteamAch14);
			SteamUserStats.GetAchievement ("ILLUS_FRATRICIDE", out SteamAch16);
			SteamUserStats.GetAchievement ("ILLUS_COLDBLOOD", out SteamAch17);
			SteamUserStats.GetAchievement ("ILLUS_50SHADES", out SteamAch18);
			SteamUserStats.GetAchievement ("ILLUS_LOSTDOLL", out SteamAch19);
			SteamUserStats.GetAchievement ("ILLUS_OLDWAYS", out SteamAch20);
			if (SteamAch01) {
				PlayerPrefs.SetInt("Ach01", 1);
			}
			if (SteamAch02) {
				PlayerPrefs.SetInt("Ach02", 1);
			}
			if (SteamAch03) {
				PlayerPrefs.SetInt("Ach03", 1);
			}
			if (SteamAch04) {
				PlayerPrefs.SetInt("Ach04", 1);
			}
			if (SteamAch05) {
				PlayerPrefs.SetInt("Ach05", 1);
			}
			if (SteamAch06) {
				PlayerPrefs.SetInt("Ach06", 1);
			}
			if (SteamAch07) {
				PlayerPrefs.SetInt("Ach07", 1);
			}
			if (SteamAch08) {
				PlayerPrefs.SetInt("Ach08", 1);
			}
			if (SteamAch09) {
				PlayerPrefs.SetInt("Ach09", 1);
			}
			if (SteamAch10) {
				PlayerPrefs.SetInt("Ach10", 1);
			}
			if (SteamAch11) {
				PlayerPrefs.SetInt("Ach11", 1);
			}
			if (SteamAch12) {
				PlayerPrefs.SetInt("Ach12", 1);
			}
			if (SteamAch13) {
				PlayerPrefs.SetInt("Ach13", 1);
			}
			if (SteamAch14) {
				PlayerPrefs.SetInt("Ach14", 1);
			}
			if (SteamAch14) {
				PlayerPrefs.SetInt("Ach14", 1);
			}
			if (SteamAch15) {
				PlayerPrefs.SetInt("Ach15", 1);
			}
			if (SteamAch16) {
				PlayerPrefs.SetInt("Ach16", 1);
			}
			if (SteamAch17) {
				PlayerPrefs.SetInt("Ach17", 1);
			}
			if (SteamAch18) {
				PlayerPrefs.SetInt("Ach18", 1);
			}
			if (SteamAch19) {
				PlayerPrefs.SetInt("Ach19", 1);
			}
			if (SteamAch20) {
				PlayerPrefs.SetInt("Ach20", 1);
			}
		}
	}
    public void Achievements(int Achi) {
		if (Achi == 1) {
			PlayerPrefs.SetInt("Ach01", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_DIEDX10");
			}
		}
		if (Achi == 2) {
			PlayerPrefs.SetInt("Ach02", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_DIEDX100");
			}
		}
		if (Achi == 3) {
			PlayerPrefs.SetInt("Ach03", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_NIGHTMARE");
			}
		}
		if (Achi == 4) {
			PlayerPrefs.SetInt("Ach04", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_SCROLLS");
			}
		}
		if (Achi == 5) {
			PlayerPrefs.SetInt("Ach05", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_IGNOREDWOLF");
			}
		}
		if (Achi == 6) {
			PlayerPrefs.SetInt("Ach06", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_FIFTHHEAD");
			}
		}
		if (Achi == 7) {
			PlayerPrefs.SetInt("Ach07", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_FIRSTFLAME");
			}
		}
		if (Achi == 8) {
			PlayerPrefs.SetInt("Ach08", 1);
			if (SteamManager.Initialized) {

			}
			SteamUserStats.SetAchievement ("ILLUS_SECONDFLAME");
		}
		if (Achi == 9) {
			PlayerPrefs.SetInt("Ach09", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_THIRDFLAME");
			}
		}
		if (Achi == 10) {
			PlayerPrefs.SetInt("Ach10", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_FOURTHFLAME");
			}
		}
		if (Achi == 11) {
			PlayerPrefs.SetInt("Ach11", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_FIFTHFLAME");
			}
		}
		if (Achi == 12) {
			PlayerPrefs.SetInt("Ach12", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_SIXTHFLAME");
			}
		}
		if (Achi == 13) {
			PlayerPrefs.SetInt("Ach13", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_UNDYINGLEVELS");
			}
		}
		if (Achi == 14) {
			PlayerPrefs.SetInt("Ach14", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_DESTROYED");
			}
		}
		if (Achi == 15) {
			PlayerPrefs.SetInt("Ach15", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_SAVED");
			}
		}
		if (Achi == 16) {
			PlayerPrefs.SetInt("Ach16", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_FRATRICIDE");
			}
		}
		if (Achi == 17) {
			PlayerPrefs.SetInt("Ach17", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_COLDBLOOD");
			}
		}
		if (Achi == 18) {
			PlayerPrefs.SetInt("Ach18", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_50SHADES");
			}
		}
		if (Achi == 19) {
			PlayerPrefs.SetInt("Ach19", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_LOSTDOLL");
			}
		}
		if (Achi == 20) {
			PlayerPrefs.SetInt("Ach20", 1);
			if (SteamManager.Initialized) {
				SteamUserStats.SetAchievement ("ILLUS_OLDWAYS");
			}
		}
		SteamUserStats.StoreStats ();
    }
	public void SetLevel(int lvl, int ch) {
        lv = lvl;
		Check = ch;
		SelLev = false;
    }
	public void SetCheck(int ch) {
		Check = ch;
	}
	public void CheckDeaths(){
		if (Deathsthislevel == 0) {
			if (lev == 2) {
				PlayerPrefs.SetInt ("Gold01", 1);
				Debug.Log ("has terminado el nivel 1 sin morir!");
			}
			if (lev == 3) {
				PlayerPrefs.SetInt ("Gold02", 1);
				Debug.Log ("has terminado el nivel 2 sin morir!");
			}
			if (lev == 4) {
				PlayerPrefs.SetInt ("Gold03", 1);
				Debug.Log ("has terminado el nivel 3 sin morir!");
			}
			if (lev == 5) {
				PlayerPrefs.SetInt ("Gold04", 1);
				Debug.Log ("has terminado el nivel 4 sin morir!");
			}
			if (lev == 6) {
				PlayerPrefs.SetInt ("Gold05", 1);
				Debug.Log ("has terminado el nivel 5 sin morir!");
			}
			if (lev == 7) {
				PlayerPrefs.SetInt ("Gold06", 1);
				Debug.Log ("has terminado el nivel 6 sin morir!");
			}
			if (lev == 8) {
				PlayerPrefs.SetInt ("Gold07", 1);
				Debug.Log ("has terminado el nivel 7 sin morir!");
			}
			if (lev == 9) {
				PlayerPrefs.SetInt ("Gold08", 1);
				Debug.Log ("has terminado el nivel 8 sin morir!");
			}
			if (lev == 10) {
				PlayerPrefs.SetInt ("Gold09", 1);
				Debug.Log ("has terminado el nivel 9 sin morir!");
			}
			if (lev == 11) {
				PlayerPrefs.SetInt ("Gold10", 1);
				Debug.Log ("has terminado el nivel 10 sin morir!");
			}
			if (lev == 12) {
				PlayerPrefs.SetInt ("Gold11", 1);
				Debug.Log ("has terminado el nivel 11 sin morir!");
			}
			if (lev == 13) {
				PlayerPrefs.SetInt ("Gold12", 1);
				Debug.Log ("has terminado el nivel 12 sin morir!");
			}
		}
		if (PlayerPrefs.GetInt ("Gold01") == 1 && PlayerPrefs.GetInt ("Gold02") == 1 && PlayerPrefs.GetInt ("Gold03") == 1 && PlayerPrefs.GetInt ("Gold04") == 1 && PlayerPrefs.GetInt ("Gold05") == 1 && PlayerPrefs.GetInt ("Gold06") == 1 && PlayerPrefs.GetInt ("Gold07") == 1 && PlayerPrefs.GetInt ("Gold08") == 1 && PlayerPrefs.GetInt ("Gold09") == 1 && PlayerPrefs.GetInt ("Gold10") == 1 && PlayerPrefs.GetInt ("Gold11") == 1 && PlayerPrefs.GetInt ("Gold12") == 1) {
			Achievements (13);
		}
	}
	public void BadGameEnding(int lvl) {
		CheckDeaths ();
		ResetDeathsPerLevel ();
		lv = lvl;
		ResetVariables();
		iTween.Stop ();
		if (ComoAntaño) {
			if (PlayerPrefs.GetInt ("Ach20") == 0) {
				Achievements (20);
			}
		}
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeInEnd();
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeOutAmbient ();
		Invoke ("LoadScreen2", 1f);
	}
	public void GoodGameEnding(int lvl) {
		CheckDeaths ();
		ResetDeathsPerLevel ();
		lv = lvl;
		ResetVariables();
		iTween.Stop ();
		if (ComoAntaño) {
			if (PlayerPrefs.GetInt ("Ach20") == 0) {
				Achievements (20);
			}
		}
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeOut ();
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeOutAmbient ();
		Invoke ("LoadScreen2", 0.1f);
	}
	public void NextLevel(int lvl) {
		CheckDeaths ();
		ResetDeathsPerLevel ();
		lv = lvl;
		ResetVariables();
		iTween.Stop ();
		if (lvl == 3) {
			Check = 4;
			PlayerPrefs.SetInt ("Save1", 4);
		}
		if (lvl == 4) {
			Check = 8;
			PlayerPrefs.SetInt ("Save1", 8);
		}
		if (lvl == 5) {
			Check = 12;
			PlayerPrefs.SetInt ("Save1", 12);
		}
		if (lvl == 6) {
			Check = 16;
			PlayerPrefs.SetInt ("Save1", 16);
		}
		if (lvl == 7) {
			Check = 20;
			PlayerPrefs.SetInt ("Save1", 20);
		}
		if (lvl == 8) {
			Check = 24;
			PlayerPrefs.SetInt ("Save1", 24);
		}
		if (lvl == 9) {
			Check = 28;
			PlayerPrefs.SetInt ("Save1", 28);
		}
		if (lvl == 10) {
			Check = 32;
			PlayerPrefs.SetInt ("Save1", 32);
		}
		if (lvl == 11) {
			Check = 36;
			PlayerPrefs.SetInt ("Save1", 36);
		}
		if (lvl == 12) {
			Check = 40;
			PlayerPrefs.SetInt ("Save1", 40);
		}
		if (lvl == 13) {
			Check = 44;
			PlayerPrefs.SetInt ("Save1", 44);
		}
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeOut ();
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeOutAmbient ();
		if (lvl == 10) {
			FsmVariables.GlobalVariables.FindFsmVector3 ("Last_Position_Camera").Value = new Vector3 (0, 0, -10);
			FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_CheckPoint").Value = new Vector3 (-12, 2.1f, 0);
		}
		if (lvl == 12) {
			//FsmVariables.GlobalVariables.FindFsmVector3 ("Last_Position_Camera").Value = new Vector3 (0, 0, -10);
			FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_CheckPoint").Value = new Vector3 (0, -9.03f, 0);
		}
		if (lvl == 13) {
			FsmVariables.GlobalVariables.FindFsmVector3 ("Last_Position_Camera").Value = new Vector3 (0, 0, -10);
			FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_CheckPoint").Value = new Vector3 (-20, -4.2f, 0);
		}
		if (lvl == 14) {
			Invoke ("LoadScreen2", 2f);
		}
		if (lvl == 4 || lvl == 6 || lvl == 10 || lvl == 12) {
			Invoke ("LoadScreen2", 2f);
		}
		if (lvl == 8) {
			Invoke ("LoadScreen3", 2f);
			FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = true;
		}
		if (lvl != 4 && lvl != 6 && lvl != 8 && lvl != 10 && lvl != 12) {
			Invoke ("LoadScreen", 2f);
			if (lvl == 3) {
				Check = 4;
			}
		}
	}
	public void NextLevel2(int lvl) {
		CheckDeaths ();
		ResetDeathsPerLevel ();
		lv = lvl;
		ResetVariables();
		iTween.Stop ();
		if (lvl == 3) {
			Check = 4;
		}
		if (lvl == 4) {
			Check = 8;
		}
		if (lvl == 5) {
			Check = 12;
		}
		if (lvl == 6) {
			Check = 16;
		}
		if (lvl == 7) {
			Check = 20;
		}
		if (lvl == 8) {
			Check = 24;
		}
		if (lvl == 9) {
			Check = 28;
		}
		if (lvl == 10) {
			Check = 32;
		}
		if (lvl == 11) {
			Check = 36;
		}
		if (lvl == 12) {
			Check = 40;
		}
		if (lvl == 13) {
			Check = 44;
		}
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeOutAmbient ();
		if (lvl == 4 || lvl == 6 || lvl == 10 || lvl == 12) {
			Invoke ("LoadScreen2", 2f);
		}
		if (lvl == 8) {
			Invoke ("LoadScreen3", 2f);
			FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = true;
		}
		if (lvl != 4 && lvl != 6 && lvl != 8 && lvl != 10 && lvl != 12) {
			Invoke ("LoadScreen", 2f);
			if (lvl == 3) {
				Check = 4;
			}
		}
	}
	public void CargarLoadScreen() {
		ResetVariables();
		iTween.Stop ();
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().FadeOut ();
		lv = 8;
		Check = 24;
		PlayerPrefs.SetInt("Save1",24);
		FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_Camera").Value = new Vector3(0,0,-10);
		FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_CheckPoint").Value = new Vector3(-13,-4.4f,0);
		Invoke ("LoadScreen2", 2f);
	}
	public void LoadScreen(){
		SceneManager.LoadScene(14);
	}
	public void LoadScreen2(){
		SceneManager.LoadScene(16);
	}
	public void LoadScreen3(){
		SceneManager.LoadScene(18);
	}
	public void SetPapiros(string[] pap, string[] txt){
		PapiHojas = pap;
		HojasText = txt;
	}
	public void SavePapiros(int hoja){
		//Debug.Log ("Salvando hoja: " + hoja);
		int TotalHojas = 0;
		PapiHojas [hoja] = "1";
		GameObject.Find ("Main Camera").GetComponent<PadDetect> ().GetHoja(HojasText[81 + hoja]);
		Papiros = null;
		for (int p = 0; p < PapiHojas.Length; p++) {
			Papiros += PapiHojas [p];
			if (PapiHojas [p] == "1") {
				TotalHojas++;
				if (TotalHojas >= 60) {
					//Debug.Log ("Have em all! " + TotalHojas);
					Achievements(4);
				}
			}
			if (p == (PapiHojas.Length)-1) {
				PlayerPrefs.SetString ("Papiros", Papiros);
			}
		}
	}
	public bool ComprobarHoja(int hoja){
		if (PapiHojas [hoja] == "1") {
			return true;
		} else
			return false;
	}
	public void ResetVariables(){
		FsmVariables.GlobalVariables.FindFsmBool("Item_1").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_2").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_3").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_4").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_5").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_6").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Kinematic_Climbs").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_1").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_2").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_3").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_4").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_5").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_6").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_7").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_8").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_9").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Only_Idle").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("RandyDirection").Value = true;
		FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = true;
	}
	public void Loading(bool bLoad){
		if (bLoad) {
			LoadingIcon.SetActive (true);
		}
		if (!bLoad) {
			LoadingIcon.SetActive (false);
		}
	}
	public void JugarConPesadilla(bool p){
		JPesadilla = p;
	}
	public void ResetDeathsPerLevel(){
		PlayerPrefs.SetInt("DeathsLevel",0);
		Deathsthislevel = 0;
	}
	public void IgnoreJerom(){
		IgnoredJerom = true;
	}
	public void Antaño(bool a){
		ComoAntaño = a;
		if (a == true) {
			ResetDeathsPerLevel ();
		}
	}
	public void ResetSteamAchievements(){
		PlayerPrefs.SetInt ("Ach01", 0);
		PlayerPrefs.SetInt ("Ach02", 0);
		PlayerPrefs.SetInt ("Ach03", 0);
		PlayerPrefs.SetInt ("Ach04", 0);
		PlayerPrefs.SetInt ("Ach05", 0);
		PlayerPrefs.SetInt ("Ach06", 0);
		PlayerPrefs.SetInt ("Ach07", 0);
		PlayerPrefs.SetInt ("Ach08", 0);
		PlayerPrefs.SetInt ("Ach09", 0);
		PlayerPrefs.SetInt ("Ach10", 0);
		PlayerPrefs.SetInt ("Ach11", 0);
		PlayerPrefs.SetInt ("Ach12", 0);
		PlayerPrefs.SetInt ("Ach13", 0);
		PlayerPrefs.SetInt ("Ach14", 1);
		PlayerPrefs.SetInt ("Ach15", 0);
		PlayerPrefs.SetInt ("Ach16", 0);
		PlayerPrefs.SetInt ("Ach17", 0);
		PlayerPrefs.SetInt ("Ach18", 0);
		PlayerPrefs.SetInt ("Ach19", 0);
		PlayerPrefs.SetInt ("Ach20", 0);
		SteamUserStats.ClearAchievement("ILLUS_DIEDX10");
		SteamUserStats.ClearAchievement ("ILLUS_DIEDX100");
		SteamUserStats.ClearAchievement ("ILLUS_NIGHTMARE");
		SteamUserStats.ClearAchievement ("ILLUS_SCROLLS");
		SteamUserStats.ClearAchievement ("ILLUS_IGNOREDWOLF");
		SteamUserStats.ClearAchievement ("ILLUS_FIFTHHEAD");
		SteamUserStats.ClearAchievement ("ILLUS_FIRSTFLAME");
		SteamUserStats.ClearAchievement ("ILLUS_SECONDFLAME");
		SteamUserStats.ClearAchievement ("ILLUS_THIRDFLAME");
		SteamUserStats.ClearAchievement ("ILLUS_FOURTHFLAME");
		SteamUserStats.ClearAchievement ("ILLUS_FIFTHFLAME");
		SteamUserStats.ClearAchievement ("ILLUS_SIXTHFLAME");
		SteamUserStats.ClearAchievement ("ILLUS_UNDYINGLEVELS");
		SteamUserStats.ClearAchievement ("ILLUS_SAVED");
		SteamUserStats.ClearAchievement ("ILLUS_FRATRICIDE");
		SteamUserStats.ClearAchievement ("ILLUS_COLDBLOOD");
		SteamUserStats.ClearAchievement ("ILLUS_50SHADES");
		SteamUserStats.ClearAchievement ("ILLUS_LOSTDOLL");
		SteamUserStats.ClearAchievement ("ILLUS_OLDWAYS");
		SteamUserStats.StoreStats ();
	}
}
