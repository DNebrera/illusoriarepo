﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using InControl;
using HutongGames.PlayMaker;
using UnityEngine.SceneManagement;
using Steamworks;

public class MainMenu : MonoBehaviour {
    public GameObject MainCamera;
	public int MenuOpt;
	public int MenuOpt2;
	public Object LoadingGestor;
	public int MenuSlide;
	public bool bMove;
    public bool bFading;
    public bool bShowButtons;
	public bool bMando;
	public float StartMoveSpeed;
	public float MoveSpeed;
	public float MaxMoveSpeed;
	public Vector2[] SlidesLoc;
	public AudioClip ButtonSound;
	public AudioClip BackSound;
	public AudioClip SlideMove;
	public AudioClip StartGame;
	//PreMainMenu
	public GameObject PressStartCanv;
	public bool bPressStart;
	public Sprite[] PressStartA;
	//Main Menu
	public bool LockLevelSel;
	public bool LockExtras;
	public bool LockOptions;
	//public bool LockConcepts;
	public GameObject MMCanvas;
    public GameObject Fadein;
    public float Vert;
	public float Hor;
	public bool bChangeOpt;
	public bool bSelected;
	public bool bTest;
	public bool bSetCheck;
	public int SetCheck;
	//Options
	public Sprite[] EffectVolA;
	public Sprite[] MusicVolA;
	public Sprite[] LanguageA;
	public Sprite[] QualityA;
	public Sprite[] QualityLow;
	public Sprite[] QualityMedium;
	public Sprite[] QualityHigh;
	public Sprite[] FullScreenA;
	public Sprite[] ControlsA;
	public Sprite[] Cont;
	public Sprite[] DifBanner;
	public Sprite[] Facil;
	public Sprite[] Normal;
	public Sprite[] Dificil;
	public GameObject OptEffVol;
	public GameObject OptMusVol;
	public GameObject OptLang;
	public GameObject OptDifBanner;
	public GameObject OptDificultad;
	public GameObject Quality;
	public GameObject QualityBanner;
	public GameObject FullScreen;
	public GameObject FullScreenBanner;
	public GameObject Controls;
	public GameObject ContBanner;
	public GameObject OptExit;
	public GameObject EffectVolumeSlider;
	public GameObject MusicVolumeSlider;
	public GameObject LanguageSpriteChanger;
	public Sprite[] LangSprite;
	public GameObject LanguageMenu;
	public GameObject OptCanvas;
    public GameObject ExitCanvas;
    public bool bOptions;
	public bool bFullScreen;
	//ControlsScreen
	public bool bControls;
	public GameObject ContCanvas;
	public GameObject ContLetter;
	public GameObject ContArrows;
    public GameObject LeftA;
    public GameObject RightA;
    public Sprite[] Conf1;
	public Sprite[] Conf2;
	public Sprite[] Conf3;
	public Sprite[] Teclado;
	//MAin Menu
	public float ChangeOptTime;
	public float ChangeOptMax;
	public int Language;
	public int Levels;
	public GameObject NewGame;
	public GameObject Continue;
	public GameObject Options;
	public GameObject Credits;
	public GameObject Exit;
	public GameObject LevelN;
	public GameObject[] Flames;
    public GameObject[] OptFlames;
    public GameObject[] ExitFlames;
    public GameObject[] ExtrasFlames;
	public GameObject[] HojasFlames;
    public Sprite[] NewgameA;
	public Sprite[] ContinueA;
	public Sprite[] OptionsA;
	public Sprite[] ExtrasA;
	public Sprite[] ExitA;
	public Sprite[] BackA;
	public Sprite[] Levelnum;
	public int[] LevelFinish;
	public Vector3[] LastPositionCamera;
	public Vector3[] LastPositionCheckpoint;
	//Sonidos
	public GameObject Musica;
	public GameObject Sonidos;
    //Exit
	public bool bExit;
	public bool Exiting;
	public bool NGame;
	public GameObject Si;
	public GameObject No;
    public GameObject Usure;
	public GameObject LoseProg;
    public Sprite[] SiA;
	public Sprite[] NoA;
	public Sprite[] Usur;
	public Sprite[] LoseP;
	public int YesoNo;
	//Extras
	public bool bExtras;
	public GameObject ExtrasCanvas;
    public GameObject ExtraLogros;
    public GameObject ExtraImágenes;
    public GameObject ExtraBack;
	public GameObject ExtraHojas;
    public GameObject Créditos;
    public Sprite[] LogrosA;
    public Sprite[] ImágenesA;
    public Sprite[] CréditosA;
	public Sprite[] HojasA;
    //Logros
    public bool bLogros;
    public int LogrosPage;
    public int[] Logros;
    public Sprite[] LogrosIcon;
    public string[] LogroText;
    public TextAsset LogrosTxt;
    public GameObject[] IconsLogr;
    public GameObject[] TextLogr;
    public TextAsset LogrosEnglish;
    public TextAsset LogrosCastellano;
    public TextAsset LogrosFrances;
	public string Papiros;
	public string pap;
	public string[] PapirHojas;
    //Concepts
    public bool bConcepts;
    public GameObject Concepts;
    public Sprite[] ConceptSprite;
    public int MaxLevel;
	//Hojas
	public bool bHojas;
	public bool bRead;
	public bool bRead2;
	public bool GoUp;
	public bool GoDown;
	public bool PestMove;
	public GameObject HojasCanvas;
	public GameObject Hojas;
	public GameObject Texto;
	public GameObject ScrollBar;
	public GameObject ScrollBarHandle;
	public GameObject FlechaArriba;
	public GameObject FlechaAbajo;
	public GameObject SelectLevel;
	public GameObject[] Pestañas;
	public GameObject[] Sellos;
	public GameObject[] TempPestañas;
	public int[] PestañasNum;
	public int SelectedPest;
	public Vector3[] PestLoc;
	public Sprite[] SelLevelA;
	public Sprite Sello;
	public Sprite SelloReal;
	public string[] Historias;
    // Use this for initialization
    void Start () {
		Cursor.visible = false;
		QualitySettings.vSyncCount = 0;
		Application.targetFrameRate = 60;
		AudioListener.pause = false;
        bFading = true;
        FadeIn();
		MenuSlide = 1;
		MoveSpeed = StartMoveSpeed;
		Time.timeScale = 1;
		bMove = true;
		if (GameObject.Find ("Supervisor") == null) {
			Instantiate (LoadingGestor);
		}
		Invoke ("ResetAntaño", 0.2f);
		FadeOutPergamino ();
        bPressStart = true;
        Invoke("EndFade", 3f);
		if (PlayerPrefs.HasKey ("Lang") == false) {
			PlayerPrefs.SetInt("Lang",1);
			Debug.Log ("" + Application.systemLanguage);
			if (Application.systemLanguage == SystemLanguage.Spanish) {
				PlayerPrefs.SetInt("Lang",2);
			}
			if (Application.systemLanguage == SystemLanguage.English) {
				PlayerPrefs.SetInt("Lang",1);
			}
		}
		if (PlayerPrefs.GetInt ("Qual") == 0) {
			PlayerPrefs.SetInt("Qual",3);
		}
		if (PlayerPrefs.GetInt ("Controls") == 0) {
			PlayerPrefs.SetInt("Controls",1);
		}
		if (PlayerPrefs.HasKey ("Papiros") == false) {
			//Debug.Log ("Papiros has no key");
			PlayerPrefs.SetString("Papiros","000000000000000000000000000000000000000000000000000000000000");
		}
		if (PlayerPrefs.HasKey ("Secrets") == false) {
			//Debug.Log ("Papiros has no key");
			PlayerPrefs.SetString("Secrets","000000");
		}
		if (PlayerPrefs.HasKey ("EffectVolume") == false) {
			//Debug.Log ("Papiros has no key");
			PlayerPrefs.SetFloat("EffectVolume",1);
		}
		if (PlayerPrefs.HasKey ("MusicVolume") == false) {
			//Debug.Log ("Papiros has no key");
			PlayerPrefs.SetFloat("MusicVolume",1);
		}
		if (PlayerPrefs.HasKey ("Deaths") == false) {
			//Debug.Log ("Papiros has no key");
			PlayerPrefs.SetInt("Deaths",0);
		}
		if (PlayerPrefs.HasKey ("DeathsLevel") == false) {
			//Debug.Log ("Papiros has no key");
			PlayerPrefs.SetInt("DeathsLevel",0);
		}
		if (PlayerPrefs.HasKey ("Dificultad") == false) {
			//Debug.Log ("Papiros has no key");
			PlayerPrefs.SetInt("Dificultad",1);
		}
		if (PlayerPrefs.GetInt ("Save1") == 0) {
			//Debug.Log ("Save is " + PlayerPrefs.GetInt("Save1"));
			PlayerPrefs.SetInt("Save1",1);
		}
		if (PlayerPrefs.HasKey ("Gold01") == false) {
			PlayerPrefs.SetInt ("Gold01", 0);
		}
		if (PlayerPrefs.HasKey ("Gold02") == false) {
			PlayerPrefs.SetInt ("Ach02", 0);
		}
		if (PlayerPrefs.HasKey ("Gold03") == false) {
			PlayerPrefs.SetInt ("Gold03", 0);
		}
		if (PlayerPrefs.HasKey ("Gold04") == false) {
			PlayerPrefs.SetInt ("Gold04", 0);
		}
		if (PlayerPrefs.HasKey ("Gold05") == false) {
			PlayerPrefs.SetInt ("Gold05", 0);
		}
		if (PlayerPrefs.HasKey ("Gold06") == false) {
			PlayerPrefs.SetInt ("Gold06", 0);
		}
		if (PlayerPrefs.HasKey ("Gold07") == false) {
			PlayerPrefs.SetInt ("Gold07", 0);
		}
		if (PlayerPrefs.HasKey ("Gold08") == false) {
			PlayerPrefs.SetInt ("Gold08", 0);
		}
		if (PlayerPrefs.HasKey ("Gold09") == false) {
			PlayerPrefs.SetInt ("Gold09", 0);
		}
		if (PlayerPrefs.HasKey ("Gold10") == false) {
			PlayerPrefs.SetInt ("Gold10", 0);
		}
		if (PlayerPrefs.HasKey ("Gold11") == false) {
			PlayerPrefs.SetInt ("Gold11", 0);
		}
		if (PlayerPrefs.HasKey ("Gold12") == false) {
			PlayerPrefs.SetInt ("Gold12", 0);
		}
		if (bSetCheck){
			PlayerPrefs.SetInt("Save1",SetCheck);
		}
		if (PlayerPrefs.GetInt ("Save1") == 1) {
			MenuOpt = 2;
			Continue.SetActive(false);
		}
		if (PlayerPrefs.GetInt ("Save1") != 1) {
			MenuOpt = 1;
			Continue.SetActive(true);
		}
		if (PlayerPrefs.HasKey ("Ach01") == false) {
			PlayerPrefs.SetInt ("Ach01", 0);
		}
		if (PlayerPrefs.HasKey ("Ach02") == false) {
			PlayerPrefs.SetInt ("Ach02", 0);
		}
		if (PlayerPrefs.HasKey ("Ach03") == false) {
			PlayerPrefs.SetInt ("Ach03", 0);
		}
		if (PlayerPrefs.HasKey ("Ach04") == false) {
			PlayerPrefs.SetInt ("Ach04", 0);
		}
		if (PlayerPrefs.HasKey ("Ach05") == false) {
			PlayerPrefs.SetInt ("Ach05", 0);
		}
		if (PlayerPrefs.HasKey ("Ach06") == false) {
			PlayerPrefs.SetInt ("Ach06", 0);
		}
		if (PlayerPrefs.HasKey ("Ach07") == false) {
			PlayerPrefs.SetInt ("Ach07", 0);
		}
		if (PlayerPrefs.HasKey ("Ach08") == false) {
			PlayerPrefs.SetInt ("Ach08", 0);
		}
		if (PlayerPrefs.HasKey ("Ach09") == false) {
			PlayerPrefs.SetInt ("Ach09", 0);
		}
		if (PlayerPrefs.HasKey ("Ach10") == false) {
			PlayerPrefs.SetInt ("Ach10", 0);
		}
		if (PlayerPrefs.HasKey ("Ach11") == false) {
			PlayerPrefs.SetInt ("Ach11", 0);
		}
		if (PlayerPrefs.HasKey ("Ach12") == false) {
			PlayerPrefs.SetInt ("Ach12", 0);
		}
		if (PlayerPrefs.HasKey ("Ach13") == false) {
			PlayerPrefs.SetInt ("Ach13", 0);
		}
		if (PlayerPrefs.HasKey ("Ach14") == false) {
			PlayerPrefs.SetInt ("Ach14", 0);
		}
		if (PlayerPrefs.HasKey ("Ach15") == false) {
			PlayerPrefs.SetInt ("Ach15", 0);
		}
		if (PlayerPrefs.HasKey ("Ach16") == false) {
			PlayerPrefs.SetInt ("Ach16", 0);
		}
		if (PlayerPrefs.HasKey ("Ach17") == false) {
			PlayerPrefs.SetInt ("Ach17", 0);
		}
		if (PlayerPrefs.HasKey ("Ach18") == false) {
			PlayerPrefs.SetInt ("Ach18", 0);
		}
		if (PlayerPrefs.HasKey ("Ach19") == false) {
			PlayerPrefs.SetInt ("Ach19", 0);
		}
		if (PlayerPrefs.HasKey ("Ach20") == false) {
			PlayerPrefs.SetInt ("Ach20", 0);
		}
		if (Screen.fullScreen) {
			bFullScreen = true;
		}
		if (!Screen.fullScreen) {
			bFullScreen = false;
		}
		bChangeOpt = true;
		Time.timeScale = 1;
		Invoke ("ExtractHojas", 1);
        Language = PlayerPrefs.GetInt ("Lang");
		Debug.Log ("Save1 is " + PlayerPrefs.GetInt("Save1"));
        SetMaxLevel(PlayerPrefs.GetInt("Save1"));
        Debug.Log ("Lang is " + PlayerPrefs.GetInt("Lang"));
        ChangeLanguaje();
		FsmVariables.GlobalVariables.FindFsmFloat("FX_Volume").Value = PlayerPrefs.GetFloat("EffectVolume");
		FsmVariables.GlobalVariables.FindFsmFloat("Music_Volume").Value = PlayerPrefs.GetFloat("MusicVolume");
		for (int h = 0; h < Pestañas.Length; h++) {
			PestLoc [h] = Pestañas [h].transform.position;
		}
	}
	
	// Update is called once per frame
	void Update () {
        Musica.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("MusicVolume");
		Sonidos.GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("EffectVolume");
		if (Input.GetKeyDown (KeyCode.Y)) {
			//GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (14);
			//SacarLogros ();
		}
		if (LockLevelSel) {
			SelectLevel.GetComponent<Image> ().color = Color.grey;
		}
		if (!LockLevelSel) {
			SelectLevel.GetComponent<Image> ().color = Color.white;
		}
		if (LockOptions) {
			Options.GetComponent<Image> ().color = Color.grey;
		}
		if (LockExtras) {
			Credits.GetComponent<Image> ().color = Color.grey;
		}
		//Cameraismoving
		if(bMove){
			bShowButtons = false;
			if (gameObject.transform.parent.position.x > SlidesLoc[MenuSlide].x){
				MainCamera.transform.Translate(new Vector3 (-MoveSpeed*Time.deltaTime,0,0));
			}
			if (gameObject.transform.parent.position.x < SlidesLoc[MenuSlide].x){
				MainCamera.transform.Translate(new Vector3 (MoveSpeed*Time.deltaTime,0,0));
			}
			if (gameObject.transform.parent.position.y > SlidesLoc[MenuSlide].y){
				MainCamera.transform.Translate(new Vector3 (0,(-MoveSpeed*Time.deltaTime)/1.66f,0));
			}
			if (gameObject.transform.parent.position.y < SlidesLoc[MenuSlide].y){
				MainCamera.transform.Translate(new Vector3 (0,(MoveSpeed*Time.deltaTime)/1.66f,0));
			}
			if (MoveSpeed <= MaxMoveSpeed) {
				MoveSpeed += 5f;
			}
			if ((new Vector2 ((SlidesLoc [MenuSlide].x - MainCamera.transform.position.x), (SlidesLoc [MenuSlide].y - MainCamera.transform.position.y)).magnitude < 5) && bChangeOpt == true){
                //Debug.Log("Click");
                bMove = false;
                MainCamera.transform.position = SlidesLoc[MenuSlide];
                if (MenuSlide == 4) {
                    //CreditosCanvas.transform.position = CredStart;
                }
            }
		}
		if (!bMove) {
			gameObject.transform.parent.position = new Vector2 (SlidesLoc [MenuSlide].x,SlidesLoc [MenuSlide].y);
			MoveSpeed = StartMoveSpeed;
			bShowButtons = true;
		}
		var inputDevice = InputManager.ActiveDevice;
		if (bChangeOpt == false) {
			ChangeOptTime += Time.deltaTime;
		}
		if (ChangeOptTime >= ChangeOptMax) {
			bChangeOpt = true;
			ChangeOptTime = 0;
		}
		Vert = inputDevice.Direction.Y;
		Hor = inputDevice.Direction.X;
		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow)) {
			Hor = -1;
		}
		if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)) {
			Hor = 1;
		}
		if ((Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.D)) || (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.RightArrow))) {
			Hor = 0;
		}
		if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.UpArrow)) {
			Vert = -1;
		}
		if (Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.DownArrow)) {
			Vert = 1;
		}
		if ((Input.GetKey (KeyCode.W) && Input.GetKey (KeyCode.S)) || (Input.GetKey (KeyCode.UpArrow) && Input.GetKey (KeyCode.DownArrow))) {
			Vert = 0;
		}
		if(bPressStart && !bFading){
			MMCanvas.SetActive(false);
			PressStartCanv.SetActive(true);
			if(inputDevice.MenuWasPressed  || inputDevice.Action1.WasPressed || inputDevice.Action2.WasPressed || inputDevice.Action3.WasPressed || inputDevice.Action4.WasPressed || Input.GetKeyDown("return") || Input.GetKeyDown("escape") || Input.GetKeyDown("space") || Input.GetKeyDown(KeyCode.LeftControl)|| Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown("e") || Input.GetKeyDown("r"))
            {
				Sonidos.GetComponent<AudioSource>().PlayOneShot(ButtonSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
				bPressStart = false;
				bChangeOpt = false;
				MoveTo (4);
                bShowButtons = false;
                PressStartCanv.SetActive(false);
                for (int f = 1; f < Flames.Length;f ++){
					Flames[f].SetActive(false);
				}
				GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().CheckSteamAchievements ();
				Invoke ("SacarLogros", 0.2f);
				//Flames[MenuOpt].SetActive(true);
			}
		}
		if (!bOptions && !bPressStart && !bExit && !bControls && !bExtras && !bLogros && !bConcepts && !bHojas && !bShowButtons) {
			if (PlayerPrefs.GetInt ("Save1") == 1) {
				MenuOpt = 2;
				Continue.SetActive(false);
			}
		}
		if (!bOptions && !bPressStart && !bExit && !bControls && !bExtras && !bLogros && !bConcepts && !bHojas && bShowButtons && !bSelected){
			for (int f = 1; f < Flames.Length;f ++){
				if(MenuOpt > 0 && MenuOpt <= 6){
					if (f == MenuOpt && !bMove && bShowButtons)
                    {
						Flames[f].SetActive(true);
					}
					if (f != MenuOpt && !bMove && bShowButtons)
                    {
						Flames[f].SetActive(false);
					}
				}
			}
			if (Vert > 0.7f && bChangeOpt) {
				MenuOpt++;
				Levels = 1;
				bChangeOpt = false;
				if (MenuOpt == 3 && LockOptions) {
					MenuOpt++;
				}
				if (MenuOpt == 4 && LockExtras) {
					MenuOpt++;
				}
				if (MenuOpt == 5 && LockLevelSel) {
					MenuOpt++;
				}
			}
			if (Vert < -0.7f && bChangeOpt) {
				MenuOpt--;
				bChangeOpt = false;
				if (MenuOpt == 5 && LockLevelSel) {
					MenuOpt--;
				}
				if (MenuOpt == 4 && LockExtras) {
					MenuOpt--;
				}
				if (MenuOpt == 3 && LockOptions) {
					MenuOpt--;
				}
			}
			if (MenuOpt > 6) {
				MenuOpt = 6;
			}
			if (PlayerPrefs.GetInt ("Save1") != 1){
				if (MenuOpt < 1) {
					MenuOpt = 1;
				}
			}
			if (PlayerPrefs.GetInt ("Save1") == 1){
				if (MenuOpt < 2) {
					MenuOpt = 2;
				}
			}
			MMCanvas.SetActive(true);
			if (MenuOpt == 1) {
				Continue.GetComponent<RectTransform> ().localScale = new Vector3 (0.9f, 0.9f, 1.25f);
			}
			if (MenuOpt != 1) {
				Continue.GetComponent<Image>().sprite = ContinueA[Language];
				Continue.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1);
			}
			if (MenuOpt == 2) {
				NewGame.GetComponent<RectTransform> ().localScale = new Vector3 (0.9f, 0.9f, 1.25f);
			}
			if (MenuOpt != 2) {
				NewGame.GetComponent<Image>().sprite = NewgameA[Language];
				NewGame.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1);
			}
			if (MenuOpt == 3) {
				Options.GetComponent<RectTransform> ().localScale = new Vector3 (0.9f, 0.9f, 1.25f);
			}
			if (MenuOpt != 3) {
				Options.GetComponent<Image>().sprite = OptionsA[Language];
				Options.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1);
			}
			if (MenuOpt == 4) {
				Credits.GetComponent<RectTransform> ().localScale = new Vector3 (0.9f, 0.9f, 1);
			}
			if (MenuOpt != 4) {
				Credits.GetComponent<Image>().sprite = ExtrasA[Language];
				Credits.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1);
				//LevelN.SetActive(false);
			}
			if (MenuOpt == 5) {
				SelectLevel.GetComponent<RectTransform> ().localScale = new Vector3 (0.8f, 0.8f, 1);
				/*HojasFlames [1].SetActive (false);
				HojasFlames [2].SetActive (true);
				LevelN.SetActive(true);
				LevelN.GetComponent<Image>().sprite = Levelnum[Levels];
				if (LevelFinish[Levels] <= 1){
					LevelN.GetComponent<Image>().color = Color.red;
				}
				if (LevelFinish[Levels] == 2){
					LevelN.GetComponent<Image>().color = Color.yellow;
				}
				if (LevelFinish[Levels] == 3){
					LevelN.GetComponent<Image>().color = Color.green;
				}
				if (LevelFinish[Levels] >= 4){
					LevelN.GetComponent<Image>().color = Color.blue;
				}
				if (LevelFinish[Levels] >= 5){
					LevelN.GetComponent<Image>().color = Color.black;
				}
				if (LevelFinish[Levels] >= 6){
					LevelN.GetComponent<Image>().color = Color.magenta;
				}
				if (Hor > 0.7f && bChangeOpt) {
					Levels++;
					if (Levels >= 18){
						Levels = 18;
					}
					bChangeOpt = false;
				}
				if (Hor < -0.7f && bChangeOpt) {
					Levels--;
					if (Levels <= 1){
						Levels = 1;
					}
					bChangeOpt = false;
				}*/
				if ((inputDevice.Action1.WasPressed || Input.GetKeyDown ("return") || Input.GetKeyDown("space")) && bChangeOpt == true) {
					
				}
			}
			if (MenuOpt != 5) {
				LevelN.SetActive(false);
				SelectLevel.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1);
			}
			if (MenuOpt == 6) {
				Exit.GetComponent<RectTransform> ().localScale = new Vector3 (0.9f, 0.9f, 1.25f);
			}
			if (MenuOpt != 6) {
				Exit.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1);
			}
			if ((inputDevice.Action1.WasPressed || Input.GetKeyDown("return") || Input.GetKeyDown("space")) && bChangeOpt == true) {
				if (MenuOpt == 1){
					bSelected = true;
					Sonidos.GetComponent<AudioSource>().PlayOneShot(StartGame, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
					if (PlayerPrefs.GetInt("Save1") != 1){
						LoadCheckpoint(PlayerPrefs.GetInt("Save1"));
					}
				}
				if (MenuOpt == 2){
					bSelected = true;
					Sonidos.GetComponent<AudioSource>().PlayOneShot(StartGame, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
					if (PlayerPrefs.GetInt ("Save1") > 1) {						
						MoveTo (7);
						bExit = true;
						NGame = true;
						YesoNo = 2;
						LoseProg.SetActive (true);
						bChangeOpt = false;
						bShowButtons = false;
						MMCanvas.SetActive(false);
					}
					if (PlayerPrefs.GetInt ("Save1") <= 1) {
						PlayerPrefs.SetInt("Save1",1);
						GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Antaño (true);
						FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = true;
						FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_Camera").Value = LastPositionCamera[1];
						FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_CheckPoint").Value = LastPositionCheckpoint[1];
						PlayerPrefs.SetInt ("IgnoredWolf", 0);
						StartCoroutine ("LevelLoadingNewGame");
						//StartCoroutine ("LevelLoading", 2);
					}
				}
				if (MenuOpt == 3){
					Sonidos.GetComponent<AudioSource>().PlayOneShot(ButtonSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
					MoveTo (3);
					bOptions = true;
					MenuOpt = 1;
                    bChangeOpt = false;
                    bShowButtons = false;
                    MMCanvas.SetActive(false);
                }
				if (MenuOpt == 4){
					Sonidos.GetComponent<AudioSource>().PlayOneShot(ButtonSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
					//GoExtras
					MoveTo(5);
					MenuOpt = 1;
					bChangeOpt = false;
					bExtras = true;
					bShowButtons = false;
					MMCanvas.SetActive(false);
				}
				if (MenuOpt == 5) {
					bSelected = true;
					Sonidos.GetComponent<AudioSource>().PlayOneShot(StartGame, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
					StartCoroutine ("SelLevel");
					GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().SelLev = true;
					/*if (Levels == 1){
						FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = true;
						FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_Camera").Value = LastPositionCamera[1];
						FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_CheckPoint").Value = LastPositionCheckpoint[1];
						StartCoroutine ("LevelLoading", 2);
					}
					if (Levels == 2){
						LoadCheckpoint(4);
						PlayerPrefs.SetInt ("Save1", 4);
					}
					if (Levels == 3){
						LoadCheckpoint(8);
						PlayerPrefs.SetInt ("Save1", 8);
					}
					if (Levels == 4){
						LoadCheckpoint(12);
						PlayerPrefs.SetInt ("Save1", 12);
					}
					if (Levels == 5){
						LoadCheckpoint(16);
						PlayerPrefs.SetInt ("Save1", 16);
					}
					if (Levels == 6){
						LoadCheckpoint(20);
						PlayerPrefs.SetInt ("Save1", 20);
					}
					if (Levels == 7){
						LoadCheckpoint(24);
						PlayerPrefs.SetInt ("Save1", 24);
					}
					if (Levels == 8){
						LoadCheckpoint(28);
						PlayerPrefs.SetInt ("Save1", 28);
					}
					if (Levels == 9){
						LoadCheckpoint(32);
						PlayerPrefs.SetInt ("Save1", 32);
					}
					if (Levels == 10){
						LoadCheckpoint(36);
						PlayerPrefs.SetInt ("Save1", 36);
					}
					if (Levels == 11){
						LoadCheckpoint(40);
						PlayerPrefs.SetInt ("Save1", 40);
					}
					if (Levels == 12){
						LoadCheckpoint(44);
						PlayerPrefs.SetInt ("Save1", 44);
					}
					if (Levels == 13){
						LoadCheckpoint(11);
						PlayerPrefs.SetInt ("Save1", 11);
					}
					if (Levels == 14){
						LoadCheckpoint(18);
						PlayerPrefs.SetInt ("Save1", 18);
					}
					if (Levels == 15){
						LoadCheckpoint(31);
						PlayerPrefs.SetInt ("Save1", 44);
					}
					if (Levels == 16){
						LoadCheckpoint(38);
						PlayerPrefs.SetInt ("Save1", 44);
					}
					if (Levels == 17){
						LoadCheckpoint(41);
						PlayerPrefs.SetInt ("Save1", 44);
					}
					if (Levels == 18){
						LoadCheckpoint(45);
						PlayerPrefs.SetInt ("Save1", 44);
					}*/
				}
				if (MenuOpt == 6){
					Sonidos.GetComponent<AudioSource>().PlayOneShot(ButtonSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
					MoveTo (7);
					bExit = true;
					Exiting = true;
					LoseProg.SetActive (false);
					YesoNo = 1;
					bChangeOpt = false;
                    bShowButtons = false;
                    MMCanvas.SetActive(false);
                }
			}
		}
		if (bOptions && bShowButtons){
            for (int f = 1; f < OptFlames.Length; f++)
            {
                if (MenuOpt > 0 && MenuOpt <= 8)
                {
                    if (f == MenuOpt && !bMove && bShowButtons)
                    {
                        OptFlames[f].SetActive(true);
                    }
                    if (f != MenuOpt && !bMove && bShowButtons)
                    {
                       OptFlames[f].SetActive(false);
                    }
                }
            }
            if (Vert > 0.7f && bChangeOpt) {
				MenuOpt++;
				bChangeOpt = false;
			}
			if (Vert < -0.7f && bChangeOpt) {
				MenuOpt--;
				bChangeOpt = false;
			}
			if (MenuOpt > 8) {
				MenuOpt = 8;
			}
			if (MenuOpt < 1) {
				MenuOpt = 1;
			}
			OptCanvas.SetActive(true);
			LanguageSpriteChanger.GetComponent<Image>().sprite = LangSprite[Language];
			ContBanner.GetComponent<Image>().sprite = Cont[PlayerPrefs.GetInt("Controls")];
			MusicVolumeSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("MusicVolume");
			EffectVolumeSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("EffectVolume");
			FullScreenBanner.GetComponent<Image>().sprite = FullScreenA[Language];
			if (bFullScreen) {
				FullScreen.GetComponent<Image>().sprite = SiA[Language];
			}
			if (!bFullScreen) {
				FullScreen.GetComponent<Image>().sprite = NoA[Language];
			}
			if (PlayerPrefs.GetInt("Qual") == 1){
				QualityBanner.GetComponent<Image>().sprite = QualityLow[Language];
			}
			if (PlayerPrefs.GetInt("Qual") == 2){
				QualityBanner.GetComponent<Image>().sprite = QualityMedium[Language];
			}
			if (PlayerPrefs.GetInt("Qual") == 3){
				QualityBanner.GetComponent<Image>().sprite = QualityHigh[Language];
			}
			if (PlayerPrefs.GetInt("Dificultad") == 1){
				OptDificultad.GetComponent<Image>().sprite = Facil[Language];
			}
			if (PlayerPrefs.GetInt("Dificultad") == 2){
				OptDificultad.GetComponent<Image>().sprite = Normal[Language];
			}
			if (PlayerPrefs.GetInt("Dificultad") == 3){
				OptDificultad.GetComponent<Image>().sprite = Dificil[Language];
			}
			if (MenuOpt == 1) {
				OptEffVol.GetComponent<Image>().sprite = EffectVolA[Language];
				OptEffVol.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1);
				if (Hor > 0.7f && bChangeOpt) {
					float M = PlayerPrefs.GetFloat("EffectVolume") + 0.1f;
					if (M >= 1){
						M = 1;
					}
					Debug.Log ("EffectVolume " + M);
					PlayerPrefs.SetFloat("EffectVolume",(M));
					FsmVariables.GlobalVariables.FindFsmFloat("FX_Volume").Value = PlayerPrefs.GetFloat("EffectVolume");
					bChangeOpt = false;
				}
				if (Hor < -0.7f && bChangeOpt) {
					float M = PlayerPrefs.GetFloat("EffectVolume") - 0.1f;
					if (M <= 0){
						M = 0;
					}
					Debug.Log ("EffectVolume " + M);
					PlayerPrefs.SetFloat("EffectVolume",(M));
					FsmVariables.GlobalVariables.FindFsmFloat("FX_Volume").Value = PlayerPrefs.GetFloat("EffectVolume");
					bChangeOpt = false;
				}
			}
			if (MenuOpt != 1) {
				OptEffVol.GetComponent<Image>().sprite = EffectVolA[Language];
				OptEffVol.GetComponent<RectTransform> ().localScale = new Vector3 (0.5f, 0.5f, 1f);
            }
			if (MenuOpt == 2) {
				OptMusVol.GetComponent<Image>().sprite = MusicVolA[Language];
				OptMusVol.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1.25f);
				if (Hor > 0.7f && bChangeOpt) {
					float M = PlayerPrefs.GetFloat("MusicVolume") + 0.1f;
					if (M >= 1){
						M = 1;
					}
					Debug.Log ("Music volume " + M);
					PlayerPrefs.SetFloat("MusicVolume",(M));
					FsmVariables.GlobalVariables.FindFsmFloat("Music_Volume").Value = PlayerPrefs.GetFloat("MusicVolume");
					bChangeOpt = false;
				}
				if (Hor < -0.7f && bChangeOpt) {
					float M = PlayerPrefs.GetFloat("MusicVolume") - 0.1f;
					if (M <= 0){
						M = 0;
					}
					Debug.Log ("Music volume " + M);
					PlayerPrefs.SetFloat("MusicVolume",(M));
					FsmVariables.GlobalVariables.FindFsmFloat("Music_Volume").Value = PlayerPrefs.GetFloat("MusicVolume");
					bChangeOpt = false;
				}
			}
			if (MenuOpt != 2) {
			    OptMusVol.GetComponent<Image>().sprite = MusicVolA[Language];
				OptMusVol.GetComponent<RectTransform> ().localScale = new Vector3 (0.5f, 0.5f, 1f);
            }
			if (MenuOpt == 3) {
				//OptLang.GetComponent<Image>().sprite = LanguageB[Language];
				OptLang.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1);
				if (Hor > 0.7f && bChangeOpt) {
					Language++;
					if (Language >= LangSprite.Length){
						Language = 1;
					}
					PlayerPrefs.SetInt("Lang",Language);
					bChangeOpt = false;
                    ChangeLanguaje();
                }
				if (Hor < -0.7f && bChangeOpt) {
					Language--;
					if (Language < 1){
						Language = LangSprite.Length - 1;
					}
					PlayerPrefs.SetInt("Lang",Language);
					bChangeOpt = false;
                    ChangeLanguaje();
				}
			}
			if (MenuOpt != 3) {
				OptLang.GetComponent<Image>().sprite = LanguageA[Language];
				OptLang.GetComponent<RectTransform> ().localScale = new Vector3 (0.5f, 0.5f, 1);
            }
			if (MenuOpt == 4) {
				//Quality.GetComponent<Image>().sprite = QualityB[Language];
				Quality.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1f);
				if (Hor > 0.7f && bChangeOpt) {
					int Q = PlayerPrefs.GetInt("Qual");
					Q++;
					if (Q >= PlayerPrefs.GetInt ("MaxRes")){
						Q = PlayerPrefs.GetInt ("MaxRes");
					}
					if (Q >= 3){
						Q = 3;
					}
					PlayerPrefs.SetInt("Qual",Q);
					ChangeQuality(Q);
					bChangeOpt = false;
				}
				if (Hor < -0.7f && bChangeOpt) {
					int Q = PlayerPrefs.GetInt("Qual");
					Q--;
					if (Q <= 1){
						Q = 1;
					}
					PlayerPrefs.SetInt("Qual",Q);
					ChangeQuality(Q);
					bChangeOpt = false;
				}
			}
			if (MenuOpt != 4) {
				Quality.GetComponent<Image>().sprite = QualityA[Language];
				Quality.GetComponent<RectTransform> ().localScale = new Vector3 (0.5f, 0.5f, 1);
            }
			if (MenuOpt != 5) {
				FullScreenBanner.GetComponent<Image>().sprite = FullScreenA[Language];
				FullScreenBanner.GetComponent<RectTransform> ().localScale = new Vector3 (0.5f, 0.5f, 1);
			}
			if (MenuOpt == 5) {
				//Controls.GetComponent<Image>().sprite = ControlsB[Language];
				FullScreenBanner.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1.25f);
				if (Hor > 0.7f && bChangeOpt) {
					bool F = Screen.fullScreen;
					if (F) {
						Screen.fullScreen = !Screen.fullScreen;
						bFullScreen = false;
					}
					if (!F) {
						Screen.fullScreen = !Screen.fullScreen;
						bFullScreen = true;
					}
					bChangeOpt = false;
					Debug.Log ("Fullscreen: " + Screen.fullScreen + " " + bFullScreen);
				}
				if (Hor < -0.7f && bChangeOpt) {
					bool F = Screen.fullScreen;
					if (F) {
						Screen.fullScreen = !Screen.fullScreen;
						bFullScreen = false;
					}
					if (!F) {
						Screen.fullScreen = !Screen.fullScreen;
						bFullScreen = true;
					}
					bChangeOpt = false;
					Debug.Log ("Fullscreen: " + Screen.fullScreen + " " + bFullScreen);
				}

			}
			if (MenuOpt != 6) {
				Controls.GetComponent<Image>().sprite = ControlsA[Language];
				Controls.GetComponent<RectTransform> ().localScale = new Vector3 (0.5f, 0.5f, 1);
			}
			if (MenuOpt == 6) {
				//Controls.GetComponent<Image>().sprite = ControlsB[Language];
				Controls.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1.25f);
			}
			if (MenuOpt == 7) {
				OptDifBanner.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1.25f);
				//OptExit.GetComponent<Image>().sprite = BackB[Language];
				if (Hor > 0.7f && bChangeOpt) {
					int Q = PlayerPrefs.GetInt("Dificultad");
					Q++;
					if (Q >= 3){
						Q = 3;
					}
					PlayerPrefs.SetInt("Dificultad",Q);
					bChangeOpt = false;
				}
				if (Hor < -0.7f && bChangeOpt) {
					int Q = PlayerPrefs.GetInt("Dificultad");
					Q--;
					if (Q <= 1){
						Q = 1;
					}
					PlayerPrefs.SetInt("Dificultad",Q);
					bChangeOpt = false;
				}
			}
			if (MenuOpt != 7) {
				OptDifBanner.GetComponent<Image>().sprite = DifBanner[Language];
				OptDifBanner.GetComponent<RectTransform> ().localScale = new Vector3 (0.5f, 0.5f, 1);
			}
			if (MenuOpt == 8) {
				OptExit.GetComponent<RectTransform> ().localScale = new Vector3 (0.7f, 0.7f, 1.25f);
				//OptExit.GetComponent<Image>().sprite = BackB[Language];
			}
			if (MenuOpt != 8) {
				OptExit.GetComponent<Image>().sprite = BackA[Language];
				OptExit.GetComponent<RectTransform> ().localScale = new Vector3 (0.5f, 0.5f, 1);
			}
			if ((inputDevice.Action1.WasPressed || Input.GetKeyDown("return") || Input.GetKeyDown("space")) && bChangeOpt) {
				if (MenuOpt == 1){
				}
				if (MenuOpt == 2){
				}
				if (MenuOpt == 6){
					Sonidos.GetComponent<AudioSource>().PlayOneShot(ButtonSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
					MoveTo (6);
					bOptions = false;
					bControls = true;
                    bShowButtons = false;
					bChangeOpt = false;
					MenuOpt = 1;
                    OptCanvas.SetActive(false);
                }
                if (MenuOpt == 8){
					Sonidos.GetComponent<AudioSource>().PlayOneShot(ButtonSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
                    bChangeOpt = false;
                    bShowButtons = false;
                    MoveTo (4);
					bOptions = false;					
					MenuOpt = 1;
                    OptCanvas.SetActive(false);
                }
			}
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("escape") || Input.GetKeyDown(KeyCode.LeftControl)) && bChangeOpt){
				Sonidos.GetComponent<AudioSource>().PlayOneShot(BackSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
                bChangeOpt = false;
                bShowButtons = false;
                MoveTo(4);
                bOptions = false;
                MenuOpt = 1;
                OptCanvas.SetActive(false);
            }
         }
		if (bControls) {
			if (bShowButtons) {
				ContArrows.SetActive (true);
			}
			if (!bMando) {
				ContLetter.GetComponent<Image> ().enabled = false;
			}
			if (bMando) {
				ContLetter.GetComponent<Image> ().enabled = true;
			}
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("escape") || Input.GetKeyDown(KeyCode.LeftControl)) && bChangeOpt) {
				Sonidos.GetComponent<AudioSource>().PlayOneShot(BackSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
				MoveTo (3);
				bOptions = true;
				bControls = false;
                LeftA.SetActive(true);
                RightA.SetActive(true);
                ContArrows.SetActive(false);
                MenuOpt = 1;
			}
			if (Hor > 0.7f && bChangeOpt && bMando) {
				int C = PlayerPrefs.GetInt("Controls");
				C++;
				if (C >= 3){
					C = 3;
				}
				PlayerPrefs.SetInt("Controls",C);
				ContBanner.GetComponent<Image>().sprite = Cont[PlayerPrefs.GetInt("Controls")];
				bChangeOpt = false;
			}
			if (Hor < -0.7f && bChangeOpt && bMando) {
				int C = PlayerPrefs.GetInt("Controls");
				C--;
				if (C <= 1){
					C = 1;
				}
				PlayerPrefs.SetInt("Controls",C);
				ContBanner.GetComponent<Image>().sprite = Cont[PlayerPrefs.GetInt("Controls")];
				bChangeOpt = false;
			}
			if (bMando) {
				if (PlayerPrefs.GetInt ("Controls") == 1) {
					ContCanvas.GetComponent<Image>().sprite = Conf1[Language];
					ContLetter.GetComponent<Image>().sprite = Cont[1];
					LeftA.SetActive(false);
					RightA.SetActive(true);
				}
				if (PlayerPrefs.GetInt ("Controls") == 2) {
					ContCanvas.GetComponent<Image>().sprite = Conf2[Language];
					ContLetter.GetComponent<Image>().sprite = Cont[2];
					LeftA.SetActive(true);
					RightA.SetActive(true);
				}
				if (PlayerPrefs.GetInt ("Controls") == 3) {
					ContCanvas.GetComponent<Image>().sprite = Conf3[Language];
					ContLetter.GetComponent<Image>().sprite = Cont[3];
					RightA.SetActive(false);
					LeftA.SetActive(true);
				}
			}
			if (!bMando) {
				ContCanvas.GetComponent<Image>().sprite = Teclado[Language];
				LeftA.SetActive(false);
				RightA.SetActive(false);
			}
		}
        if (bLogros) {
            for (int A = 0; A <= 3; A++)
            {
                IconsLogr[A].GetComponent<Image>().sprite = LogrosIcon[(4 * (LogrosPage - 1) + A)];
                TextLogr[A].GetComponent<Text>().text = LogroText[(4 * (LogrosPage - 1) + A)];
				if (Logros[(A +(4*(LogrosPage-1)))] == 0) {
					IconsLogr[A].GetComponent<Image>().sprite = LogrosIcon[20];
					if (Language == 1)
					{
						TextLogr[A].GetComponent<Text>().text = "Locked";
					}
					if (Language == 2)
					{
						TextLogr[A].GetComponent<Text>().text = "Bloqueado";
					}
					if (Language == 3)
					{
						TextLogr[A].GetComponent<Text>().text = "Locked";
					}
					if (Language == 4)
					{
						TextLogr[A].GetComponent<Text>().text = "Locked";
					}
                    
                }
            }
            if (bShowButtons)
            {
                ContArrows.SetActive(true);
            }
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("escape") || Input.GetKeyDown(KeyCode.LeftControl)) && bChangeOpt)
            {
				Sonidos.GetComponent<AudioSource>().PlayOneShot(BackSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
                //Debug.Log("Back from Logros!");
                MoveTo(5);
                bExtras = true;
                bLogros = false;
				bShowButtons = false;
                LeftA.SetActive(true);
                RightA.SetActive(true);
                ContArrows.SetActive(false);
                bChangeOpt = false;
                MenuOpt = 1;
            }
            if (Hor > 0.7f && bChangeOpt)
            {
                LeftA.SetActive(true);
                RightA.SetActive(true);
                int P = LogrosPage;
                P++;
                if (P >= 5)
                {
                    P = 5;
                    RightA.SetActive(false);
                }
                LogrosPage = P;
                bChangeOpt = false;
            }
            if (Hor < -0.7f && bChangeOpt)
            {
                LeftA.SetActive(true);
                RightA.SetActive(true);
                int P = LogrosPage;
                P--;
                if (P <= 1)
                {
                    P = 1;
                    LeftA.SetActive(false);
                }
                LogrosPage = P;
                bChangeOpt = false;
            }
        }
		//Exit
		if(bExit){
			if (YesoNo == 1 && bShowButtons && bChangeOpt && !bMove){
                //ExitCanvas.SetActive(true);
				Si.GetComponent<RectTransform> ().localScale = new Vector3 (1.5f, 1.5f, 1);
				No.GetComponent<RectTransform> ().localScale = new Vector3 (1f, 1f, 1);
				ExitFlames[0].transform.Rotate(0,0,4f*Time.timeScale);
                //ExitFlames[1].SetActive(false);
            }
			if (YesoNo == 2 && bShowButtons && bChangeOpt && !bMove){
                //ExitCanvas.SetActive(true);
				No.GetComponent<RectTransform> ().localScale = new Vector3 (1.5f, 1.5f, 1);
				Si.GetComponent<RectTransform> ().localScale = new Vector3 (1f, 1f, 1);
                //ExitFlames[0].SetActive(false);
				ExitFlames[1].transform.Rotate(0,0,4f*Time.timeScale);
            }
			if ((inputDevice.Action1.WasPressed || Input.GetKeyDown("return") || Input.GetKeyDown("space")) && bChangeOpt) {
				Sonidos.GetComponent<AudioSource>().PlayOneShot(ButtonSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
				if (YesoNo == 1) {
					if (Exiting) {
						SteamAPI.Shutdown ();
						Application.Quit();
						Debug.Log ("Exited");
					}
					if (NGame) {
						PlayerPrefs.SetInt("Save1",1);
						GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Antaño (true);
						FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = true;
						FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_Camera").Value = LastPositionCamera[1];
						FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_CheckPoint").Value = LastPositionCheckpoint[1];
						PlayerPrefs.SetInt ("IgnoredWolf", 0);
						PlayerPrefs.SetInt("DeathsLevel",0);
						PlayerPrefs.SetString("Secrets","000000");
						StartCoroutine ("LevelLoadingNewGame");
						//StartCoroutine ("LevelLoading", 2);
					}
				}
				if (YesoNo == 2) {
					if (NGame) {
						bSelected = false;
					}
					MoveTo(4);
					bChangeOpt = false;
					bExit = false;
					MenuOpt = 1;
					Debug.Log ("Back");
                    YesoNo = 1;
                    ExitCanvas.SetActive(false);
					Exiting = false;
					NGame = false;
                }
			}	
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("escape") || Input.GetKeyDown(KeyCode.LeftControl))) {
				Sonidos.GetComponent<AudioSource>().PlayOneShot(BackSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
				if (NGame) {
					bSelected = false;
				}
                MoveTo(4);
                bChangeOpt = false;
                bExit = false;
                MenuOpt = 1;
                Debug.Log("Back");
                YesoNo = 1;
                ExitCanvas.SetActive(false);
            }
			if (Hor > 0.7f && bChangeOpt) {
				if (YesoNo == 1) {
					YesoNo = 2;
				}
				bChangeOpt = false;
			}
			if (Hor < -0.7f && bChangeOpt) {
				if (YesoNo == 2) {
					YesoNo = 1;
				}
				bChangeOpt = false;
			}
		} 
		//Extras
		if (bExtras && bShowButtons)
        {
            for (int f = 1; f < ExtrasFlames.Length; f++)
            {
                if (MenuOpt > 0 && MenuOpt <= 5)
                {
                    if (f == MenuOpt && !bMove && bShowButtons)
                    {
                        ExtrasFlames[f].SetActive(true);
                    }
                    if (f != MenuOpt && !bMove && bShowButtons)
                    {
                        ExtrasFlames[f].SetActive(false);
                    }
                }
            }
            if (Vert > 0.7f && bChangeOpt)
            {
                MenuOpt++;
                bChangeOpt = false;
            }
            if (Vert < -0.7f && bChangeOpt)
            {
                MenuOpt--;
                bChangeOpt = false;
            }
            if (MenuOpt > 5)
            {
                MenuOpt = 5;
            }
            if (MenuOpt < 1)
            {
                MenuOpt = 1;
            }
            ExtrasCanvas.SetActive(true);
			if (MenuOpt == 1) {
				ExtraLogros.transform.localScale = new Vector3 (0.9f, 0.9f, 1);
			}
			if (MenuOpt == 2) {
				ExtraImágenes.transform.localScale = new Vector3 (0.9f, 0.9f, 1);
			}
			if (MenuOpt == 3) {
				ExtraHojas.transform.localScale = new Vector3 (0.9f, 0.9f, 1);
			}
			if (MenuOpt == 4) {
				Créditos.transform.localScale = new Vector3 (0.9f, 0.9f, 1);
			}
			if (MenuOpt == 5) {
				ExtraBack.transform.localScale = new Vector3 (0.9f, 0.9f, 1);
			}
            if (MenuOpt != 1)
            {
                ExtraLogros.GetComponent<Image>().sprite = LogrosA[Language];
				ExtraLogros.transform.localScale = new Vector3 (0.7f, 0.7f, 1);
            }
            if (MenuOpt != 2)
            {
                ExtraImágenes.GetComponent<Image>().sprite = ImágenesA[Language];
				ExtraImágenes.transform.localScale = new Vector3 (0.7f, 0.7f, 1);
            }
			if (MenuOpt != 3)
			{
				ExtraHojas.GetComponent<Image>().sprite = HojasA[Language];
				ExtraHojas.transform.localScale = new Vector3 (0.7f, 0.7f, 1);
			}
            if (MenuOpt != 4)
            {
                Créditos.GetComponent<Image>().sprite = CréditosA[Language];
				Créditos.transform.localScale = new Vector3 (0.7f, 0.7f, 1);
            }
            if (MenuOpt != 5)
            {
                ExtraBack.GetComponent<Image>().sprite = BackA[Language];
				ExtraBack.transform.localScale = new Vector3 (0.7f, 0.7f, 1);
            }
			if ((inputDevice.Action1.WasPressed || Input.GetKeyDown("return") || Input.GetKeyDown("space")) && bChangeOpt)
            {
				Sonidos.GetComponent<AudioSource>().PlayOneShot(ButtonSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
                if (MenuOpt == 1)
                {
                    bChangeOpt = false;
                    bShowButtons = false;
                    MoveTo(2);
                    bExtras = false;
                    bLogros = true;
                    LogrosPage = 1;
                    LeftA.SetActive(false);
                    MenuOpt = 1;
                    ExtrasCanvas.SetActive(false);
                }
                if (MenuOpt == 2)
                {
                    bChangeOpt = false;
                    bShowButtons = false;
                    MoveTo(8);
                    bExtras = false;
                    bConcepts = true;
                    LeftA.SetActive(false);
                    MenuOpt = 1;
                    ExtrasCanvas.SetActive(false);
                }
                if (MenuOpt == 4)
                {
                    FadeOut();
                    Invoke("LoadCredits", 2);
                }
                if (MenuOpt == 3)
                {
                    bChangeOpt = false;
                    bShowButtons = false;
                    MoveTo(9);
                    bExtras = false;
                    bHojas = true;
					ExtrasCanvas.SetActive(false);
                    LeftA.SetActive(false);
                    MenuOpt = 1;
					MenuOpt2 = 1;
					Levels = 1;
					LevelN.GetComponent<Image>().sprite = Levelnum[Levels];
					GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().SetPapiros (PapirHojas, LogroText);
					for (int h = 0; h < 7; h++) {
						Pestañas [h].SetActive (true);
						PestañasNum [h] = h-2;
					}
                }
                if (MenuOpt == 5)
                {
                    bChangeOpt = false;
                    bShowButtons = false;
                    MoveTo(4);
                    bExtras = false;
                    MenuOpt = 1;
                    ExtrasCanvas.SetActive(false);
                }
            }
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("escape") || Input.GetKeyDown(KeyCode.LeftControl)) && bChangeOpt) {
				//Debug.Log ("Back from extras!");
				Sonidos.GetComponent<AudioSource>().PlayOneShot(BackSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
				MoveTo(4);
				bChangeOpt = false;
				bExtras = false;
				MenuOpt = 1;
                ExtrasCanvas.SetActive(false);
            }
		}
        //Concepts
        if (bConcepts){
            if (bShowButtons)
            {
                ContArrows.SetActive(true);
            }
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("escape") || Input.GetKeyDown(KeyCode.LeftControl)) && bChangeOpt)
            {
				Sonidos.GetComponent<AudioSource>().PlayOneShot(BackSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
                //Debug.Log("Back!");
                MoveTo(5);
                bExtras = true;
                bConcepts = false;
                LeftA.SetActive(true);
                RightA.SetActive(true);
                ContArrows.SetActive(false);
                bChangeOpt = false;
                MenuOpt = 1;
            }
            if (Hor > 0.7f && bChangeOpt)
            {
                LeftA.SetActive(true);
                RightA.SetActive(true);
                int C = MenuOpt;
                C++;
                if (C >= 35)
                {
                    C = 35;
                    RightA.SetActive(false);
                }
                MenuOpt = C;
                bChangeOpt = false;
            }
            if (Hor < -0.7f && bChangeOpt)
            {
                LeftA.SetActive(true);
                RightA.SetActive(true);
                int C = MenuOpt;
                C--;
                if (C <= 1)
                {
                    C = 1;
                    LeftA.SetActive(false);
                }
                MenuOpt = C;
                bChangeOpt = false;
            }
            Concepts.GetComponent<Image>().sprite = ConceptSprite[49];
            if(MenuOpt <= (MaxLevel * 3))
            {
                Concepts.GetComponent<Image>().sprite = ConceptSprite[MenuOpt];
            }
        }
		//Hojas
		if (bHojas) {
			SelectedPest = PestañasNum [3];
			for (int h = 0; h < 7; h++) {
				if (ComprobarHoja(PestañasNum[h])) {
					if (PestañasNum [h] > 0) {
						//Texto.GetComponent<Text> ().text = LogroText [16 + MenuOpt2];
						Pestañas[h].GetComponent<Pergamino>().Set(PestañasNum [h],true);
						Pestañas[h].GetComponentInChildren<Text>().text = LogroText [20 + PestañasNum[h]];
					}
				}
				if (!ComprobarHoja (PestañasNum [h])) {
					Pestañas[h].GetComponent<Pergamino>().Set(PestañasNum [h],false);
					Pestañas[h].GetComponentInChildren<Text>().text = PestañasNum[h] + "";
				}
				if (PestañasNum [h] < 1 || PestañasNum [h] > 60) {
					Pestañas[h].GetComponentInChildren<Text>().text = null;
					Pestañas [h].SetActive (false);
				}
			}
			for (int h = 0; h < 7; h++) {
				if (GoDown) {
					//Debug.Log ("* " + (PestLoc[3].y - Pestañas[3].transform.position.y));
					Pestañas [h].transform.Translate(new Vector3(0,-80f*Time.fixedDeltaTime,0));
					if ((Pestañas[3].transform.position.y - PestLoc[3].y) <= 0.1f) {
						//Debug.Log ("Click!");
						GoUp = false;
						GoDown = false;
						PestMove = true;
						for (int b = 0; b < 7; b++) {
							Pestañas [b].transform.position = PestLoc[b];
						}
					}
				}
				if (GoUp) {					
					Pestañas [h].transform.Translate(new Vector3(0,80f*Time.fixedDeltaTime,0));
					if ((PestLoc[3].y - Pestañas[3].transform.position.y) <= 0.1f) {
						//Debug.Log ("Click!" + (PestLoc[3].y - Pestañas[3].transform.position.y));
						GoUp = false;
						GoDown = false;
						PestMove = true;
						for (int b = 0; b < 7; b++) {
							Pestañas [b].transform.position = PestLoc[b];
						}
					}
				}
			}

			if (bShowButtons)
			{
				if (ContArrows.activeSelf == false) {
					ContArrows.SetActive(true);
				}
				HojasCanvas.SetActive (true);
				if (SelectedPest <= 7 && LeftA.activeSelf == true) {
					LeftA.SetActive(false);
				}
				if (SelectedPest >= 54 && RightA.activeSelf == true) {
					RightA.SetActive(false);
				}
				if (SelectedPest > 7 && LeftA.activeSelf == false) {
					LeftA.SetActive(true);
					LeftA.GetComponent<Image> ().CrossFadeAlpha (RightA.GetComponent<Image> ().color.a, 0, false);
				}
				if (SelectedPest < 54 && RightA.activeSelf == false) {
					RightA.SetActive(true);
					RightA.GetComponent<Image> ().CrossFadeAlpha (LeftA.GetComponent<Image> ().color.a, 0, false);
				}
			}
			if ((inputDevice.Action1.WasPressed || Input.GetKeyDown ("return") || Input.GetKeyDown("space")) && bChangeOpt && !bRead) {
				if (ComprobarHoja (SelectedPest)) {
					//Cabecera.GetComponent<Text> ().text = LogroText [SelectedPest+82];
					string Txt = LogroText [SelectedPest+81];
					Texto.GetComponent<Text> ().text = Txt.Replace(";","\n\n");
					ScrollBar.GetComponent<Scrollbar> ().value = 1;
					FadeInPergamino ();
					//Texto.GetComponent<Text> ().text = LogroText [((SelectedPest-1)*3)+83];
					//TextoDerecha.GetComponent<Text> ().text = LogroText [((SelectedPest-1)*3)+84];
					//Hojas.SetActive (true);
					bRead = true;
				}
			}
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("escape") || Input.GetKeyDown(KeyCode.LeftControl)) && bChangeOpt && !bRead)
			{
				Sonidos.GetComponent<AudioSource>().PlayOneShot(BackSound, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
				MoveTo(5);
				bExtras = true;
				bHojas = false;
				LeftA.SetActive(true);
				RightA.SetActive(true);
				ContArrows.SetActive(false);
				//HojasCanvas.SetActive (false);
				FadeOutPergamino ();
				bChangeOpt = false;
				MenuOpt = 1;
				SelectedPest = 1;
				bRead = false;
			}
			if (Vert > 0.7f && bChangeOpt && PestMove && SelectedPest <= 59 && !bRead) {
				Invoke ("ComprobarPestañas", 0.1f);
				PestMove = false;
				for (int h = 0; h < 7; h++) {
					PestañasNum [h] ++;
				}
				GoDown = false;
				GoUp = true;
				TempPestañas [6] = Pestañas [0];
				TempPestañas [5] = Pestañas [6];
				TempPestañas [4] = Pestañas [5];
				TempPestañas [3] = Pestañas [4];
				TempPestañas [2] = Pestañas [3];
				TempPestañas [1] = Pestañas [2];
				TempPestañas [0] = Pestañas [1];
				Pestañas [0] = TempPestañas [0];
				Pestañas [1] = TempPestañas [1];
				Pestañas [2] = TempPestañas [2];
				Pestañas [3] = TempPestañas [3];
				Pestañas [4] = TempPestañas [4];
				Pestañas [5] = TempPestañas [5];
				Pestañas [6] = TempPestañas [6];
				bChangeOpt = false;
			}
			if (Vert < -0.7f && bChangeOpt && PestMove && SelectedPest >= 2 && !bRead) {
				Invoke ("ComprobarPestañas", 0.1f);
				PestMove = false;
				for (int h = 0; h < 7; h++) {
					PestañasNum [h] --;
				}
				GoUp = false;
				GoDown = true;
				TempPestañas [6] = Pestañas [5];
				TempPestañas [5] = Pestañas [4];
				TempPestañas [4] = Pestañas [3];
				TempPestañas [3] = Pestañas [2];
				TempPestañas [2] = Pestañas [1];
				TempPestañas [1] = Pestañas [0];
				TempPestañas [0] = Pestañas [6];
				Pestañas [0] = TempPestañas [0];
				Pestañas [1] = TempPestañas [1];
				Pestañas [2] = TempPestañas [2];
				Pestañas [3] = TempPestañas [3];
				Pestañas [4] = TempPestañas [4];
				Pestañas [5] = TempPestañas [5];
				Pestañas [6] = TempPestañas [6];
				bChangeOpt = false;
			}
			if (MenuOpt > 2) {
				MenuOpt = 2;
			}
			if (MenuOpt < 1) {
				MenuOpt = 1;
			}
			if (MenuOpt == 1) {
				//HojasFlames [1].SetActive (true);
				//HojasFlames [2].SetActive (false);
				if (Hor > 0.7f && bChangeOpt && SelectedPest < 53 && !bRead)
				{
					for (int h = 0; h < 7; h++) {
						PestañasNum [h] +=7;
					}
					bChangeOpt = false;
					Invoke ("ComprobarPestañas", 0.1f);
				}
				if (Hor < -0.7f && bChangeOpt && SelectedPest > 7 && !bRead)
				{
					for (int h = 0; h < 7; h++) {
						PestañasNum [h] -=7;
					}
					bChangeOpt = false;
					Invoke ("ComprobarPestañas", 0.1f);
				}
			}
			if (bRead) {
				if (ScrollBar.GetComponent<Scrollbar> ().size >= 0.99995f) {
					FlechaAbajo.SetActive (false);
					FlechaArriba.SetActive (false);
				}
				if (ScrollBar.GetComponent<Scrollbar> ().size < 0.99995f && bRead2) {
					if (SelectedPest != 60) {
						if (Vert < -0.7f && bChangeOpt) {
							ScrollBar.GetComponent<Scrollbar> ().value += 0.04f;
						}
						if (Vert > 0.7f && bChangeOpt) {
							ScrollBar.GetComponent<Scrollbar> ().value -= 0.04f;
						}
						if (ScrollBar.GetComponent<Scrollbar> ().value > 0) {
							FlechaAbajo.SetActive (true);
						}
						if (ScrollBar.GetComponent<Scrollbar> ().value == 0) {
							FlechaAbajo.SetActive (false);
						}
						if (ScrollBar.GetComponent<Scrollbar> ().value <= 0.9f) {
							FlechaArriba.SetActive (true);
						}
						if (ScrollBar.GetComponent<Scrollbar> ().value > 0.9f) {
							FlechaArriba.SetActive (false);
						}
					}
					if (SelectedPest == 60) {
						if (Vert < -0.7f && bChangeOpt) {
							ScrollBar.GetComponent<Scrollbar> ().value += 0.005f;
						}
						if (Vert > 0.7f && bChangeOpt) {
							ScrollBar.GetComponent<Scrollbar> ().value -= 0.005f;
						}
						if (ScrollBar.GetComponent<Scrollbar> ().value > 0) {
							FlechaAbajo.SetActive (true);
						}
						if (ScrollBar.GetComponent<Scrollbar> ().value == 0) {
							FlechaAbajo.SetActive (false);
						}
					}
				}
				if ((inputDevice.Action2.WasPressed || Input.GetKeyDown ("escape") || Input.GetKeyDown(KeyCode.LeftControl)) && bChangeOpt) {
					bRead = false;
					bRead2 = false;
					FadeOutPergamino ();
					FlechaAbajo.SetActive (false);
				}			
			}
		}
	}
	public void ChangeQuality(int Quality){
		int Q = Quality;
		if (Q > PlayerPrefs.GetInt ("MaxRes")) {
			Q = PlayerPrefs.GetInt ("MaxRes");
		}
		if (Q == 1){
			QualitySettings.SetQualityLevel(0, true);
			Screen.SetResolution(960,540,bFullScreen);
		}
		if (Q == 2){
			QualitySettings.SetQualityLevel(1, true);
			Screen.SetResolution(1280,720,bFullScreen);
		}
		if (Q == 3){
			QualitySettings.SetQualityLevel(2, true);
			Screen.SetResolution(1920,1080,bFullScreen);
		}
	}
	public void ComprobarPestañas(){
		for (int h = 0; h < 7; h++) {
			if (PestañasNum [h] >= 1 && PestañasNum [h] <= 60) {
				Pestañas [h].SetActive (true);
			}
		}
	}
    public void ChangeLanguaje() {
        Continue.GetComponent<Image>().sprite = ContinueA[Language];
        NewGame.GetComponent<Image>().sprite = NewgameA[Language];
        Options.GetComponent<Image>().sprite = OptionsA[Language];
        Credits.GetComponent<Image>().sprite = ExtrasA[Language];
        Exit.GetComponent<Image>().sprite = ExitA[Language];
		ExtraHojas.GetComponent<Image>().sprite = HojasA[Language];
		SelectLevel.GetComponent<Image>().sprite = SelLevelA[Language];
        Si.GetComponent<Image>().sprite = SiA[Language];
        No.GetComponent<Image>().sprite = NoA[Language];
        OptEffVol.GetComponent<Image>().sprite = EffectVolA[Language];
        OptMusVol.GetComponent<Image>().sprite = MusicVolA[Language];
        OptLang.GetComponent<Image>().sprite = LanguageA[Language];
        Quality.GetComponent<Image>().sprite = QualityA[Language];
		FullScreenBanner.GetComponent<Image>().sprite = FullScreenA[Language];
		if (bFullScreen) {
			FullScreen.GetComponent<Image>().sprite = SiA[Language];
		}
		if (!bFullScreen) {
			FullScreen.GetComponent<Image>().sprite = NoA[Language];
		}
		LoseProg.GetComponent<Image>().sprite = LoseP[Language];
		OptDifBanner.GetComponent<Image>().sprite = DifBanner[Language];
		OptDificultad.GetComponent<Image>().sprite = Normal[Language];
		if (PlayerPrefs.GetInt("Qual") == 1){
			QualityBanner.GetComponent<Image>().sprite = QualityLow[Language];
		}
		if (PlayerPrefs.GetInt("Qual") == 2){
			QualityBanner.GetComponent<Image>().sprite = QualityMedium[Language];
		}
		if (PlayerPrefs.GetInt("Qual") == 3){
			QualityBanner.GetComponent<Image>().sprite = QualityHigh[Language];
		}
        Controls.GetComponent<Image>().sprite = ControlsA[Language];
        Usure.GetComponent<Image>().sprite = Usur[Language];
        ExtraLogros.GetComponent<Image>().sprite = LogrosA[Language];
        ExtraImágenes.GetComponent<Image>().sprite = ImágenesA[Language];
        Créditos.GetComponent<Image>().sprite = CréditosA[Language];
        ExtraBack.GetComponent<Image>().sprite = BackA[Language];
		OptExit.GetComponent<Image>().sprite = BackA[Language];
		LanguageSpriteChanger.GetComponent<Image>().sprite = LangSprite[Language];
		ContBanner.GetComponent<Image>().sprite = Cont[PlayerPrefs.GetInt("Controls")];
		MusicVolumeSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("MusicVolume");
		EffectVolumeSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("EffectVolume");
		PressStartCanv.GetComponentInChildren<Image> ().sprite = PressStartA [Language];
        if (Language == 1)
        {
            LogrosTxt = LogrosEnglish;
        }
        if (Language == 2)
        {
            LogrosTxt = LogrosCastellano;
        }
		if (Language == 3)
		{
			LogrosTxt = LogrosCastellano;
		}
		if (Language == 4)
		{
			LogrosTxt = LogrosCastellano;
		}
		if (LogrosTxt != null)
		{
			LogroText = (LogrosTxt.text.Split('\n'));
			//Debug.Log ("" + CurrentLang[1]);
		}
    }
    public void ResetVariables(){
		FsmVariables.GlobalVariables.FindFsmBool("Item_1").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_2").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_3").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_4").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_5").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Item_6").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Kinematic_Climbs").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_1").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_2").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_3").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_4").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_5").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_6").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_7").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_8").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Misc_9").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("Only_Idle").Value = false;
		FsmVariables.GlobalVariables.FindFsmBool("RandyDirection").Value = true;
		FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = true;
	}
	public void LoadCheckpoint(int Check){
		Debug.Log ("Loading Checkpoint: " + Check);
		FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_Camera").Value = LastPositionCamera[Check];
		FsmVariables.GlobalVariables.FindFsmVector3("Last_Position_CheckPoint").Value = LastPositionCheckpoint[Check];
		//Level1
		if (Check == 2){
			FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_5").Value = false;
			StartCoroutine ("LevelLoading", 2);
		}
		if (Check == 3){
			FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_5").Value = false;
			StartCoroutine ("LevelLoading", 2);
		}
		if (Check == 4){
			ResetVariables();
			StartCoroutine ("LevelLoading", 3);
			FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Ini").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Cinematic").Value = true;
		}
		if (Check == 5){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_1").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_2").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_3").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_4").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Item_1").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Item_2").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Item_3").Value = false;
			StartCoroutine ("LevelLoading", 3);
		}
		if (Check == 6){
			FsmVariables.GlobalVariables.FindFsmBool("Item_1").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Item_2").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Item_3").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_1").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_2").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_3").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_4").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_5").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_6").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_7").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_8").Value = true;
			StartCoroutine ("LevelLoading", 3);
		}
		if (Check == 7){
			StartCoroutine ("LevelLoading", 3);
		}
		if (Check == 8){
			ResetVariables();
			StartCoroutine ("LevelLoading", 4);
		}
		if (Check == 9){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_6").Value = true;
			StartCoroutine ("LevelLoading", 4);
		}
		if (Check == 10){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_1").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_2").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_3").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_4").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_5").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_6").Value = true;
			StartCoroutine ("LevelLoading", 4);
		}
		if (Check == 11){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_1").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_2").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_3").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_4").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_5").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_6").Value = true;
			StartCoroutine ("LevelLoading", 4);
		}
		if (Check == 12){
			ResetVariables();
			StartCoroutine ("LevelLoading", 5);
		}
		if (Check == 13){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_5").Value = false;
			StartCoroutine ("LevelLoading", 5);
		}
		if (Check == 14){
			StartCoroutine ("LevelLoading", 5);
		}
		if (Check == 15){
			StartCoroutine ("LevelLoading", 5);
		}
		if (Check == 16){
			ResetVariables();
			StartCoroutine ("LevelLoading", 6);
		}
		if (Check == 17){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_4").Value = true;
            StartCoroutine("LevelLoading", 6);
		}
		if (Check == 18){
			FsmVariables.GlobalVariables.FindFsmBool("Item_1").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Item_2").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Item_3").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Item_4").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Item_5").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Item_6").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_4").Value = true;
			StartCoroutine ("LevelLoading", 6);
		}
		if (Check == 19){
			StartCoroutine ("LevelLoading", 6);
		}
		if (Check == 20){
			ResetVariables();
			StartCoroutine ("LevelLoading", 7);
		}
		if (Check == 21){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			StartCoroutine ("LevelLoading", 7);
		}
		if (Check == 22){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			StartCoroutine ("LevelLoading", 7);
		}
		if (Check == 23){
			StartCoroutine ("LevelLoading", 7);
		}
		if (Check == 24){
			ResetVariables();
			StartCoroutine ("LevelLoading", 8);
		}
		if (Check == 25){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_2").Value = true;
			StartCoroutine ("LevelLoading", 8);
		}
		if (Check == 26){
			StartCoroutine ("LevelLoading", 8);
		}
		if (Check == 27){
			StartCoroutine ("LevelLoading", 8);
		}
		if (Check == 28){
			ResetVariables();
			StartCoroutine ("LevelLoading", 9);
		}
		if (Check == 29){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			StartCoroutine ("LevelLoading", 9);
		}
		if (Check == 30){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_2").Value = true;
			StartCoroutine ("LevelLoading", 9);
		}
		if (Check == 31){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_2").Value = true;
			StartCoroutine ("LevelLoading", 9);
		}
		if (Check == 32){
			ResetVariables();
			StartCoroutine ("LevelLoading", 10);
		}
		if (Check == 33){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_1").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_2").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_3").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Item_1").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Item_2").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Item_3").Value = false;
			FsmVariables.GlobalVariables.FindFsmBool("Item_4").Value = false;
			StartCoroutine ("LevelLoading", 10);
		}
		if (Check == 34){
			StartCoroutine ("LevelLoading", 10);
		}
		if (Check == 35){
			StartCoroutine ("LevelLoading", 10);
		}
		if (Check == 36){
			ResetVariables();
			StartCoroutine ("LevelLoading", 11);
		}
		if (Check == 37){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_1").Value = true;
			StartCoroutine ("LevelLoading", 11);
		}
		if (Check == 38){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			FsmVariables.GlobalVariables.FindFsmBool("Misc_1").Value = true;
			StartCoroutine ("LevelLoading", 11);
		}
		if (Check == 39){
			StartCoroutine ("LevelLoading", 11);
		}
		if (Check == 40){
			ResetVariables();
			StartCoroutine ("LevelLoading", 12);
		}
		if (Check == 41){
			FsmVariables.GlobalVariables.FindFsmBool("Misc_0").Value = true;
			StartCoroutine ("LevelLoading", 12);
		}
		if (Check == 42){
			StartCoroutine ("LevelLoading", 12);
		}
		if (Check == 43){
			StartCoroutine ("LevelLoading", 12);
		}
		if (Check == 44){
			ResetVariables();
			StartCoroutine ("LevelLoading", 13);
		}
		if (Check == 45){
			StartCoroutine ("LevelLoading", 13);
		}
	}
	IEnumerator LevelLoading (int level){
        FadeOut();
		yield return new WaitForSeconds (2);
        SceneManager.LoadScene(14);
		GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().SetPapiros (PapirHojas, LogroText);
		GameObject.Find("Supervisor").GetComponent<LoadingGestor>().SetLevel(level, PlayerPrefs.GetInt("Save1"));
        Debug.Log("Supervisor gets level: " + level);
        //Time.timeScale = 0;
    }
	IEnumerator SelLevel (){
		FadeOut();
		yield return new WaitForSeconds (2);
		SceneManager.LoadScene(14);
		GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().SetPapiros (PapirHojas, LogroText);
		Debug.Log("Supervisor sets Level select screen");
		//Time.timeScale = 0;
	}
	IEnumerator LevelLoadingNewGame (){
		FadeOut();
		yield return new WaitForSeconds (2);
		SceneManager.LoadScene(16);
		GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().SetPapiros (PapirHojas, LogroText);
		GameObject.Find("Supervisor").GetComponent<LoadingGestor>().SetLevel(2, PlayerPrefs.GetInt("Save1"));
		Debug.Log("Supervisor starting new game");
		//Time.timeScale = 0;
	}
	public void MoveTo (int Loc){
		MenuSlide = Loc;
		bMove = true;
		Sonidos.GetComponent<AudioSource>().PlayOneShot(SlideMove, (Sonidos.GetComponent<AudioSource>().volume*PlayerPrefs.GetFloat("EffectVolume")));
	}
    public void FadeIn()
    {
        //Debug.Log("Fading in");
        Fadein.GetComponent<Image>().CrossFadeAlpha(0f, 4.0f, false);
    }
    public void FadeOut()
    {
        //Debug.Log("Fading out");
        Fadein.GetComponent<Image>().CrossFadeAlpha(1f, 2.0f, false);
    }
	public void FadeInPergamino(){
		Hojas.GetComponent<Image> ().CrossFadeAlpha (1, 1, false);
		Invoke ("ShowScrollBar", 1);
	}
	public void FadeOutPergamino(){
		Texto.GetComponent<Text> ().CrossFadeAlpha (0, 1, false);
		Hojas.GetComponent<Image> ().CrossFadeAlpha (0, 1, false);
		Invoke ("HideScrollBar", 1);
	}
	public void ShowScrollBar (){
		//ScrollBarHandle.SetActive (true);
		ScrollBar.GetComponent<Scrollbar> ().value = 1;
		Texto.GetComponent<Text> ().CrossFadeAlpha (1, 0.5f, false);
		bRead2 = true;
		//Debug.Log ("ShowScrollBar");
	}
	public void HideScrollBar (){
		//ScrollBarHandle.SetActive (false);
		//Debug.Log ("HideScrollBar");
	}
    public void EndFade() {
        bFading = false;
    }
    public void LoadCredits() {
        SceneManager.LoadScene(17);
    }
    public void SetMaxLevel(int lvl)
    {
        if(lvl <= 4)
        {
            MaxLevel = 1;
        }
        if (lvl > 4 && lvl <= 8)
        {
            MaxLevel = 2;
        }
        if (lvl > 8 && lvl <= 12)
        {
            MaxLevel = 3;
        }
        if (lvl > 12 && lvl <= 16)
        {
            MaxLevel = 4;
        }
        if (lvl > 16 && lvl <= 20)
        {
            MaxLevel = 5;
        }
        if (lvl > 20 && lvl <= 24)
        {
            MaxLevel = 6;
        }
        if (lvl > 24 && lvl <= 28)
        {
            MaxLevel = 7;
        }
        if (lvl > 28 && lvl <= 32)
        {
            MaxLevel = 8;
        }
        if (lvl > 32 && lvl <= 36)
        {
            MaxLevel = 9;
        }
        if (lvl > 36 && lvl <= 40)
        {
            MaxLevel = 10;
        }
        if (lvl > 40 && lvl <= 44)
        {
            MaxLevel = 11;
        }
        if (lvl > 44 && lvl <= 45)
        {
            MaxLevel = 12;
        }
    }
	public void ExtractHojas(){
		Papiros = PlayerPrefs.GetString ("Papiros");
		Debug.Log ("Papiros is: " + PlayerPrefs.GetString ("Papiros"));
		int charnum = 0;
		foreach (char letter in Papiros.ToCharArray()) {
			//Debug.Log ("" + letter);
			PapirHojas [charnum] = letter.ToString();
			charnum++;
		}
	}
	public bool ComprobarHoja(int hoja){
		if (hoja >= 0 && hoja <= 60) {
			if (PapirHojas [hoja] == "1") {
				return true;
			} else
				return false;
		}else
			return false;
		
	}
	public void SetMando(bool m){
		bMando = m;
	}
	public void SacarLogros(){
		GameObject Sup = GameObject.Find ("Supervisor");
		for (int l = 0; l < Logros.Length; l++) {
			Logros [l] = 0;
		}
		if (PlayerPrefs.GetInt ("Ach01") == 1) {
			Debug.Log ("Achievement 1 got");
			Logros [0] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach02") == 1) {
			Debug.Log ("Achievement 2 got");
			Logros [1] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach03") == 1) {
			Debug.Log ("Achievement 3 got");
			Logros [2] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach04") == 1) {
			Debug.Log ("Achievement 4 got");
			Logros [3] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach05") == 1) {
			Debug.Log ("Achievement 5 got");
			Logros [4] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach06") == 1) {
			Debug.Log ("Achievement 6 got");
			Logros [5] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach07") == 1) {
			Debug.Log ("Achievement 7 got");
			Logros [6] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach08") == 1) {
			Debug.Log ("Achievement 8 got");
			Logros [7] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach09") == 1) {
			Debug.Log ("Achievement 9 got");
			Logros [8] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach10") == 1) {
			Debug.Log ("Achievement 10 got");
			Logros [9] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach11") == 1) {
			Debug.Log ("Achievement 11 got");
			Logros [10] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach12") == 1) {
			Debug.Log ("Achievement 12 got");
			Logros [11] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach13") == 1) {
			Debug.Log ("Achievement 13 got");
			Logros [12] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach14") == 1) {
			Debug.Log ("Achievement 14 got");
			LockLevelSel = false;
			Logros [13] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach15") == 1) {
			Debug.Log ("Achievement 15 got");
			LockLevelSel = false;
			Logros [14] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach16") == 1) {
			Debug.Log ("Achievement 16 got");
			Logros [15] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach17") == 1) {
			Debug.Log ("Achievement 17 got");
			Logros [16] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach18") == 1) {
			Debug.Log ("Achievement 18 got");
			Logros [17] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach19") == 1) {
			Debug.Log ("Achievement 19 got");
			Logros [18] = 1;
		}
		if (PlayerPrefs.GetInt ("Ach20") == 1) {
			Debug.Log ("Achievement 20 got");
			Logros [19] = 1;
		}
		if (Logros [0] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (1);
		}
		if (Logros [1] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (2);
		}
		if (Logros [2] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (3);
		}
		if (Logros [3] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (4);
		}
		if (Logros [4] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (5);
		}
		if (Logros [5] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (6);
		}
		if (Logros [6] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (7);
		}
		if (Logros [7] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (8);
		}
		if (Logros [8] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (9);
		}
		if (Logros [9] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (10);
		}
		if (Logros [10] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (11);
		}
		if (Logros [11] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (12);
		}
		if (Logros [12] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (13);
		}
		if (Logros [13] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (14);
		}
		if (Logros [14] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (15);
		}
		if (Logros [15] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (16);
		}
		if (Logros [16] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (17);
		}
		if (Logros [17] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (18);
		}
		if (Logros [18] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (19);
		}
		if (Logros [19] == 1) {
			Sup.GetComponent<LoadingGestor> ().Achievements (20);
		}
	}
	public void ResetearLogros(){
		for (int l = 0; l < Logros.Length; l++) {
			Logros [l] = 0;
		}
		PlayerPrefs.SetInt ("Ach01", 0);
		PlayerPrefs.SetInt ("Ach02", 0);
		PlayerPrefs.SetInt ("Ach03", 0);
		PlayerPrefs.SetInt ("Ach04", 0);
		PlayerPrefs.SetInt ("Ach05", 0);
		PlayerPrefs.SetInt ("Ach06", 0);
		PlayerPrefs.SetInt ("Ach07", 0);
		PlayerPrefs.SetInt ("Ach08", 0);
		PlayerPrefs.SetInt ("Ach09", 0);
		PlayerPrefs.SetInt ("Ach10", 0);
		PlayerPrefs.SetInt ("Ach11", 0);
		PlayerPrefs.SetInt ("Ach12", 0);
		PlayerPrefs.SetInt ("Ach13", 0);
		PlayerPrefs.SetInt ("Ach14", 1);
		PlayerPrefs.SetInt ("Ach15", 0);
		PlayerPrefs.SetInt ("Ach16", 0);
		PlayerPrefs.SetInt ("Ach17", 0);
		PlayerPrefs.SetInt ("Ach18", 0);
		PlayerPrefs.SetInt ("Ach19", 0);
		PlayerPrefs.SetInt ("Ach20", 0);
		SacarLogros ();
	}
	public void ResetAntaño(){
		GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Antaño (false);
	}
}
