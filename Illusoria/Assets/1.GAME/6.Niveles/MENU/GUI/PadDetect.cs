﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using InControl;
using HutongGames.PlayMaker;
using UnityStandardAssets.ImageEffects;
using UnityEngine.SceneManagement;

public class PadDetect : MonoBehaviour {
	public GameObject Randy;
	public GameObject LoadingG;
	public int Level;
	public Camera NormalCamara;
	public bool bJumping = false;
	public bool bPaused = false;
	public bool bDisabled = false;
	public bool bLoading = false;
	public bool bReading = false;
	public bool LockReset;
	public bool LockOptions;
	public bool bmando;
	public bool MoveCamera;
	public Vector2 CameraDir;
	public float CameraSpeed;
	public int man;
	public string[] mandos;
	public bool bRight;
	public bool bLeft;
	public int Checkpoint;
	public float EffectVolume;
	public float MusicVolume;
	//In Game Menu
	public GameObject Canvas;
	public int IGMOpt;
	public float Vert;
	public float Hor;
	public GameObject SavingIMG;
	public GameObject HojaIMG;
	public GameObject HojasTEXT;
	public GameObject LeerHojaIMG;
	public GameObject LeerHojaSlider;
	public GameObject LeerHojaFlechaAbajo;
	public GameObject LeerHojaFlechaArriba;
	public GameObject EngranajeIMG;
	public GameObject AchievementIMG;
	public GameObject Recurso;
	public bool bChangeOpt;
	public bool bCinematic;
	public bool bHojaGot;
	public AudioClip HojaGet;
	public float HojagetTime;
	public int bH;
	public float ChangeOptTime;
	public float ChangeOptMax;
	public GameObject Resume;
	public GameObject Restart;
	public GameObject Options;
	public GameObject Exit;
    public GameObject NormCamera;
	public GameObject[] IGMSelector;
	public GameObject[] IGMOptSelector;
    public Sprite[] ResumeA;
	public Sprite[] ResumeB;
	public Sprite[] RestartA;
	public Sprite[] RestartB;
	public Sprite[] SkipA;
	public Sprite[] SkipB;
	public Sprite[] OptionsA;
	public Sprite[] OptionsB;
	public Sprite[] ExitA;
	public Sprite[] ExitB;
	//options
	public int nControls;
	public bool bOptions;
	public Sprite[] EffectVolA;
	public Sprite[] EffectVolB;
	public Sprite[] MusicVolA;
	public Sprite[] MusicVolB;
	public Sprite[] LanguageA;
	public Sprite[] LanguageB;
	public Sprite[] ControlsA;
	public Sprite[] ControlsB;
	public Sprite[] Cont;
	public GameObject OptEffVol;
	public GameObject OptMusVol;
	public GameObject OptLang;
	public GameObject Controls;
	public GameObject ContBanner;
	public GameObject OptExit;
	public GameObject EffectVolumeSlider;
	public GameObject MusicVolumeSlider;
	public GameObject LanguageSpriteChanger;
	public Sprite[] LangSprite;
	public GameObject LanguageMenu;
	public GameObject OptCanvas;
	public GameObject IGMCanvas;
	//Dialogos
	public AudioClip[] Musica;
	public AudioClip MuerteRandyFogonazo;
	public GameObject MusicaAudioSource;
	public GameObject FondoAudioSource;
	public bool fadingoutAudio;
	public bool fadingoutNoise;
	public GameObject IntroText;
	public GameObject SecretOrbe;
	public bool bDialog;
	public bool bIncapacitado;
	public bool bDuringLine;
	public bool bFinishedLine;
	public bool bWaitforaction;
	private IEnumerator Textroutine;
	public GameObject DialogUI;
	public GameObject DialogText;
	public GameObject DialogPort;
	public GameObject DialogTextLeft;
	public GameObject DialogPortLeft;
	public GameObject DialogTextRight;
	public GameObject DialogPortRight;
	public GameObject Arrow;
	public TextAsset Dialog;
	public AudioClip LetterSound;
	public AudioClip ShakeCamSound;
	public AudioClip OptionsAcceptSound;
	public AudioClip OptionsBackSound;
	public float letterwait = 0.05f;
	public bool SpkRight = false;
	public bool SpkLeft = true;
	public int currentline;
	public int endatline;
	public int conv;
	public int cinematic;
	public Sprite[] Portraits;
	public GameObject Fadein;
	//Localization e idioma
	public int Language;
	public string[] CurrentLang;
	public int[] WhatPort;
	public int[] LeftorRight;
	public TextAsset English;
	public TextAsset Castellano;
	public TextAsset Frances;
	//AI
	public GameObject LookAtTarget;
	public bool bLookAt;
	public Vector2 GoToTarget;
	public bool bGoTo;
        // Use this for initialization
    void Start () {
		LeerHojaIMG.SetActive (false);
		//LeerHojaIMG.GetComponent<Image>().CrossFadeAlpha(0, 0f, false);
		//LeerHojaIMG.GetComponentInChildren<Text>().CrossFadeAlpha(0, 0f, false);
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
		//Checkpoint = PlayerPrefs.GetInt ("Save1");
		//Debug.Log ("Your Save is: " + Checkpoint);
		Language = PlayerPrefs.GetInt ("Lang");
		nControls = PlayerPrefs.GetInt ("Controls");
		EffectVolume = PlayerPrefs.GetFloat("EffectVolume");
		Debug.Log ("Effects: " + PlayerPrefs.GetFloat("EffectVolume"));
		MusicVolume = PlayerPrefs.GetFloat("MusicVolume");
		SetLanguage();
		bChangeOpt = true;
		LoadingG = GameObject.Find ("Supervisor");
		HojaIMG.GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
		HojaIMG.GetComponentInChildren<Text>().CrossFadeAlpha(0f, 0f, false);
		Cursor.visible = false;
		bIncapacitado = false;
		//LockReset = false;
		if (!LoadingG.GetComponent<LoadingGestor> ().ComprobarHoja (0)) {
			//Debug.Log ("No tienes esa hoja");
			//LoadingG.GetComponent<LoadingGestor> ().SavePapiros ();
		}
	}
	
	// Update is called once per frame
	void Update () {
        var inputDevice = InputManager.ActiveDevice;
		Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Restart").Value = false;
		if (Input.GetKeyDown (KeyCode.T)) {
			//Cursor.visible = true;
			//FadeInEnd ();
			//ShakeCam (0.5f,2);
			//MoveTo (17, 0);
			//LoadingG.GetComponent<LoadingGestor>().RandyDeath(1);
			//LoadingG.GetComponent<LoadingGestor>().NextLevel(4);
			//FsmVariables.GlobalVariables.FindFsmBool("Item_3").Value = true;
			//FsmVariables.GlobalVariables.FindFsmBool("Item_1").Value = true;
			//FsmVariables.GlobalVariables.FindFsmBool("Item_2").Value = true;
			//DisableCont(true);
        }
		if (Input.GetKeyDown (KeyCode.L)) {
			//GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().NextLevel (12);
			//MoveCamTo(160, 120);
			//DisableContSoltar(true);
		}
		if (MoveCamera) {
			//Debug.Log ("" + new Vector3 ((CameraDir.x - gameObject.transform.position.x), (CameraDir.y - gameObject.transform.position.y), -10).magnitude);
			gameObject.transform.Translate (new Vector3 ((CameraDir.x - gameObject.transform.position.x), (CameraDir.y - gameObject.transform.position.y), 0).normalized*(CameraSpeed*Time.deltaTime*Time.timeScale*100));
			if ((new Vector3 ((CameraDir.x - gameObject.transform.position.x), (CameraDir.y - gameObject.transform.position.y), 0).magnitude) <= 0.25f) {
				//Debug.Log ("Moving Camera3");
				MoveCamera = false;
				bWaitforaction = false;
				gameObject.transform.position = new Vector3 (CameraDir.x, CameraDir.y, -10);
			}
		}
		if (LockOptions) {
			Options.GetComponent<Image>().color = Color.grey;
		}
		if (LockReset && !bCinematic) {
			Restart.GetComponent<Image>().color = Color.grey;
		}
		if (!LockReset) {
			Restart.GetComponent<Image>().color = Color.white;
		}
		if (bHojaGot) {
			HojagetTime += Time.deltaTime;
			if (HojagetTime >= 7) {
				//Debug.Log ("HojaTime end");
				bHojaGot = false;
				LeerHojaIMG.SetActive (false);
				HojaIMG.GetComponent<Image>().CrossFadeAlpha(0f, 0.5f, false);
				HojaIMG.GetComponentInChildren<Text>().CrossFadeAlpha(0f, 0.5f, false);
				HojagetTime = 0;
			}
		}
		if (bReading) {
			if (LeerHojaSlider.GetComponent<Scrollbar> ().size >= 0.99995f) {
				LeerHojaFlechaAbajo.SetActive (false);
				LeerHojaFlechaArriba.SetActive (false);
			}
			if ((LeerHojaSlider.GetComponent<Scrollbar> ().size < 0.99995f && LeerHojaSlider.GetComponent<Scrollbar> ().size > 0.4f)) {
				if (Vert < -0.7f && bChangeOpt) {
					LeerHojaSlider.GetComponent<Scrollbar> ().value += 0.05f;
				}
				if (Vert > 0.7f && bChangeOpt) {
					LeerHojaSlider.GetComponent<Scrollbar> ().value -= 0.05f;
				}
				if (LeerHojaSlider.GetComponent<Scrollbar> ().value > 0) {
					LeerHojaFlechaAbajo.SetActive (true);
				}
				if (LeerHojaSlider.GetComponent<Scrollbar> ().value == 0) {
					LeerHojaFlechaAbajo.SetActive (false);
				}
				if (LeerHojaSlider.GetComponent<Scrollbar> ().value <= 0.9f) {
					LeerHojaFlechaArriba.SetActive (true);
				}
				if (LeerHojaSlider.GetComponent<Scrollbar> ().value > 0.9f) {
					LeerHojaFlechaArriba.SetActive (false);
				}
			}
			if (LeerHojaSlider.GetComponent<Scrollbar> ().size <= 0.4f) {
				if (Vert < -0.7f && bChangeOpt) {
					LeerHojaSlider.GetComponent<Scrollbar> ().value += 0.005f;
				}
				if (Vert > 0.7f && bChangeOpt) {
					LeerHojaSlider.GetComponent<Scrollbar> ().value -= 0.005f;
				}
				if (LeerHojaSlider.GetComponent<Scrollbar> ().value > 0) {
					LeerHojaFlechaAbajo.SetActive (true);
				}
				if (LeerHojaSlider.GetComponent<Scrollbar> ().value == 0) {
					LeerHojaFlechaAbajo.SetActive (false);
				}
				if (LeerHojaSlider.GetComponent<Scrollbar> ().value <= 0.9f) {
					LeerHojaFlechaArriba.SetActive (true);
				}
				if (LeerHojaSlider.GetComponent<Scrollbar> ().value > 0.9f) {
					LeerHojaFlechaArriba.SetActive (false);
				}
			}
		}
		if (fadingoutAudio) {
			MusicaAudioSource.GetComponent<AudioSource> ().volume -= 0.005f;
			if (MusicaAudioSource.GetComponent<AudioSource> ().volume <= 0) {
				fadingoutAudio = false;
				MusicaAudioSource.GetComponent<AudioSource> ().Stop ();
				MusicaAudioSource.GetComponent<AudioSource> ().volume = MusicVolume;
				FondoAudioSource.GetComponent<AudioSource> ().volume = EffectVolume;
			}
		}
		if (fadingoutNoise) {
			FondoAudioSource.GetComponent<AudioSource> ().volume -= 0.02f;
			if (FondoAudioSource.GetComponent<AudioSource> ().volume <= 0) {
				fadingoutNoise = false;
				FondoAudioSource.GetComponent<AudioSource> ().Stop ();
				MusicaAudioSource.GetComponent<AudioSource> ().volume = MusicVolume;
				FondoAudioSource.GetComponent<AudioSource> ().volume = EffectVolume;
			}
		}
		if ((inputDevice.Command.WasPressed || Input.GetKeyDown("escape")) && !bLoading && bChangeOpt){
			//gameObject.GetComponent<AudioSource> ().PlayOneShot (OptionsAcceptSound,GetComponent<AudioSource>().volume*EffectVolume);
			Input.ResetInputAxes();
			//Debug.Log ("PAused!");
			Pausegame(bPaused);
            //NormalCamara.GetComponent<BloomOptimized>().enabled = true;
            if (NormalCamara.GetComponent<VignetteAndChromaticAberration>() != null)
            {
                NormalCamara.GetComponent<VignetteAndChromaticAberration>().enabled = true;
            }
        }
		Vert = inputDevice.Direction.Y;
		Hor = inputDevice.Direction.X;
		if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow)) {
			Hor = -1;
		}
		if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)) {
			Hor = 1;
		}
		if ((Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.D)) || (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.RightArrow))) {
			Hor = 0;
		}
		if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.UpArrow)) {
			Vert = -1;
		}
		if (Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.DownArrow)) {
			Vert = 1;
		}
		if ((Input.GetKey (KeyCode.W) && Input.GetKey (KeyCode.S)) || (Input.GetKey (KeyCode.UpArrow) && Input.GetKey (KeyCode.DownArrow))) {
			Vert = 0;
		}
		if (bChangeOpt == false) {
			ChangeOptTime += Time.unscaledDeltaTime;
		}
		if (ChangeOptTime >= ChangeOptMax) {
			bChangeOpt = true;
			ChangeOptTime = 0;
		}
		//Main Menu
		if (bPaused && !bHojaGot) {
			
			if(bCinematic){
				Restart.GetComponent<Image>().sprite = SkipA[Language];
				Restart.GetComponent<Image>().color = Color.white;
			}
			if(!bCinematic){
				Restart.GetComponent<Image>().sprite = RestartA[Language];
			}
            if (Vert > 0.7f && bChangeOpt) {
				IGMOpt++;
				bChangeOpt = false;
				if ((LockReset && !bCinematic)&& IGMOpt == 2) {
					IGMOpt++;
				}
				if (LockOptions && IGMOpt == 3) {
					IGMOpt++;
				}
			}
			if (Vert < -0.7f && bChangeOpt) {
				IGMOpt--;
				bChangeOpt = false;
				if (LockOptions && IGMOpt == 3) {
					IGMOpt--;
				}
				if ((LockReset && !bCinematic)&& IGMOpt == 2) {
					IGMOpt--;
				}
			}
			if(!bOptions){
				OptCanvas.SetActive(false);
				IGMCanvas.SetActive(true);
				if (IGMOpt > 4) {
					IGMOpt = 4;
				}
				if (IGMOpt < 1) {
					IGMOpt = 1;
				}
				for (int f = 1; f < 5; f++) {
					IGMSelector [f].SetActive (false);
				}
				IGMSelector [IGMOpt].SetActive (true);
				if (IGMOpt == 1) {
					Resume.transform.localScale = new Vector3 (1, 1, 1);
				}
				if (IGMOpt != 1) {
					Resume.GetComponent<Image>().sprite = ResumeA[Language];
					Resume.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
				}
				if (IGMOpt == 2) {
					Restart.transform.localScale = new Vector3 (1, 1, 1);
				}
				if (IGMOpt != 2) {
					Restart.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
				}
				if (IGMOpt == 3) {
					Options.transform.localScale = new Vector3 (1, 1, 1);
				}
				if (IGMOpt != 3) {
					Options.GetComponent<Image>().sprite = OptionsA[Language];
					Options.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
				}
				if (IGMOpt == 4) {
					Exit.transform.localScale = new Vector3 (1, 1, 1);
				}
				if (IGMOpt != 4) {
					Exit.GetComponent<Image>().sprite = ExitA[Language];
					Exit.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
				}
				if ((inputDevice.Action1.WasPressed || Input.GetKeyDown("return") || Input.GetKeyDown(KeyCode.Space)) && bPaused && bChangeOpt){
					
					if(IGMOpt == 1 && bChangeOpt){
						//gameObject.GetComponent<AudioSource> ().PlayOneShot (OptionsBackSound,GetComponent<AudioSource>().volume*EffectVolume);
						Pausegame (true);
						bChangeOpt = false;
                        //NormalCamara.GetComponent<BloomOptimized>().enabled = true;
                        if (NormalCamara.GetComponent<VignetteAndChromaticAberration>() != null)
                        {
                            NormalCamara.GetComponent<VignetteAndChromaticAberration>().enabled = true;
                        }
                    }
					if(IGMOpt == 2){
						//gameObject.GetComponent<AudioSource> ().PlayOneShot (OptionsAcceptSound,GetComponent<AudioSource>().volume*EffectVolume);
						if (!bCinematic && !LockReset){
							FadeOut ();
							CancelInvoke ();
							iTween.Stop ();
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Restart").Value = true;
						}
						if (bCinematic){
							CancelInvoke ();
							FadeOutIntroText ();
							Pausegame (true);
							bChangeOpt = false;
							bIncapacitado = true;
							//NormalCamara.GetComponent<BloomOptimized>().enabled = true;
							if (NormalCamara.GetComponent<VignetteAndChromaticAberration>() != null)
							{
								NormalCamara.GetComponent<VignetteAndChromaticAberration>().enabled = true;
							}
							FadeOut ();
							StartCoroutine ("DisableMenu", 2);
							Invoke ("CancelCinematic", 2);
						}
					}
					if(IGMOpt == 3){
						//gameObject.GetComponent<AudioSource> ().PlayOneShot (OptionsAcceptSound,GetComponent<AudioSource>().volume*EffectVolume);
						bOptions = true;
						IGMOpt = 1;
					}
					if(IGMOpt == 4){
						//gameObject.GetComponent<AudioSource> ().PlayOneShot (OptionsAcceptSound,GetComponent<AudioSource>().volume*EffectVolume);
						SceneManager.LoadScene(1);
						iTween.Stop ();
					}
				}
				if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("escape") || Input.GetKeyDown(KeyCode.LeftControl)) && bPaused && bChangeOpt){
					//Debug.Log ("Unpaused!");
					//gameObject.GetComponent<AudioSource> ().PlayOneShot (OptionsBackSound,GetComponent<AudioSource>().volume*EffectVolume);
					Pausegame (true);
					bChangeOpt = false;
                    //NormalCamara.GetComponent<BloomOptimized>().enabled = true;
                    if (NormalCamara.GetComponent<VignetteAndChromaticAberration>() != null)
                    {
                        NormalCamara.GetComponent<VignetteAndChromaticAberration>().enabled = true;
                    }
                }
			}
			if (bOptions){
				if (IGMOpt > 5) {
					IGMOpt = 5;
				}
				if (IGMOpt < 1) {
					IGMOpt = 1;
				}
				for (int f = 1; f < 6; f++) {
					IGMOptSelector [f].SetActive (false);
				}
				IGMOptSelector [IGMOpt].SetActive (true);
				OptCanvas.SetActive(true);
				IGMCanvas.SetActive(false);
				LanguageSpriteChanger.GetComponent<Image>().sprite = LangSprite[Language];
				ContBanner.GetComponent<Image>().sprite = Cont[PlayerPrefs.GetInt("Controls")];
				MusicVolumeSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("MusicVolume");
				EffectVolumeSlider.GetComponent<Slider>().value = PlayerPrefs.GetFloat("EffectVolume");
				if (IGMOpt == 1) {
					OptEffVol.transform.localScale = new Vector3 (1, 1, 1);
					if (Hor > 0.7f && bChangeOpt) {
						float F = PlayerPrefs.GetFloat("EffectVolume") + 0.1f;
						if (F >= 1){
							F = 1;
						}
						Debug.Log ("Effecto volume " + F);
						PlayerPrefs.SetFloat("EffectVolume",(F));
						EffectVolume = PlayerPrefs.GetFloat("EffectVolume");
						FsmVariables.GlobalVariables.FindFsmFloat("FX_Volume").Value = PlayerPrefs.GetFloat("EffectVolume");
						bChangeOpt = false;
					}
					if (Hor < -0.7f && bChangeOpt) {
						float F = PlayerPrefs.GetFloat("EffectVolume") - 0.1f;
						if (F <= 0){
							F = 0;
						}
						Debug.Log ("Effecto volume " + F);
						PlayerPrefs.SetFloat("EffectVolume",(F));
						EffectVolume = PlayerPrefs.GetFloat("EffectVolume");
						FsmVariables.GlobalVariables.FindFsmFloat("FX_Volume").Value = PlayerPrefs.GetFloat("EffectVolume");
						bChangeOpt = false;
					}
				}
				if (IGMOpt != 1) {
					OptEffVol.GetComponent<Image>().sprite = EffectVolA[Language];
					OptEffVol.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
				}
				if (IGMOpt == 2) {
					OptMusVol.transform.localScale = new Vector3 (1, 1, 1);
					if (Hor > 0.7f && bChangeOpt) {
						float M = PlayerPrefs.GetFloat("MusicVolume") + 0.1f;
						if (M >= 1){
							M = 1;
						}
						//Debug.Log ("Music volume " + M);
						PlayerPrefs.SetFloat("MusicVolume",(M));
						MusicVolume = PlayerPrefs.GetFloat("MusicVolume");
						MusicaAudioSource.GetComponent<AudioSource> ().volume = MusicVolume;
						FsmVariables.GlobalVariables.FindFsmFloat("Music_Volume").Value = PlayerPrefs.GetFloat("MusicVolume");
						bChangeOpt = false;
					}
					if (Hor < -0.7f && bChangeOpt) {
						float M = PlayerPrefs.GetFloat("MusicVolume") - 0.1f;
						if (M <= 0){
							M = 0;
						}
						//Debug.Log ("Music volume " + M);
						PlayerPrefs.SetFloat("MusicVolume",(M));
						MusicVolume = PlayerPrefs.GetFloat("MusicVolume");
						MusicaAudioSource.GetComponent<AudioSource> ().volume = MusicVolume;
						FsmVariables.GlobalVariables.FindFsmFloat("Music_Volume").Value = PlayerPrefs.GetFloat("MusicVolume");
						bChangeOpt = false;
					}
				}
				if (IGMOpt != 2) {
					OptMusVol.GetComponent<Image>().sprite = MusicVolA[Language];
					OptMusVol.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
				}
				if (IGMOpt == 3) {
					OptLang.transform.localScale = new Vector3 (1, 1, 1);
					if (Hor > 0.7f && bChangeOpt && !bDialog) {
						Language++;
						if (Language >= LangSprite.Length){
							Language = 1;
						}
						PlayerPrefs.SetInt("Lang",Language);
						SetLanguage();
						bChangeOpt = false;
					}
					if (Hor < -0.7f && bChangeOpt && !bDialog) {
						Language--;
						if (Language < 1){
							Language = LangSprite.Length - 1;
						}
						PlayerPrefs.SetInt("Lang",Language);
						SetLanguage();
						bChangeOpt = false;
					}
				}
				if (IGMOpt != 3) {
					OptLang.GetComponent<Image>().sprite = LanguageA[Language];
					OptLang.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
				}
				if (IGMOpt == 4) {
					Controls.transform.localScale = new Vector3 (1, 1, 1);
					if (Hor > 0.7f && bChangeOpt) {
						int C = PlayerPrefs.GetInt("Controls");
						C++;
						if (C >= 3){
							C = 3;
						}
						PlayerPrefs.SetInt("Controls",C);
						nControls = C;
						bChangeOpt = false;
					}
					if (Hor < -0.7f && bChangeOpt) {
						int C = PlayerPrefs.GetInt("Controls");
						C--;
						if (C <= 1){
							C = 1;
						}
						PlayerPrefs.SetInt("Controls",C);
						nControls = C;
						bChangeOpt = false;
					}
				}
				if (IGMOpt != 4) {
					Controls.GetComponent<Image>().sprite = ControlsA[Language];
					Controls.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
				}
				if (IGMOpt == 5) {
					OptExit.transform.localScale = new Vector3 (1, 1, 1);
				}
				if (IGMOpt != 5) {
					OptExit.GetComponent<Image>().sprite = ExitB[Language];
					OptExit.transform.localScale = new Vector3 (0.85f, 0.85f, 1);
				}
				if ((inputDevice.Action1.WasPressed || Input.GetKeyDown("return") || Input.GetKeyDown(KeyCode.Space)) && bPaused && bChangeOpt) {
					//gameObject.GetComponent<AudioSource> ().PlayOneShot (OptionsAcceptSound,GetComponent<AudioSource>().volume*EffectVolume);
					if (IGMOpt == 1){
					}
					if (IGMOpt == 2){
					}
					if (IGMOpt == 3){
					}
					if (IGMOpt == 5){
						bOptions = false;
						IGMOpt = 1;
						bChangeOpt = false;
					}
				}
				if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("escape") || Input.GetKeyDown(KeyCode.LeftControl)) && bPaused && bChangeOpt) {
					//gameObject.GetComponent<AudioSource> ().PlayOneShot (OptionsBackSound,GetComponent<AudioSource>().volume*EffectVolume);
					bOptions = false;
					IGMOpt = 1;
					bChangeOpt = false;
				}
			}
		}

		if (!bPaused){
			//Time.timeScale = 1.0f;
			//Gamepads
			if (bDisabled || bDialog){
				/*Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = 0;
				Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Vertical").Value = 0;
				Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = false;
				Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = false;*/
			}
			if (!bDisabled && !bDialog && !bCinematic){
				if (bmando) {
					Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = inputDevice.Direction.X;
					Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Vertical").Value = inputDevice.Direction.Y;
				}
				if (!bmando) {
					Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = 0;
					Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Vertical").Value = 0;
					if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow)) {
						Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = -1;
					}
					if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)) {
						Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = 1;
					}
					if ((Input.GetKey (KeyCode.A) && Input.GetKey (KeyCode.D)) || (Input.GetKey (KeyCode.LeftArrow) && Input.GetKey (KeyCode.RightArrow))) {
						Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = 0;
					}
					if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.UpArrow)) {
						Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Vertical").Value = -1;
					}
					if (Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.DownArrow)) {
						Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Vertical").Value = 1;
					}
					if ((Input.GetKey (KeyCode.W) && Input.GetKey (KeyCode.S)) || (Input.GetKey (KeyCode.UpArrow) && Input.GetKey (KeyCode.DownArrow))) {
						Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Vertical").Value = 0;
					}
				}			
				Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Jump").Value = false;
				Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Use").Value = false;
				if (bmando) {
					if (nControls == 1){
						if (inputDevice.Action1.WasPressed && bChangeOpt && !bCinematic){
							//Debug.Log ("Salto");
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Jump").Value = true;
						}
						//Use
						if (inputDevice.Action4.WasPressed && !bPaused){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Use").Value = true;
						}
						if (inputDevice.Action3.WasPressed && !bPaused){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = true;
						}
						if (inputDevice.Action3.WasReleased && !bPaused){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = false;
						}
						if (inputDevice.Action2.WasPressed && !bPaused && bChangeOpt){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = true;
						}
						if (inputDevice.Action2.WasReleased && !bPaused){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = false;
						}
					}
					if (nControls == 2){
						if (inputDevice.Action1.WasPressed && bChangeOpt && !bCinematic){
							//Debug.Log ("Salto2");
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Jump").Value = true;
						}
						//Use
						if (inputDevice.Action4.WasPressed && !bPaused){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Use").Value = true;
						}
						if (inputDevice.Action2.WasPressed && !bPaused){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = true;
						}
						if (inputDevice.Action2.WasReleased && !bPaused){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = false;
						}
						if (inputDevice.Action3.WasPressed && !bPaused && bChangeOpt){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = true;
						}
						if (inputDevice.Action3.WasReleased && !bPaused){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = false;
						}
					}
					if (nControls == 3){
						if (inputDevice.Action3.WasPressed && bChangeOpt && !bCinematic){
							//Debug.Log ("Salto3");
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Jump").Value = true;
						}
						//Use
						if (inputDevice.Action2.WasPressed && !bPaused){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Use").Value = true;
						}
						if (inputDevice.LeftBumper.IsPressed && !bPaused){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = true;
						}
						if (inputDevice.LeftBumper.WasReleased && !bPaused){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = false;
						}
						if (inputDevice.RightBumper.WasPressed && !bPaused && bChangeOpt){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = true;
						}
						if (inputDevice.RightBumper.WasReleased && !bPaused){
							Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = false;
						}
					}
				}
				//Keyboard
				if (!bmando){					
					if (Input.GetKeyDown("space") && bChangeOpt && !bCinematic){
						//Debug.Log ("Salto");
						Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Jump").Value = true;
					}
					//Use
					if (Input.GetKeyDown(KeyCode.E) && !bPaused){
						Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Use").Value = true;
					}
					if (Input.GetKeyDown(KeyCode.LeftShift) && !bPaused){
						Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = true;
					}
					if (Input.GetKeyUp(KeyCode.LeftShift) && !bPaused){
						Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = false;
					}
					if (Input.GetKeyDown(KeyCode.LeftControl) && !bPaused && bChangeOpt){
						Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = true;
					}
					if (Input.GetKeyUp(KeyCode.LeftControl) && !bPaused){
						Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = false;
					}
				}
			}
		}
		//controles durante dialogos
		if (bDialog && bDuringLine) {
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("return") || Input.GetKeyDown(KeyCode.Space) || inputDevice.Action1.WasPressed) && !bPaused && bChangeOpt && !bIncapacitado){
				bChangeOpt = false;
				StopCoroutine ("TypeText");
				DialogText.GetComponent<Text>().text = CurrentLang[currentline];
				bDuringLine = false;
				bFinishedLine = true;
			}
		}
		if (bDialog && bFinishedLine && !bWaitforaction) {
			Arrow.SetActive (true);
			if ((inputDevice.Action2.WasPressed || Input.GetKeyDown("return") || Input.GetKeyDown(KeyCode.Space) || inputDevice.Action1.WasPressed) && !bPaused && bChangeOpt && !bIncapacitado) {
				SpecialEvent (currentline);
				bChangeOpt = false;
				bFinishedLine = false;
				if (currentline == endatline) {
					Arrow.SetActive (false);
					EndDial ();
				}
				if (currentline < endatline) {
					Arrow.SetActive (false);
					currentline++;
					StartCoroutine ("TypeText");
				}
			}
		}
		if (!bDialog) {
			DialogUI.SetActive (false);
		}
		if (bLookAt) {
			float M;
			M = (LookAtTarget.transform.position.x - Randy.transform.position.x);
			if (M > 1 && Randy.GetComponent<tk2dSprite> ().scale.x < 0) {
				Turn ();
			}
			if (M < 1 && Randy.GetComponent<tk2dSprite> ().scale.x > 0) {
				Turn ();
			}
		}
		if (bGoTo) {
			float M;
			M = (GoToTarget.x - Randy.transform.position.x);
			if (M > 0) {
				Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = 1;
			}
			if (M < 0) {
				Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = -1;
			}
			//Debug.Log ("" + new Vector2 ((GoToTarget.x - Randy.transform.position.x), (GoToTarget.y - Randy.transform.position.y)).magnitude);
			if (new Vector2 ((GoToTarget.x - Randy.transform.position.x), (GoToTarget.y - Randy.transform.position.y)).magnitude < 2.7f) {
				bGoTo = false;
				Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = 0;
			}
		}
}
	public void Pausegame(bool pause){
		var inputDevice = InputManager.ActiveDevice;
		if (!pause && !bHojaGot){
			//Debug.Log ("Pausa On");
			bPaused = true;
			AudioListener.pause = true;
			Time.timeScale = 0.0f;
			Canvas.SetActive(true);
			IGMOpt = 1;
			bChangeOpt = true;
			bOptions = false;
			iTween.Pause();
			if (Level == 3 && Checkpoint < 5) {
				int r = 0;
				if (FsmVariables.GlobalVariables.FindFsmBool ("Item_1").Value) {
					r++;
				}
				if (FsmVariables.GlobalVariables.FindFsmBool ("Item_2").Value) {
					r++;
				}
				if (FsmVariables.GlobalVariables.FindFsmBool ("Item_3").Value) {
					r++;
				}
				Recurso.SetActive (true);
				Recurso.GetComponentInChildren<Text> ().text = "x " + r;
			}
			if (Level == 3 && Checkpoint >= 5) {
				Recurso.SetActive (false);
			}
		}
		if (!pause && bHojaGot){
			//Debug.Log ("Pausa On");
			bPaused = true;
			LeerHojaIMG.SetActive (true);
			//LeerHojaIMG.GetComponent<Image>().CrossFadeAlpha(1f, 0, false);
			//LeerHojaIMG.GetComponentInChildren<Text>().CrossFadeAlpha(1, 0, false);
			LeerHojaSlider.GetComponent<Scrollbar> ().value = 1;
			AudioListener.pause = true;
			Time.timeScale = 0.0f;
			//Canvas.SetActive(true);
			bChangeOpt = true;
			bReading = true;
			iTween.Pause();
		}
		if (pause){
			//Debug.Log ("Pausa Off");
			if (nControls == 1){
				if (!inputDevice.Action3.IsPressed){
					Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = false;
				}
				if (!inputDevice.Action2.IsPressed){
					Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = false;
				}
			}
			if (nControls == 2){
				if (!inputDevice.Action3.IsPressed){
					Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = false;
				}
				if (!inputDevice.Action2.IsPressed){
					Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = false;
				}
			}
			if (nControls == 3){
				if (!inputDevice.LeftBumper.IsPressed){
					Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = false;
				}
				if (!inputDevice.RightBumper.IsPressed){
					Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = false;
				}
			}
			bPaused = false;
			bReading = false;
			AudioListener.pause = false;
			Time.timeScale = 1.0f;
			Canvas.SetActive(false);
			iTween.Resume();
			bHojaGot = false;
			LeerHojaIMG.SetActive (false);
			HojaIMG.GetComponent<Image>().CrossFadeAlpha(0f, 0.5f, false);
			HojaIMG.GetComponentInChildren<Text>().CrossFadeAlpha(0f, 0.5f, false);
			LeerHojaFlechaAbajo.SetActive (false);
			HojagetTime = 0;
		}
	}
    void OnDisable()
    {
        
    }
    public void DisableCont(bool dis){
		//Debug.Log ("Disabled Controls: " + dis);
		if (!dis){
			bDisabled = false;
		}
		if (dis){
			bDisabled = true;
			ResetControls ();
		}
	}
	public void DisableContSoltar(bool dis){
		//Debug.Log ("Disabled Controls: " + dis);
		GameObject.Find("Soltador").GetComponent<Soltador>().Check();
		if (!dis){
			bDisabled = false;
		}
		if (dis){
			bDisabled = true;
			ResetControls ();
		}
	}
	public void Reset(){
		bIncapacitado = true;
		Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Restart").Value = true;
	}
	//CONVERSACIONES
	public void Dial(int StartLine, int lastline){
		currentline = StartLine;
		endatline = lastline;
		bWaitforaction = false;
		bDialog = true;
		ResetControls ();
		DialogUI.SetActive (true);
		StartCoroutine ("TypeText");
	}
	public void EndDial(){
		DialogUI.SetActive (false);
		bDialog = false;
		StopCoroutine ("TypeText");
	}
	IEnumerator TypeText () 
	{
		if (LeftorRight [currentline] == 0) {
			DialSpeaker (true);
		}
		if (LeftorRight [currentline] == 1) {
			DialSpeaker (false);
		}
		bDuringLine = true;
		if (WhatPort [currentline] > 0) {
			DialogPort.SetActive(true);
			DialogPort.GetComponent<Image> ().sprite = Portraits [WhatPort[currentline]];
			DialogText.GetComponent<Text> ().alignment = TextAnchor.UpperLeft;
		}
		if (WhatPort [currentline] == 0) {
			DialogPort.SetActive(true);
			DialogPort.GetComponent<Image> ().sprite = Portraits [WhatPort[currentline]];
			DialogText.GetComponent<Text> ().alignment = TextAnchor.UpperLeft;

		}
		foreach (char letter in CurrentLang[currentline].ToCharArray()) 
		{
			DialogText.GetComponent<Text>().text += letter;
			GetComponent<AudioSource>().PlayOneShot(LetterSound, (GetComponent<AudioSource>().volume*EffectVolume));
			yield return 0;
			yield return new WaitForSeconds (letterwait);
		}
		bDuringLine = false;
		bFinishedLine = true;
	}
	//Eventos especiales de los dialogos
	public void SpecialEvent(int line){
		/*if (line == 17) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().TransformBoss ();
		}
		if (line == 25) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().Phase1Start ();
		}
		if (line == 36 || line == 31 || line == 41) {
			GameObject.Find ("NightmareMichael").GetComponent<MandWBoss> ().StartAttack (2);
			GameObject.Find ("NightmareWolfgang").GetComponent<MandWBoss> ().StartAttack (2);
		}
		if (line == 48) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().Phase2Start (true);
		}
		if (line == 55) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().Phase2Start (false);
		}
		if (line == 57) {
			GameObject.Find ("NightmareMichael").GetComponent<MandWBoss> ().Hurt ();
		}*/
		if (line == 39) {
			MoveCamTo2 (68, 0,0.05f);
			Invoke ("DelayedConvoA", 2);
		}
		if (line == 41) {
			GameObject.Find ("MM_Intro").GetComponent<PlayMakerFSM> ().enabled = true;
			LoadingG.GetComponent<LoadingGestor> ().JugarConPesadilla (true);
		}
		if (line == 44) {
			GameObject.Find ("Aparicion_MM_2").GetComponent<BoxCollider> ().enabled = true;
			MoveCamTo2 (-255, -36, 0.03f);
			DisableCont (false);
		}
		if (line == 50) {
			GameObject.Find ("Scroll_MM_1").GetComponent<PlayMakerFSM> ().enabled = true;
			//DisableCont (false);
		}
		if (line == 55) {
			GameObject.Find ("Activador").GetComponent<PlayMakerFSM> ().enabled = true;
			//DisableCont (false);
		}
		if (line == 58) {
			GameObject.Find ("Cambio_De_Nivel").GetComponent<PlayMakerFSM> ().enabled = true;
			MoveCamTo2 (280, 63f,0.03f);
			//DisableCont (false);
		}
		if (line == 72) {
			SecretOrbe.SetActive(true);
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (7);
		}
		if (line == 76) {
			SecretOrbe.SetActive(true);
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (8);
		}
		if (line == 80) {
			SecretOrbe.SetActive(true);
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (9);
		}
		if (line == 84) {
			SecretOrbe.SetActive(true);
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (10);
		}
		if (line == 88) {
			SecretOrbe.SetActive(true);
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (11);
		}
		if (line == 92) {
			SecretOrbe.SetActive(true);
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().Achievements (12);
		}
		if (line == 97) {
			FadeIn ();
			FsmVariables.GlobalVariables.FindFsmBool("Wake_Up_Cinematic").Value = false;
			//GameObject.Find ("Randy").GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Wake_Up_Wait").Value = false;
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().DelayMove (3, 2.62f);
			PlayMusic (1);
		}
		if (line == 99) {
			LockReset = false;
		}
		if (line == 105) {			
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (-2, 2.62f);
			LookAt (true, GameObject.Find ("Michael"));
			bWaitforaction = true;
		}
		if (line == 106) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (-1.9f, 2.62f);
		}
		if (line == 108) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (5, 2.62f);
			bWaitforaction = true;
		}
		if (line == 109) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (-2, 2.62f);
			bWaitforaction = true;
		}
		if (line == 111) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (-1.9f, 2.62f);
		}
		if (line == 114) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (3, 2.62f);
			bWaitforaction = true;
		}
		if (line == 115) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (2.9f, 2.62f);
		}
		if (line == 118) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (14, 2.62f);
			MoveTo (18, 0);
			LookAt (false, null);
			//OffCinematic (0);
		}
		if (line == 121) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (20.9f, 2.62f);
		}
		if (line == 127) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (20, 2.62f);
		}
		if (line == 131) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (15, 2.62f);
		}
		if (line == 133) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (20.1f, 2.62f);
		}
		if (line == 135) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (22, 2.62f);
			MoveTo (20, 0);
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (18, 2.62f);
			bWaitforaction = true;
			FadeOutSong ();
		}
		if (line == 136) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (21.9f, 2.62f);
		}
		if (line == 139) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (32, 2.62f);
			MoveTo (37, 0);
		}
		if (line == 141) {
			Turn ();
		}
		if (line == 150) {
			DisableCont (false);
			OffCinematic (0);
		}
		if (line == 156) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().bWalk = false;
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (100, 2.62f);
		}
		if (line == 157) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (100.1f, 2.62f);
		}
		if (line == 172) {
			OffCinematic (0);
		}
		if (line == 178) {
			bWaitforaction = true;
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (125, 2.62f);
			Turn ();
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (115, 2.62f);
			GameObject.Find ("TriggerB").GetComponent<Triggers> ().Desactivar ();
		}
		if (line == 180) {
			bWaitforaction = true;
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (141, 2.62f);
			MoveTo (135, 0);
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (132, 2.62f);

		}
		if (line == 181) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (140.9f, 2.62f);
			LookAt (true, GameObject.Find ("Wolfgang"));
			//PlayMusic (1);
		}
		if (line == 188) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (137f, 2.62f);
			bWaitforaction = true;
		}
		if (line == 189) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (142, 2.62f);
			bWaitforaction = true;
		}
		if (line == 190) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (137, 2.62f);
			bWaitforaction = true;
		}
		if (line == 191) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (142, 2.62f);
			bWaitforaction = true;
		}
		if (line == 193) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (141.9f, 2.62f);
		}
		if (line == 194) {
			FadeOutSong ();
		}
		if (line == 195) {
			PlayMusic (4);
		}
		if (line == 196) {
			FadeOutSong ();
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (142f, 2.62f);
		}
		if (line == 201) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (140f, 2.62f);
			bWaitforaction = true;
		}
		if (line == 209) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (136f, 2.62f);
			bWaitforaction = true;
		}
		if (line == 220) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (135.9f, 2.62f);
		}
		if (line == 223) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (133f, 2.62f);
		}
		if (line == 226) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (133.1f, 2.62f);
		}
		if (line == 227) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (139.5f, 2.62f);
		}
		if (line == 229) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (136f, 2.62f);
		}
		if (line == 232) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (135.9f, 2.62f);
		}
		if (line == 234) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (136f, 2.62f);
		}
		if (line == 235) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (135.9f, 2.62f);
			LookAt (false, null);
		}
		if (line == 241) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (142.5f, 2.62f);
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (133f, 2.62f);
			OffCinematic (0);
		}
		if (line == 243) {
			MoveTo (457, 23);
			bWaitforaction = true;
		}
		if (line == 249) {			
			GameObject.Find ("GestorFuegoFatuo").GetComponent<GestorFFatuo> ().Cinematic ();
			OffCinematic (0);
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().Vanish ();
		}
		if (line == 256) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (0.9f, -4.142f);
		}
		if (line == 257) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (0.8f, -4.142f);
		}
		if (line == 258) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (4.65f, -4.142f);
		}
		if (line == 260) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (4.66f, -4.142f);
		}
		if (line == 262) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (4.4f, -4.142f);
		}
		if (line == 266) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (0.9f, -4.142f);
		}
		if (line == 268) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (0.8f, -4.142f);
		}
		if (line == 280) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (0.9f, -4.142f);
		}
		if (line == 286) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (0.6f, -4.142f);
		}
		if (line == 288) {
			FadeOutSong ();
			MoveTo (10f, -4.204f);
		}
		if (line == 292) {
			MoveTo (23f, -4.204f);
		}
		if (line == 293) {
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().CinematicMove (18);
		}
		if (line == 294) {
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().CinematicMove (26);
			LoadingG.GetComponent<LoadingGestor> ().CargarLoadScreen ();
		}
		if (line == 307) {
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().CinematicPoof (false);
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().IgnoreJerom ();
			bWaitforaction = true;
			StopWait (1);
		}
		if (line == 311) {
			MoveCamTo (150, 98.7f);
			bWaitforaction = true;
			StopWait (2);
		}
		if (line == 316) {
			MoveCamToRandy (0.05f);
		}
		if (line == 317) {
			DisableCont (false);
			OffCinematic (0);
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().StartFight ();
			GameObject.Find ("Jerom").transform.position = new Vector2 (162, 99.9f);
		}
		if (line == 327) {
			DisableCont (false);
			OffCinematic (0);
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().StartPhase2 ();
			GameObject.Find ("Jerom").GetComponent<Jerom> ().MoveTo (168);
		}
		if (line == 332) {
			GameObject.Find ("Jerom").GetComponent<Jerom> ().MoveTo (162.5f);	
		}
		if (line == 334) {
			GameObject.Find ("Jerom").GetComponent<Jerom> ().MoveTo (162);	
		}
		if (line == 343) {
			ShakeCam (0.5f, 0.25f);
		}
		if (line == 344) {
			ShakeCam (0.75f, 0.25f);
		}
		if (line == 345) {
			ShakeCam (1f, 0.25f);
		}
		if (line == 355) {
			GameObject.Find ("Jerom").GetComponent<Jerom> ().MoveTo (162.5f);	
		}
		if (line == 358) {
			//Go to NightmareAmelia
			Flash3();
			gameObject.GetComponent<AudioSource> ().PlayOneShot (ShakeCamSound,GetComponent<AudioSource>().volume*EffectVolume);
			Invoke ("GoToNightAmelia", 0.5f);
			Invoke ("FlashOut2", 1);
		}
		if (line == 362) {
			//Go to Randys
			Randy.transform.position = new Vector3 (9, 108, 0);
			gameObject.transform.position = new Vector3 (9, 99, -10);
			Invoke ("FlashOut2", 1);
		}
		if (line == 371) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (44.13f, 0);
		}
		if (line == 372) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (41.51f, 0);
			PlayMusic (1);
		}
		if (line == 380) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (44.12f, 0);
		}
		if (line == 381) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (41.52f, 0);
		}
		if (line == 385) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (41.51f, 0);
		}
		if (line == 388) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (41.52f, 0);
		}
		if (line == 393) {
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Moveto (41.51f, 0);
		}
		if (line == 417) {
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Moveto (44.13f, 0);
		}
		if (line == 419) {
			FadeOutSong ();
		}
		if (line == 421) {
			ShakeCam (0.5f, 0.5f);
			gameObject.GetComponent<AudioSource> ().PlayOneShot (ShakeCamSound,GetComponent<AudioSource>().volume*EffectVolume);
			bWaitforaction = true;
			StopWait (1);
			//GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Mutate ();
			//GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Mutate ();
		}
		if (line == 423) {
			ShakeCam (1f, 0.5f);
			gameObject.GetComponent<AudioSource> ().PlayOneShot (ShakeCamSound,GetComponent<AudioSource>().volume*EffectVolume);
			bWaitforaction = true;
			StopWait (1);
			//GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().Mutate ();
			//GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().Mutate ();
		}
		if (line == 424) {
			Flash ();
			ShakeCam (2f, 0.5f);
			gameObject.GetComponent<AudioSource> ().PlayOneShot (ShakeCamSound,GetComponent<AudioSource>().volume*EffectVolume);
			bWaitforaction = true;
			StopWait (1);
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().TransformBoss ();
		}
		if (line == 425) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().Phase1Start ();
			PlayMusic (7);
			DisableCont (false);
			OffCinematic (0);
		}
		if (line == 427) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().DeactivateAbsorbs ();
		}
		if (line == 428) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().Phase2Jump ();
		}
		if (line == 430) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().DeactivateAbsorbs ();
		}
		if (line == 431) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().Phase2Jump ();
		}
		if (line == 433) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().DeactivateAbsorbs ();
		}
		if (line == 435) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().Phase2Jump ();
		}
		if (line == 437) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().DeactivateAbsorbs ();
		}
		if (line == 444) {
			GameObject.Find ("NightmareMichael").GetComponent<MandWBoss> ().WaitingDeath ();
		}
		if (line == 446) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().DeactivateAbsorbs ();
		}
		if (line == 453) {
			GameObject.Find ("NightmareWolfgang").GetComponent<MandWBoss> ().WaitingDeath ();
		}
		if (line == 458) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().Phase2Start (true);
			DisableCont (false);
			PlayMusic (7);
		}
		if (line == 463) {
			GameObject.Find ("MichaelAndWolfgangBossController").GetComponent<MandWController> ().Phase2Start (false);
			DisableCont (false);
			PlayMusic (7);
		}
		if (line == 473) {
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().StartPhase3 ();
			DisableCont (false);
		}
		if (line == 476) {
			DisableCont (true);
			Invoke ("DelayedConvoC", 0.5f);
		}
		//AmeliaBoss
		/*if (line == 248) {
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().StartFight ();
		}
		if (line == 247) {
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().StartPhase2 ();
		}
		if (line == 250) {
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().StartPhase3 ();
		}
		if (line == 255) {
			GameObject.Find ("HeartofDarkness").GetComponent<HeartOfDarkness> ().SetAlive ();
		}
		if (line == 256) {
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().TPtoSecretBoss ();
		}
		if (line == 257) {
			GameObject.Find ("HeartofDarkness").GetComponent<HeartOfDarkness> ().StartFight ();
			DisableCont (false);
		}
		if (line == 259) {
			GameObject.Find ("Amelia3").GetComponent<Amelia3> ().Revive ();
			Flash ();
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().TPtoFinalBoss ();
		}
		if (line == 260) {
			Flash ();
			GameObject.Find ("Amelia3").GetComponent<Amelia3> ().StartFight ();
		}*/
	}
	public void StopWait(int s){
		Invoke ("StopWaitforAction", s);
	}
	public void StopWaitforAction(){
		bWaitforaction = false;
	}
	public void ShakeCam(float i, float time){
		iTween.ShakePosition (gameObject,new Vector3(i,i,0),time);
	}
	public void DialSpeaker(bool left){
		if (left) {
			SpkRight = false;
			SpkLeft = true;
			DialogPortRight.SetActive(false);
			DialogPortLeft.SetActive(true);
			DialogTextRight.SetActive(false);
			DialogTextLeft.SetActive(true);
			DialogPort = DialogPortLeft;
			DialogText = DialogTextLeft;
			DialogText.GetComponent<Text>().text = "";
		}
		if (!left) {
			SpkLeft = false;
			SpkRight = true;
			DialogPortRight.SetActive(true);
			DialogPortLeft.SetActive(false);
			DialogTextRight.SetActive(true);
			DialogTextLeft.SetActive(false);
			DialogPort = DialogPortRight;
			DialogText = DialogTextRight;
			DialogText.GetComponent<Text>().text = "";
		}

	}
	public void SaveCheckpoint(int Check){
		if (Check != 0  && Check > PlayerPrefs.GetInt ("Save1")){
			PlayerPrefs.SetInt ("Save1", Check);
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().SetCheck (Check);
			Checkpoint = PlayerPrefs.GetInt ("Save1");
			StartCoroutine("Saving");
			Debug.Log ("Your Save is: " + Checkpoint);
		}
	}
	public void SetCheckpoint(int ch){
		Checkpoint = ch;
		Debug.Log ("Set checkpoint to " + Checkpoint);
	}
	IEnumerator Saving() {
		SavingIMG.SetActive(true);
		PlayerPrefs.Save();
		yield return new WaitForSeconds(2);
		SavingIMG.SetActive(false);
	}
	public void SetLanguage(){
		if (Language == 1){
			Dialog = English;
		}
		if (Language == 2){
			Dialog = Castellano;
		}
		if (Dialog != null) {
			CurrentLang = (Dialog.text.Split ('\n'));
			//Debug.Log ("" + CurrentLang[1]);
		}
		IntroText.GetComponent<Text> ().text = CurrentLang [Level-1];
		Resume.GetComponent<Image>().sprite = ResumeA[Language];
		Restart.GetComponent<Image>().sprite = SkipA[Language];
		Restart.GetComponent<Image>().sprite = RestartA[Language];
		Options.GetComponent<Image>().sprite = OptionsA[Language];
		Exit.GetComponent<Image>().sprite = ExitA[Language];
		OptEffVol.GetComponent<Image>().sprite = EffectVolA[Language];
		OptMusVol.GetComponent<Image>().sprite = MusicVolA[Language];
		OptLang.GetComponent<Image>().sprite = LanguageA[Language];
		Controls.GetComponent<Image>().sprite = ControlsA[Language];
		OptExit.GetComponent<Image>().sprite = ExitB[Language];
	}
	IEnumerator Loaded(){
		yield return new WaitForEndOfFrame ();
		bLoading = true;
		bChangeOpt = false;
	}
	public void FadeIn()
	{
		//Debug.Log("Fading in");
		Fadein.GetComponent<Image>().CrossFadeAlpha(0f, 4.0f, false);
		bGoTo = false;
		Invoke ("DesactivarFadeout", 4);
	}
	public void FadeInEnd()
	{
		Debug.Log("Fading in End");
		ActivarFadeout ();
		Fadein.GetComponent<Image> ().color = Color.red;
		Fadein.GetComponent<Image>().CrossFadeAlpha(1f, 0.25f, false);
		bGoTo = false;
	}
	public void FadeOut()
	{
		ActivarFadeout ();
		Fadein.GetComponent<Image>().CrossFadeAlpha(1f, 2.0f, false);
		//IntroText.GetComponent<Text>().CrossFadeAlpha(1f, 1.0f, false);
	}
	public void DesactivarFadeout(){
		Fadein.SetActive (false);
	}
	public void ActivarFadeout(){
		Fadein.GetComponent<Image>().CrossFadeAlpha(0f, 0f, false);
		Fadein.SetActive (true);
	}
	public void Flash(){
		ActivarFadeout ();
		Fadein.GetComponent<Image> ().color = Color.white;
		Fadein.GetComponent<Image>().CrossFadeAlpha(1f, 0.1f, false);
		Invoke ("FlashOut", 0.3f);
	}
	public void Flash2(){
		ActivarFadeout ();
		Fadein.GetComponent<Image> ().color = Color.white;
		Fadein.GetComponent<Image>().CrossFadeAlpha(1f, 0.1f, false);
	}
	public void Flash3(){
		ActivarFadeout ();
		Fadein.GetComponent<Image> ().color = Color.white;
		Fadein.GetComponent<Image>().CrossFadeAlpha(1f, 0.6f, false);
	}
	public void FlashOut(){
		Fadein.GetComponent<Image>().CrossFadeAlpha(0f, 0.1f, false);
		Invoke ("ResetFadeout", 0.1f);
	}
	public void FlashOut2(){
		Fadein.GetComponent<Image>().CrossFadeAlpha(0f, 1, false);
		Invoke ("ResetFadeout", 1f);
	}
	public void ResetFadeout(){
		Fadein.GetComponent<Image> ().color = Color.black;
		DesactivarFadeout ();
	}
	public void DialWaitFor(bool w){
		bWaitforaction = w;
	}
	public void ResetControls(){
		Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = 0;
		Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Vertical").Value = 0;
		Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Fire").Value = false;
		Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Run").Value = false;
		Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmBool("Use").Value = false;
	}
	public void LookAt (bool act, GameObject Target){
		LookAtTarget = Target;
		bLookAt = act;
	}
	public void MoveTo (float X, float Y){
		Debug.Log ("MovingTo" + X + "," + Y);
		GoToTarget = new Vector2 (X,Y);
		bGoTo = true;
	}
	public void MoveCamTo (float X, float Y){
		CameraDir = new Vector2 (X,Y);
		MoveCamera = true;
	}
	public void MoveCamTo2 (float X, float Y, float V){
		CameraSpeed = V;
		CameraDir = new Vector2 (X,Y);
		MoveCamera = true;
	}
	public void MoveCamToRandy (float V){
		CameraSpeed = V;
		CameraDir = new Vector2 (Randy.transform.position.x,gameObject.transform.position.y);
		MoveCamera = true;
	}
	public void StopMoveCam (){
		MoveCamera = false;
	}
	public void Turn(){
		if (Randy.GetComponent<tk2dSprite> ().scale.x < 0) {
			Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = 1;
			Invoke ("StopTurn", 0.1f);
		}
		if (Randy.GetComponent<tk2dSprite> ().scale.x > 0) {
			Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = -1;
			Invoke ("StopTurn", 0.1f);
		}
	}
	public void StopTurn(){
		if (Randy.GetComponent<PlayMakerFSM> ().FsmVariables.FindFsmFloat ("Horizontal").Value != 0) {
			Randy.GetComponent<PlayMakerFSM>().FsmVariables.FindFsmFloat("Horizontal").Value = 0;
		}
	}
	public void ToggleCinematic(int cin){
		cinematic = cin;
		bool c = bCinematic;
		if (c) {
			bCinematic = false;
		}
		if (!c) {
			bCinematic = true;
		}
	}
	public void OffCinematic(int cin){
		cinematic = cin;
		bCinematic = false;
	}
	public void CancelCinematic(){
		FadeOutSong ();
		EndDial ();
		bGoTo = false;
		DisableCont (false);
		bIncapacitado = false;
		//ResetControls ();
		if (cinematic == 1) {
			Randy.transform.position = new Vector2 (37, -2.6f);
			gameObject.transform.position = new Vector3 (34, 0, -10);
			FadeIn ();
			GameObject.Find ("Michael").transform.position = new Vector2 (18,-2.62f);
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().ChangeState (0);
			GameObject.Find ("Wolfgang").transform.position = new Vector2 (32, -2.6f);
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().ChangeState (0);
			GameObject.Find ("TriggerA").GetComponent<Triggers> ().Desactivar ();
			LookAt (false, null);

		}
		if (cinematic == 2) {
			Randy.transform.position = new Vector2 (104, -2.6f);
			gameObject.transform.position = new Vector3 (102, 0, -10);
			FadeIn ();
			GameObject.Find ("Michael").transform.position = new Vector2 (100,-2.62f);
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().ChangeState (0);
			GameObject.Find ("Wolfgang").transform.position = new Vector2 (108, -2.6f);
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().ChangeState (0);
		}
		if (cinematic == 3) {
			Randy.transform.position = new Vector2 (135, -2.6f);
			gameObject.transform.position = new Vector3 (136, 0, -10);
			FadeIn ();
			GameObject.Find ("Michael").transform.position = new Vector2 (133,-2.62f);
			GameObject.Find ("Michael").GetComponent<Michael_Wolfgang> ().ChangeState (0);
			GameObject.Find ("Wolfgang").transform.position = new Vector2 (142, -2.6f);
			GameObject.Find ("Wolfgang").GetComponent<Michael_Wolfgang> ().ChangeState (0);
			LookAt (false, null);
		}
		if (cinematic == 4) {
			Randy.transform.position = new Vector2 (450, 23.08f);
			gameObject.transform.position = new Vector3 (450, 27, -10);
			if (GameObject.Find ("Amelia") != null) {
				GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().Vanish ();
				GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().StopFlare ();
			}
			GameObject.Find ("TriggerA").GetComponent<Triggers> ().Desactivar ();
			GameObject.Find ("TriggerB").GetComponent<Triggers> ().Desactivar ();
			FadeIn ();
			LookAt (false, null);
			GameObject.Find ("GestorFuegoFatuo").GetComponent<GestorFFatuo> ().Cinematic ();
		}
		if (cinematic == 5) {
			FadeIn ();
			LoadingG.GetComponent<LoadingGestor> ().CargarLoadScreen ();
		}
		if (cinematic == 6) {
			Randy.transform.position = new Vector2 (148, 94.695f);
			gameObject.transform.position = new Vector3 (150, 98.7f, -10);
			GameObject.Find ("Amelia").GetComponent<AmeliaBoss> ().DelayedStartFight ();
			GameObject.Find ("StartTrigger").GetComponent<Triggers> ().Desactivar ();
			GameObject.Find ("StartBossTriggerB").GetComponent<Triggers> ().Desactivar ();
			GameObject.Find ("Supervisor").GetComponent<LoadingGestor> ().IgnoreJerom ();
			FadeIn ();
			LookAt (false, null);
			DisableCont (true);
		}
		if (cinematic == 7) {
			FadeIn ();
			DisableCont (true);
			Invoke ("DelayedConvoB", 1);
			LookAt (false, null);
		}
		OffCinematic (0);
	}
	public void DelayedStopMoving(){
		bGoTo = false;
	}
	IEnumerator DisableMenu(int sec){
		bLoading = true;
		yield return new WaitForSeconds (sec);
		bLoading = false;
	}
	public void LockRes(){
		Debug.Log ("Lock reset");
		LockReset = true;
	}
	public void UnlockRes(){
		Debug.Log ("Unlock reset");
		LockReset = false;
	}
	public void OnLevelWasLoaded(int level){
		Level = level;
		AudioListener.pause = false;
		if (level != 3) {
			Debug.Log ("Fading out!");
			FadeIn ();
		}
		if (level == 3) {
			Invoke ("FadeOutIntroText", 5);
		}
	}
	public void FadeOutIntroText(){
		IntroText.GetComponent<Text>().CrossFadeAlpha(0f, 2.0f, false);
	}
	public void HideIntroText(){
		IntroText.GetComponent<Text>().CrossFadeAlpha(0f, 0.0f, false);
	}
	public void FadeInIntroText(){
		IntroText.GetComponent<Text>().CrossFadeAlpha(0f, 0.0f, false);
		IntroText.GetComponent<Text>().CrossFadeAlpha(1f, 1.0f, false);
		Invoke ("FadeOutIntroText", 5);
	}
	public void GetHoja(string h){
		bHojaGot = true;
		//Debug.Log ("Texto es: " + h);
		gameObject.GetComponent<AudioSource> ().PlayOneShot (HojaGet,GetComponent<AudioSource>().volume*EffectVolume);
		HojasTEXT.GetComponent<Text>().text = h;
		HojasTEXT.GetComponent<Text> ().text = h.Replace(";","\n\n");
		HojaIMG.GetComponent<Image>().CrossFadeAlpha(1f, 0.5f, false);
		if (PlayerPrefs.GetInt ("Lang") == 1) {
			HojaIMG.GetComponentInChildren<Text> ().text = "Press Start";
		}
		if (PlayerPrefs.GetInt ("Lang") == 2) {
			HojaIMG.GetComponentInChildren<Text> ().text = "Presiona Start";
		}
		HojaIMG.GetComponentInChildren<Text>().CrossFadeAlpha(1f, 1.5f, false);
	}
	public void PlayMusic(int song){
		fadingoutAudio = false;
		MusicaAudioSource.GetComponent<AudioSource> ().volume = MusicVolume;
		FondoAudioSource.GetComponent<AudioSource> ().volume = EffectVolume/3;
		MusicaAudioSource.GetComponent<AudioSource> ().clip = Musica[song];
		MusicaAudioSource.GetComponent<AudioSource> ().Play ();
		if (song != 3) {
			MusicaAudioSource.GetComponent<AudioSource> ().loop = true;
		}
		if (song == 3) {
			MusicaAudioSource.GetComponent<AudioSource> ().loop = false;
		}
	}
	public void PlayInverseMusic(int song){
		fadingoutAudio = false;
		MusicaAudioSource.GetComponent<AudioSource> ().volume = MusicVolume;
		FondoAudioSource.GetComponent<AudioSource> ().volume = EffectVolume/3;
		MusicaAudioSource.GetComponent<AudioSource> ().clip = Musica[song];
		MusicaAudioSource.GetComponent<AudioSource> ().Play ();
		MusicaAudioSource.GetComponent<AudioSource> ().pitch = -0.5f;
		if (song != 3) {
			MusicaAudioSource.GetComponent<AudioSource> ().loop = true;
		}
		if (song == 3) {
			MusicaAudioSource.GetComponent<AudioSource> ().loop = false;
		}
	}
	public void FadeOutSong(){
		fadingoutAudio = true;
	}
	public void FadeOutAmbient(){
		//fadingoutNoise = true;
	}
	public void AchievementUnlock(int ach){
		
	}
	public void PlugMando(bool p){
		if (p) {
			bmando = true;
		}
		if (!p) {
			bmando = false;
		}
	}
	public void DelayedConvoA(){
		Dial (40, 41);
	}
	public void DelayedConvoB(){
		Dial (420, 425);
	}
	public void DelayedConvoC(){
		Dial (315, 317);
	}
	public void MuerteFogonazoRandy(){
		GetComponent<AudioSource>().PlayOneShot(MuerteRandyFogonazo, (GetComponent<AudioSource>().volume*EffectVolume));
	}
	public void Dead(){
		bIncapacitado = true;
	}
	public void GoToNightAmelia(){
		Randy.transform.position = new Vector3 (211, 145, 0);
		gameObject.transform.position = new Vector3 (211, 145, -10);
	}
	public void GoToRandys(){
		Randy.transform.position = new Vector3 (211, 145, 0);
		gameObject.transform.position = new Vector3 (211, 145, -10);
	}
 }
