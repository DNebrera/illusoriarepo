using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Parp : MonoBehaviour {
	public Color color;
	public bool bincrease = false;
	public bool bControlled;
	// Use this for initialization
	void Start () {
		color = this.GetComponent<Image> ().color;
		Increase (true);
	}
	
	// Update is called once per frame
	void Update () {
		if (color.a >= 1 && !bControlled){bincrease = false;} 
		if (color.a <= 0 && !bControlled){bincrease = true;}
		if (!bincrease) {
			color.a -= 0.05f;
			GetComponent<Image> ().color = color;
		}
		if (bincrease) {
			color.a += 0.05f;
			GetComponent<Image> ().color = color;
		}

	}
	public void Increase(bool bInc){
		if (bControlled) {
			if (bInc) {bincrease = true;
			}
			if (!bInc) {bincrease = false;
			}
		}
	}
}
