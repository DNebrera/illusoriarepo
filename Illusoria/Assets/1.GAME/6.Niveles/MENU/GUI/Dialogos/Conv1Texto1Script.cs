﻿using UnityEngine;
using System.Collections;

public class Conv1Texto1Script : MonoBehaviour {

	public float letterPause = 0.2f;
	public bool activarTexto = false;
	public string message;
	public int variable = 50;
	public bool desactivarTexto = false;
	public AudioClip sound;

	void Start () 
	{
		this.GetComponent<GUIText>().fontSize = (Screen.width/variable);
		this.GetComponent<GUIText>().pixelOffset = new Vector2(Screen.width/2, Screen.height/5.2f);
		message = GetComponent<GUIText>().text;
		GetComponent<GUIText>().text = "";	
		this.GetComponent<GUIText>().enabled = false;
	}
		
	void Update()
	{
		this.GetComponent<GUIText>().fontSize = (Screen.width/variable);
		if(activarTexto)
		{
			this.GetComponent<GUIText>().enabled = true;
			StartCoroutine("TypeText");
			activarTexto = false;
		}
		if(desactivarTexto)
		{
			StopCoroutine ("TypeText");
		
			GetComponent<GUIText>().text = message;
		}
		//GetComponent<AudioSource>().volume = GeneralMusic.volumenFX;
	}

	IEnumerator TypeText () 
	{
		foreach (char letter in message.ToCharArray()) 
		{
			GetComponent<GUIText>().text += letter;
			GetComponent<AudioSource>().PlayOneShot(sound);
			yield return 0;
			yield return new WaitForSeconds (letterPause);
		}      
	}
}