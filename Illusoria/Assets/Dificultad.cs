﻿using UnityEngine;
using System.Collections;

public class Dificultad : MonoBehaviour {
	public bool Monstruo;
	public bool Facil;
	public bool Dificil;
	public int Dif;
	// Use this for initialization
	void Start () {
		Dif =  PlayerPrefs.GetInt("Dificultad");
		if (Monstruo) {
			if (Dif <= 2 && Dificil) {
				gameObject.SetActive (false);
			}
			if (Dif == 1 && Facil) {
				gameObject.SetActive (false);
			}
		}
		if (!Monstruo) {
			if (Dif == 3 && Dificil) {
				gameObject.SetActive (false);
			}
			if (Dif >= 2 && Facil) {
				gameObject.SetActive (false);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
