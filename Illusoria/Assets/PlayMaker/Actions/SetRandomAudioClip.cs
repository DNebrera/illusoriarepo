// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
    [ActionCategory(ActionCategory.Audio)]
    [Tooltip("Sets a Random Audio Clip of the AudioSource component on a Game Object.")]
    public class SetRandomAudioClip : FsmStateAction
    {
        [RequiredField]
        [CheckForComponent(typeof(AudioSource))]
        [Tooltip("The GameObject with the AudioSource component.")]
        public FsmOwnerDefault gameObject;

        [CompoundArray("Audio Clips", "Audio Clip", "Weight")]
        public AudioClip[] audioClips;

        [HasFloatSlider(0, 1)]
        public FsmFloat[] weights;

        public override void Reset()
        {
            gameObject = null;
            audioClips = new AudioClip[3];
            weights = new FsmFloat[] { 1, 1, 1 };
        }

        public override void OnEnter()
        {

            DoRandomClip();

            Finish();
        }

        void DoRandomClip()
        {
            var go = Fsm.GetOwnerDefaultTarget(gameObject);
            int randomIndex = ActionHelpers.GetRandomWeightedIndex(weights);
            if (go != null)
            {

                if (audioClips.Length == 0) return;
                {
                    var audio = go.GetComponent<AudioSource>();
                    if (randomIndex != -1)
                    {
                        if (go == null) return;
                        audio.clip = audioClips[randomIndex];

                    }
                }
            }
        }
    }
}