﻿using UnityEngine;
using System.Collections;

public class EstalactitaPunta : MonoBehaviour {
	public GameObject Estalactita;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void OnTriggerEnter(Collider Col){
		if (Col.tag == "Ground") {
			Estalactita.GetComponent<Estalactita>().Crash();
		}
	}
}
