﻿using UnityEngine;
using System.Collections;
using HutongGames.PlayMaker;

public class DetectorSecreto : MonoBehaviour {
	private GameObject MainCamera;
	public GameObject Playmaker;
	public GameObject Audio;
	public GameObject Blockade;
	public int Secret;
	public string Zonas;
	public string[] Sec;
	public int UnloSec;

	// Use this for initialization
	void Start () {
		MainCamera = GameObject.Find ("Main Camera");
		ExtractSecrets ();
		if (Sec [Secret-1] == "1") {
			if (Secret == 1) {
				FsmVariables.GlobalVariables.FindFsmBool("Orbe_1").Value = true;
			}
			if (Secret == 2) {
				FsmVariables.GlobalVariables.FindFsmBool("Orbe_2").Value = true;
			}
			if (Secret == 3) {
				FsmVariables.GlobalVariables.FindFsmBool("Orbe_3").Value = true;
			}
			if (Secret == 4) {
				FsmVariables.GlobalVariables.FindFsmBool("Orbe_4").Value = true;
			}
			if (Secret == 5) {
				FsmVariables.GlobalVariables.FindFsmBool("Orbe_5").Value = true;
			}
			if (Secret == 6) {
				FsmVariables.GlobalVariables.FindFsmBool("Orbe_6").Value = true;
			}
		}
	}
	// Update is called once per frame
	void Update () {
		
	}
	public void OnTriggerEnter(Collider Col){
		if (Col.tag == "Randy"){
			SecretConvo (Secret);
		}
	}
	public void SecretConvo(int S){
		Sec [S - 1] = "1";
		Debug.Log ("Secrets obtained: " + UnloSec);
		if (UnloSec == 0) {
			MainCamera.GetComponent<PadDetect>().Dial (69,72);
			Audio.SetActive(true);
		}
		if (UnloSec == 1) {
			MainCamera.GetComponent<PadDetect>().Dial (73,76);
			Audio.SetActive(true);
		}
		if (UnloSec == 2) {
			MainCamera.GetComponent<PadDetect>().Dial (77,80);
			Audio.SetActive(true);
		}
		if (UnloSec == 3) {
			MainCamera.GetComponent<PadDetect>().Dial (81,84);
			Audio.SetActive(true);
		}
		if (UnloSec == 4) {
			MainCamera.GetComponent<PadDetect>().Dial (85,88);
			Audio.SetActive(true);
		}
		if (UnloSec == 5) {
			MainCamera.GetComponent<PadDetect>().Dial (89,92);
			Audio.SetActive(true);
		}
		SaveSecrets ();
	}
	public void ExtractSecrets(){
		Zonas = PlayerPrefs.GetString ("Secrets");
		Debug.Log ("Secrets is: " + PlayerPrefs.GetString ("Secrets"));
		int charnum = 0;
		UnloSec = 0;
		foreach (char letter in Zonas.ToCharArray()) {
			Sec [charnum] = letter.ToString();
			if (Sec [charnum] == "1") {
				UnloSec++;
			}
			charnum++;
		}
	}
	public void SaveSecrets(){
		PlayerPrefs.SetString ("Secrets", "" + Sec [0] + Sec [1] + Sec [2] + Sec [3] + Sec [4] + Sec [5]);
		Debug.Log ("Secrets is now: " + PlayerPrefs.GetString ("Secrets"));
	}
}
