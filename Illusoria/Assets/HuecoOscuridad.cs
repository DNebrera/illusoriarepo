﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HuecoOscuridad : MonoBehaviour {
	public GameObject Oscuridad;
	public bool bPequeño;
	public GameObject Randy;
	public float DistRandy;
	// Use this for initialization
	void Start () {
		Randy = GameObject.Find ("Randy");
	}
	
	// Update is called once per frame
	void Update () {
		//DistRandy = (Randy.transform.position - this.transform.position).magnitude;
	}
	void OnTriggerEnter(Collider other)	{		
		if (other.tag == "Luz") {
			Debug.Log ("Entered Main Light");
			if (other.GetComponent<HuecoOscuridad> ().bPequeño && !bPequeño) {
				other.GetComponent<HuecoOscuridad> ().Desactivar ();
			}
		}
	}
	void OnTriggerExit(Collider other)	{		
		if (other.tag == "Luz") {
			Debug.Log ("Exited Main Light");
			if (other.GetComponent<HuecoOscuridad> ().bPequeño && !bPequeño) {
				other.GetComponent<HuecoOscuridad> ().Activar ();
			}
		}
	}
	public void Desactivar(){
		Oscuridad.SetActive (false);
	}
	public void Activar(){
		Oscuridad.SetActive (true);
	}
}
